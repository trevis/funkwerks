﻿//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                                            //
//                          WARNING                           //
//                                                            //
//           DO NOT MAKE LOCAL CHANGES TO THIS FILE           //
//               EDIT THE .tt TEMPLATE INSTEAD                //
//                                                            //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//


using ACMessageDefs.Enums;
using ACMessageDefs.Lib;
using ACMessageDefs.Messages;
using ACMessageDefs.HandlerEventArgs;
using System;
using System.IO;

namespace ACMessageDefs.HandlerEventArgs {
    /// <summary>
    /// Contains data about a message
    /// </summary>
    public class MessageEventArgs {
        /// <summary>
        /// Direction the message was sent in (S2C or C2S)
        /// </summary>
        public MessageDirection Direction;

        /// <summary>
        /// The type of message this is
        /// </summary>
        public MessageType Type;

        /// <summary>
        /// The actual message data
        /// </summary>
        public IACDataType Data;

        public MessageEventArgs(MessageDirection direction, MessageType type, IACDataType data) {
            Direction = direction;
            Type = type;
            Data = data;
        }
    }

    public class UnknownMessageTypeEventArgs {
        public MessageDirection Direction;
        public MessageType Type;
        public byte[] RawData;

        public UnknownMessageTypeEventArgs(MessageDirection direction, MessageType type, byte[] rawData) {
            Direction = direction;
            Type = type;
            RawData = rawData;
        }
    }

    /// <summary>
    /// Allegiance_AllegianceUpdateAborted_S2C Event Args
    /// </summary>
    public class Allegiance_AllegianceUpdateAborted_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_AllegianceUpdateAborted_S2C Message Data
        /// </summary>
        public Allegiance_AllegianceUpdateAborted_S2C Data;
    
        public Allegiance_AllegianceUpdateAborted_S2C_EventArgs(Allegiance_AllegianceUpdateAborted_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_PopUpString_S2C Event Args
    /// </summary>
    public class Communication_PopUpString_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_PopUpString_S2C Message Data
        /// </summary>
        public Communication_PopUpString_S2C Data;
    
        public Communication_PopUpString_S2C_EventArgs(Communication_PopUpString_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_PlayerOptionChangedEvent_C2S Event Args
    /// </summary>
    public class Character_PlayerOptionChangedEvent_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_PlayerOptionChangedEvent_C2S Message Data
        /// </summary>
        public Character_PlayerOptionChangedEvent_C2S Data;
    
        public Character_PlayerOptionChangedEvent_C2S_EventArgs(Character_PlayerOptionChangedEvent_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_TargetedMeleeAttack_C2S Event Args
    /// </summary>
    public class Combat_TargetedMeleeAttack_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Combat_TargetedMeleeAttack_C2S Message Data
        /// </summary>
        public Combat_TargetedMeleeAttack_C2S Data;
    
        public Combat_TargetedMeleeAttack_C2S_EventArgs(Combat_TargetedMeleeAttack_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_TargetedMissileAttack_C2S Event Args
    /// </summary>
    public class Combat_TargetedMissileAttack_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Combat_TargetedMissileAttack_C2S Message Data
        /// </summary>
        public Combat_TargetedMissileAttack_C2S Data;
    
        public Combat_TargetedMissileAttack_C2S_EventArgs(Combat_TargetedMissileAttack_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_SetAFKMode_C2S Event Args
    /// </summary>
    public class Communication_SetAFKMode_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_SetAFKMode_C2S Message Data
        /// </summary>
        public Communication_SetAFKMode_C2S Data;
    
        public Communication_SetAFKMode_C2S_EventArgs(Communication_SetAFKMode_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_SetAFKMessage_C2S Event Args
    /// </summary>
    public class Communication_SetAFKMessage_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_SetAFKMessage_C2S Message Data
        /// </summary>
        public Communication_SetAFKMessage_C2S Data;
    
        public Communication_SetAFKMessage_C2S_EventArgs(Communication_SetAFKMessage_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Login_PlayerDescription_S2C Event Args
    /// </summary>
    public class Login_PlayerDescription_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Login_PlayerDescription_S2C Message Data
        /// </summary>
        public Login_PlayerDescription_S2C Data;
    
        public Login_PlayerDescription_S2C_EventArgs(Login_PlayerDescription_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_Talk_C2S Event Args
    /// </summary>
    public class Communication_Talk_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_Talk_C2S Message Data
        /// </summary>
        public Communication_Talk_C2S Data;
    
        public Communication_Talk_C2S_EventArgs(Communication_Talk_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Social_RemoveFriend_C2S Event Args
    /// </summary>
    public class Social_RemoveFriend_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Social_RemoveFriend_C2S Message Data
        /// </summary>
        public Social_RemoveFriend_C2S Data;
    
        public Social_RemoveFriend_C2S_EventArgs(Social_RemoveFriend_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Social_AddFriend_C2S Event Args
    /// </summary>
    public class Social_AddFriend_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Social_AddFriend_C2S Message Data
        /// </summary>
        public Social_AddFriend_C2S Data;
    
        public Social_AddFriend_C2S_EventArgs(Social_AddFriend_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_PutItemInContainer_C2S Event Args
    /// </summary>
    public class Inventory_PutItemInContainer_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_PutItemInContainer_C2S Message Data
        /// </summary>
        public Inventory_PutItemInContainer_C2S Data;
    
        public Inventory_PutItemInContainer_C2S_EventArgs(Inventory_PutItemInContainer_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_GetAndWieldItem_C2S Event Args
    /// </summary>
    public class Inventory_GetAndWieldItem_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_GetAndWieldItem_C2S Message Data
        /// </summary>
        public Inventory_GetAndWieldItem_C2S Data;
    
        public Inventory_GetAndWieldItem_C2S_EventArgs(Inventory_GetAndWieldItem_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_DropItem_C2S Event Args
    /// </summary>
    public class Inventory_DropItem_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_DropItem_C2S Message Data
        /// </summary>
        public Inventory_DropItem_C2S Data;
    
        public Inventory_DropItem_C2S_EventArgs(Inventory_DropItem_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_SwearAllegiance_C2S Event Args
    /// </summary>
    public class Allegiance_SwearAllegiance_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_SwearAllegiance_C2S Message Data
        /// </summary>
        public Allegiance_SwearAllegiance_C2S Data;
    
        public Allegiance_SwearAllegiance_C2S_EventArgs(Allegiance_SwearAllegiance_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_BreakAllegiance_C2S Event Args
    /// </summary>
    public class Allegiance_BreakAllegiance_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_BreakAllegiance_C2S Message Data
        /// </summary>
        public Allegiance_BreakAllegiance_C2S Data;
    
        public Allegiance_BreakAllegiance_C2S_EventArgs(Allegiance_BreakAllegiance_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_UpdateRequest_C2S Event Args
    /// </summary>
    public class Allegiance_UpdateRequest_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_UpdateRequest_C2S Message Data
        /// </summary>
        public Allegiance_UpdateRequest_C2S Data;
    
        public Allegiance_UpdateRequest_C2S_EventArgs(Allegiance_UpdateRequest_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_AllegianceUpdate_S2C Event Args
    /// </summary>
    public class Allegiance_AllegianceUpdate_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_AllegianceUpdate_S2C Message Data
        /// </summary>
        public Allegiance_AllegianceUpdate_S2C Data;
    
        public Allegiance_AllegianceUpdate_S2C_EventArgs(Allegiance_AllegianceUpdate_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Social_FriendsUpdate_S2C Event Args
    /// </summary>
    public class Social_FriendsUpdate_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Social_FriendsUpdate_S2C Message Data
        /// </summary>
        public Social_FriendsUpdate_S2C Data;
    
        public Social_FriendsUpdate_S2C_EventArgs(Social_FriendsUpdate_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_ServerSaysContainID_S2C Event Args
    /// </summary>
    public class Item_ServerSaysContainID_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_ServerSaysContainID_S2C Message Data
        /// </summary>
        public Item_ServerSaysContainID_S2C Data;
    
        public Item_ServerSaysContainID_S2C_EventArgs(Item_ServerSaysContainID_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_WearItem_S2C Event Args
    /// </summary>
    public class Item_WearItem_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_WearItem_S2C Message Data
        /// </summary>
        public Item_WearItem_S2C Data;
    
        public Item_WearItem_S2C_EventArgs(Item_WearItem_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_ServerSaysRemove_S2C Event Args
    /// </summary>
    public class Item_ServerSaysRemove_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_ServerSaysRemove_S2C Message Data
        /// </summary>
        public Item_ServerSaysRemove_S2C Data;
    
        public Item_ServerSaysRemove_S2C_EventArgs(Item_ServerSaysRemove_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Social_ClearFriends_C2S Event Args
    /// </summary>
    public class Social_ClearFriends_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Social_ClearFriends_C2S Message Data
        /// </summary>
        public Social_ClearFriends_C2S Data;
    
        public Social_ClearFriends_C2S_EventArgs(Social_ClearFriends_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_TeleToPKLArena_C2S Event Args
    /// </summary>
    public class Character_TeleToPKLArena_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_TeleToPKLArena_C2S Message Data
        /// </summary>
        public Character_TeleToPKLArena_C2S Data;
    
        public Character_TeleToPKLArena_C2S_EventArgs(Character_TeleToPKLArena_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_TeleToPKArena_C2S Event Args
    /// </summary>
    public class Character_TeleToPKArena_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_TeleToPKArena_C2S Message Data
        /// </summary>
        public Character_TeleToPKArena_C2S Data;
    
        public Character_TeleToPKArena_C2S_EventArgs(Character_TeleToPKArena_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Social_CharacterTitleTable_S2C Event Args
    /// </summary>
    public class Social_CharacterTitleTable_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Social_CharacterTitleTable_S2C Message Data
        /// </summary>
        public Social_CharacterTitleTable_S2C Data;
    
        public Social_CharacterTitleTable_S2C_EventArgs(Social_CharacterTitleTable_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Social_AddOrSetCharacterTitle_S2C Event Args
    /// </summary>
    public class Social_AddOrSetCharacterTitle_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Social_AddOrSetCharacterTitle_S2C Message Data
        /// </summary>
        public Social_AddOrSetCharacterTitle_S2C Data;
    
        public Social_AddOrSetCharacterTitle_S2C_EventArgs(Social_AddOrSetCharacterTitle_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Social_SetDisplayCharacterTitle_C2S Event Args
    /// </summary>
    public class Social_SetDisplayCharacterTitle_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Social_SetDisplayCharacterTitle_C2S Message Data
        /// </summary>
        public Social_SetDisplayCharacterTitle_C2S Data;
    
        public Social_SetDisplayCharacterTitle_C2S_EventArgs(Social_SetDisplayCharacterTitle_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_QueryAllegianceName_C2S Event Args
    /// </summary>
    public class Allegiance_QueryAllegianceName_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_QueryAllegianceName_C2S Message Data
        /// </summary>
        public Allegiance_QueryAllegianceName_C2S Data;
    
        public Allegiance_QueryAllegianceName_C2S_EventArgs(Allegiance_QueryAllegianceName_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_ClearAllegianceName_C2S Event Args
    /// </summary>
    public class Allegiance_ClearAllegianceName_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_ClearAllegianceName_C2S Message Data
        /// </summary>
        public Allegiance_ClearAllegianceName_C2S Data;
    
        public Allegiance_ClearAllegianceName_C2S_EventArgs(Allegiance_ClearAllegianceName_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_TalkDirect_C2S Event Args
    /// </summary>
    public class Communication_TalkDirect_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_TalkDirect_C2S Message Data
        /// </summary>
        public Communication_TalkDirect_C2S Data;
    
        public Communication_TalkDirect_C2S_EventArgs(Communication_TalkDirect_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_SetAllegianceName_C2S Event Args
    /// </summary>
    public class Allegiance_SetAllegianceName_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_SetAllegianceName_C2S Message Data
        /// </summary>
        public Allegiance_SetAllegianceName_C2S Data;
    
        public Allegiance_SetAllegianceName_C2S_EventArgs(Allegiance_SetAllegianceName_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_UseWithTargetEvent_C2S Event Args
    /// </summary>
    public class Inventory_UseWithTargetEvent_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_UseWithTargetEvent_C2S Message Data
        /// </summary>
        public Inventory_UseWithTargetEvent_C2S Data;
    
        public Inventory_UseWithTargetEvent_C2S_EventArgs(Inventory_UseWithTargetEvent_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_UseEvent_C2S Event Args
    /// </summary>
    public class Inventory_UseEvent_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_UseEvent_C2S Message Data
        /// </summary>
        public Inventory_UseEvent_C2S Data;
    
        public Inventory_UseEvent_C2S_EventArgs(Inventory_UseEvent_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_SetAllegianceOfficer_C2S Event Args
    /// </summary>
    public class Allegiance_SetAllegianceOfficer_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_SetAllegianceOfficer_C2S Message Data
        /// </summary>
        public Allegiance_SetAllegianceOfficer_C2S Data;
    
        public Allegiance_SetAllegianceOfficer_C2S_EventArgs(Allegiance_SetAllegianceOfficer_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_SetAllegianceOfficerTitle_C2S Event Args
    /// </summary>
    public class Allegiance_SetAllegianceOfficerTitle_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_SetAllegianceOfficerTitle_C2S Message Data
        /// </summary>
        public Allegiance_SetAllegianceOfficerTitle_C2S Data;
    
        public Allegiance_SetAllegianceOfficerTitle_C2S_EventArgs(Allegiance_SetAllegianceOfficerTitle_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_ListAllegianceOfficerTitles_C2S Event Args
    /// </summary>
    public class Allegiance_ListAllegianceOfficerTitles_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_ListAllegianceOfficerTitles_C2S Message Data
        /// </summary>
        public Allegiance_ListAllegianceOfficerTitles_C2S Data;
    
        public Allegiance_ListAllegianceOfficerTitles_C2S_EventArgs(Allegiance_ListAllegianceOfficerTitles_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_ClearAllegianceOfficerTitles_C2S Event Args
    /// </summary>
    public class Allegiance_ClearAllegianceOfficerTitles_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_ClearAllegianceOfficerTitles_C2S Message Data
        /// </summary>
        public Allegiance_ClearAllegianceOfficerTitles_C2S Data;
    
        public Allegiance_ClearAllegianceOfficerTitles_C2S_EventArgs(Allegiance_ClearAllegianceOfficerTitles_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_DoAllegianceLockAction_C2S Event Args
    /// </summary>
    public class Allegiance_DoAllegianceLockAction_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_DoAllegianceLockAction_C2S Message Data
        /// </summary>
        public Allegiance_DoAllegianceLockAction_C2S Data;
    
        public Allegiance_DoAllegianceLockAction_C2S_EventArgs(Allegiance_DoAllegianceLockAction_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_SetAllegianceApprovedVassal_C2S Event Args
    /// </summary>
    public class Allegiance_SetAllegianceApprovedVassal_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_SetAllegianceApprovedVassal_C2S Message Data
        /// </summary>
        public Allegiance_SetAllegianceApprovedVassal_C2S Data;
    
        public Allegiance_SetAllegianceApprovedVassal_C2S_EventArgs(Allegiance_SetAllegianceApprovedVassal_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_AllegianceChatGag_C2S Event Args
    /// </summary>
    public class Allegiance_AllegianceChatGag_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_AllegianceChatGag_C2S Message Data
        /// </summary>
        public Allegiance_AllegianceChatGag_C2S Data;
    
        public Allegiance_AllegianceChatGag_C2S_EventArgs(Allegiance_AllegianceChatGag_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_DoAllegianceHouseAction_C2S Event Args
    /// </summary>
    public class Allegiance_DoAllegianceHouseAction_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_DoAllegianceHouseAction_C2S Message Data
        /// </summary>
        public Allegiance_DoAllegianceHouseAction_C2S Data;
    
        public Allegiance_DoAllegianceHouseAction_C2S_EventArgs(Allegiance_DoAllegianceHouseAction_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Train_TrainAttribute2nd_C2S Event Args
    /// </summary>
    public class Train_TrainAttribute2nd_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Train_TrainAttribute2nd_C2S Message Data
        /// </summary>
        public Train_TrainAttribute2nd_C2S Data;
    
        public Train_TrainAttribute2nd_C2S_EventArgs(Train_TrainAttribute2nd_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Train_TrainAttribute_C2S Event Args
    /// </summary>
    public class Train_TrainAttribute_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Train_TrainAttribute_C2S Message Data
        /// </summary>
        public Train_TrainAttribute_C2S Data;
    
        public Train_TrainAttribute_C2S_EventArgs(Train_TrainAttribute_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Train_TrainSkill_C2S Event Args
    /// </summary>
    public class Train_TrainSkill_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Train_TrainSkill_C2S Message Data
        /// </summary>
        public Train_TrainSkill_C2S Data;
    
        public Train_TrainSkill_C2S_EventArgs(Train_TrainSkill_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Train_TrainSkillAdvancementClass_C2S Event Args
    /// </summary>
    public class Train_TrainSkillAdvancementClass_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Train_TrainSkillAdvancementClass_C2S Message Data
        /// </summary>
        public Train_TrainSkillAdvancementClass_C2S Data;
    
        public Train_TrainSkillAdvancementClass_C2S_EventArgs(Train_TrainSkillAdvancementClass_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Magic_CastUntargetedSpell_C2S Event Args
    /// </summary>
    public class Magic_CastUntargetedSpell_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Magic_CastUntargetedSpell_C2S Message Data
        /// </summary>
        public Magic_CastUntargetedSpell_C2S Data;
    
        public Magic_CastUntargetedSpell_C2S_EventArgs(Magic_CastUntargetedSpell_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Magic_CastTargetedSpell_C2S Event Args
    /// </summary>
    public class Magic_CastTargetedSpell_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Magic_CastTargetedSpell_C2S Message Data
        /// </summary>
        public Magic_CastTargetedSpell_C2S Data;
    
        public Magic_CastTargetedSpell_C2S_EventArgs(Magic_CastTargetedSpell_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_StopViewingObjectContents_S2C Event Args
    /// </summary>
    public class Item_StopViewingObjectContents_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_StopViewingObjectContents_S2C Message Data
        /// </summary>
        public Item_StopViewingObjectContents_S2C Data;
    
        public Item_StopViewingObjectContents_S2C_EventArgs(Item_StopViewingObjectContents_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_ChangeCombatMode_C2S Event Args
    /// </summary>
    public class Combat_ChangeCombatMode_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Combat_ChangeCombatMode_C2S Message Data
        /// </summary>
        public Combat_ChangeCombatMode_C2S Data;
    
        public Combat_ChangeCombatMode_C2S_EventArgs(Combat_ChangeCombatMode_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_StackableMerge_C2S Event Args
    /// </summary>
    public class Inventory_StackableMerge_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_StackableMerge_C2S Message Data
        /// </summary>
        public Inventory_StackableMerge_C2S Data;
    
        public Inventory_StackableMerge_C2S_EventArgs(Inventory_StackableMerge_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_StackableSplitToContainer_C2S Event Args
    /// </summary>
    public class Inventory_StackableSplitToContainer_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_StackableSplitToContainer_C2S Message Data
        /// </summary>
        public Inventory_StackableSplitToContainer_C2S Data;
    
        public Inventory_StackableSplitToContainer_C2S_EventArgs(Inventory_StackableSplitToContainer_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_StackableSplitTo3D_C2S Event Args
    /// </summary>
    public class Inventory_StackableSplitTo3D_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_StackableSplitTo3D_C2S Message Data
        /// </summary>
        public Inventory_StackableSplitTo3D_C2S Data;
    
        public Inventory_StackableSplitTo3D_C2S_EventArgs(Inventory_StackableSplitTo3D_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_ModifyCharacterSquelch_C2S Event Args
    /// </summary>
    public class Communication_ModifyCharacterSquelch_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_ModifyCharacterSquelch_C2S Message Data
        /// </summary>
        public Communication_ModifyCharacterSquelch_C2S Data;
    
        public Communication_ModifyCharacterSquelch_C2S_EventArgs(Communication_ModifyCharacterSquelch_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_ModifyAccountSquelch_C2S Event Args
    /// </summary>
    public class Communication_ModifyAccountSquelch_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_ModifyAccountSquelch_C2S Message Data
        /// </summary>
        public Communication_ModifyAccountSquelch_C2S Data;
    
        public Communication_ModifyAccountSquelch_C2S_EventArgs(Communication_ModifyAccountSquelch_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_ModifyGlobalSquelch_C2S Event Args
    /// </summary>
    public class Communication_ModifyGlobalSquelch_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_ModifyGlobalSquelch_C2S Message Data
        /// </summary>
        public Communication_ModifyGlobalSquelch_C2S Data;
    
        public Communication_ModifyGlobalSquelch_C2S_EventArgs(Communication_ModifyGlobalSquelch_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_TalkDirectByName_C2S Event Args
    /// </summary>
    public class Communication_TalkDirectByName_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_TalkDirectByName_C2S Message Data
        /// </summary>
        public Communication_TalkDirectByName_C2S Data;
    
        public Communication_TalkDirectByName_C2S_EventArgs(Communication_TalkDirectByName_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Vendor_Buy_C2S Event Args
    /// </summary>
    public class Vendor_Buy_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Vendor_Buy_C2S Message Data
        /// </summary>
        public Vendor_Buy_C2S Data;
    
        public Vendor_Buy_C2S_EventArgs(Vendor_Buy_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Vendor_Sell_C2S Event Args
    /// </summary>
    public class Vendor_Sell_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Vendor_Sell_C2S Message Data
        /// </summary>
        public Vendor_Sell_C2S Data;
    
        public Vendor_Sell_C2S_EventArgs(Vendor_Sell_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Vendor_VendorInfo_S2C Event Args
    /// </summary>
    public class Vendor_VendorInfo_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Vendor_VendorInfo_S2C Message Data
        /// </summary>
        public Vendor_VendorInfo_S2C Data;
    
        public Vendor_VendorInfo_S2C_EventArgs(Vendor_VendorInfo_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_TeleToLifestone_C2S Event Args
    /// </summary>
    public class Character_TeleToLifestone_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_TeleToLifestone_C2S Message Data
        /// </summary>
        public Character_TeleToLifestone_C2S Data;
    
        public Character_TeleToLifestone_C2S_EventArgs(Character_TeleToLifestone_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_StartBarber_S2C Event Args
    /// </summary>
    public class Character_StartBarber_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Character_StartBarber_S2C Message Data
        /// </summary>
        public Character_StartBarber_S2C Data;
    
        public Character_StartBarber_S2C_EventArgs(Character_StartBarber_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_ServerSaysAttemptFailed_S2C Event Args
    /// </summary>
    public class Character_ServerSaysAttemptFailed_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Character_ServerSaysAttemptFailed_S2C Message Data
        /// </summary>
        public Character_ServerSaysAttemptFailed_S2C Data;
    
        public Character_ServerSaysAttemptFailed_S2C_EventArgs(Character_ServerSaysAttemptFailed_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_LoginCompleteNotification_C2S Event Args
    /// </summary>
    public class Character_LoginCompleteNotification_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_LoginCompleteNotification_C2S Message Data
        /// </summary>
        public Character_LoginCompleteNotification_C2S Data;
    
        public Character_LoginCompleteNotification_C2S_EventArgs(Character_LoginCompleteNotification_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_Create_C2S Event Args
    /// </summary>
    public class Fellowship_Create_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_Create_C2S Message Data
        /// </summary>
        public Fellowship_Create_C2S Data;
    
        public Fellowship_Create_C2S_EventArgs(Fellowship_Create_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_Quit_S2C Event Args
    /// </summary>
    public class Fellowship_Quit_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_Quit_S2C Message Data
        /// </summary>
        public Fellowship_Quit_S2C Data;
    
        public Fellowship_Quit_S2C_EventArgs(Fellowship_Quit_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_Quit_C2S Event Args
    /// </summary>
    public class Fellowship_Quit_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_Quit_C2S Message Data
        /// </summary>
        public Fellowship_Quit_C2S Data;
    
        public Fellowship_Quit_C2S_EventArgs(Fellowship_Quit_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_Dismiss_S2C Event Args
    /// </summary>
    public class Fellowship_Dismiss_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_Dismiss_S2C Message Data
        /// </summary>
        public Fellowship_Dismiss_S2C Data;
    
        public Fellowship_Dismiss_S2C_EventArgs(Fellowship_Dismiss_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_Dismiss_C2S Event Args
    /// </summary>
    public class Fellowship_Dismiss_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_Dismiss_C2S Message Data
        /// </summary>
        public Fellowship_Dismiss_C2S Data;
    
        public Fellowship_Dismiss_C2S_EventArgs(Fellowship_Dismiss_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_Recruit_C2S Event Args
    /// </summary>
    public class Fellowship_Recruit_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_Recruit_C2S Message Data
        /// </summary>
        public Fellowship_Recruit_C2S Data;
    
        public Fellowship_Recruit_C2S_EventArgs(Fellowship_Recruit_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_UpdateRequest_C2S Event Args
    /// </summary>
    public class Fellowship_UpdateRequest_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_UpdateRequest_C2S Message Data
        /// </summary>
        public Fellowship_UpdateRequest_C2S Data;
    
        public Fellowship_UpdateRequest_C2S_EventArgs(Fellowship_UpdateRequest_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Writing_BookAddPage_C2S Event Args
    /// </summary>
    public class Writing_BookAddPage_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Writing_BookAddPage_C2S Message Data
        /// </summary>
        public Writing_BookAddPage_C2S Data;
    
        public Writing_BookAddPage_C2S_EventArgs(Writing_BookAddPage_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Writing_BookModifyPage_C2S Event Args
    /// </summary>
    public class Writing_BookModifyPage_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Writing_BookModifyPage_C2S Message Data
        /// </summary>
        public Writing_BookModifyPage_C2S Data;
    
        public Writing_BookModifyPage_C2S_EventArgs(Writing_BookModifyPage_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Writing_BookData_C2S Event Args
    /// </summary>
    public class Writing_BookData_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Writing_BookData_C2S Message Data
        /// </summary>
        public Writing_BookData_C2S Data;
    
        public Writing_BookData_C2S_EventArgs(Writing_BookData_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Writing_BookDeletePage_C2S Event Args
    /// </summary>
    public class Writing_BookDeletePage_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Writing_BookDeletePage_C2S Message Data
        /// </summary>
        public Writing_BookDeletePage_C2S Data;
    
        public Writing_BookDeletePage_C2S_EventArgs(Writing_BookDeletePage_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Writing_BookPageData_C2S Event Args
    /// </summary>
    public class Writing_BookPageData_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Writing_BookPageData_C2S Message Data
        /// </summary>
        public Writing_BookPageData_C2S Data;
    
        public Writing_BookPageData_C2S_EventArgs(Writing_BookPageData_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Writing_BookOpen_S2C Event Args
    /// </summary>
    public class Writing_BookOpen_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Writing_BookOpen_S2C Message Data
        /// </summary>
        public Writing_BookOpen_S2C Data;
    
        public Writing_BookOpen_S2C_EventArgs(Writing_BookOpen_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Writing_BookAddPageResponse_S2C Event Args
    /// </summary>
    public class Writing_BookAddPageResponse_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Writing_BookAddPageResponse_S2C Message Data
        /// </summary>
        public Writing_BookAddPageResponse_S2C Data;
    
        public Writing_BookAddPageResponse_S2C_EventArgs(Writing_BookAddPageResponse_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Writing_BookDeletePageResponse_S2C Event Args
    /// </summary>
    public class Writing_BookDeletePageResponse_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Writing_BookDeletePageResponse_S2C Message Data
        /// </summary>
        public Writing_BookDeletePageResponse_S2C Data;
    
        public Writing_BookDeletePageResponse_S2C_EventArgs(Writing_BookDeletePageResponse_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Writing_BookPageDataResponse_S2C Event Args
    /// </summary>
    public class Writing_BookPageDataResponse_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Writing_BookPageDataResponse_S2C Message Data
        /// </summary>
        public Writing_BookPageDataResponse_S2C Data;
    
        public Writing_BookPageDataResponse_S2C_EventArgs(Writing_BookPageDataResponse_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Writing_SetInscription_C2S Event Args
    /// </summary>
    public class Writing_SetInscription_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Writing_SetInscription_C2S Message Data
        /// </summary>
        public Writing_SetInscription_C2S Data;
    
        public Writing_SetInscription_C2S_EventArgs(Writing_SetInscription_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_GetInscriptionResponse_S2C Event Args
    /// </summary>
    public class Item_GetInscriptionResponse_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_GetInscriptionResponse_S2C Message Data
        /// </summary>
        public Item_GetInscriptionResponse_S2C Data;
    
        public Item_GetInscriptionResponse_S2C_EventArgs(Item_GetInscriptionResponse_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_Appraise_C2S Event Args
    /// </summary>
    public class Item_Appraise_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Item_Appraise_C2S Message Data
        /// </summary>
        public Item_Appraise_C2S Data;
    
        public Item_Appraise_C2S_EventArgs(Item_Appraise_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_SetAppraiseInfo_S2C Event Args
    /// </summary>
    public class Item_SetAppraiseInfo_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_SetAppraiseInfo_S2C Message Data
        /// </summary>
        public Item_SetAppraiseInfo_S2C Data;
    
        public Item_SetAppraiseInfo_S2C_EventArgs(Item_SetAppraiseInfo_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_GiveObjectRequest_C2S Event Args
    /// </summary>
    public class Inventory_GiveObjectRequest_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_GiveObjectRequest_C2S Message Data
        /// </summary>
        public Inventory_GiveObjectRequest_C2S Data;
    
        public Inventory_GiveObjectRequest_C2S_EventArgs(Inventory_GiveObjectRequest_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Advocate_Teleport_C2S Event Args
    /// </summary>
    public class Advocate_Teleport_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Advocate_Teleport_C2S Message Data
        /// </summary>
        public Advocate_Teleport_C2S Data;
    
        public Advocate_Teleport_C2S_EventArgs(Advocate_Teleport_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_AbuseLogRequest_C2S Event Args
    /// </summary>
    public class Character_AbuseLogRequest_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_AbuseLogRequest_C2S Message Data
        /// </summary>
        public Character_AbuseLogRequest_C2S Data;
    
        public Character_AbuseLogRequest_C2S_EventArgs(Character_AbuseLogRequest_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_AddToChannel_C2S Event Args
    /// </summary>
    public class Communication_AddToChannel_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_AddToChannel_C2S Message Data
        /// </summary>
        public Communication_AddToChannel_C2S Data;
    
        public Communication_AddToChannel_C2S_EventArgs(Communication_AddToChannel_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_RemoveFromChannel_C2S Event Args
    /// </summary>
    public class Communication_RemoveFromChannel_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_RemoveFromChannel_C2S Message Data
        /// </summary>
        public Communication_RemoveFromChannel_C2S Data;
    
        public Communication_RemoveFromChannel_C2S_EventArgs(Communication_RemoveFromChannel_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_ChannelBroadcast_S2C Event Args
    /// </summary>
    public class Communication_ChannelBroadcast_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_ChannelBroadcast_S2C Message Data
        /// </summary>
        public Communication_ChannelBroadcast_S2C Data;
    
        public Communication_ChannelBroadcast_S2C_EventArgs(Communication_ChannelBroadcast_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_ChannelBroadcast_C2S Event Args
    /// </summary>
    public class Communication_ChannelBroadcast_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_ChannelBroadcast_C2S Message Data
        /// </summary>
        public Communication_ChannelBroadcast_C2S Data;
    
        public Communication_ChannelBroadcast_C2S_EventArgs(Communication_ChannelBroadcast_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_ChannelList_S2C Event Args
    /// </summary>
    public class Communication_ChannelList_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_ChannelList_S2C Message Data
        /// </summary>
        public Communication_ChannelList_S2C Data;
    
        public Communication_ChannelList_S2C_EventArgs(Communication_ChannelList_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_ChannelList_C2S Event Args
    /// </summary>
    public class Communication_ChannelList_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_ChannelList_C2S Message Data
        /// </summary>
        public Communication_ChannelList_C2S Data;
    
        public Communication_ChannelList_C2S_EventArgs(Communication_ChannelList_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_ChannelIndex_S2C Event Args
    /// </summary>
    public class Communication_ChannelIndex_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_ChannelIndex_S2C Message Data
        /// </summary>
        public Communication_ChannelIndex_S2C Data;
    
        public Communication_ChannelIndex_S2C_EventArgs(Communication_ChannelIndex_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_ChannelIndex_C2S Event Args
    /// </summary>
    public class Communication_ChannelIndex_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_ChannelIndex_C2S Message Data
        /// </summary>
        public Communication_ChannelIndex_C2S Data;
    
        public Communication_ChannelIndex_C2S_EventArgs(Communication_ChannelIndex_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_NoLongerViewingContents_C2S Event Args
    /// </summary>
    public class Inventory_NoLongerViewingContents_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_NoLongerViewingContents_C2S Message Data
        /// </summary>
        public Inventory_NoLongerViewingContents_C2S Data;
    
        public Inventory_NoLongerViewingContents_C2S_EventArgs(Inventory_NoLongerViewingContents_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_OnViewContents_S2C Event Args
    /// </summary>
    public class Item_OnViewContents_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_OnViewContents_S2C Message Data
        /// </summary>
        public Item_OnViewContents_S2C Data;
    
        public Item_OnViewContents_S2C_EventArgs(Item_OnViewContents_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_UpdateStackSize_S2C Event Args
    /// </summary>
    public class Item_UpdateStackSize_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_UpdateStackSize_S2C Message Data
        /// </summary>
        public Item_UpdateStackSize_S2C Data;
    
        public Item_UpdateStackSize_S2C_EventArgs(Item_UpdateStackSize_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_ServerSaysMoveItem_S2C Event Args
    /// </summary>
    public class Item_ServerSaysMoveItem_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_ServerSaysMoveItem_S2C Message Data
        /// </summary>
        public Item_ServerSaysMoveItem_S2C Data;
    
        public Item_ServerSaysMoveItem_S2C_EventArgs(Item_ServerSaysMoveItem_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_StackableSplitToWield_C2S Event Args
    /// </summary>
    public class Inventory_StackableSplitToWield_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_StackableSplitToWield_C2S Message Data
        /// </summary>
        public Inventory_StackableSplitToWield_C2S Data;
    
        public Inventory_StackableSplitToWield_C2S_EventArgs(Inventory_StackableSplitToWield_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_AddShortCut_C2S Event Args
    /// </summary>
    public class Character_AddShortCut_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_AddShortCut_C2S Message Data
        /// </summary>
        public Character_AddShortCut_C2S Data;
    
        public Character_AddShortCut_C2S_EventArgs(Character_AddShortCut_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_RemoveShortCut_C2S Event Args
    /// </summary>
    public class Character_RemoveShortCut_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_RemoveShortCut_C2S Message Data
        /// </summary>
        public Character_RemoveShortCut_C2S Data;
    
        public Character_RemoveShortCut_C2S_EventArgs(Character_RemoveShortCut_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_HandlePlayerDeathEvent_S2C Event Args
    /// </summary>
    public class Combat_HandlePlayerDeathEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Combat_HandlePlayerDeathEvent_S2C Message Data
        /// </summary>
        public Combat_HandlePlayerDeathEvent_S2C Data;
    
        public Combat_HandlePlayerDeathEvent_S2C_EventArgs(Combat_HandlePlayerDeathEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_CharacterOptionsEvent_C2S Event Args
    /// </summary>
    public class Character_CharacterOptionsEvent_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_CharacterOptionsEvent_C2S Message Data
        /// </summary>
        public Character_CharacterOptionsEvent_C2S Data;
    
        public Character_CharacterOptionsEvent_C2S_EventArgs(Character_CharacterOptionsEvent_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_HandleAttackDoneEvent_S2C Event Args
    /// </summary>
    public class Combat_HandleAttackDoneEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Combat_HandleAttackDoneEvent_S2C Message Data
        /// </summary>
        public Combat_HandleAttackDoneEvent_S2C Data;
    
        public Combat_HandleAttackDoneEvent_S2C_EventArgs(Combat_HandleAttackDoneEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Magic_RemoveSpell_S2C Event Args
    /// </summary>
    public class Magic_RemoveSpell_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Magic_RemoveSpell_S2C Message Data
        /// </summary>
        public Magic_RemoveSpell_S2C Data;
    
        public Magic_RemoveSpell_S2C_EventArgs(Magic_RemoveSpell_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Magic_RemoveSpell_C2S Event Args
    /// </summary>
    public class Magic_RemoveSpell_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Magic_RemoveSpell_C2S Message Data
        /// </summary>
        public Magic_RemoveSpell_C2S Data;
    
        public Magic_RemoveSpell_C2S_EventArgs(Magic_RemoveSpell_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_HandleVictimNotificationEventSelf_S2C Event Args
    /// </summary>
    public class Combat_HandleVictimNotificationEventSelf_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Combat_HandleVictimNotificationEventSelf_S2C Message Data
        /// </summary>
        public Combat_HandleVictimNotificationEventSelf_S2C Data;
    
        public Combat_HandleVictimNotificationEventSelf_S2C_EventArgs(Combat_HandleVictimNotificationEventSelf_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_HandleVictimNotificationEventOther_S2C Event Args
    /// </summary>
    public class Combat_HandleVictimNotificationEventOther_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Combat_HandleVictimNotificationEventOther_S2C Message Data
        /// </summary>
        public Combat_HandleVictimNotificationEventOther_S2C Data;
    
        public Combat_HandleVictimNotificationEventOther_S2C_EventArgs(Combat_HandleVictimNotificationEventOther_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_HandleAttackerNotificationEvent_S2C Event Args
    /// </summary>
    public class Combat_HandleAttackerNotificationEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Combat_HandleAttackerNotificationEvent_S2C Message Data
        /// </summary>
        public Combat_HandleAttackerNotificationEvent_S2C Data;
    
        public Combat_HandleAttackerNotificationEvent_S2C_EventArgs(Combat_HandleAttackerNotificationEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_HandleDefenderNotificationEvent_S2C Event Args
    /// </summary>
    public class Combat_HandleDefenderNotificationEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Combat_HandleDefenderNotificationEvent_S2C Message Data
        /// </summary>
        public Combat_HandleDefenderNotificationEvent_S2C Data;
    
        public Combat_HandleDefenderNotificationEvent_S2C_EventArgs(Combat_HandleDefenderNotificationEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_HandleEvasionAttackerNotificationEvent_S2C Event Args
    /// </summary>
    public class Combat_HandleEvasionAttackerNotificationEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Combat_HandleEvasionAttackerNotificationEvent_S2C Message Data
        /// </summary>
        public Combat_HandleEvasionAttackerNotificationEvent_S2C Data;
    
        public Combat_HandleEvasionAttackerNotificationEvent_S2C_EventArgs(Combat_HandleEvasionAttackerNotificationEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_HandleEvasionDefenderNotificationEvent_S2C Event Args
    /// </summary>
    public class Combat_HandleEvasionDefenderNotificationEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Combat_HandleEvasionDefenderNotificationEvent_S2C Message Data
        /// </summary>
        public Combat_HandleEvasionDefenderNotificationEvent_S2C Data;
    
        public Combat_HandleEvasionDefenderNotificationEvent_S2C_EventArgs(Combat_HandleEvasionDefenderNotificationEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_CancelAttack_C2S Event Args
    /// </summary>
    public class Combat_CancelAttack_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Combat_CancelAttack_C2S Message Data
        /// </summary>
        public Combat_CancelAttack_C2S Data;
    
        public Combat_CancelAttack_C2S_EventArgs(Combat_CancelAttack_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_HandleCommenceAttackEvent_S2C Event Args
    /// </summary>
    public class Combat_HandleCommenceAttackEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Combat_HandleCommenceAttackEvent_S2C Message Data
        /// </summary>
        public Combat_HandleCommenceAttackEvent_S2C Data;
    
        public Combat_HandleCommenceAttackEvent_S2C_EventArgs(Combat_HandleCommenceAttackEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_QueryHealth_C2S Event Args
    /// </summary>
    public class Combat_QueryHealth_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Combat_QueryHealth_C2S Message Data
        /// </summary>
        public Combat_QueryHealth_C2S Data;
    
        public Combat_QueryHealth_C2S_EventArgs(Combat_QueryHealth_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Combat_QueryHealthResponse_S2C Event Args
    /// </summary>
    public class Combat_QueryHealthResponse_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Combat_QueryHealthResponse_S2C Message Data
        /// </summary>
        public Combat_QueryHealthResponse_S2C Data;
    
        public Combat_QueryHealthResponse_S2C_EventArgs(Combat_QueryHealthResponse_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_QueryAge_C2S Event Args
    /// </summary>
    public class Character_QueryAge_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_QueryAge_C2S Message Data
        /// </summary>
        public Character_QueryAge_C2S Data;
    
        public Character_QueryAge_C2S_EventArgs(Character_QueryAge_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_QueryAgeResponse_S2C Event Args
    /// </summary>
    public class Character_QueryAgeResponse_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Character_QueryAgeResponse_S2C Message Data
        /// </summary>
        public Character_QueryAgeResponse_S2C Data;
    
        public Character_QueryAgeResponse_S2C_EventArgs(Character_QueryAgeResponse_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_QueryBirth_C2S Event Args
    /// </summary>
    public class Character_QueryBirth_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_QueryBirth_C2S Message Data
        /// </summary>
        public Character_QueryBirth_C2S Data;
    
        public Character_QueryBirth_C2S_EventArgs(Character_QueryBirth_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_UseDone_S2C Event Args
    /// </summary>
    public class Item_UseDone_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_UseDone_S2C Message Data
        /// </summary>
        public Item_UseDone_S2C Data;
    
        public Item_UseDone_S2C_EventArgs(Item_UseDone_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_FellowUpdateDone_S2C Event Args
    /// </summary>
    public class Fellowship_FellowUpdateDone_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_FellowUpdateDone_S2C Message Data
        /// </summary>
        public Fellowship_FellowUpdateDone_S2C Data;
    
        public Fellowship_FellowUpdateDone_S2C_EventArgs(Fellowship_FellowUpdateDone_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_FellowStatsDone_S2C Event Args
    /// </summary>
    public class Fellowship_FellowStatsDone_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_FellowStatsDone_S2C Message Data
        /// </summary>
        public Fellowship_FellowStatsDone_S2C Data;
    
        public Fellowship_FellowStatsDone_S2C_EventArgs(Fellowship_FellowStatsDone_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_AppraiseDone_S2C Event Args
    /// </summary>
    public class Item_AppraiseDone_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_AppraiseDone_S2C Message Data
        /// </summary>
        public Item_AppraiseDone_S2C Data;
    
        public Item_AppraiseDone_S2C_EventArgs(Item_AppraiseDone_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateRemoveIntEvent_S2C Event Args
    /// </summary>
    public class Qualities_PrivateRemoveIntEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateRemoveIntEvent_S2C Message Data
        /// </summary>
        public Qualities_PrivateRemoveIntEvent_S2C Data;
    
        public Qualities_PrivateRemoveIntEvent_S2C_EventArgs(Qualities_PrivateRemoveIntEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_RemoveIntEvent_S2C Event Args
    /// </summary>
    public class Qualities_RemoveIntEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_RemoveIntEvent_S2C Message Data
        /// </summary>
        public Qualities_RemoveIntEvent_S2C Data;
    
        public Qualities_RemoveIntEvent_S2C_EventArgs(Qualities_RemoveIntEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateRemoveBoolEvent_S2C Event Args
    /// </summary>
    public class Qualities_PrivateRemoveBoolEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateRemoveBoolEvent_S2C Message Data
        /// </summary>
        public Qualities_PrivateRemoveBoolEvent_S2C Data;
    
        public Qualities_PrivateRemoveBoolEvent_S2C_EventArgs(Qualities_PrivateRemoveBoolEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_RemoveBoolEvent_S2C Event Args
    /// </summary>
    public class Qualities_RemoveBoolEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_RemoveBoolEvent_S2C Message Data
        /// </summary>
        public Qualities_RemoveBoolEvent_S2C Data;
    
        public Qualities_RemoveBoolEvent_S2C_EventArgs(Qualities_RemoveBoolEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateRemoveFloatEvent_S2C Event Args
    /// </summary>
    public class Qualities_PrivateRemoveFloatEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateRemoveFloatEvent_S2C Message Data
        /// </summary>
        public Qualities_PrivateRemoveFloatEvent_S2C Data;
    
        public Qualities_PrivateRemoveFloatEvent_S2C_EventArgs(Qualities_PrivateRemoveFloatEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_RemoveFloatEvent_S2C Event Args
    /// </summary>
    public class Qualities_RemoveFloatEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_RemoveFloatEvent_S2C Message Data
        /// </summary>
        public Qualities_RemoveFloatEvent_S2C Data;
    
        public Qualities_RemoveFloatEvent_S2C_EventArgs(Qualities_RemoveFloatEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateRemoveStringEvent_S2C Event Args
    /// </summary>
    public class Qualities_PrivateRemoveStringEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateRemoveStringEvent_S2C Message Data
        /// </summary>
        public Qualities_PrivateRemoveStringEvent_S2C Data;
    
        public Qualities_PrivateRemoveStringEvent_S2C_EventArgs(Qualities_PrivateRemoveStringEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_RemoveStringEvent_S2C Event Args
    /// </summary>
    public class Qualities_RemoveStringEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_RemoveStringEvent_S2C Message Data
        /// </summary>
        public Qualities_RemoveStringEvent_S2C Data;
    
        public Qualities_RemoveStringEvent_S2C_EventArgs(Qualities_RemoveStringEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateRemoveDataIDEvent_S2C Event Args
    /// </summary>
    public class Qualities_PrivateRemoveDataIDEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateRemoveDataIDEvent_S2C Message Data
        /// </summary>
        public Qualities_PrivateRemoveDataIDEvent_S2C Data;
    
        public Qualities_PrivateRemoveDataIDEvent_S2C_EventArgs(Qualities_PrivateRemoveDataIDEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_RemoveDataIDEvent_S2C Event Args
    /// </summary>
    public class Qualities_RemoveDataIDEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_RemoveDataIDEvent_S2C Message Data
        /// </summary>
        public Qualities_RemoveDataIDEvent_S2C Data;
    
        public Qualities_RemoveDataIDEvent_S2C_EventArgs(Qualities_RemoveDataIDEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateRemoveInstanceIDEvent_S2C Event Args
    /// </summary>
    public class Qualities_PrivateRemoveInstanceIDEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateRemoveInstanceIDEvent_S2C Message Data
        /// </summary>
        public Qualities_PrivateRemoveInstanceIDEvent_S2C Data;
    
        public Qualities_PrivateRemoveInstanceIDEvent_S2C_EventArgs(Qualities_PrivateRemoveInstanceIDEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_RemoveInstanceIDEvent_S2C Event Args
    /// </summary>
    public class Qualities_RemoveInstanceIDEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_RemoveInstanceIDEvent_S2C Message Data
        /// </summary>
        public Qualities_RemoveInstanceIDEvent_S2C Data;
    
        public Qualities_RemoveInstanceIDEvent_S2C_EventArgs(Qualities_RemoveInstanceIDEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateRemovePositionEvent_S2C Event Args
    /// </summary>
    public class Qualities_PrivateRemovePositionEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateRemovePositionEvent_S2C Message Data
        /// </summary>
        public Qualities_PrivateRemovePositionEvent_S2C Data;
    
        public Qualities_PrivateRemovePositionEvent_S2C_EventArgs(Qualities_PrivateRemovePositionEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_RemovePositionEvent_S2C Event Args
    /// </summary>
    public class Qualities_RemovePositionEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_RemovePositionEvent_S2C Message Data
        /// </summary>
        public Qualities_RemovePositionEvent_S2C Data;
    
        public Qualities_RemovePositionEvent_S2C_EventArgs(Qualities_RemovePositionEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_Emote_C2S Event Args
    /// </summary>
    public class Communication_Emote_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_Emote_C2S Message Data
        /// </summary>
        public Communication_Emote_C2S Data;
    
        public Communication_Emote_C2S_EventArgs(Communication_Emote_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_HearEmote_S2C Event Args
    /// </summary>
    public class Communication_HearEmote_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_HearEmote_S2C Message Data
        /// </summary>
        public Communication_HearEmote_S2C Data;
    
        public Communication_HearEmote_S2C_EventArgs(Communication_HearEmote_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_SoulEmote_C2S Event Args
    /// </summary>
    public class Communication_SoulEmote_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Communication_SoulEmote_C2S Message Data
        /// </summary>
        public Communication_SoulEmote_C2S Data;
    
        public Communication_SoulEmote_C2S_EventArgs(Communication_SoulEmote_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_HearSoulEmote_S2C Event Args
    /// </summary>
    public class Communication_HearSoulEmote_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_HearSoulEmote_S2C Message Data
        /// </summary>
        public Communication_HearSoulEmote_S2C Data;
    
        public Communication_HearSoulEmote_S2C_EventArgs(Communication_HearSoulEmote_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_AddSpellFavorite_C2S Event Args
    /// </summary>
    public class Character_AddSpellFavorite_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_AddSpellFavorite_C2S Message Data
        /// </summary>
        public Character_AddSpellFavorite_C2S Data;
    
        public Character_AddSpellFavorite_C2S_EventArgs(Character_AddSpellFavorite_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_RemoveSpellFavorite_C2S Event Args
    /// </summary>
    public class Character_RemoveSpellFavorite_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_RemoveSpellFavorite_C2S Message Data
        /// </summary>
        public Character_RemoveSpellFavorite_C2S Data;
    
        public Character_RemoveSpellFavorite_C2S_EventArgs(Character_RemoveSpellFavorite_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_RequestPing_C2S Event Args
    /// </summary>
    public class Character_RequestPing_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_RequestPing_C2S Message Data
        /// </summary>
        public Character_RequestPing_C2S Data;
    
        public Character_RequestPing_C2S_EventArgs(Character_RequestPing_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_ReturnPing_S2C Event Args
    /// </summary>
    public class Character_ReturnPing_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Character_ReturnPing_S2C Message Data
        /// </summary>
        public Character_ReturnPing_S2C Data;
    
        public Character_ReturnPing_S2C_EventArgs(Character_ReturnPing_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_SetSquelchDB_S2C Event Args
    /// </summary>
    public class Communication_SetSquelchDB_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_SetSquelchDB_S2C Message Data
        /// </summary>
        public Communication_SetSquelchDB_S2C Data;
    
        public Communication_SetSquelchDB_S2C_EventArgs(Communication_SetSquelchDB_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_OpenTradeNegotiations_C2S Event Args
    /// </summary>
    public class Trade_OpenTradeNegotiations_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Trade_OpenTradeNegotiations_C2S Message Data
        /// </summary>
        public Trade_OpenTradeNegotiations_C2S Data;
    
        public Trade_OpenTradeNegotiations_C2S_EventArgs(Trade_OpenTradeNegotiations_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_CloseTradeNegotiations_C2S Event Args
    /// </summary>
    public class Trade_CloseTradeNegotiations_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Trade_CloseTradeNegotiations_C2S Message Data
        /// </summary>
        public Trade_CloseTradeNegotiations_C2S Data;
    
        public Trade_CloseTradeNegotiations_C2S_EventArgs(Trade_CloseTradeNegotiations_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_AddToTrade_C2S Event Args
    /// </summary>
    public class Trade_AddToTrade_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Trade_AddToTrade_C2S Message Data
        /// </summary>
        public Trade_AddToTrade_C2S Data;
    
        public Trade_AddToTrade_C2S_EventArgs(Trade_AddToTrade_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_AcceptTrade_C2S Event Args
    /// </summary>
    public class Trade_AcceptTrade_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Trade_AcceptTrade_C2S Message Data
        /// </summary>
        public Trade_AcceptTrade_C2S Data;
    
        public Trade_AcceptTrade_C2S_EventArgs(Trade_AcceptTrade_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_DeclineTrade_C2S Event Args
    /// </summary>
    public class Trade_DeclineTrade_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Trade_DeclineTrade_C2S Message Data
        /// </summary>
        public Trade_DeclineTrade_C2S Data;
    
        public Trade_DeclineTrade_C2S_EventArgs(Trade_DeclineTrade_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_RegisterTrade_S2C Event Args
    /// </summary>
    public class Trade_RegisterTrade_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Trade_RegisterTrade_S2C Message Data
        /// </summary>
        public Trade_RegisterTrade_S2C Data;
    
        public Trade_RegisterTrade_S2C_EventArgs(Trade_RegisterTrade_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_OpenTrade_S2C Event Args
    /// </summary>
    public class Trade_OpenTrade_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Trade_OpenTrade_S2C Message Data
        /// </summary>
        public Trade_OpenTrade_S2C Data;
    
        public Trade_OpenTrade_S2C_EventArgs(Trade_OpenTrade_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_CloseTrade_S2C Event Args
    /// </summary>
    public class Trade_CloseTrade_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Trade_CloseTrade_S2C Message Data
        /// </summary>
        public Trade_CloseTrade_S2C Data;
    
        public Trade_CloseTrade_S2C_EventArgs(Trade_CloseTrade_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_AddToTrade_S2C Event Args
    /// </summary>
    public class Trade_AddToTrade_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Trade_AddToTrade_S2C Message Data
        /// </summary>
        public Trade_AddToTrade_S2C Data;
    
        public Trade_AddToTrade_S2C_EventArgs(Trade_AddToTrade_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_RemoveFromTrade_S2C Event Args
    /// </summary>
    public class Trade_RemoveFromTrade_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Trade_RemoveFromTrade_S2C Message Data
        /// </summary>
        public Trade_RemoveFromTrade_S2C Data;
    
        public Trade_RemoveFromTrade_S2C_EventArgs(Trade_RemoveFromTrade_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_AcceptTrade_S2C Event Args
    /// </summary>
    public class Trade_AcceptTrade_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Trade_AcceptTrade_S2C Message Data
        /// </summary>
        public Trade_AcceptTrade_S2C Data;
    
        public Trade_AcceptTrade_S2C_EventArgs(Trade_AcceptTrade_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_DeclineTrade_S2C Event Args
    /// </summary>
    public class Trade_DeclineTrade_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Trade_DeclineTrade_S2C Message Data
        /// </summary>
        public Trade_DeclineTrade_S2C Data;
    
        public Trade_DeclineTrade_S2C_EventArgs(Trade_DeclineTrade_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_ResetTrade_C2S Event Args
    /// </summary>
    public class Trade_ResetTrade_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Trade_ResetTrade_C2S Message Data
        /// </summary>
        public Trade_ResetTrade_C2S Data;
    
        public Trade_ResetTrade_C2S_EventArgs(Trade_ResetTrade_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_ResetTrade_S2C Event Args
    /// </summary>
    public class Trade_ResetTrade_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Trade_ResetTrade_S2C Message Data
        /// </summary>
        public Trade_ResetTrade_S2C Data;
    
        public Trade_ResetTrade_S2C_EventArgs(Trade_ResetTrade_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_TradeFailure_S2C Event Args
    /// </summary>
    public class Trade_TradeFailure_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Trade_TradeFailure_S2C Message Data
        /// </summary>
        public Trade_TradeFailure_S2C Data;
    
        public Trade_TradeFailure_S2C_EventArgs(Trade_TradeFailure_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Trade_ClearTradeAcceptance_S2C Event Args
    /// </summary>
    public class Trade_ClearTradeAcceptance_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Trade_ClearTradeAcceptance_S2C Message Data
        /// </summary>
        public Trade_ClearTradeAcceptance_S2C Data;
    
        public Trade_ClearTradeAcceptance_S2C_EventArgs(Trade_ClearTradeAcceptance_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_ClearPlayerConsentList_C2S Event Args
    /// </summary>
    public class Character_ClearPlayerConsentList_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_ClearPlayerConsentList_C2S Message Data
        /// </summary>
        public Character_ClearPlayerConsentList_C2S Data;
    
        public Character_ClearPlayerConsentList_C2S_EventArgs(Character_ClearPlayerConsentList_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_DisplayPlayerConsentList_C2S Event Args
    /// </summary>
    public class Character_DisplayPlayerConsentList_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_DisplayPlayerConsentList_C2S Message Data
        /// </summary>
        public Character_DisplayPlayerConsentList_C2S Data;
    
        public Character_DisplayPlayerConsentList_C2S_EventArgs(Character_DisplayPlayerConsentList_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_RemoveFromPlayerConsentList_C2S Event Args
    /// </summary>
    public class Character_RemoveFromPlayerConsentList_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_RemoveFromPlayerConsentList_C2S Message Data
        /// </summary>
        public Character_RemoveFromPlayerConsentList_C2S Data;
    
        public Character_RemoveFromPlayerConsentList_C2S_EventArgs(Character_RemoveFromPlayerConsentList_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_AddPlayerPermission_C2S Event Args
    /// </summary>
    public class Character_AddPlayerPermission_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_AddPlayerPermission_C2S Message Data
        /// </summary>
        public Character_AddPlayerPermission_C2S Data;
    
        public Character_AddPlayerPermission_C2S_EventArgs(Character_AddPlayerPermission_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_BuyHouse_C2S Event Args
    /// </summary>
    public class House_BuyHouse_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_BuyHouse_C2S Message Data
        /// </summary>
        public House_BuyHouse_C2S Data;
    
        public House_BuyHouse_C2S_EventArgs(House_BuyHouse_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_HouseProfile_S2C Event Args
    /// </summary>
    public class House_HouseProfile_S2C_EventArgs : EventArgs {
        /// <summary>
        /// House_HouseProfile_S2C Message Data
        /// </summary>
        public House_HouseProfile_S2C Data;
    
        public House_HouseProfile_S2C_EventArgs(House_HouseProfile_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_QueryHouse_C2S Event Args
    /// </summary>
    public class House_QueryHouse_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_QueryHouse_C2S Message Data
        /// </summary>
        public House_QueryHouse_C2S Data;
    
        public House_QueryHouse_C2S_EventArgs(House_QueryHouse_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_AbandonHouse_C2S Event Args
    /// </summary>
    public class House_AbandonHouse_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_AbandonHouse_C2S Message Data
        /// </summary>
        public House_AbandonHouse_C2S Data;
    
        public House_AbandonHouse_C2S_EventArgs(House_AbandonHouse_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_RemovePlayerPermission_C2S Event Args
    /// </summary>
    public class Character_RemovePlayerPermission_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_RemovePlayerPermission_C2S Message Data
        /// </summary>
        public Character_RemovePlayerPermission_C2S Data;
    
        public Character_RemovePlayerPermission_C2S_EventArgs(Character_RemovePlayerPermission_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_RentHouse_C2S Event Args
    /// </summary>
    public class House_RentHouse_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_RentHouse_C2S Message Data
        /// </summary>
        public House_RentHouse_C2S Data;
    
        public House_RentHouse_C2S_EventArgs(House_RentHouse_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_SetDesiredComponentLevel_C2S Event Args
    /// </summary>
    public class Character_SetDesiredComponentLevel_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_SetDesiredComponentLevel_C2S Message Data
        /// </summary>
        public Character_SetDesiredComponentLevel_C2S Data;
    
        public Character_SetDesiredComponentLevel_C2S_EventArgs(Character_SetDesiredComponentLevel_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_HouseData_S2C Event Args
    /// </summary>
    public class House_HouseData_S2C_EventArgs : EventArgs {
        /// <summary>
        /// House_HouseData_S2C Message Data
        /// </summary>
        public House_HouseData_S2C Data;
    
        public House_HouseData_S2C_EventArgs(House_HouseData_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_HouseStatus_S2C Event Args
    /// </summary>
    public class House_HouseStatus_S2C_EventArgs : EventArgs {
        /// <summary>
        /// House_HouseStatus_S2C Message Data
        /// </summary>
        public House_HouseStatus_S2C Data;
    
        public House_HouseStatus_S2C_EventArgs(House_HouseStatus_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_UpdateRentTime_S2C Event Args
    /// </summary>
    public class House_UpdateRentTime_S2C_EventArgs : EventArgs {
        /// <summary>
        /// House_UpdateRentTime_S2C Message Data
        /// </summary>
        public House_UpdateRentTime_S2C Data;
    
        public House_UpdateRentTime_S2C_EventArgs(House_UpdateRentTime_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_UpdateRentPayment_S2C Event Args
    /// </summary>
    public class House_UpdateRentPayment_S2C_EventArgs : EventArgs {
        /// <summary>
        /// House_UpdateRentPayment_S2C Message Data
        /// </summary>
        public House_UpdateRentPayment_S2C Data;
    
        public House_UpdateRentPayment_S2C_EventArgs(House_UpdateRentPayment_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_AddPermanentGuest_C2S Event Args
    /// </summary>
    public class House_AddPermanentGuest_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_AddPermanentGuest_C2S Message Data
        /// </summary>
        public House_AddPermanentGuest_C2S Data;
    
        public House_AddPermanentGuest_C2S_EventArgs(House_AddPermanentGuest_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_RemovePermanentGuest_C2S Event Args
    /// </summary>
    public class House_RemovePermanentGuest_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_RemovePermanentGuest_C2S Message Data
        /// </summary>
        public House_RemovePermanentGuest_C2S Data;
    
        public House_RemovePermanentGuest_C2S_EventArgs(House_RemovePermanentGuest_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_SetOpenHouseStatus_C2S Event Args
    /// </summary>
    public class House_SetOpenHouseStatus_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_SetOpenHouseStatus_C2S Message Data
        /// </summary>
        public House_SetOpenHouseStatus_C2S Data;
    
        public House_SetOpenHouseStatus_C2S_EventArgs(House_SetOpenHouseStatus_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_UpdateRestrictions_S2C Event Args
    /// </summary>
    public class House_UpdateRestrictions_S2C_EventArgs : EventArgs {
        /// <summary>
        /// House_UpdateRestrictions_S2C Message Data
        /// </summary>
        public House_UpdateRestrictions_S2C Data;
    
        public House_UpdateRestrictions_S2C_EventArgs(House_UpdateRestrictions_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_ChangeStoragePermission_C2S Event Args
    /// </summary>
    public class House_ChangeStoragePermission_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_ChangeStoragePermission_C2S Message Data
        /// </summary>
        public House_ChangeStoragePermission_C2S Data;
    
        public House_ChangeStoragePermission_C2S_EventArgs(House_ChangeStoragePermission_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_BootSpecificHouseGuest_C2S Event Args
    /// </summary>
    public class House_BootSpecificHouseGuest_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_BootSpecificHouseGuest_C2S Message Data
        /// </summary>
        public House_BootSpecificHouseGuest_C2S Data;
    
        public House_BootSpecificHouseGuest_C2S_EventArgs(House_BootSpecificHouseGuest_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_RemoveAllStoragePermission_C2S Event Args
    /// </summary>
    public class House_RemoveAllStoragePermission_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_RemoveAllStoragePermission_C2S Message Data
        /// </summary>
        public House_RemoveAllStoragePermission_C2S Data;
    
        public House_RemoveAllStoragePermission_C2S_EventArgs(House_RemoveAllStoragePermission_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_RequestFullGuestList_C2S Event Args
    /// </summary>
    public class House_RequestFullGuestList_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_RequestFullGuestList_C2S Message Data
        /// </summary>
        public House_RequestFullGuestList_C2S Data;
    
        public House_RequestFullGuestList_C2S_EventArgs(House_RequestFullGuestList_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_SetMotd_C2S Event Args
    /// </summary>
    public class Allegiance_SetMotd_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_SetMotd_C2S Message Data
        /// </summary>
        public Allegiance_SetMotd_C2S Data;
    
        public Allegiance_SetMotd_C2S_EventArgs(Allegiance_SetMotd_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_QueryMotd_C2S Event Args
    /// </summary>
    public class Allegiance_QueryMotd_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_QueryMotd_C2S Message Data
        /// </summary>
        public Allegiance_QueryMotd_C2S Data;
    
        public Allegiance_QueryMotd_C2S_EventArgs(Allegiance_QueryMotd_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_ClearMotd_C2S Event Args
    /// </summary>
    public class Allegiance_ClearMotd_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_ClearMotd_C2S Message Data
        /// </summary>
        public Allegiance_ClearMotd_C2S Data;
    
        public Allegiance_ClearMotd_C2S_EventArgs(Allegiance_ClearMotd_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_UpdateHAR_S2C Event Args
    /// </summary>
    public class House_UpdateHAR_S2C_EventArgs : EventArgs {
        /// <summary>
        /// House_UpdateHAR_S2C Message Data
        /// </summary>
        public House_UpdateHAR_S2C Data;
    
        public House_UpdateHAR_S2C_EventArgs(House_UpdateHAR_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_QueryLord_C2S Event Args
    /// </summary>
    public class House_QueryLord_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_QueryLord_C2S Message Data
        /// </summary>
        public House_QueryLord_C2S Data;
    
        public House_QueryLord_C2S_EventArgs(House_QueryLord_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_HouseTransaction_S2C Event Args
    /// </summary>
    public class House_HouseTransaction_S2C_EventArgs : EventArgs {
        /// <summary>
        /// House_HouseTransaction_S2C Message Data
        /// </summary>
        public House_HouseTransaction_S2C Data;
    
        public House_HouseTransaction_S2C_EventArgs(House_HouseTransaction_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_AddAllStoragePermission_C2S Event Args
    /// </summary>
    public class House_AddAllStoragePermission_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_AddAllStoragePermission_C2S Message Data
        /// </summary>
        public House_AddAllStoragePermission_C2S Data;
    
        public House_AddAllStoragePermission_C2S_EventArgs(House_AddAllStoragePermission_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_RemoveAllPermanentGuests_C2S Event Args
    /// </summary>
    public class House_RemoveAllPermanentGuests_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_RemoveAllPermanentGuests_C2S Message Data
        /// </summary>
        public House_RemoveAllPermanentGuests_C2S Data;
    
        public House_RemoveAllPermanentGuests_C2S_EventArgs(House_RemoveAllPermanentGuests_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_BootEveryone_C2S Event Args
    /// </summary>
    public class House_BootEveryone_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_BootEveryone_C2S Message Data
        /// </summary>
        public House_BootEveryone_C2S Data;
    
        public House_BootEveryone_C2S_EventArgs(House_BootEveryone_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_TeleToHouse_C2S Event Args
    /// </summary>
    public class House_TeleToHouse_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_TeleToHouse_C2S Message Data
        /// </summary>
        public House_TeleToHouse_C2S Data;
    
        public House_TeleToHouse_C2S_EventArgs(House_TeleToHouse_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_QueryItemMana_C2S Event Args
    /// </summary>
    public class Item_QueryItemMana_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Item_QueryItemMana_C2S Message Data
        /// </summary>
        public Item_QueryItemMana_C2S Data;
    
        public Item_QueryItemMana_C2S_EventArgs(Item_QueryItemMana_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_QueryItemManaResponse_S2C Event Args
    /// </summary>
    public class Item_QueryItemManaResponse_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_QueryItemManaResponse_S2C Message Data
        /// </summary>
        public Item_QueryItemManaResponse_S2C Data;
    
        public Item_QueryItemManaResponse_S2C_EventArgs(Item_QueryItemManaResponse_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_SetHooksVisibility_C2S Event Args
    /// </summary>
    public class House_SetHooksVisibility_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_SetHooksVisibility_C2S Message Data
        /// </summary>
        public House_SetHooksVisibility_C2S Data;
    
        public House_SetHooksVisibility_C2S_EventArgs(House_SetHooksVisibility_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_ModifyAllegianceGuestPermission_C2S Event Args
    /// </summary>
    public class House_ModifyAllegianceGuestPermission_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_ModifyAllegianceGuestPermission_C2S Message Data
        /// </summary>
        public House_ModifyAllegianceGuestPermission_C2S Data;
    
        public House_ModifyAllegianceGuestPermission_C2S_EventArgs(House_ModifyAllegianceGuestPermission_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_ModifyAllegianceStoragePermission_C2S Event Args
    /// </summary>
    public class House_ModifyAllegianceStoragePermission_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_ModifyAllegianceStoragePermission_C2S Message Data
        /// </summary>
        public House_ModifyAllegianceStoragePermission_C2S Data;
    
        public House_ModifyAllegianceStoragePermission_C2S_EventArgs(House_ModifyAllegianceStoragePermission_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Game_Join_C2S Event Args
    /// </summary>
    public class Game_Join_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Game_Join_C2S Message Data
        /// </summary>
        public Game_Join_C2S Data;
    
        public Game_Join_C2S_EventArgs(Game_Join_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Game_Quit_C2S Event Args
    /// </summary>
    public class Game_Quit_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Game_Quit_C2S Message Data
        /// </summary>
        public Game_Quit_C2S Data;
    
        public Game_Quit_C2S_EventArgs(Game_Quit_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Game_Move_C2S Event Args
    /// </summary>
    public class Game_Move_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Game_Move_C2S Message Data
        /// </summary>
        public Game_Move_C2S Data;
    
        public Game_Move_C2S_EventArgs(Game_Move_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Game_MovePass_C2S Event Args
    /// </summary>
    public class Game_MovePass_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Game_MovePass_C2S Message Data
        /// </summary>
        public Game_MovePass_C2S Data;
    
        public Game_MovePass_C2S_EventArgs(Game_MovePass_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Game_Stalemate_C2S Event Args
    /// </summary>
    public class Game_Stalemate_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Game_Stalemate_C2S Message Data
        /// </summary>
        public Game_Stalemate_C2S Data;
    
        public Game_Stalemate_C2S_EventArgs(Game_Stalemate_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_ListAvailableHouses_C2S Event Args
    /// </summary>
    public class House_ListAvailableHouses_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_ListAvailableHouses_C2S Message Data
        /// </summary>
        public House_ListAvailableHouses_C2S Data;
    
        public House_ListAvailableHouses_C2S_EventArgs(House_ListAvailableHouses_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_AvailableHouses_S2C Event Args
    /// </summary>
    public class House_AvailableHouses_S2C_EventArgs : EventArgs {
        /// <summary>
        /// House_AvailableHouses_S2C Message Data
        /// </summary>
        public House_AvailableHouses_S2C Data;
    
        public House_AvailableHouses_S2C_EventArgs(House_AvailableHouses_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_ConfirmationRequest_S2C Event Args
    /// </summary>
    public class Character_ConfirmationRequest_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Character_ConfirmationRequest_S2C Message Data
        /// </summary>
        public Character_ConfirmationRequest_S2C Data;
    
        public Character_ConfirmationRequest_S2C_EventArgs(Character_ConfirmationRequest_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_ConfirmationResponse_C2S Event Args
    /// </summary>
    public class Character_ConfirmationResponse_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_ConfirmationResponse_C2S Message Data
        /// </summary>
        public Character_ConfirmationResponse_C2S Data;
    
        public Character_ConfirmationResponse_C2S_EventArgs(Character_ConfirmationResponse_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_ConfirmationDone_S2C Event Args
    /// </summary>
    public class Character_ConfirmationDone_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Character_ConfirmationDone_S2C Message Data
        /// </summary>
        public Character_ConfirmationDone_S2C Data;
    
        public Character_ConfirmationDone_S2C_EventArgs(Character_ConfirmationDone_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_BreakAllegianceBoot_C2S Event Args
    /// </summary>
    public class Allegiance_BreakAllegianceBoot_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_BreakAllegianceBoot_C2S Message Data
        /// </summary>
        public Allegiance_BreakAllegianceBoot_C2S Data;
    
        public Allegiance_BreakAllegianceBoot_C2S_EventArgs(Allegiance_BreakAllegianceBoot_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// House_TeleToMansion_C2S Event Args
    /// </summary>
    public class House_TeleToMansion_C2S_EventArgs : EventArgs {
        /// <summary>
        /// House_TeleToMansion_C2S Message Data
        /// </summary>
        public House_TeleToMansion_C2S Data;
    
        public House_TeleToMansion_C2S_EventArgs(House_TeleToMansion_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_Suicide_C2S Event Args
    /// </summary>
    public class Character_Suicide_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_Suicide_C2S Message Data
        /// </summary>
        public Character_Suicide_C2S Data;
    
        public Character_Suicide_C2S_EventArgs(Character_Suicide_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_AllegianceLoginNotificationEvent_S2C Event Args
    /// </summary>
    public class Allegiance_AllegianceLoginNotificationEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_AllegianceLoginNotificationEvent_S2C Message Data
        /// </summary>
        public Allegiance_AllegianceLoginNotificationEvent_S2C Data;
    
        public Allegiance_AllegianceLoginNotificationEvent_S2C_EventArgs(Allegiance_AllegianceLoginNotificationEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_AllegianceInfoRequest_C2S Event Args
    /// </summary>
    public class Allegiance_AllegianceInfoRequest_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_AllegianceInfoRequest_C2S Message Data
        /// </summary>
        public Allegiance_AllegianceInfoRequest_C2S Data;
    
        public Allegiance_AllegianceInfoRequest_C2S_EventArgs(Allegiance_AllegianceInfoRequest_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_AllegianceInfoResponseEvent_S2C Event Args
    /// </summary>
    public class Allegiance_AllegianceInfoResponseEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_AllegianceInfoResponseEvent_S2C Message Data
        /// </summary>
        public Allegiance_AllegianceInfoResponseEvent_S2C Data;
    
        public Allegiance_AllegianceInfoResponseEvent_S2C_EventArgs(Allegiance_AllegianceInfoResponseEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_CreateTinkeringTool_C2S Event Args
    /// </summary>
    public class Inventory_CreateTinkeringTool_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_CreateTinkeringTool_C2S Message Data
        /// </summary>
        public Inventory_CreateTinkeringTool_C2S Data;
    
        public Inventory_CreateTinkeringTool_C2S_EventArgs(Inventory_CreateTinkeringTool_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Game_JoinGameResponse_S2C Event Args
    /// </summary>
    public class Game_JoinGameResponse_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Game_JoinGameResponse_S2C Message Data
        /// </summary>
        public Game_JoinGameResponse_S2C Data;
    
        public Game_JoinGameResponse_S2C_EventArgs(Game_JoinGameResponse_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Game_StartGame_S2C Event Args
    /// </summary>
    public class Game_StartGame_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Game_StartGame_S2C Message Data
        /// </summary>
        public Game_StartGame_S2C Data;
    
        public Game_StartGame_S2C_EventArgs(Game_StartGame_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Game_MoveResponse_S2C Event Args
    /// </summary>
    public class Game_MoveResponse_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Game_MoveResponse_S2C Message Data
        /// </summary>
        public Game_MoveResponse_S2C Data;
    
        public Game_MoveResponse_S2C_EventArgs(Game_MoveResponse_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Game_OpponentTurn_S2C Event Args
    /// </summary>
    public class Game_OpponentTurn_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Game_OpponentTurn_S2C Message Data
        /// </summary>
        public Game_OpponentTurn_S2C Data;
    
        public Game_OpponentTurn_S2C_EventArgs(Game_OpponentTurn_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Game_OpponentStalemateState_S2C Event Args
    /// </summary>
    public class Game_OpponentStalemateState_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Game_OpponentStalemateState_S2C Message Data
        /// </summary>
        public Game_OpponentStalemateState_S2C Data;
    
        public Game_OpponentStalemateState_S2C_EventArgs(Game_OpponentStalemateState_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_SpellbookFilterEvent_C2S Event Args
    /// </summary>
    public class Character_SpellbookFilterEvent_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_SpellbookFilterEvent_C2S Message Data
        /// </summary>
        public Character_SpellbookFilterEvent_C2S Data;
    
        public Character_SpellbookFilterEvent_C2S_EventArgs(Character_SpellbookFilterEvent_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_WeenieError_S2C Event Args
    /// </summary>
    public class Communication_WeenieError_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_WeenieError_S2C Message Data
        /// </summary>
        public Communication_WeenieError_S2C Data;
    
        public Communication_WeenieError_S2C_EventArgs(Communication_WeenieError_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_WeenieErrorWithString_S2C Event Args
    /// </summary>
    public class Communication_WeenieErrorWithString_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_WeenieErrorWithString_S2C Message Data
        /// </summary>
        public Communication_WeenieErrorWithString_S2C Data;
    
        public Communication_WeenieErrorWithString_S2C_EventArgs(Communication_WeenieErrorWithString_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Game_GameOver_S2C Event Args
    /// </summary>
    public class Game_GameOver_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Game_GameOver_S2C Message Data
        /// </summary>
        public Game_GameOver_S2C Data;
    
        public Game_GameOver_S2C_EventArgs(Game_GameOver_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_TeleToMarketplace_C2S Event Args
    /// </summary>
    public class Character_TeleToMarketplace_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_TeleToMarketplace_C2S Message Data
        /// </summary>
        public Character_TeleToMarketplace_C2S Data;
    
        public Character_TeleToMarketplace_C2S_EventArgs(Character_TeleToMarketplace_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_EnterPKLite_C2S Event Args
    /// </summary>
    public class Character_EnterPKLite_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_EnterPKLite_C2S Message Data
        /// </summary>
        public Character_EnterPKLite_C2S Data;
    
        public Character_EnterPKLite_C2S_EventArgs(Character_EnterPKLite_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_AssignNewLeader_C2S Event Args
    /// </summary>
    public class Fellowship_AssignNewLeader_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_AssignNewLeader_C2S Message Data
        /// </summary>
        public Fellowship_AssignNewLeader_C2S Data;
    
        public Fellowship_AssignNewLeader_C2S_EventArgs(Fellowship_AssignNewLeader_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_ChangeFellowOpeness_C2S Event Args
    /// </summary>
    public class Fellowship_ChangeFellowOpeness_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_ChangeFellowOpeness_C2S Message Data
        /// </summary>
        public Fellowship_ChangeFellowOpeness_C2S Data;
    
        public Fellowship_ChangeFellowOpeness_C2S_EventArgs(Fellowship_ChangeFellowOpeness_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_ChatRoomTracker_S2C Event Args
    /// </summary>
    public class Communication_ChatRoomTracker_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_ChatRoomTracker_S2C Message Data
        /// </summary>
        public Communication_ChatRoomTracker_S2C Data;
    
        public Communication_ChatRoomTracker_S2C_EventArgs(Communication_ChatRoomTracker_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_AllegianceChatBoot_C2S Event Args
    /// </summary>
    public class Allegiance_AllegianceChatBoot_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_AllegianceChatBoot_C2S Message Data
        /// </summary>
        public Allegiance_AllegianceChatBoot_C2S Data;
    
        public Allegiance_AllegianceChatBoot_C2S_EventArgs(Allegiance_AllegianceChatBoot_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_AddAllegianceBan_C2S Event Args
    /// </summary>
    public class Allegiance_AddAllegianceBan_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_AddAllegianceBan_C2S Message Data
        /// </summary>
        public Allegiance_AddAllegianceBan_C2S Data;
    
        public Allegiance_AddAllegianceBan_C2S_EventArgs(Allegiance_AddAllegianceBan_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_RemoveAllegianceBan_C2S Event Args
    /// </summary>
    public class Allegiance_RemoveAllegianceBan_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_RemoveAllegianceBan_C2S Message Data
        /// </summary>
        public Allegiance_RemoveAllegianceBan_C2S Data;
    
        public Allegiance_RemoveAllegianceBan_C2S_EventArgs(Allegiance_RemoveAllegianceBan_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_ListAllegianceBans_C2S Event Args
    /// </summary>
    public class Allegiance_ListAllegianceBans_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_ListAllegianceBans_C2S Message Data
        /// </summary>
        public Allegiance_ListAllegianceBans_C2S Data;
    
        public Allegiance_ListAllegianceBans_C2S_EventArgs(Allegiance_ListAllegianceBans_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_RemoveAllegianceOfficer_C2S Event Args
    /// </summary>
    public class Allegiance_RemoveAllegianceOfficer_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_RemoveAllegianceOfficer_C2S Message Data
        /// </summary>
        public Allegiance_RemoveAllegianceOfficer_C2S Data;
    
        public Allegiance_RemoveAllegianceOfficer_C2S_EventArgs(Allegiance_RemoveAllegianceOfficer_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_ListAllegianceOfficers_C2S Event Args
    /// </summary>
    public class Allegiance_ListAllegianceOfficers_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_ListAllegianceOfficers_C2S Message Data
        /// </summary>
        public Allegiance_ListAllegianceOfficers_C2S Data;
    
        public Allegiance_ListAllegianceOfficers_C2S_EventArgs(Allegiance_ListAllegianceOfficers_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_ClearAllegianceOfficers_C2S Event Args
    /// </summary>
    public class Allegiance_ClearAllegianceOfficers_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_ClearAllegianceOfficers_C2S Message Data
        /// </summary>
        public Allegiance_ClearAllegianceOfficers_C2S Data;
    
        public Allegiance_ClearAllegianceOfficers_C2S_EventArgs(Allegiance_ClearAllegianceOfficers_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Allegiance_RecallAllegianceHometown_C2S Event Args
    /// </summary>
    public class Allegiance_RecallAllegianceHometown_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Allegiance_RecallAllegianceHometown_C2S Message Data
        /// </summary>
        public Allegiance_RecallAllegianceHometown_C2S Data;
    
        public Allegiance_RecallAllegianceHometown_C2S_EventArgs(Allegiance_RecallAllegianceHometown_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Admin_QueryPluginList_S2C Event Args
    /// </summary>
    public class Admin_QueryPluginList_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Admin_QueryPluginList_S2C Message Data
        /// </summary>
        public Admin_QueryPluginList_S2C Data;
    
        public Admin_QueryPluginList_S2C_EventArgs(Admin_QueryPluginList_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Admin_QueryPluginListResponse_C2S Event Args
    /// </summary>
    public class Admin_QueryPluginListResponse_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Admin_QueryPluginListResponse_C2S Message Data
        /// </summary>
        public Admin_QueryPluginListResponse_C2S Data;
    
        public Admin_QueryPluginListResponse_C2S_EventArgs(Admin_QueryPluginListResponse_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Admin_QueryPlugin_S2C Event Args
    /// </summary>
    public class Admin_QueryPlugin_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Admin_QueryPlugin_S2C Message Data
        /// </summary>
        public Admin_QueryPlugin_S2C Data;
    
        public Admin_QueryPlugin_S2C_EventArgs(Admin_QueryPlugin_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Admin_QueryPluginResponse_C2S Event Args
    /// </summary>
    public class Admin_QueryPluginResponse_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Admin_QueryPluginResponse_C2S Message Data
        /// </summary>
        public Admin_QueryPluginResponse_C2S Data;
    
        public Admin_QueryPluginResponse_C2S_EventArgs(Admin_QueryPluginResponse_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Admin_QueryPluginResponse2_S2C Event Args
    /// </summary>
    public class Admin_QueryPluginResponse2_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Admin_QueryPluginResponse2_S2C Message Data
        /// </summary>
        public Admin_QueryPluginResponse2_S2C Data;
    
        public Admin_QueryPluginResponse2_S2C_EventArgs(Admin_QueryPluginResponse2_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_SalvageOperationsResultData_S2C Event Args
    /// </summary>
    public class Inventory_SalvageOperationsResultData_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_SalvageOperationsResultData_S2C Message Data
        /// </summary>
        public Inventory_SalvageOperationsResultData_S2C Data;
    
        public Inventory_SalvageOperationsResultData_S2C_EventArgs(Inventory_SalvageOperationsResultData_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateRemoveInt64Event_S2C Event Args
    /// </summary>
    public class Qualities_PrivateRemoveInt64Event_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateRemoveInt64Event_S2C Message Data
        /// </summary>
        public Qualities_PrivateRemoveInt64Event_S2C Data;
    
        public Qualities_PrivateRemoveInt64Event_S2C_EventArgs(Qualities_PrivateRemoveInt64Event_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_RemoveInt64Event_S2C Event Args
    /// </summary>
    public class Qualities_RemoveInt64Event_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_RemoveInt64Event_S2C Message Data
        /// </summary>
        public Qualities_RemoveInt64Event_S2C Data;
    
        public Qualities_RemoveInt64Event_S2C_EventArgs(Qualities_RemoveInt64Event_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_HearSpeech_S2C Event Args
    /// </summary>
    public class Communication_HearSpeech_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_HearSpeech_S2C Message Data
        /// </summary>
        public Communication_HearSpeech_S2C Data;
    
        public Communication_HearSpeech_S2C_EventArgs(Communication_HearSpeech_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_HearRangedSpeech_S2C Event Args
    /// </summary>
    public class Communication_HearRangedSpeech_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_HearRangedSpeech_S2C Message Data
        /// </summary>
        public Communication_HearRangedSpeech_S2C Data;
    
        public Communication_HearRangedSpeech_S2C_EventArgs(Communication_HearRangedSpeech_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_HearDirectSpeech_S2C Event Args
    /// </summary>
    public class Communication_HearDirectSpeech_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_HearDirectSpeech_S2C Message Data
        /// </summary>
        public Communication_HearDirectSpeech_S2C Data;
    
        public Communication_HearDirectSpeech_S2C_EventArgs(Communication_HearDirectSpeech_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_FullUpdate_S2C Event Args
    /// </summary>
    public class Fellowship_FullUpdate_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_FullUpdate_S2C Message Data
        /// </summary>
        public Fellowship_FullUpdate_S2C Data;
    
        public Fellowship_FullUpdate_S2C_EventArgs(Fellowship_FullUpdate_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_Disband_S2C Event Args
    /// </summary>
    public class Fellowship_Disband_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_Disband_S2C Message Data
        /// </summary>
        public Fellowship_Disband_S2C Data;
    
        public Fellowship_Disband_S2C_EventArgs(Fellowship_Disband_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Fellowship_UpdateFellow_S2C Event Args
    /// </summary>
    public class Fellowship_UpdateFellow_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Fellowship_UpdateFellow_S2C Message Data
        /// </summary>
        public Fellowship_UpdateFellow_S2C Data;
    
        public Fellowship_UpdateFellow_S2C_EventArgs(Fellowship_UpdateFellow_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Magic_UpdateSpell_S2C Event Args
    /// </summary>
    public class Magic_UpdateSpell_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Magic_UpdateSpell_S2C Message Data
        /// </summary>
        public Magic_UpdateSpell_S2C Data;
    
        public Magic_UpdateSpell_S2C_EventArgs(Magic_UpdateSpell_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Magic_UpdateEnchantment_S2C Event Args
    /// </summary>
    public class Magic_UpdateEnchantment_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Magic_UpdateEnchantment_S2C Message Data
        /// </summary>
        public Magic_UpdateEnchantment_S2C Data;
    
        public Magic_UpdateEnchantment_S2C_EventArgs(Magic_UpdateEnchantment_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Magic_RemoveEnchantment_S2C Event Args
    /// </summary>
    public class Magic_RemoveEnchantment_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Magic_RemoveEnchantment_S2C Message Data
        /// </summary>
        public Magic_RemoveEnchantment_S2C Data;
    
        public Magic_RemoveEnchantment_S2C_EventArgs(Magic_RemoveEnchantment_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Magic_UpdateMultipleEnchantments_S2C Event Args
    /// </summary>
    public class Magic_UpdateMultipleEnchantments_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Magic_UpdateMultipleEnchantments_S2C Message Data
        /// </summary>
        public Magic_UpdateMultipleEnchantments_S2C Data;
    
        public Magic_UpdateMultipleEnchantments_S2C_EventArgs(Magic_UpdateMultipleEnchantments_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Magic_RemoveMultipleEnchantments_S2C Event Args
    /// </summary>
    public class Magic_RemoveMultipleEnchantments_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Magic_RemoveMultipleEnchantments_S2C Message Data
        /// </summary>
        public Magic_RemoveMultipleEnchantments_S2C Data;
    
        public Magic_RemoveMultipleEnchantments_S2C_EventArgs(Magic_RemoveMultipleEnchantments_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Magic_PurgeEnchantments_S2C Event Args
    /// </summary>
    public class Magic_PurgeEnchantments_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Magic_PurgeEnchantments_S2C Message Data
        /// </summary>
        public Magic_PurgeEnchantments_S2C Data;
    
        public Magic_PurgeEnchantments_S2C_EventArgs(Magic_PurgeEnchantments_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Magic_DispelEnchantment_S2C Event Args
    /// </summary>
    public class Magic_DispelEnchantment_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Magic_DispelEnchantment_S2C Message Data
        /// </summary>
        public Magic_DispelEnchantment_S2C Data;
    
        public Magic_DispelEnchantment_S2C_EventArgs(Magic_DispelEnchantment_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Magic_DispelMultipleEnchantments_S2C Event Args
    /// </summary>
    public class Magic_DispelMultipleEnchantments_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Magic_DispelMultipleEnchantments_S2C Message Data
        /// </summary>
        public Magic_DispelMultipleEnchantments_S2C Data;
    
        public Magic_DispelMultipleEnchantments_S2C_EventArgs(Magic_DispelMultipleEnchantments_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Misc_PortalStormBrewing_S2C Event Args
    /// </summary>
    public class Misc_PortalStormBrewing_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Misc_PortalStormBrewing_S2C Message Data
        /// </summary>
        public Misc_PortalStormBrewing_S2C Data;
    
        public Misc_PortalStormBrewing_S2C_EventArgs(Misc_PortalStormBrewing_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Misc_PortalStormImminent_S2C Event Args
    /// </summary>
    public class Misc_PortalStormImminent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Misc_PortalStormImminent_S2C Message Data
        /// </summary>
        public Misc_PortalStormImminent_S2C Data;
    
        public Misc_PortalStormImminent_S2C_EventArgs(Misc_PortalStormImminent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Misc_PortalStorm_S2C Event Args
    /// </summary>
    public class Misc_PortalStorm_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Misc_PortalStorm_S2C Message Data
        /// </summary>
        public Misc_PortalStorm_S2C Data;
    
        public Misc_PortalStorm_S2C_EventArgs(Misc_PortalStorm_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Misc_PortalStormSubsided_S2C Event Args
    /// </summary>
    public class Misc_PortalStormSubsided_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Misc_PortalStormSubsided_S2C Message Data
        /// </summary>
        public Misc_PortalStormSubsided_S2C Data;
    
        public Misc_PortalStormSubsided_S2C_EventArgs(Misc_PortalStormSubsided_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateInt_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateInt_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateInt_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateInt_S2C Data;
    
        public Qualities_PrivateUpdateInt_S2C_EventArgs(Qualities_PrivateUpdateInt_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateInt_S2C Event Args
    /// </summary>
    public class Qualities_UpdateInt_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateInt_S2C Message Data
        /// </summary>
        public Qualities_UpdateInt_S2C Data;
    
        public Qualities_UpdateInt_S2C_EventArgs(Qualities_UpdateInt_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateInt64_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateInt64_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateInt64_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateInt64_S2C Data;
    
        public Qualities_PrivateUpdateInt64_S2C_EventArgs(Qualities_PrivateUpdateInt64_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateInt64_S2C Event Args
    /// </summary>
    public class Qualities_UpdateInt64_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateInt64_S2C Message Data
        /// </summary>
        public Qualities_UpdateInt64_S2C Data;
    
        public Qualities_UpdateInt64_S2C_EventArgs(Qualities_UpdateInt64_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateBool_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateBool_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateBool_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateBool_S2C Data;
    
        public Qualities_PrivateUpdateBool_S2C_EventArgs(Qualities_PrivateUpdateBool_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateBool_S2C Event Args
    /// </summary>
    public class Qualities_UpdateBool_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateBool_S2C Message Data
        /// </summary>
        public Qualities_UpdateBool_S2C Data;
    
        public Qualities_UpdateBool_S2C_EventArgs(Qualities_UpdateBool_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateFloat_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateFloat_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateFloat_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateFloat_S2C Data;
    
        public Qualities_PrivateUpdateFloat_S2C_EventArgs(Qualities_PrivateUpdateFloat_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateFloat_S2C Event Args
    /// </summary>
    public class Qualities_UpdateFloat_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateFloat_S2C Message Data
        /// </summary>
        public Qualities_UpdateFloat_S2C Data;
    
        public Qualities_UpdateFloat_S2C_EventArgs(Qualities_UpdateFloat_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateString_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateString_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateString_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateString_S2C Data;
    
        public Qualities_PrivateUpdateString_S2C_EventArgs(Qualities_PrivateUpdateString_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateString_S2C Event Args
    /// </summary>
    public class Qualities_UpdateString_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateString_S2C Message Data
        /// </summary>
        public Qualities_UpdateString_S2C Data;
    
        public Qualities_UpdateString_S2C_EventArgs(Qualities_UpdateString_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateDataID_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateDataID_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateDataID_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateDataID_S2C Data;
    
        public Qualities_PrivateUpdateDataID_S2C_EventArgs(Qualities_PrivateUpdateDataID_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateDataID_S2C Event Args
    /// </summary>
    public class Qualities_UpdateDataID_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateDataID_S2C Message Data
        /// </summary>
        public Qualities_UpdateDataID_S2C Data;
    
        public Qualities_UpdateDataID_S2C_EventArgs(Qualities_UpdateDataID_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateInstanceID_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateInstanceID_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateInstanceID_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateInstanceID_S2C Data;
    
        public Qualities_PrivateUpdateInstanceID_S2C_EventArgs(Qualities_PrivateUpdateInstanceID_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateInstanceID_S2C Event Args
    /// </summary>
    public class Qualities_UpdateInstanceID_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateInstanceID_S2C Message Data
        /// </summary>
        public Qualities_UpdateInstanceID_S2C Data;
    
        public Qualities_UpdateInstanceID_S2C_EventArgs(Qualities_UpdateInstanceID_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdatePosition_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdatePosition_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdatePosition_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdatePosition_S2C Data;
    
        public Qualities_PrivateUpdatePosition_S2C_EventArgs(Qualities_PrivateUpdatePosition_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdatePosition_S2C Event Args
    /// </summary>
    public class Qualities_UpdatePosition_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdatePosition_S2C Message Data
        /// </summary>
        public Qualities_UpdatePosition_S2C Data;
    
        public Qualities_UpdatePosition_S2C_EventArgs(Qualities_UpdatePosition_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateSkill_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateSkill_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateSkill_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateSkill_S2C Data;
    
        public Qualities_PrivateUpdateSkill_S2C_EventArgs(Qualities_PrivateUpdateSkill_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateSkill_S2C Event Args
    /// </summary>
    public class Qualities_UpdateSkill_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateSkill_S2C Message Data
        /// </summary>
        public Qualities_UpdateSkill_S2C Data;
    
        public Qualities_UpdateSkill_S2C_EventArgs(Qualities_UpdateSkill_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateSkillLevel_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateSkillLevel_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateSkillLevel_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateSkillLevel_S2C Data;
    
        public Qualities_PrivateUpdateSkillLevel_S2C_EventArgs(Qualities_PrivateUpdateSkillLevel_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateSkillLevel_S2C Event Args
    /// </summary>
    public class Qualities_UpdateSkillLevel_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateSkillLevel_S2C Message Data
        /// </summary>
        public Qualities_UpdateSkillLevel_S2C Data;
    
        public Qualities_UpdateSkillLevel_S2C_EventArgs(Qualities_UpdateSkillLevel_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateSkillAC_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateSkillAC_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateSkillAC_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateSkillAC_S2C Data;
    
        public Qualities_PrivateUpdateSkillAC_S2C_EventArgs(Qualities_PrivateUpdateSkillAC_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateSkillAC_S2C Event Args
    /// </summary>
    public class Qualities_UpdateSkillAC_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateSkillAC_S2C Message Data
        /// </summary>
        public Qualities_UpdateSkillAC_S2C Data;
    
        public Qualities_UpdateSkillAC_S2C_EventArgs(Qualities_UpdateSkillAC_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateAttribute_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateAttribute_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateAttribute_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateAttribute_S2C Data;
    
        public Qualities_PrivateUpdateAttribute_S2C_EventArgs(Qualities_PrivateUpdateAttribute_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateAttribute_S2C Event Args
    /// </summary>
    public class Qualities_UpdateAttribute_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateAttribute_S2C Message Data
        /// </summary>
        public Qualities_UpdateAttribute_S2C Data;
    
        public Qualities_UpdateAttribute_S2C_EventArgs(Qualities_UpdateAttribute_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateAttributeLevel_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateAttributeLevel_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateAttributeLevel_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateAttributeLevel_S2C Data;
    
        public Qualities_PrivateUpdateAttributeLevel_S2C_EventArgs(Qualities_PrivateUpdateAttributeLevel_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateAttributeLevel_S2C Event Args
    /// </summary>
    public class Qualities_UpdateAttributeLevel_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateAttributeLevel_S2C Message Data
        /// </summary>
        public Qualities_UpdateAttributeLevel_S2C Data;
    
        public Qualities_UpdateAttributeLevel_S2C_EventArgs(Qualities_UpdateAttributeLevel_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateAttribute2nd_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateAttribute2nd_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateAttribute2nd_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateAttribute2nd_S2C Data;
    
        public Qualities_PrivateUpdateAttribute2nd_S2C_EventArgs(Qualities_PrivateUpdateAttribute2nd_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateAttribute2nd_S2C Event Args
    /// </summary>
    public class Qualities_UpdateAttribute2nd_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateAttribute2nd_S2C Message Data
        /// </summary>
        public Qualities_UpdateAttribute2nd_S2C Data;
    
        public Qualities_UpdateAttribute2nd_S2C_EventArgs(Qualities_UpdateAttribute2nd_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_PrivateUpdateAttribute2ndLevel_S2C Event Args
    /// </summary>
    public class Qualities_PrivateUpdateAttribute2ndLevel_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_PrivateUpdateAttribute2ndLevel_S2C Message Data
        /// </summary>
        public Qualities_PrivateUpdateAttribute2ndLevel_S2C Data;
    
        public Qualities_PrivateUpdateAttribute2ndLevel_S2C_EventArgs(Qualities_PrivateUpdateAttribute2ndLevel_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Qualities_UpdateAttribute2ndLevel_S2C Event Args
    /// </summary>
    public class Qualities_UpdateAttribute2ndLevel_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Qualities_UpdateAttribute2ndLevel_S2C Message Data
        /// </summary>
        public Qualities_UpdateAttribute2ndLevel_S2C Data;
    
        public Qualities_UpdateAttribute2ndLevel_S2C_EventArgs(Qualities_UpdateAttribute2ndLevel_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_TransientString_S2C Event Args
    /// </summary>
    public class Communication_TransientString_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_TransientString_S2C Message Data
        /// </summary>
        public Communication_TransientString_S2C Data;
    
        public Communication_TransientString_S2C_EventArgs(Communication_TransientString_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_FinishBarber_C2S Event Args
    /// </summary>
    public class Character_FinishBarber_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_FinishBarber_C2S Message Data
        /// </summary>
        public Character_FinishBarber_C2S Data;
    
        public Character_FinishBarber_C2S_EventArgs(Character_FinishBarber_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Magic_PurgeBadEnchantments_S2C Event Args
    /// </summary>
    public class Magic_PurgeBadEnchantments_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Magic_PurgeBadEnchantments_S2C Message Data
        /// </summary>
        public Magic_PurgeBadEnchantments_S2C Data;
    
        public Magic_PurgeBadEnchantments_S2C_EventArgs(Magic_PurgeBadEnchantments_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Social_SendClientContractTrackerTable_S2C Event Args
    /// </summary>
    public class Social_SendClientContractTrackerTable_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Social_SendClientContractTrackerTable_S2C Message Data
        /// </summary>
        public Social_SendClientContractTrackerTable_S2C Data;
    
        public Social_SendClientContractTrackerTable_S2C_EventArgs(Social_SendClientContractTrackerTable_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Social_SendClientContractTracker_S2C Event Args
    /// </summary>
    public class Social_SendClientContractTracker_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Social_SendClientContractTracker_S2C Message Data
        /// </summary>
        public Social_SendClientContractTracker_S2C Data;
    
        public Social_SendClientContractTracker_S2C_EventArgs(Social_SendClientContractTracker_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Social_AbandonContract_C2S Event Args
    /// </summary>
    public class Social_AbandonContract_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Social_AbandonContract_C2S Message Data
        /// </summary>
        public Social_AbandonContract_C2S Data;
    
        public Social_AbandonContract_C2S_EventArgs(Social_AbandonContract_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Admin_Environs_S2C Event Args
    /// </summary>
    public class Admin_Environs_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Admin_Environs_S2C Message Data
        /// </summary>
        public Admin_Environs_S2C Data;
    
        public Admin_Environs_S2C_EventArgs(Admin_Environs_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Movement_PositionAndMovementEvent_S2C Event Args
    /// </summary>
    public class Movement_PositionAndMovementEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Movement_PositionAndMovementEvent_S2C Message Data
        /// </summary>
        public Movement_PositionAndMovementEvent_S2C Data;
    
        public Movement_PositionAndMovementEvent_S2C_EventArgs(Movement_PositionAndMovementEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Movement_Jump_C2S Event Args
    /// </summary>
    public class Movement_Jump_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Movement_Jump_C2S Message Data
        /// </summary>
        public Movement_Jump_C2S Data;
    
        public Movement_Jump_C2S_EventArgs(Movement_Jump_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Movement_MoveToState_C2S Event Args
    /// </summary>
    public class Movement_MoveToState_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Movement_MoveToState_C2S Message Data
        /// </summary>
        public Movement_MoveToState_C2S Data;
    
        public Movement_MoveToState_C2S_EventArgs(Movement_MoveToState_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Movement_DoMovementCommand_C2S Event Args
    /// </summary>
    public class Movement_DoMovementCommand_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Movement_DoMovementCommand_C2S Message Data
        /// </summary>
        public Movement_DoMovementCommand_C2S Data;
    
        public Movement_DoMovementCommand_C2S_EventArgs(Movement_DoMovementCommand_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_ObjDescEvent_S2C Event Args
    /// </summary>
    public class Item_ObjDescEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_ObjDescEvent_S2C Message Data
        /// </summary>
        public Item_ObjDescEvent_S2C Data;
    
        public Item_ObjDescEvent_S2C_EventArgs(Item_ObjDescEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_SetPlayerVisualDesc_S2C Event Args
    /// </summary>
    public class Character_SetPlayerVisualDesc_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Character_SetPlayerVisualDesc_S2C Message Data
        /// </summary>
        public Character_SetPlayerVisualDesc_S2C Data;
    
        public Character_SetPlayerVisualDesc_S2C_EventArgs(Character_SetPlayerVisualDesc_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_CharGenVerificationResponse_S2C Event Args
    /// </summary>
    public class Character_CharGenVerificationResponse_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Character_CharGenVerificationResponse_S2C Message Data
        /// </summary>
        public Character_CharGenVerificationResponse_S2C Data;
    
        public Character_CharGenVerificationResponse_S2C_EventArgs(Character_CharGenVerificationResponse_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Login_AwaitingSubscriptionExpiration_S2C Event Args
    /// </summary>
    public class Login_AwaitingSubscriptionExpiration_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Login_AwaitingSubscriptionExpiration_S2C Message Data
        /// </summary>
        public Login_AwaitingSubscriptionExpiration_S2C Data;
    
        public Login_AwaitingSubscriptionExpiration_S2C_EventArgs(Login_AwaitingSubscriptionExpiration_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Login_LogOffCharacter_C2S Event Args
    /// </summary>
    public class Login_LogOffCharacter_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Login_LogOffCharacter_C2S Message Data
        /// </summary>
        public Login_LogOffCharacter_C2S Data;
    
        public Login_LogOffCharacter_C2S_EventArgs(Login_LogOffCharacter_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Login_LogOffCharacter_S2C Event Args
    /// </summary>
    public class Login_LogOffCharacter_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Login_LogOffCharacter_S2C Message Data
        /// </summary>
        public Login_LogOffCharacter_S2C Data;
    
        public Login_LogOffCharacter_S2C_EventArgs(Login_LogOffCharacter_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_CharacterDelete_C2S Event Args
    /// </summary>
    public class Character_CharacterDelete_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_CharacterDelete_C2S Message Data
        /// </summary>
        public Character_CharacterDelete_C2S Data;
    
        public Character_CharacterDelete_C2S_EventArgs(Character_CharacterDelete_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_CharacterDelete_S2C Event Args
    /// </summary>
    public class Character_CharacterDelete_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Character_CharacterDelete_S2C Message Data
        /// </summary>
        public Character_CharacterDelete_S2C Data;
    
        public Character_CharacterDelete_S2C_EventArgs(Character_CharacterDelete_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_SendCharGenResult_C2S Event Args
    /// </summary>
    public class Character_SendCharGenResult_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Character_SendCharGenResult_C2S Message Data
        /// </summary>
        public Character_SendCharGenResult_C2S Data;
    
        public Character_SendCharGenResult_C2S_EventArgs(Character_SendCharGenResult_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Login_SendEnterWorld_C2S Event Args
    /// </summary>
    public class Login_SendEnterWorld_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Login_SendEnterWorld_C2S Message Data
        /// </summary>
        public Login_SendEnterWorld_C2S Data;
    
        public Login_SendEnterWorld_C2S_EventArgs(Login_SendEnterWorld_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Login_LoginCharacterSet_S2C Event Args
    /// </summary>
    public class Login_LoginCharacterSet_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Login_LoginCharacterSet_S2C Message Data
        /// </summary>
        public Login_LoginCharacterSet_S2C Data;
    
        public Login_LoginCharacterSet_S2C_EventArgs(Login_LoginCharacterSet_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Character_CharacterError_S2C Event Args
    /// </summary>
    public class Character_CharacterError_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Character_CharacterError_S2C Message Data
        /// </summary>
        public Character_CharacterError_S2C Data;
    
        public Character_CharacterError_S2C_EventArgs(Character_CharacterError_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Movement_StopMovementCommand_C2S Event Args
    /// </summary>
    public class Movement_StopMovementCommand_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Movement_StopMovementCommand_C2S Message Data
        /// </summary>
        public Movement_StopMovementCommand_C2S Data;
    
        public Movement_StopMovementCommand_C2S_EventArgs(Movement_StopMovementCommand_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Object_SendForceObjdesc_C2S Event Args
    /// </summary>
    public class Object_SendForceObjdesc_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Object_SendForceObjdesc_C2S Message Data
        /// </summary>
        public Object_SendForceObjdesc_C2S Data;
    
        public Object_SendForceObjdesc_C2S_EventArgs(Object_SendForceObjdesc_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_CreateObject_S2C Event Args
    /// </summary>
    public class Item_CreateObject_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_CreateObject_S2C Message Data
        /// </summary>
        public Item_CreateObject_S2C Data;
    
        public Item_CreateObject_S2C_EventArgs(Item_CreateObject_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Login_CreatePlayer_S2C Event Args
    /// </summary>
    public class Login_CreatePlayer_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Login_CreatePlayer_S2C Message Data
        /// </summary>
        public Login_CreatePlayer_S2C Data;
    
        public Login_CreatePlayer_S2C_EventArgs(Login_CreatePlayer_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_DeleteObject_S2C Event Args
    /// </summary>
    public class Item_DeleteObject_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_DeleteObject_S2C Message Data
        /// </summary>
        public Item_DeleteObject_S2C Data;
    
        public Item_DeleteObject_S2C_EventArgs(Item_DeleteObject_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Movement_PositionEvent_S2C Event Args
    /// </summary>
    public class Movement_PositionEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Movement_PositionEvent_S2C Message Data
        /// </summary>
        public Movement_PositionEvent_S2C Data;
    
        public Movement_PositionEvent_S2C_EventArgs(Movement_PositionEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_ParentEvent_S2C Event Args
    /// </summary>
    public class Item_ParentEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_ParentEvent_S2C Message Data
        /// </summary>
        public Item_ParentEvent_S2C Data;
    
        public Item_ParentEvent_S2C_EventArgs(Item_ParentEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Inventory_PickupEvent_S2C Event Args
    /// </summary>
    public class Inventory_PickupEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Inventory_PickupEvent_S2C Message Data
        /// </summary>
        public Inventory_PickupEvent_S2C Data;
    
        public Inventory_PickupEvent_S2C_EventArgs(Inventory_PickupEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_SetState_S2C Event Args
    /// </summary>
    public class Item_SetState_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_SetState_S2C Message Data
        /// </summary>
        public Item_SetState_S2C Data;
    
        public Item_SetState_S2C_EventArgs(Item_SetState_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Movement_SetObjectMovement_S2C Event Args
    /// </summary>
    public class Movement_SetObjectMovement_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Movement_SetObjectMovement_S2C Message Data
        /// </summary>
        public Movement_SetObjectMovement_S2C Data;
    
        public Movement_SetObjectMovement_S2C_EventArgs(Movement_SetObjectMovement_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Movement_VectorUpdate_S2C Event Args
    /// </summary>
    public class Movement_VectorUpdate_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Movement_VectorUpdate_S2C Message Data
        /// </summary>
        public Movement_VectorUpdate_S2C Data;
    
        public Movement_VectorUpdate_S2C_EventArgs(Movement_VectorUpdate_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Effects_SoundEvent_S2C Event Args
    /// </summary>
    public class Effects_SoundEvent_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Effects_SoundEvent_S2C Message Data
        /// </summary>
        public Effects_SoundEvent_S2C Data;
    
        public Effects_SoundEvent_S2C_EventArgs(Effects_SoundEvent_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Effects_PlayerTeleport_S2C Event Args
    /// </summary>
    public class Effects_PlayerTeleport_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Effects_PlayerTeleport_S2C Message Data
        /// </summary>
        public Effects_PlayerTeleport_S2C Data;
    
        public Effects_PlayerTeleport_S2C_EventArgs(Effects_PlayerTeleport_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Movement_AutonomyLevel_C2S Event Args
    /// </summary>
    public class Movement_AutonomyLevel_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Movement_AutonomyLevel_C2S Message Data
        /// </summary>
        public Movement_AutonomyLevel_C2S Data;
    
        public Movement_AutonomyLevel_C2S_EventArgs(Movement_AutonomyLevel_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Movement_AutonomousPosition_C2S Event Args
    /// </summary>
    public class Movement_AutonomousPosition_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Movement_AutonomousPosition_C2S Message Data
        /// </summary>
        public Movement_AutonomousPosition_C2S Data;
    
        public Movement_AutonomousPosition_C2S_EventArgs(Movement_AutonomousPosition_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Effects_PlayScriptID_S2C Event Args
    /// </summary>
    public class Effects_PlayScriptID_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Effects_PlayScriptID_S2C Message Data
        /// </summary>
        public Effects_PlayScriptID_S2C Data;
    
        public Effects_PlayScriptID_S2C_EventArgs(Effects_PlayScriptID_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Effects_PlayScriptType_S2C Event Args
    /// </summary>
    public class Effects_PlayScriptType_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Effects_PlayScriptType_S2C Message Data
        /// </summary>
        public Effects_PlayScriptType_S2C Data;
    
        public Effects_PlayScriptType_S2C_EventArgs(Effects_PlayScriptType_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Login_AccountBanned_S2C Event Args
    /// </summary>
    public class Login_AccountBanned_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Login_AccountBanned_S2C Message Data
        /// </summary>
        public Login_AccountBanned_S2C Data;
    
        public Login_AccountBanned_S2C_EventArgs(Login_AccountBanned_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Login_SendEnterWorldRequest_C2S Event Args
    /// </summary>
    public class Login_SendEnterWorldRequest_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Login_SendEnterWorldRequest_C2S Message Data
        /// </summary>
        public Login_SendEnterWorldRequest_C2S Data;
    
        public Login_SendEnterWorldRequest_C2S_EventArgs(Login_SendEnterWorldRequest_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Movement_Jump_NonAutonomous_C2S Event Args
    /// </summary>
    public class Movement_Jump_NonAutonomous_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Movement_Jump_NonAutonomous_C2S Message Data
        /// </summary>
        public Movement_Jump_NonAutonomous_C2S Data;
    
        public Movement_Jump_NonAutonomous_C2S_EventArgs(Movement_Jump_NonAutonomous_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Admin_ReceiveAccountData_S2C Event Args
    /// </summary>
    public class Admin_ReceiveAccountData_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Admin_ReceiveAccountData_S2C Message Data
        /// </summary>
        public Admin_ReceiveAccountData_S2C Data;
    
        public Admin_ReceiveAccountData_S2C_EventArgs(Admin_ReceiveAccountData_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Admin_ReceivePlayerData_S2C Event Args
    /// </summary>
    public class Admin_ReceivePlayerData_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Admin_ReceivePlayerData_S2C Message Data
        /// </summary>
        public Admin_ReceivePlayerData_S2C Data;
    
        public Admin_ReceivePlayerData_S2C_EventArgs(Admin_ReceivePlayerData_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Admin_SendAdminGetServerVersion_C2S Event Args
    /// </summary>
    public class Admin_SendAdminGetServerVersion_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Admin_SendAdminGetServerVersion_C2S Message Data
        /// </summary>
        public Admin_SendAdminGetServerVersion_C2S Data;
    
        public Admin_SendAdminGetServerVersion_C2S_EventArgs(Admin_SendAdminGetServerVersion_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Social_SendFriendsCommand_C2S Event Args
    /// </summary>
    public class Social_SendFriendsCommand_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Social_SendFriendsCommand_C2S Message Data
        /// </summary>
        public Social_SendFriendsCommand_C2S Data;
    
        public Social_SendFriendsCommand_C2S_EventArgs(Social_SendFriendsCommand_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Admin_SendAdminRestoreCharacter_C2S Event Args
    /// </summary>
    public class Admin_SendAdminRestoreCharacter_C2S_EventArgs : EventArgs {
        /// <summary>
        /// Admin_SendAdminRestoreCharacter_C2S Message Data
        /// </summary>
        public Admin_SendAdminRestoreCharacter_C2S Data;
    
        public Admin_SendAdminRestoreCharacter_C2S_EventArgs(Admin_SendAdminRestoreCharacter_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Item_UpdateObject_S2C Event Args
    /// </summary>
    public class Item_UpdateObject_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Item_UpdateObject_S2C Message Data
        /// </summary>
        public Item_UpdateObject_S2C Data;
    
        public Item_UpdateObject_S2C_EventArgs(Item_UpdateObject_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Login_AccountBooted_S2C Event Args
    /// </summary>
    public class Login_AccountBooted_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Login_AccountBooted_S2C Message Data
        /// </summary>
        public Login_AccountBooted_S2C Data;
    
        public Login_AccountBooted_S2C_EventArgs(Login_AccountBooted_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_TurbineChat_S2C Event Args
    /// </summary>
    public class Communication_TurbineChat_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_TurbineChat_S2C Message Data
        /// </summary>
        public Communication_TurbineChat_S2C Data;
    
        public Communication_TurbineChat_S2C_EventArgs(Communication_TurbineChat_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Login_EnterGame_ServerReady_S2C Event Args
    /// </summary>
    public class Login_EnterGame_ServerReady_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Login_EnterGame_ServerReady_S2C Message Data
        /// </summary>
        public Login_EnterGame_ServerReady_S2C Data;
    
        public Login_EnterGame_ServerReady_S2C_EventArgs(Login_EnterGame_ServerReady_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Communication_TextboxString_S2C Event Args
    /// </summary>
    public class Communication_TextboxString_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Communication_TextboxString_S2C Message Data
        /// </summary>
        public Communication_TextboxString_S2C Data;
    
        public Communication_TextboxString_S2C_EventArgs(Communication_TextboxString_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// Login_WorldInfo_S2C Event Args
    /// </summary>
    public class Login_WorldInfo_S2C_EventArgs : EventArgs {
        /// <summary>
        /// Login_WorldInfo_S2C Message Data
        /// </summary>
        public Login_WorldInfo_S2C Data;
    
        public Login_WorldInfo_S2C_EventArgs(Login_WorldInfo_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// DDD_DataMessage_S2C Event Args
    /// </summary>
    public class DDD_DataMessage_S2C_EventArgs : EventArgs {
        /// <summary>
        /// DDD_DataMessage_S2C Message Data
        /// </summary>
        public DDD_DataMessage_S2C Data;
    
        public DDD_DataMessage_S2C_EventArgs(DDD_DataMessage_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// DDD_RequestDataMessage_C2S Event Args
    /// </summary>
    public class DDD_RequestDataMessage_C2S_EventArgs : EventArgs {
        /// <summary>
        /// DDD_RequestDataMessage_C2S Message Data
        /// </summary>
        public DDD_RequestDataMessage_C2S Data;
    
        public DDD_RequestDataMessage_C2S_EventArgs(DDD_RequestDataMessage_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// DDD_ErrorMessage_S2C Event Args
    /// </summary>
    public class DDD_ErrorMessage_S2C_EventArgs : EventArgs {
        /// <summary>
        /// DDD_ErrorMessage_S2C Message Data
        /// </summary>
        public DDD_ErrorMessage_S2C Data;
    
        public DDD_ErrorMessage_S2C_EventArgs(DDD_ErrorMessage_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// DDD_InterrogationMessage_S2C Event Args
    /// </summary>
    public class DDD_InterrogationMessage_S2C_EventArgs : EventArgs {
        /// <summary>
        /// DDD_InterrogationMessage_S2C Message Data
        /// </summary>
        public DDD_InterrogationMessage_S2C Data;
    
        public DDD_InterrogationMessage_S2C_EventArgs(DDD_InterrogationMessage_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// DDD_InterrogationResponseMessage_C2S Event Args
    /// </summary>
    public class DDD_InterrogationResponseMessage_C2S_EventArgs : EventArgs {
        /// <summary>
        /// DDD_InterrogationResponseMessage_C2S Message Data
        /// </summary>
        public DDD_InterrogationResponseMessage_C2S Data;
    
        public DDD_InterrogationResponseMessage_C2S_EventArgs(DDD_InterrogationResponseMessage_C2S data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// DDD_BeginDDDMessage_S2C Event Args
    /// </summary>
    public class DDD_BeginDDDMessage_S2C_EventArgs : EventArgs {
        /// <summary>
        /// DDD_BeginDDDMessage_S2C Message Data
        /// </summary>
        public DDD_BeginDDDMessage_S2C Data;
    
        public DDD_BeginDDDMessage_S2C_EventArgs(DDD_BeginDDDMessage_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// DDD_OnEndDDD_S2C Event Args
    /// </summary>
    public class DDD_OnEndDDD_S2C_EventArgs : EventArgs {
        /// <summary>
        /// DDD_OnEndDDD_S2C Message Data
        /// </summary>
        public DDD_OnEndDDD_S2C Data;
    
        public DDD_OnEndDDD_S2C_EventArgs(DDD_OnEndDDD_S2C data) {
            Data = data;
        }
    }
    
    /// <summary>
    /// DDD_EndDDDMessage_C2S Event Args
    /// </summary>
    public class DDD_EndDDDMessage_C2S_EventArgs : EventArgs {
        /// <summary>
        /// DDD_EndDDDMessage_C2S Message Data
        /// </summary>
        public DDD_EndDDDMessage_C2S Data;
    
        public DDD_EndDDDMessage_C2S_EventArgs(DDD_EndDDDMessage_C2S data) {
            Data = data;
        }
    }
    
}

namespace ACMessageDefs {
    public class MessageHandler {
        /// <summary>
        /// Fired for every valid parsed message
        /// </summary>
        public event EventHandler<MessageEventArgs> Message;

        /// <summary>
        /// Fired when an unknown message type was encountered
        /// </summary>
        public event EventHandler<UnknownMessageTypeEventArgs> UnknownMessageType;
        /// <summary>
        /// Fired on message type 0x0003 Allegiance_AllegianceUpdateAborted_S2C. Allegiance update cancelled
        /// </summary>
        public event EventHandler<Allegiance_AllegianceUpdateAborted_S2C_EventArgs> Allegiance_AllegianceUpdateAborted_S2C;
    
        /// <summary>
        /// Fired on message type 0x0004 Communication_PopUpString_S2C. Display a message in a popup message window.
        /// </summary>
        public event EventHandler<Communication_PopUpString_S2C_EventArgs> Communication_PopUpString_S2C;
    
        /// <summary>
        /// Fired on message type 0x0005 Character_PlayerOptionChangedEvent_C2S. Set a single character option.
        /// </summary>
        public event EventHandler<Character_PlayerOptionChangedEvent_C2S_EventArgs> Character_PlayerOptionChangedEvent_C2S;
    
        /// <summary>
        /// Fired on message type 0x0008 Combat_TargetedMeleeAttack_C2S. Starts a melee attack against a target
        /// </summary>
        public event EventHandler<Combat_TargetedMeleeAttack_C2S_EventArgs> Combat_TargetedMeleeAttack_C2S;
    
        /// <summary>
        /// Fired on message type 0x000A Combat_TargetedMissileAttack_C2S. Starts a missle attack against a target
        /// </summary>
        public event EventHandler<Combat_TargetedMissileAttack_C2S_EventArgs> Combat_TargetedMissileAttack_C2S;
    
        /// <summary>
        /// Fired on message type 0x000F Communication_SetAFKMode_C2S. Set AFK mode.
        /// </summary>
        public event EventHandler<Communication_SetAFKMode_C2S_EventArgs> Communication_SetAFKMode_C2S;
    
        /// <summary>
        /// Fired on message type 0x0010 Communication_SetAFKMessage_C2S. Set AFK message.
        /// </summary>
        public event EventHandler<Communication_SetAFKMessage_C2S_EventArgs> Communication_SetAFKMessage_C2S;
    
        /// <summary>
        /// Fired on message type 0x0013 Login_PlayerDescription_S2C. Information describing your character.
        /// </summary>
        public event EventHandler<Login_PlayerDescription_S2C_EventArgs> Login_PlayerDescription_S2C;
    
        /// <summary>
        /// Fired on message type 0x0015 Communication_Talk_C2S. Talking
        /// </summary>
        public event EventHandler<Communication_Talk_C2S_EventArgs> Communication_Talk_C2S;
    
        /// <summary>
        /// Fired on message type 0x0017 Social_RemoveFriend_C2S. Removes a friend
        /// </summary>
        public event EventHandler<Social_RemoveFriend_C2S_EventArgs> Social_RemoveFriend_C2S;
    
        /// <summary>
        /// Fired on message type 0x0018 Social_AddFriend_C2S. Adds a friend
        /// </summary>
        public event EventHandler<Social_AddFriend_C2S_EventArgs> Social_AddFriend_C2S;
    
        /// <summary>
        /// Fired on message type 0x0019 Inventory_PutItemInContainer_C2S. Store an item in a container.
        /// </summary>
        public event EventHandler<Inventory_PutItemInContainer_C2S_EventArgs> Inventory_PutItemInContainer_C2S;
    
        /// <summary>
        /// Fired on message type 0x001A Inventory_GetAndWieldItem_C2S. Gets and weilds an item.
        /// </summary>
        public event EventHandler<Inventory_GetAndWieldItem_C2S_EventArgs> Inventory_GetAndWieldItem_C2S;
    
        /// <summary>
        /// Fired on message type 0x001B Inventory_DropItem_C2S. Drop an item.
        /// </summary>
        public event EventHandler<Inventory_DropItem_C2S_EventArgs> Inventory_DropItem_C2S;
    
        /// <summary>
        /// Fired on message type 0x001D Allegiance_SwearAllegiance_C2S. Swear allegiance
        /// </summary>
        public event EventHandler<Allegiance_SwearAllegiance_C2S_EventArgs> Allegiance_SwearAllegiance_C2S;
    
        /// <summary>
        /// Fired on message type 0x001E Allegiance_BreakAllegiance_C2S. Break allegiance
        /// </summary>
        public event EventHandler<Allegiance_BreakAllegiance_C2S_EventArgs> Allegiance_BreakAllegiance_C2S;
    
        /// <summary>
        /// Fired on message type 0x001F Allegiance_UpdateRequest_C2S. Allegiance update request
        /// </summary>
        public event EventHandler<Allegiance_UpdateRequest_C2S_EventArgs> Allegiance_UpdateRequest_C2S;
    
        /// <summary>
        /// Fired on message type 0x0020 Allegiance_AllegianceUpdate_S2C. Returns info related to your monarch, patron and vassals.
        /// </summary>
        public event EventHandler<Allegiance_AllegianceUpdate_S2C_EventArgs> Allegiance_AllegianceUpdate_S2C;
    
        /// <summary>
        /// Fired on message type 0x0021 Social_FriendsUpdate_S2C. Friends list update
        /// </summary>
        public event EventHandler<Social_FriendsUpdate_S2C_EventArgs> Social_FriendsUpdate_S2C;
    
        /// <summary>
        /// Fired on message type 0x0022 Item_ServerSaysContainID_S2C. Store an item in a container.
        /// </summary>
        public event EventHandler<Item_ServerSaysContainID_S2C_EventArgs> Item_ServerSaysContainID_S2C;
    
        /// <summary>
        /// Fired on message type 0x0023 Item_WearItem_S2C. Equip an item.
        /// </summary>
        public event EventHandler<Item_WearItem_S2C_EventArgs> Item_WearItem_S2C;
    
        /// <summary>
        /// Fired on message type 0x0024 Item_ServerSaysRemove_S2C. Sent every time an object you are aware of ceases to exist. Merely running out of range does not generate this message - in that case, the client just automatically stops tracking it after receiving no updates for a while (which I presume is a very short while).
        /// </summary>
        public event EventHandler<Item_ServerSaysRemove_S2C_EventArgs> Item_ServerSaysRemove_S2C;
    
        /// <summary>
        /// Fired on message type 0x0025 Social_ClearFriends_C2S. Clears friend list
        /// </summary>
        public event EventHandler<Social_ClearFriends_C2S_EventArgs> Social_ClearFriends_C2S;
    
        /// <summary>
        /// Fired on message type 0x0026 Character_TeleToPKLArena_C2S. Teleport to the PKLite Arena
        /// </summary>
        public event EventHandler<Character_TeleToPKLArena_C2S_EventArgs> Character_TeleToPKLArena_C2S;
    
        /// <summary>
        /// Fired on message type 0x0027 Character_TeleToPKArena_C2S. Teleport to the PK Arena
        /// </summary>
        public event EventHandler<Character_TeleToPKArena_C2S_EventArgs> Character_TeleToPKArena_C2S;
    
        /// <summary>
        /// Fired on message type 0x0029 Social_CharacterTitleTable_S2C. Titles for the current character.
        /// </summary>
        public event EventHandler<Social_CharacterTitleTable_S2C_EventArgs> Social_CharacterTitleTable_S2C;
    
        /// <summary>
        /// Fired on message type 0x002B Social_AddOrSetCharacterTitle_S2C. Set a title for the current character.
        /// </summary>
        public event EventHandler<Social_AddOrSetCharacterTitle_S2C_EventArgs> Social_AddOrSetCharacterTitle_S2C;
    
        /// <summary>
        /// Fired on message type 0x002C Social_SetDisplayCharacterTitle_C2S. Sets a character's display title
        /// </summary>
        public event EventHandler<Social_SetDisplayCharacterTitle_C2S_EventArgs> Social_SetDisplayCharacterTitle_C2S;
    
        /// <summary>
        /// Fired on message type 0x0030 Allegiance_QueryAllegianceName_C2S. Query the allegiance name
        /// </summary>
        public event EventHandler<Allegiance_QueryAllegianceName_C2S_EventArgs> Allegiance_QueryAllegianceName_C2S;
    
        /// <summary>
        /// Fired on message type 0x0031 Allegiance_ClearAllegianceName_C2S. Clears the allegiance name
        /// </summary>
        public event EventHandler<Allegiance_ClearAllegianceName_C2S_EventArgs> Allegiance_ClearAllegianceName_C2S;
    
        /// <summary>
        /// Fired on message type 0x0032 Communication_TalkDirect_C2S. Direct message by ID
        /// </summary>
        public event EventHandler<Communication_TalkDirect_C2S_EventArgs> Communication_TalkDirect_C2S;
    
        /// <summary>
        /// Fired on message type 0x0033 Allegiance_SetAllegianceName_C2S. Sets the allegiance name
        /// </summary>
        public event EventHandler<Allegiance_SetAllegianceName_C2S_EventArgs> Allegiance_SetAllegianceName_C2S;
    
        /// <summary>
        /// Fired on message type 0x0035 Inventory_UseWithTargetEvent_C2S. Attempt to use an item with a target.
        /// </summary>
        public event EventHandler<Inventory_UseWithTargetEvent_C2S_EventArgs> Inventory_UseWithTargetEvent_C2S;
    
        /// <summary>
        /// Fired on message type 0x0036 Inventory_UseEvent_C2S. Attempt to use an item.
        /// </summary>
        public event EventHandler<Inventory_UseEvent_C2S_EventArgs> Inventory_UseEvent_C2S;
    
        /// <summary>
        /// Fired on message type 0x003B Allegiance_SetAllegianceOfficer_C2S. Sets an allegiance officer
        /// </summary>
        public event EventHandler<Allegiance_SetAllegianceOfficer_C2S_EventArgs> Allegiance_SetAllegianceOfficer_C2S;
    
        /// <summary>
        /// Fired on message type 0x003C Allegiance_SetAllegianceOfficerTitle_C2S. Sets an allegiance officer title for a given level
        /// </summary>
        public event EventHandler<Allegiance_SetAllegianceOfficerTitle_C2S_EventArgs> Allegiance_SetAllegianceOfficerTitle_C2S;
    
        /// <summary>
        /// Fired on message type 0x003D Allegiance_ListAllegianceOfficerTitles_C2S. List the allegiance officer titles
        /// </summary>
        public event EventHandler<Allegiance_ListAllegianceOfficerTitles_C2S_EventArgs> Allegiance_ListAllegianceOfficerTitles_C2S;
    
        /// <summary>
        /// Fired on message type 0x003E Allegiance_ClearAllegianceOfficerTitles_C2S. Clears the allegiance officer titles
        /// </summary>
        public event EventHandler<Allegiance_ClearAllegianceOfficerTitles_C2S_EventArgs> Allegiance_ClearAllegianceOfficerTitles_C2S;
    
        /// <summary>
        /// Fired on message type 0x003F Allegiance_DoAllegianceLockAction_C2S. Perform the allegiance lock action
        /// </summary>
        public event EventHandler<Allegiance_DoAllegianceLockAction_C2S_EventArgs> Allegiance_DoAllegianceLockAction_C2S;
    
        /// <summary>
        /// Fired on message type 0x0040 Allegiance_SetAllegianceApprovedVassal_C2S. Sets a person as an approved vassal
        /// </summary>
        public event EventHandler<Allegiance_SetAllegianceApprovedVassal_C2S_EventArgs> Allegiance_SetAllegianceApprovedVassal_C2S;
    
        /// <summary>
        /// Fired on message type 0x0041 Allegiance_AllegianceChatGag_C2S. Gags a person in allegiance chat
        /// </summary>
        public event EventHandler<Allegiance_AllegianceChatGag_C2S_EventArgs> Allegiance_AllegianceChatGag_C2S;
    
        /// <summary>
        /// Fired on message type 0x0042 Allegiance_DoAllegianceHouseAction_C2S. Perform the allegiance house action
        /// </summary>
        public event EventHandler<Allegiance_DoAllegianceHouseAction_C2S_EventArgs> Allegiance_DoAllegianceHouseAction_C2S;
    
        /// <summary>
        /// Fired on message type 0x0044 Train_TrainAttribute2nd_C2S. Spend XP to raise a vital.
        /// </summary>
        public event EventHandler<Train_TrainAttribute2nd_C2S_EventArgs> Train_TrainAttribute2nd_C2S;
    
        /// <summary>
        /// Fired on message type 0x0045 Train_TrainAttribute_C2S. Spend XP to raise an attribute.
        /// </summary>
        public event EventHandler<Train_TrainAttribute_C2S_EventArgs> Train_TrainAttribute_C2S;
    
        /// <summary>
        /// Fired on message type 0x0046 Train_TrainSkill_C2S. Spend XP to raise a skill.
        /// </summary>
        public event EventHandler<Train_TrainSkill_C2S_EventArgs> Train_TrainSkill_C2S;
    
        /// <summary>
        /// Fired on message type 0x0047 Train_TrainSkillAdvancementClass_C2S. Spend skill credits to train a skill.
        /// </summary>
        public event EventHandler<Train_TrainSkillAdvancementClass_C2S_EventArgs> Train_TrainSkillAdvancementClass_C2S;
    
        /// <summary>
        /// Fired on message type 0x0048 Magic_CastUntargetedSpell_C2S. Cast a spell with no target.
        /// </summary>
        public event EventHandler<Magic_CastUntargetedSpell_C2S_EventArgs> Magic_CastUntargetedSpell_C2S;
    
        /// <summary>
        /// Fired on message type 0x004A Magic_CastTargetedSpell_C2S. Cast a spell on a target
        /// </summary>
        public event EventHandler<Magic_CastTargetedSpell_C2S_EventArgs> Magic_CastTargetedSpell_C2S;
    
        /// <summary>
        /// Fired on message type 0x0052 Item_StopViewingObjectContents_S2C. Close Container - Only sent when explicitly closed
        /// </summary>
        public event EventHandler<Item_StopViewingObjectContents_S2C_EventArgs> Item_StopViewingObjectContents_S2C;
    
        /// <summary>
        /// Fired on message type 0x0053 Combat_ChangeCombatMode_C2S. Changes the combat mode
        /// </summary>
        public event EventHandler<Combat_ChangeCombatMode_C2S_EventArgs> Combat_ChangeCombatMode_C2S;
    
        /// <summary>
        /// Fired on message type 0x0054 Inventory_StackableMerge_C2S. Merges one stack with another
        /// </summary>
        public event EventHandler<Inventory_StackableMerge_C2S_EventArgs> Inventory_StackableMerge_C2S;
    
        /// <summary>
        /// Fired on message type 0x0055 Inventory_StackableSplitToContainer_C2S. Split a stack and place it into a container
        /// </summary>
        public event EventHandler<Inventory_StackableSplitToContainer_C2S_EventArgs> Inventory_StackableSplitToContainer_C2S;
    
        /// <summary>
        /// Fired on message type 0x0056 Inventory_StackableSplitTo3D_C2S. Split a stack and place it into the world
        /// </summary>
        public event EventHandler<Inventory_StackableSplitTo3D_C2S_EventArgs> Inventory_StackableSplitTo3D_C2S;
    
        /// <summary>
        /// Fired on message type 0x0058 Communication_ModifyCharacterSquelch_C2S. Changes an account squelch
        /// </summary>
        public event EventHandler<Communication_ModifyCharacterSquelch_C2S_EventArgs> Communication_ModifyCharacterSquelch_C2S;
    
        /// <summary>
        /// Fired on message type 0x0059 Communication_ModifyAccountSquelch_C2S. Changes an account squelch
        /// </summary>
        public event EventHandler<Communication_ModifyAccountSquelch_C2S_EventArgs> Communication_ModifyAccountSquelch_C2S;
    
        /// <summary>
        /// Fired on message type 0x005B Communication_ModifyGlobalSquelch_C2S. Changes the global filters, /filter -type as well as /chat and /notell
        /// </summary>
        public event EventHandler<Communication_ModifyGlobalSquelch_C2S_EventArgs> Communication_ModifyGlobalSquelch_C2S;
    
        /// <summary>
        /// Fired on message type 0x005D Communication_TalkDirectByName_C2S. Direct message by name
        /// </summary>
        public event EventHandler<Communication_TalkDirectByName_C2S_EventArgs> Communication_TalkDirectByName_C2S;
    
        /// <summary>
        /// Fired on message type 0x005F Vendor_Buy_C2S. Buy from a vendor
        /// </summary>
        public event EventHandler<Vendor_Buy_C2S_EventArgs> Vendor_Buy_C2S;
    
        /// <summary>
        /// Fired on message type 0x0060 Vendor_Sell_C2S. Sell to a vendor
        /// </summary>
        public event EventHandler<Vendor_Sell_C2S_EventArgs> Vendor_Sell_C2S;
    
        /// <summary>
        /// Fired on message type 0x0062 Vendor_VendorInfo_S2C. Open the buy/sell panel for a merchant.
        /// </summary>
        public event EventHandler<Vendor_VendorInfo_S2C_EventArgs> Vendor_VendorInfo_S2C;
    
        /// <summary>
        /// Fired on message type 0x0063 Character_TeleToLifestone_C2S. Teleport to your lifestone. (/lifestone)
        /// </summary>
        public event EventHandler<Character_TeleToLifestone_C2S_EventArgs> Character_TeleToLifestone_C2S;
    
        /// <summary>
        /// Fired on message type 0x0075 Character_StartBarber_S2C. Opens barber UI
        /// </summary>
        public event EventHandler<Character_StartBarber_S2C_EventArgs> Character_StartBarber_S2C;
    
        /// <summary>
        /// Fired on message type 0x00A0 Character_ServerSaysAttemptFailed_S2C. Failure to give an item
        /// </summary>
        public event EventHandler<Character_ServerSaysAttemptFailed_S2C_EventArgs> Character_ServerSaysAttemptFailed_S2C;
    
        /// <summary>
        /// Fired on message type 0x00A1 Character_LoginCompleteNotification_C2S. The client is ready for the character to materialize after portalling or logging on.
        /// </summary>
        public event EventHandler<Character_LoginCompleteNotification_C2S_EventArgs> Character_LoginCompleteNotification_C2S;
    
        /// <summary>
        /// Fired on message type 0x00A2 Fellowship_Create_C2S. Create a fellowship
        /// </summary>
        public event EventHandler<Fellowship_Create_C2S_EventArgs> Fellowship_Create_C2S;
    
        /// <summary>
        /// Fired on message type 0x00A3 Fellowship_Quit_S2C. Member left fellowship
        /// </summary>
        public event EventHandler<Fellowship_Quit_S2C_EventArgs> Fellowship_Quit_S2C;
    
        /// <summary>
        /// Fired on message type 0x00A3 Fellowship_Quit_C2S. Quit the fellowship
        /// </summary>
        public event EventHandler<Fellowship_Quit_C2S_EventArgs> Fellowship_Quit_C2S;
    
        /// <summary>
        /// Fired on message type 0x00A4 Fellowship_Dismiss_S2C. Member dismissed from fellowship
        /// </summary>
        public event EventHandler<Fellowship_Dismiss_S2C_EventArgs> Fellowship_Dismiss_S2C;
    
        /// <summary>
        /// Fired on message type 0x00A4 Fellowship_Dismiss_C2S. Dismiss a player from the fellowship
        /// </summary>
        public event EventHandler<Fellowship_Dismiss_C2S_EventArgs> Fellowship_Dismiss_C2S;
    
        /// <summary>
        /// Fired on message type 0x00A5 Fellowship_Recruit_C2S. Recruit a player to the fellowship
        /// </summary>
        public event EventHandler<Fellowship_Recruit_C2S_EventArgs> Fellowship_Recruit_C2S;
    
        /// <summary>
        /// Fired on message type 0x00A6 Fellowship_UpdateRequest_C2S. Update request
        /// </summary>
        public event EventHandler<Fellowship_UpdateRequest_C2S_EventArgs> Fellowship_UpdateRequest_C2S;
    
        /// <summary>
        /// Fired on message type 0x00AA Writing_BookAddPage_C2S. Request update to book data (seems to be sent after failed add page)
        /// </summary>
        public event EventHandler<Writing_BookAddPage_C2S_EventArgs> Writing_BookAddPage_C2S;
    
        /// <summary>
        /// Fired on message type 0x00AB Writing_BookModifyPage_C2S. Updates a page in a book
        /// </summary>
        public event EventHandler<Writing_BookModifyPage_C2S_EventArgs> Writing_BookModifyPage_C2S;
    
        /// <summary>
        /// Fired on message type 0x00AC Writing_BookData_C2S. Add a page to a book
        /// </summary>
        public event EventHandler<Writing_BookData_C2S_EventArgs> Writing_BookData_C2S;
    
        /// <summary>
        /// Fired on message type 0x00AD Writing_BookDeletePage_C2S. Removes a page from a book
        /// </summary>
        public event EventHandler<Writing_BookDeletePage_C2S_EventArgs> Writing_BookDeletePage_C2S;
    
        /// <summary>
        /// Fired on message type 0x00AE Writing_BookPageData_C2S. Requests data for a page in a book
        /// </summary>
        public event EventHandler<Writing_BookPageData_C2S_EventArgs> Writing_BookPageData_C2S;
    
        /// <summary>
        /// Fired on message type 0x00B4 Writing_BookOpen_S2C. Sent when you first open a book, contains the entire table of contents.
        /// </summary>
        public event EventHandler<Writing_BookOpen_S2C_EventArgs> Writing_BookOpen_S2C;
    
        /// <summary>
        /// Fired on message type 0x00B6 Writing_BookAddPageResponse_S2C. Response to an attempt to add a page to a book.
        /// </summary>
        public event EventHandler<Writing_BookAddPageResponse_S2C_EventArgs> Writing_BookAddPageResponse_S2C;
    
        /// <summary>
        /// Fired on message type 0x00B7 Writing_BookDeletePageResponse_S2C. Response to an attempt to delete a page from a book.
        /// </summary>
        public event EventHandler<Writing_BookDeletePageResponse_S2C_EventArgs> Writing_BookDeletePageResponse_S2C;
    
        /// <summary>
        /// Fired on message type 0x00B8 Writing_BookPageDataResponse_S2C. Contains the text of a single page of a book, parchment or sign.
        /// </summary>
        public event EventHandler<Writing_BookPageDataResponse_S2C_EventArgs> Writing_BookPageDataResponse_S2C;
    
        /// <summary>
        /// Fired on message type 0x00BF Writing_SetInscription_C2S. Sets the inscription on an object
        /// </summary>
        public event EventHandler<Writing_SetInscription_C2S_EventArgs> Writing_SetInscription_C2S;
    
        /// <summary>
        /// Fired on message type 0x00C3 Item_GetInscriptionResponse_S2C. Get Inscription Response, doesn't seem to be really used by client
        /// </summary>
        public event EventHandler<Item_GetInscriptionResponse_S2C_EventArgs> Item_GetInscriptionResponse_S2C;
    
        /// <summary>
        /// Fired on message type 0x00C8 Item_Appraise_C2S. Appraise something
        /// </summary>
        public event EventHandler<Item_Appraise_C2S_EventArgs> Item_Appraise_C2S;
    
        /// <summary>
        /// Fired on message type 0x00C9 Item_SetAppraiseInfo_S2C. The result of an attempt to assess an item or creature.
        /// </summary>
        public event EventHandler<Item_SetAppraiseInfo_S2C_EventArgs> Item_SetAppraiseInfo_S2C;
    
        /// <summary>
        /// Fired on message type 0x00CD Inventory_GiveObjectRequest_C2S. Give an item to someone.
        /// </summary>
        public event EventHandler<Inventory_GiveObjectRequest_C2S_EventArgs> Inventory_GiveObjectRequest_C2S;
    
        /// <summary>
        /// Fired on message type 0x00D6 Advocate_Teleport_C2S. Advocate Teleport
        /// </summary>
        public event EventHandler<Advocate_Teleport_C2S_EventArgs> Advocate_Teleport_C2S;
    
        /// <summary>
        /// Fired on message type 0x0140 Character_AbuseLogRequest_C2S. Sends an abuse report.
        /// </summary>
        public event EventHandler<Character_AbuseLogRequest_C2S_EventArgs> Character_AbuseLogRequest_C2S;
    
        /// <summary>
        /// Fired on message type 0x0145 Communication_AddToChannel_C2S. Joins a chat channel
        /// </summary>
        public event EventHandler<Communication_AddToChannel_C2S_EventArgs> Communication_AddToChannel_C2S;
    
        /// <summary>
        /// Fired on message type 0x0146 Communication_RemoveFromChannel_C2S. Leaves a chat channel
        /// </summary>
        public event EventHandler<Communication_RemoveFromChannel_C2S_EventArgs> Communication_RemoveFromChannel_C2S;
    
        /// <summary>
        /// Fired on message type 0x0147 Communication_ChannelBroadcast_S2C. ChannelBroadcast: Group Chat
        /// </summary>
        public event EventHandler<Communication_ChannelBroadcast_S2C_EventArgs> Communication_ChannelBroadcast_S2C;
    
        /// <summary>
        /// Fired on message type 0x0147 Communication_ChannelBroadcast_C2S. Sends a message to a chat channel
        /// </summary>
        public event EventHandler<Communication_ChannelBroadcast_C2S_EventArgs> Communication_ChannelBroadcast_C2S;
    
        /// <summary>
        /// Fired on message type 0x0148 Communication_ChannelList_S2C. ChannelList: Provides list of characters listening to a channel, I assume in response to a command
        /// </summary>
        public event EventHandler<Communication_ChannelList_S2C_EventArgs> Communication_ChannelList_S2C;
    
        /// <summary>
        /// Fired on message type 0x0148 Communication_ChannelList_C2S. 
        /// </summary>
        public event EventHandler<Communication_ChannelList_C2S_EventArgs> Communication_ChannelList_C2S;
    
        /// <summary>
        /// Fired on message type 0x0149 Communication_ChannelIndex_S2C. ChannelIndex: Provides list of channels available to the player, I assume in response to a command
        /// </summary>
        public event EventHandler<Communication_ChannelIndex_S2C_EventArgs> Communication_ChannelIndex_S2C;
    
        /// <summary>
        /// Fired on message type 0x0149 Communication_ChannelIndex_C2S. Requests a channel index
        /// </summary>
        public event EventHandler<Communication_ChannelIndex_C2S_EventArgs> Communication_ChannelIndex_C2S;
    
        /// <summary>
        /// Fired on message type 0x0195 Inventory_NoLongerViewingContents_C2S. Stop viewing the contents of a container
        /// </summary>
        public event EventHandler<Inventory_NoLongerViewingContents_C2S_EventArgs> Inventory_NoLongerViewingContents_C2S;
    
        /// <summary>
        /// Fired on message type 0x0196 Item_OnViewContents_S2C. Set Pack Contents
        /// </summary>
        public event EventHandler<Item_OnViewContents_S2C_EventArgs> Item_OnViewContents_S2C;
    
        /// <summary>
        /// Fired on message type 0x0197 Item_UpdateStackSize_S2C. For stackable items, this changes the number of items in the stack.
        /// </summary>
        public event EventHandler<Item_UpdateStackSize_S2C_EventArgs> Item_UpdateStackSize_S2C;
    
        /// <summary>
        /// Fired on message type 0x019A Item_ServerSaysMoveItem_S2C. ServerSaysMoveItem: Removes an item from inventory (when you place it on the ground or give it away)
        /// </summary>
        public event EventHandler<Item_ServerSaysMoveItem_S2C_EventArgs> Item_ServerSaysMoveItem_S2C;
    
        /// <summary>
        /// Fired on message type 0x019B Inventory_StackableSplitToWield_C2S. Splits an item to a wield location.
        /// </summary>
        public event EventHandler<Inventory_StackableSplitToWield_C2S_EventArgs> Inventory_StackableSplitToWield_C2S;
    
        /// <summary>
        /// Fired on message type 0x019C Character_AddShortCut_C2S. Add an item to the shortcut bar.
        /// </summary>
        public event EventHandler<Character_AddShortCut_C2S_EventArgs> Character_AddShortCut_C2S;
    
        /// <summary>
        /// Fired on message type 0x019D Character_RemoveShortCut_C2S. Remove an item from the shortcut bar.
        /// </summary>
        public event EventHandler<Character_RemoveShortCut_C2S_EventArgs> Character_RemoveShortCut_C2S;
    
        /// <summary>
        /// Fired on message type 0x019E Combat_HandlePlayerDeathEvent_S2C. A Player Kill occurred nearby (also sent for suicides).
        /// </summary>
        public event EventHandler<Combat_HandlePlayerDeathEvent_S2C_EventArgs> Combat_HandlePlayerDeathEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01A1 Character_CharacterOptionsEvent_C2S. Set multiple character options.
        /// </summary>
        public event EventHandler<Character_CharacterOptionsEvent_C2S_EventArgs> Character_CharacterOptionsEvent_C2S;
    
        /// <summary>
        /// Fired on message type 0x01A7 Combat_HandleAttackDoneEvent_S2C. HandleAttackDoneEvent: Melee attack completed
        /// </summary>
        public event EventHandler<Combat_HandleAttackDoneEvent_S2C_EventArgs> Combat_HandleAttackDoneEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01A8 Magic_RemoveSpell_S2C. RemoveSpell: Delete a spell from your spellbook.
        /// </summary>
        public event EventHandler<Magic_RemoveSpell_S2C_EventArgs> Magic_RemoveSpell_S2C;
    
        /// <summary>
        /// Fired on message type 0x01A8 Magic_RemoveSpell_C2S. Removes a spell from the spell book
        /// </summary>
        public event EventHandler<Magic_RemoveSpell_C2S_EventArgs> Magic_RemoveSpell_C2S;
    
        /// <summary>
        /// Fired on message type 0x01AC Combat_HandleVictimNotificationEventSelf_S2C. You just died.
        /// </summary>
        public event EventHandler<Combat_HandleVictimNotificationEventSelf_S2C_EventArgs> Combat_HandleVictimNotificationEventSelf_S2C;
    
        /// <summary>
        /// Fired on message type 0x01AD Combat_HandleVictimNotificationEventOther_S2C. Message for a death, something you killed or your own death message.
        /// </summary>
        public event EventHandler<Combat_HandleVictimNotificationEventOther_S2C_EventArgs> Combat_HandleVictimNotificationEventOther_S2C;
    
        /// <summary>
        /// Fired on message type 0x01B1 Combat_HandleAttackerNotificationEvent_S2C. HandleAttackerNotificationEvent: You have hit your target with a melee attack.
        /// </summary>
        public event EventHandler<Combat_HandleAttackerNotificationEvent_S2C_EventArgs> Combat_HandleAttackerNotificationEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01B2 Combat_HandleDefenderNotificationEvent_S2C. HandleDefenderNotificationEvent: You have been hit by a creature's melee attack.
        /// </summary>
        public event EventHandler<Combat_HandleDefenderNotificationEvent_S2C_EventArgs> Combat_HandleDefenderNotificationEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01B3 Combat_HandleEvasionAttackerNotificationEvent_S2C. HandleEvasionAttackerNotificationEvent: Your target has evaded your melee attack.
        /// </summary>
        public event EventHandler<Combat_HandleEvasionAttackerNotificationEvent_S2C_EventArgs> Combat_HandleEvasionAttackerNotificationEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01B4 Combat_HandleEvasionDefenderNotificationEvent_S2C. HandleEvasionDefenderNotificationEvent: You have evaded a creature's melee attack.
        /// </summary>
        public event EventHandler<Combat_HandleEvasionDefenderNotificationEvent_S2C_EventArgs> Combat_HandleEvasionDefenderNotificationEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01B7 Combat_CancelAttack_C2S. Cancels an attack
        /// </summary>
        public event EventHandler<Combat_CancelAttack_C2S_EventArgs> Combat_CancelAttack_C2S;
    
        /// <summary>
        /// Fired on message type 0x01B8 Combat_HandleCommenceAttackEvent_S2C. HandleCommenceAttackEvent: Start melee attack
        /// </summary>
        public event EventHandler<Combat_HandleCommenceAttackEvent_S2C_EventArgs> Combat_HandleCommenceAttackEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01BF Combat_QueryHealth_C2S. Query's a creatures health
        /// </summary>
        public event EventHandler<Combat_QueryHealth_C2S_EventArgs> Combat_QueryHealth_C2S;
    
        /// <summary>
        /// Fired on message type 0x01C0 Combat_QueryHealthResponse_S2C. QueryHealthResponse: Update a creature's health bar.
        /// </summary>
        public event EventHandler<Combat_QueryHealthResponse_S2C_EventArgs> Combat_QueryHealthResponse_S2C;
    
        /// <summary>
        /// Fired on message type 0x01C2 Character_QueryAge_C2S. Query a character's age
        /// </summary>
        public event EventHandler<Character_QueryAge_C2S_EventArgs> Character_QueryAge_C2S;
    
        /// <summary>
        /// Fired on message type 0x01C3 Character_QueryAgeResponse_S2C. QueryAgeResponse: happens when you do /age in the game
        /// </summary>
        public event EventHandler<Character_QueryAgeResponse_S2C_EventArgs> Character_QueryAgeResponse_S2C;
    
        /// <summary>
        /// Fired on message type 0x01C4 Character_QueryBirth_C2S. Query a character's birth day
        /// </summary>
        public event EventHandler<Character_QueryBirth_C2S_EventArgs> Character_QueryBirth_C2S;
    
        /// <summary>
        /// Fired on message type 0x01C7 Item_UseDone_S2C. UseDone: Ready. Previous action complete
        /// </summary>
        public event EventHandler<Item_UseDone_S2C_EventArgs> Item_UseDone_S2C;
    
        /// <summary>
        /// Fired on message type 0x01C9 Fellowship_FellowUpdateDone_S2C. Fellow update is done
        /// </summary>
        public event EventHandler<Fellowship_FellowUpdateDone_S2C_EventArgs> Fellowship_FellowUpdateDone_S2C;
    
        /// <summary>
        /// Fired on message type 0x01CA Fellowship_FellowStatsDone_S2C. Fellow stats are done
        /// </summary>
        public event EventHandler<Fellowship_FellowStatsDone_S2C_EventArgs> Fellowship_FellowStatsDone_S2C;
    
        /// <summary>
        /// Fired on message type 0x01CB Item_AppraiseDone_S2C. Close Assess Panel
        /// </summary>
        public event EventHandler<Item_AppraiseDone_S2C_EventArgs> Item_AppraiseDone_S2C;
    
        /// <summary>
        /// Fired on message type 0x01D1 Qualities_PrivateRemoveIntEvent_S2C. Private Remove Int Event
        /// </summary>
        public event EventHandler<Qualities_PrivateRemoveIntEvent_S2C_EventArgs> Qualities_PrivateRemoveIntEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01D2 Qualities_RemoveIntEvent_S2C. Remove Int Event
        /// </summary>
        public event EventHandler<Qualities_RemoveIntEvent_S2C_EventArgs> Qualities_RemoveIntEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01D3 Qualities_PrivateRemoveBoolEvent_S2C. Private Remove Bool Event
        /// </summary>
        public event EventHandler<Qualities_PrivateRemoveBoolEvent_S2C_EventArgs> Qualities_PrivateRemoveBoolEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01D4 Qualities_RemoveBoolEvent_S2C. Remove Bool Event
        /// </summary>
        public event EventHandler<Qualities_RemoveBoolEvent_S2C_EventArgs> Qualities_RemoveBoolEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01D5 Qualities_PrivateRemoveFloatEvent_S2C. Private Remove Float Event
        /// </summary>
        public event EventHandler<Qualities_PrivateRemoveFloatEvent_S2C_EventArgs> Qualities_PrivateRemoveFloatEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01D6 Qualities_RemoveFloatEvent_S2C. Remove Float Event
        /// </summary>
        public event EventHandler<Qualities_RemoveFloatEvent_S2C_EventArgs> Qualities_RemoveFloatEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01D7 Qualities_PrivateRemoveStringEvent_S2C. Private Remove String Event
        /// </summary>
        public event EventHandler<Qualities_PrivateRemoveStringEvent_S2C_EventArgs> Qualities_PrivateRemoveStringEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01D8 Qualities_RemoveStringEvent_S2C. Remove String Event
        /// </summary>
        public event EventHandler<Qualities_RemoveStringEvent_S2C_EventArgs> Qualities_RemoveStringEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01D9 Qualities_PrivateRemoveDataIDEvent_S2C. Private Remove DID Event
        /// </summary>
        public event EventHandler<Qualities_PrivateRemoveDataIDEvent_S2C_EventArgs> Qualities_PrivateRemoveDataIDEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01DA Qualities_RemoveDataIDEvent_S2C. Remove DID Event
        /// </summary>
        public event EventHandler<Qualities_RemoveDataIDEvent_S2C_EventArgs> Qualities_RemoveDataIDEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01DB Qualities_PrivateRemoveInstanceIDEvent_S2C. Private Remove IID Event
        /// </summary>
        public event EventHandler<Qualities_PrivateRemoveInstanceIDEvent_S2C_EventArgs> Qualities_PrivateRemoveInstanceIDEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01DC Qualities_RemoveInstanceIDEvent_S2C. Remove IID Event
        /// </summary>
        public event EventHandler<Qualities_RemoveInstanceIDEvent_S2C_EventArgs> Qualities_RemoveInstanceIDEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01DD Qualities_PrivateRemovePositionEvent_S2C. Private Remove Position Event
        /// </summary>
        public event EventHandler<Qualities_PrivateRemovePositionEvent_S2C_EventArgs> Qualities_PrivateRemovePositionEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01DE Qualities_RemovePositionEvent_S2C. Remove Position Event
        /// </summary>
        public event EventHandler<Qualities_RemovePositionEvent_S2C_EventArgs> Qualities_RemovePositionEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x01DF Communication_Emote_C2S. Emote message
        /// </summary>
        public event EventHandler<Communication_Emote_C2S_EventArgs> Communication_Emote_C2S;
    
        /// <summary>
        /// Fired on message type 0x01E0 Communication_HearEmote_S2C. Indirect '/e' text.
        /// </summary>
        public event EventHandler<Communication_HearEmote_S2C_EventArgs> Communication_HearEmote_S2C;
    
        /// <summary>
        /// Fired on message type 0x01E1 Communication_SoulEmote_C2S. Soul emote message
        /// </summary>
        public event EventHandler<Communication_SoulEmote_C2S_EventArgs> Communication_SoulEmote_C2S;
    
        /// <summary>
        /// Fired on message type 0x01E2 Communication_HearSoulEmote_S2C. Contains the text associated with an emote action.
        /// </summary>
        public event EventHandler<Communication_HearSoulEmote_S2C_EventArgs> Communication_HearSoulEmote_S2C;
    
        /// <summary>
        /// Fired on message type 0x01E3 Character_AddSpellFavorite_C2S. Add a spell to a spell bar.
        /// </summary>
        public event EventHandler<Character_AddSpellFavorite_C2S_EventArgs> Character_AddSpellFavorite_C2S;
    
        /// <summary>
        /// Fired on message type 0x01E4 Character_RemoveSpellFavorite_C2S. Remove a spell from a spell bar.
        /// </summary>
        public event EventHandler<Character_RemoveSpellFavorite_C2S_EventArgs> Character_RemoveSpellFavorite_C2S;
    
        /// <summary>
        /// Fired on message type 0x01E9 Character_RequestPing_C2S. Request a ping
        /// </summary>
        public event EventHandler<Character_RequestPing_C2S_EventArgs> Character_RequestPing_C2S;
    
        /// <summary>
        /// Fired on message type 0x01EA Character_ReturnPing_S2C. Ping Reply
        /// </summary>
        public event EventHandler<Character_ReturnPing_S2C_EventArgs> Character_ReturnPing_S2C;
    
        /// <summary>
        /// Fired on message type 0x01F4 Communication_SetSquelchDB_S2C. Squelch and Filter List
        /// </summary>
        public event EventHandler<Communication_SetSquelchDB_S2C_EventArgs> Communication_SetSquelchDB_S2C;
    
        /// <summary>
        /// Fired on message type 0x01F6 Trade_OpenTradeNegotiations_C2S. Starts trading with another player.
        /// </summary>
        public event EventHandler<Trade_OpenTradeNegotiations_C2S_EventArgs> Trade_OpenTradeNegotiations_C2S;
    
        /// <summary>
        /// Fired on message type 0x01F7 Trade_CloseTradeNegotiations_C2S. Ends trading, when trade window is closed?
        /// </summary>
        public event EventHandler<Trade_CloseTradeNegotiations_C2S_EventArgs> Trade_CloseTradeNegotiations_C2S;
    
        /// <summary>
        /// Fired on message type 0x01F8 Trade_AddToTrade_C2S. Adds an object to the trade.
        /// </summary>
        public event EventHandler<Trade_AddToTrade_C2S_EventArgs> Trade_AddToTrade_C2S;
    
        /// <summary>
        /// Fired on message type 0x01FA Trade_AcceptTrade_C2S. Accepts a trade.
        /// </summary>
        public event EventHandler<Trade_AcceptTrade_C2S_EventArgs> Trade_AcceptTrade_C2S;
    
        /// <summary>
        /// Fired on message type 0x01FB Trade_DeclineTrade_C2S. Declines a trade, when cancel is clicked?
        /// </summary>
        public event EventHandler<Trade_DeclineTrade_C2S_EventArgs> Trade_DeclineTrade_C2S;
    
        /// <summary>
        /// Fired on message type 0x01FD Trade_RegisterTrade_S2C. RegisterTrade: Send to begin a trade and display the trade window
        /// </summary>
        public event EventHandler<Trade_RegisterTrade_S2C_EventArgs> Trade_RegisterTrade_S2C;
    
        /// <summary>
        /// Fired on message type 0x01FE Trade_OpenTrade_S2C. OpenTrade: Open trade window
        /// </summary>
        public event EventHandler<Trade_OpenTrade_S2C_EventArgs> Trade_OpenTrade_S2C;
    
        /// <summary>
        /// Fired on message type 0x01FF Trade_CloseTrade_S2C. CloseTrade: End trading
        /// </summary>
        public event EventHandler<Trade_CloseTrade_S2C_EventArgs> Trade_CloseTrade_S2C;
    
        /// <summary>
        /// Fired on message type 0x0200 Trade_AddToTrade_S2C. RemoveFromTrade: Item was removed from trade window
        /// </summary>
        public event EventHandler<Trade_AddToTrade_S2C_EventArgs> Trade_AddToTrade_S2C;
    
        /// <summary>
        /// Fired on message type 0x0201 Trade_RemoveFromTrade_S2C. Removes an item from the trade window, not sure if this is used still?
        /// </summary>
        public event EventHandler<Trade_RemoveFromTrade_S2C_EventArgs> Trade_RemoveFromTrade_S2C;
    
        /// <summary>
        /// Fired on message type 0x0202 Trade_AcceptTrade_S2C. AcceptTrade: The trade was accepted
        /// </summary>
        public event EventHandler<Trade_AcceptTrade_S2C_EventArgs> Trade_AcceptTrade_S2C;
    
        /// <summary>
        /// Fired on message type 0x0203 Trade_DeclineTrade_S2C. DeclineTrade: The trade was declined
        /// </summary>
        public event EventHandler<Trade_DeclineTrade_S2C_EventArgs> Trade_DeclineTrade_S2C;
    
        /// <summary>
        /// Fired on message type 0x0204 Trade_ResetTrade_C2S. Resets the trade, when clear all is clicked?
        /// </summary>
        public event EventHandler<Trade_ResetTrade_C2S_EventArgs> Trade_ResetTrade_C2S;
    
        /// <summary>
        /// Fired on message type 0x0205 Trade_ResetTrade_S2C. ResetTrade: The trade window was reset
        /// </summary>
        public event EventHandler<Trade_ResetTrade_S2C_EventArgs> Trade_ResetTrade_S2C;
    
        /// <summary>
        /// Fired on message type 0x0207 Trade_TradeFailure_S2C. TradeFailure: Failure to add a trade item
        /// </summary>
        public event EventHandler<Trade_TradeFailure_S2C_EventArgs> Trade_TradeFailure_S2C;
    
        /// <summary>
        /// Fired on message type 0x0208 Trade_ClearTradeAcceptance_S2C. ClearTradeAcceptance: Failure to complete a trade
        /// </summary>
        public event EventHandler<Trade_ClearTradeAcceptance_S2C_EventArgs> Trade_ClearTradeAcceptance_S2C;
    
        /// <summary>
        /// Fired on message type 0x0216 Character_ClearPlayerConsentList_C2S. Clears the player's corpse looting consent list, /consent clear
        /// </summary>
        public event EventHandler<Character_ClearPlayerConsentList_C2S_EventArgs> Character_ClearPlayerConsentList_C2S;
    
        /// <summary>
        /// Fired on message type 0x0217 Character_DisplayPlayerConsentList_C2S. Display the player's corpse looting consent list, /consent who 
        /// </summary>
        public event EventHandler<Character_DisplayPlayerConsentList_C2S_EventArgs> Character_DisplayPlayerConsentList_C2S;
    
        /// <summary>
        /// Fired on message type 0x0218 Character_RemoveFromPlayerConsentList_C2S. Remove your corpse looting permission for the given player, /consent remove 
        /// </summary>
        public event EventHandler<Character_RemoveFromPlayerConsentList_C2S_EventArgs> Character_RemoveFromPlayerConsentList_C2S;
    
        /// <summary>
        /// Fired on message type 0x0219 Character_AddPlayerPermission_C2S. Grants a player corpse looting permission, /permit add
        /// </summary>
        public event EventHandler<Character_AddPlayerPermission_C2S_EventArgs> Character_AddPlayerPermission_C2S;
    
        /// <summary>
        /// Fired on message type 0x021C House_BuyHouse_C2S. Buy a house
        /// </summary>
        public event EventHandler<House_BuyHouse_C2S_EventArgs> House_BuyHouse_C2S;
    
        /// <summary>
        /// Fired on message type 0x021D House_HouseProfile_S2C. Buy a dwelling or pay maintenance
        /// </summary>
        public event EventHandler<House_HouseProfile_S2C_EventArgs> House_HouseProfile_S2C;
    
        /// <summary>
        /// Fired on message type 0x021E House_QueryHouse_C2S. Query your house info, during signin
        /// </summary>
        public event EventHandler<House_QueryHouse_C2S_EventArgs> House_QueryHouse_C2S;
    
        /// <summary>
        /// Fired on message type 0x021F House_AbandonHouse_C2S. Abandons your house
        /// </summary>
        public event EventHandler<House_AbandonHouse_C2S_EventArgs> House_AbandonHouse_C2S;
    
        /// <summary>
        /// Fired on message type 0x0220 Character_RemovePlayerPermission_C2S. Revokes a player's corpse looting permission, /permit remove
        /// </summary>
        public event EventHandler<Character_RemovePlayerPermission_C2S_EventArgs> Character_RemovePlayerPermission_C2S;
    
        /// <summary>
        /// Fired on message type 0x0221 House_RentHouse_C2S. Pay rent for a house
        /// </summary>
        public event EventHandler<House_RentHouse_C2S_EventArgs> House_RentHouse_C2S;
    
        /// <summary>
        /// Fired on message type 0x0224 Character_SetDesiredComponentLevel_C2S. Sets a new fill complevel for a component
        /// </summary>
        public event EventHandler<Character_SetDesiredComponentLevel_C2S_EventArgs> Character_SetDesiredComponentLevel_C2S;
    
        /// <summary>
        /// Fired on message type 0x0225 House_HouseData_S2C. House panel information for owners.
        /// </summary>
        public event EventHandler<House_HouseData_S2C_EventArgs> House_HouseData_S2C;
    
        /// <summary>
        /// Fired on message type 0x0226 House_HouseStatus_S2C. House Data
        /// </summary>
        public event EventHandler<House_HouseStatus_S2C_EventArgs> House_HouseStatus_S2C;
    
        /// <summary>
        /// Fired on message type 0x0227 House_UpdateRentTime_S2C. Update Rent Time
        /// </summary>
        public event EventHandler<House_UpdateRentTime_S2C_EventArgs> House_UpdateRentTime_S2C;
    
        /// <summary>
        /// Fired on message type 0x0228 House_UpdateRentPayment_S2C. Update Rent Payment
        /// </summary>
        public event EventHandler<House_UpdateRentPayment_S2C_EventArgs> House_UpdateRentPayment_S2C;
    
        /// <summary>
        /// Fired on message type 0x0245 House_AddPermanentGuest_C2S. Adds a guest to your house guest list
        /// </summary>
        public event EventHandler<House_AddPermanentGuest_C2S_EventArgs> House_AddPermanentGuest_C2S;
    
        /// <summary>
        /// Fired on message type 0x0246 House_RemovePermanentGuest_C2S. Removes a specific player from your house guest list, /house guest remove
        /// </summary>
        public event EventHandler<House_RemovePermanentGuest_C2S_EventArgs> House_RemovePermanentGuest_C2S;
    
        /// <summary>
        /// Fired on message type 0x0247 House_SetOpenHouseStatus_C2S. Sets your house open status
        /// </summary>
        public event EventHandler<House_SetOpenHouseStatus_C2S_EventArgs> House_SetOpenHouseStatus_C2S;
    
        /// <summary>
        /// Fired on message type 0x0248 House_UpdateRestrictions_S2C. Update Restrictions
        /// </summary>
        public event EventHandler<House_UpdateRestrictions_S2C_EventArgs> House_UpdateRestrictions_S2C;
    
        /// <summary>
        /// Fired on message type 0x0249 House_ChangeStoragePermission_C2S. Changes a specific players storage permission, /house storage add/remove
        /// </summary>
        public event EventHandler<House_ChangeStoragePermission_C2S_EventArgs> House_ChangeStoragePermission_C2S;
    
        /// <summary>
        /// Fired on message type 0x024A House_BootSpecificHouseGuest_C2S. Boots a specific player from your house /house boot
        /// </summary>
        public event EventHandler<House_BootSpecificHouseGuest_C2S_EventArgs> House_BootSpecificHouseGuest_C2S;
    
        /// <summary>
        /// Fired on message type 0x024C House_RemoveAllStoragePermission_C2S. Removes all storage permissions, /house storage remove_all
        /// </summary>
        public event EventHandler<House_RemoveAllStoragePermission_C2S_EventArgs> House_RemoveAllStoragePermission_C2S;
    
        /// <summary>
        /// Fired on message type 0x024D House_RequestFullGuestList_C2S. Requests your full guest list, /house guest list
        /// </summary>
        public event EventHandler<House_RequestFullGuestList_C2S_EventArgs> House_RequestFullGuestList_C2S;
    
        /// <summary>
        /// Fired on message type 0x0254 Allegiance_SetMotd_C2S. Sets the allegiance message of the day, /allegiance motd set
        /// </summary>
        public event EventHandler<Allegiance_SetMotd_C2S_EventArgs> Allegiance_SetMotd_C2S;
    
        /// <summary>
        /// Fired on message type 0x0255 Allegiance_QueryMotd_C2S. Query the motd, /allegiance motd
        /// </summary>
        public event EventHandler<Allegiance_QueryMotd_C2S_EventArgs> Allegiance_QueryMotd_C2S;
    
        /// <summary>
        /// Fired on message type 0x0256 Allegiance_ClearMotd_C2S. Clear the motd, /allegiance motd clear
        /// </summary>
        public event EventHandler<Allegiance_ClearMotd_C2S_EventArgs> Allegiance_ClearMotd_C2S;
    
        /// <summary>
        /// Fired on message type 0x0257 House_UpdateHAR_S2C. House Guest List
        /// </summary>
        public event EventHandler<House_UpdateHAR_S2C_EventArgs> House_UpdateHAR_S2C;
    
        /// <summary>
        /// Fired on message type 0x0258 House_QueryLord_C2S. Gets SlumLord info, sent after getting a failed house transaction
        /// </summary>
        public event EventHandler<House_QueryLord_C2S_EventArgs> House_QueryLord_C2S;
    
        /// <summary>
        /// Fired on message type 0x0259 House_HouseTransaction_S2C. House Profile
        /// </summary>
        public event EventHandler<House_HouseTransaction_S2C_EventArgs> House_HouseTransaction_S2C;
    
        /// <summary>
        /// Fired on message type 0x025C House_AddAllStoragePermission_C2S. Adds all to your storage permissions, /house storage add -all
        /// </summary>
        public event EventHandler<House_AddAllStoragePermission_C2S_EventArgs> House_AddAllStoragePermission_C2S;
    
        /// <summary>
        /// Fired on message type 0x025E House_RemoveAllPermanentGuests_C2S. Removes all guests, /house guest remove_all
        /// </summary>
        public event EventHandler<House_RemoveAllPermanentGuests_C2S_EventArgs> House_RemoveAllPermanentGuests_C2S;
    
        /// <summary>
        /// Fired on message type 0x025F House_BootEveryone_C2S. Boot everyone from your house, /house boot -all
        /// </summary>
        public event EventHandler<House_BootEveryone_C2S_EventArgs> House_BootEveryone_C2S;
    
        /// <summary>
        /// Fired on message type 0x0262 House_TeleToHouse_C2S. Teleports you to your house, /house recall
        /// </summary>
        public event EventHandler<House_TeleToHouse_C2S_EventArgs> House_TeleToHouse_C2S;
    
        /// <summary>
        /// Fired on message type 0x0263 Item_QueryItemMana_C2S. Queries an item's mana
        /// </summary>
        public event EventHandler<Item_QueryItemMana_C2S_EventArgs> Item_QueryItemMana_C2S;
    
        /// <summary>
        /// Fired on message type 0x0264 Item_QueryItemManaResponse_S2C. Update an item's mana bar.
        /// </summary>
        public event EventHandler<Item_QueryItemManaResponse_S2C_EventArgs> Item_QueryItemManaResponse_S2C;
    
        /// <summary>
        /// Fired on message type 0x0266 House_SetHooksVisibility_C2S. Modify whether house hooks are visibile or not, /house hooks on/off
        /// </summary>
        public event EventHandler<House_SetHooksVisibility_C2S_EventArgs> House_SetHooksVisibility_C2S;
    
        /// <summary>
        /// Fired on message type 0x0267 House_ModifyAllegianceGuestPermission_C2S. Modify whether allegiance members are guests, /house guest add_allegiance/remove_allegiance
        /// </summary>
        public event EventHandler<House_ModifyAllegianceGuestPermission_C2S_EventArgs> House_ModifyAllegianceGuestPermission_C2S;
    
        /// <summary>
        /// Fired on message type 0x0268 House_ModifyAllegianceStoragePermission_C2S. Modify whether allegiance members can access storage, /house storage add_allegiance/remove_allegiance
        /// </summary>
        public event EventHandler<House_ModifyAllegianceStoragePermission_C2S_EventArgs> House_ModifyAllegianceStoragePermission_C2S;
    
        /// <summary>
        /// Fired on message type 0x0269 Game_Join_C2S. Joins a chess game
        /// </summary>
        public event EventHandler<Game_Join_C2S_EventArgs> Game_Join_C2S;
    
        /// <summary>
        /// Fired on message type 0x026A Game_Quit_C2S. Quits a chess game
        /// </summary>
        public event EventHandler<Game_Quit_C2S_EventArgs> Game_Quit_C2S;
    
        /// <summary>
        /// Fired on message type 0x026B Game_Move_C2S. Makes a chess move
        /// </summary>
        public event EventHandler<Game_Move_C2S_EventArgs> Game_Move_C2S;
    
        /// <summary>
        /// Fired on message type 0x026D Game_MovePass_C2S. Skip a move?
        /// </summary>
        public event EventHandler<Game_MovePass_C2S_EventArgs> Game_MovePass_C2S;
    
        /// <summary>
        /// Fired on message type 0x026E Game_Stalemate_C2S. Offer or confirm stalemate
        /// </summary>
        public event EventHandler<Game_Stalemate_C2S_EventArgs> Game_Stalemate_C2S;
    
        /// <summary>
        /// Fired on message type 0x0270 House_ListAvailableHouses_C2S. Lists available house /house available
        /// </summary>
        public event EventHandler<House_ListAvailableHouses_C2S_EventArgs> House_ListAvailableHouses_C2S;
    
        /// <summary>
        /// Fired on message type 0x0271 House_AvailableHouses_S2C. Display a list of available dwellings in the chat window.
        /// </summary>
        public event EventHandler<House_AvailableHouses_S2C_EventArgs> House_AvailableHouses_S2C;
    
        /// <summary>
        /// Fired on message type 0x0274 Character_ConfirmationRequest_S2C. Display a confirmation panel.
        /// </summary>
        public event EventHandler<Character_ConfirmationRequest_S2C_EventArgs> Character_ConfirmationRequest_S2C;
    
        /// <summary>
        /// Fired on message type 0x0275 Character_ConfirmationResponse_C2S. Confirms a dialog
        /// </summary>
        public event EventHandler<Character_ConfirmationResponse_C2S_EventArgs> Character_ConfirmationResponse_C2S;
    
        /// <summary>
        /// Fired on message type 0x0276 Character_ConfirmationDone_S2C. Confirmation done
        /// </summary>
        public event EventHandler<Character_ConfirmationDone_S2C_EventArgs> Character_ConfirmationDone_S2C;
    
        /// <summary>
        /// Fired on message type 0x0277 Allegiance_BreakAllegianceBoot_C2S. Boots a player from the allegiance, optionally all characters on their account
        /// </summary>
        public event EventHandler<Allegiance_BreakAllegianceBoot_C2S_EventArgs> Allegiance_BreakAllegianceBoot_C2S;
    
        /// <summary>
        /// Fired on message type 0x0278 House_TeleToMansion_C2S. Teleports player to their allegiance housing, /house mansion_recall
        /// </summary>
        public event EventHandler<House_TeleToMansion_C2S_EventArgs> House_TeleToMansion_C2S;
    
        /// <summary>
        /// Fired on message type 0x0279 Character_Suicide_C2S. Player is commiting suicide
        /// </summary>
        public event EventHandler<Character_Suicide_C2S_EventArgs> Character_Suicide_C2S;
    
        /// <summary>
        /// Fired on message type 0x027A Allegiance_AllegianceLoginNotificationEvent_S2C. Display an allegiance login/logout message in the chat window.
        /// </summary>
        public event EventHandler<Allegiance_AllegianceLoginNotificationEvent_S2C_EventArgs> Allegiance_AllegianceLoginNotificationEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x027B Allegiance_AllegianceInfoRequest_C2S. Request allegiance info for a player
        /// </summary>
        public event EventHandler<Allegiance_AllegianceInfoRequest_C2S_EventArgs> Allegiance_AllegianceInfoRequest_C2S;
    
        /// <summary>
        /// Fired on message type 0x027C Allegiance_AllegianceInfoResponseEvent_S2C. Returns data for a player's allegiance information
        /// </summary>
        public event EventHandler<Allegiance_AllegianceInfoResponseEvent_S2C_EventArgs> Allegiance_AllegianceInfoResponseEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0x027D Inventory_CreateTinkeringTool_C2S. Salvages items
        /// </summary>
        public event EventHandler<Inventory_CreateTinkeringTool_C2S_EventArgs> Inventory_CreateTinkeringTool_C2S;
    
        /// <summary>
        /// Fired on message type 0x0281 Game_JoinGameResponse_S2C. Joining game response
        /// </summary>
        public event EventHandler<Game_JoinGameResponse_S2C_EventArgs> Game_JoinGameResponse_S2C;
    
        /// <summary>
        /// Fired on message type 0x0282 Game_StartGame_S2C. Start game
        /// </summary>
        public event EventHandler<Game_StartGame_S2C_EventArgs> Game_StartGame_S2C;
    
        /// <summary>
        /// Fired on message type 0x0283 Game_MoveResponse_S2C. Move response
        /// </summary>
        public event EventHandler<Game_MoveResponse_S2C_EventArgs> Game_MoveResponse_S2C;
    
        /// <summary>
        /// Fired on message type 0x0284 Game_OpponentTurn_S2C. Opponent Turn
        /// </summary>
        public event EventHandler<Game_OpponentTurn_S2C_EventArgs> Game_OpponentTurn_S2C;
    
        /// <summary>
        /// Fired on message type 0x0285 Game_OpponentStalemateState_S2C. Opponent Stalemate State
        /// </summary>
        public event EventHandler<Game_OpponentStalemateState_S2C_EventArgs> Game_OpponentStalemateState_S2C;
    
        /// <summary>
        /// Fired on message type 0x0286 Character_SpellbookFilterEvent_C2S. Changes the spell book filter
        /// </summary>
        public event EventHandler<Character_SpellbookFilterEvent_C2S_EventArgs> Character_SpellbookFilterEvent_C2S;
    
        /// <summary>
        /// Fired on message type 0x028A Communication_WeenieError_S2C. Display a status message in the chat window.
        /// </summary>
        public event EventHandler<Communication_WeenieError_S2C_EventArgs> Communication_WeenieError_S2C;
    
        /// <summary>
        /// Fired on message type 0x028B Communication_WeenieErrorWithString_S2C. Display a parameterized status message in the chat window.
        /// </summary>
        public event EventHandler<Communication_WeenieErrorWithString_S2C_EventArgs> Communication_WeenieErrorWithString_S2C;
    
        /// <summary>
        /// Fired on message type 0x028C Game_GameOver_S2C. End of Chess game
        /// </summary>
        public event EventHandler<Game_GameOver_S2C_EventArgs> Game_GameOver_S2C;
    
        /// <summary>
        /// Fired on message type 0x028D Character_TeleToMarketplace_C2S. Teleport to the marketplace
        /// </summary>
        public event EventHandler<Character_TeleToMarketplace_C2S_EventArgs> Character_TeleToMarketplace_C2S;
    
        /// <summary>
        /// Fired on message type 0x028F Character_EnterPKLite_C2S. Enter PKLite mode
        /// </summary>
        public event EventHandler<Character_EnterPKLite_C2S_EventArgs> Character_EnterPKLite_C2S;
    
        /// <summary>
        /// Fired on message type 0x0290 Fellowship_AssignNewLeader_C2S. Fellowship Assign a new leader
        /// </summary>
        public event EventHandler<Fellowship_AssignNewLeader_C2S_EventArgs> Fellowship_AssignNewLeader_C2S;
    
        /// <summary>
        /// Fired on message type 0x0291 Fellowship_ChangeFellowOpeness_C2S. Fellowship Change openness
        /// </summary>
        public event EventHandler<Fellowship_ChangeFellowOpeness_C2S_EventArgs> Fellowship_ChangeFellowOpeness_C2S;
    
        /// <summary>
        /// Fired on message type 0x0295 Communication_ChatRoomTracker_S2C. Set Turbine Chat channel numbers.
        /// </summary>
        public event EventHandler<Communication_ChatRoomTracker_S2C_EventArgs> Communication_ChatRoomTracker_S2C;
    
        /// <summary>
        /// Fired on message type 0x02A0 Allegiance_AllegianceChatBoot_C2S. Boots a player from the allegiance chat
        /// </summary>
        public event EventHandler<Allegiance_AllegianceChatBoot_C2S_EventArgs> Allegiance_AllegianceChatBoot_C2S;
    
        /// <summary>
        /// Fired on message type 0x02A1 Allegiance_AddAllegianceBan_C2S. Bans a player from the allegiance
        /// </summary>
        public event EventHandler<Allegiance_AddAllegianceBan_C2S_EventArgs> Allegiance_AddAllegianceBan_C2S;
    
        /// <summary>
        /// Fired on message type 0x02A2 Allegiance_RemoveAllegianceBan_C2S. Removes a player ban from the allegiance
        /// </summary>
        public event EventHandler<Allegiance_RemoveAllegianceBan_C2S_EventArgs> Allegiance_RemoveAllegianceBan_C2S;
    
        /// <summary>
        /// Fired on message type 0x02A3 Allegiance_ListAllegianceBans_C2S. Display allegiance bans
        /// </summary>
        public event EventHandler<Allegiance_ListAllegianceBans_C2S_EventArgs> Allegiance_ListAllegianceBans_C2S;
    
        /// <summary>
        /// Fired on message type 0x02A5 Allegiance_RemoveAllegianceOfficer_C2S. Removes an allegiance officer
        /// </summary>
        public event EventHandler<Allegiance_RemoveAllegianceOfficer_C2S_EventArgs> Allegiance_RemoveAllegianceOfficer_C2S;
    
        /// <summary>
        /// Fired on message type 0x02A6 Allegiance_ListAllegianceOfficers_C2S. List allegiance officers
        /// </summary>
        public event EventHandler<Allegiance_ListAllegianceOfficers_C2S_EventArgs> Allegiance_ListAllegianceOfficers_C2S;
    
        /// <summary>
        /// Fired on message type 0x02A7 Allegiance_ClearAllegianceOfficers_C2S. Clear allegiance officers
        /// </summary>
        public event EventHandler<Allegiance_ClearAllegianceOfficers_C2S_EventArgs> Allegiance_ClearAllegianceOfficers_C2S;
    
        /// <summary>
        /// Fired on message type 0x02AB Allegiance_RecallAllegianceHometown_C2S. Recalls to players allegiance hometown
        /// </summary>
        public event EventHandler<Allegiance_RecallAllegianceHometown_C2S_EventArgs> Allegiance_RecallAllegianceHometown_C2S;
    
        /// <summary>
        /// Fired on message type 0x02AE Admin_QueryPluginList_S2C. TODO: QueryPluginList
        /// </summary>
        public event EventHandler<Admin_QueryPluginList_S2C_EventArgs> Admin_QueryPluginList_S2C;
    
        /// <summary>
        /// Fired on message type 0x02AF Admin_QueryPluginListResponse_C2S. Admin Returns a plugin list response
        /// </summary>
        public event EventHandler<Admin_QueryPluginListResponse_C2S_EventArgs> Admin_QueryPluginListResponse_C2S;
    
        /// <summary>
        /// Fired on message type 0x02B1 Admin_QueryPlugin_S2C. TODO: QueryPlugin
        /// </summary>
        public event EventHandler<Admin_QueryPlugin_S2C_EventArgs> Admin_QueryPlugin_S2C;
    
        /// <summary>
        /// Fired on message type 0x02B2 Admin_QueryPluginResponse_C2S. Admin Returns plugin info
        /// </summary>
        public event EventHandler<Admin_QueryPluginResponse_C2S_EventArgs> Admin_QueryPluginResponse_C2S;
    
        /// <summary>
        /// Fired on message type 0x02B3 Admin_QueryPluginResponse2_S2C. TODO: QueryPluginResponse
        /// </summary>
        public event EventHandler<Admin_QueryPluginResponse2_S2C_EventArgs> Admin_QueryPluginResponse2_S2C;
    
        /// <summary>
        /// Fired on message type 0x02B4 Inventory_SalvageOperationsResultData_S2C. Salvage operation results
        /// </summary>
        public event EventHandler<Inventory_SalvageOperationsResultData_S2C_EventArgs> Inventory_SalvageOperationsResultData_S2C;
    
        /// <summary>
        /// Fired on message type 0x02B8 Qualities_PrivateRemoveInt64Event_S2C. Private Remove Int Event
        /// </summary>
        public event EventHandler<Qualities_PrivateRemoveInt64Event_S2C_EventArgs> Qualities_PrivateRemoveInt64Event_S2C;
    
        /// <summary>
        /// Fired on message type 0x02B9 Qualities_RemoveInt64Event_S2C. Private Remove Int Event
        /// </summary>
        public event EventHandler<Qualities_RemoveInt64Event_S2C_EventArgs> Qualities_RemoveInt64Event_S2C;
    
        /// <summary>
        /// Fired on message type 0x02BB Communication_HearSpeech_S2C. A message to be displayed in the chat window, spoken by a nearby player, NPC or creature
        /// </summary>
        public event EventHandler<Communication_HearSpeech_S2C_EventArgs> Communication_HearSpeech_S2C;
    
        /// <summary>
        /// Fired on message type 0x02BC Communication_HearRangedSpeech_S2C. A message to be displayed in the chat window, spoken by a nearby player, NPC or creature
        /// </summary>
        public event EventHandler<Communication_HearRangedSpeech_S2C_EventArgs> Communication_HearRangedSpeech_S2C;
    
        /// <summary>
        /// Fired on message type 0x02BD Communication_HearDirectSpeech_S2C. Someone has sent you a @tell.
        /// </summary>
        public event EventHandler<Communication_HearDirectSpeech_S2C_EventArgs> Communication_HearDirectSpeech_S2C;
    
        /// <summary>
        /// Fired on message type 0x02BE Fellowship_FullUpdate_S2C. Create or join a fellowship
        /// </summary>
        public event EventHandler<Fellowship_FullUpdate_S2C_EventArgs> Fellowship_FullUpdate_S2C;
    
        /// <summary>
        /// Fired on message type 0x02BF Fellowship_Disband_S2C. Disband your fellowship.
        /// </summary>
        public event EventHandler<Fellowship_Disband_S2C_EventArgs> Fellowship_Disband_S2C;
    
        /// <summary>
        /// Fired on message type 0x02C0 Fellowship_UpdateFellow_S2C. Add/Update a member to your fellowship.
        /// </summary>
        public event EventHandler<Fellowship_UpdateFellow_S2C_EventArgs> Fellowship_UpdateFellow_S2C;
    
        /// <summary>
        /// Fired on message type 0x02C1 Magic_UpdateSpell_S2C. Add a spell to your spellbook.
        /// </summary>
        public event EventHandler<Magic_UpdateSpell_S2C_EventArgs> Magic_UpdateSpell_S2C;
    
        /// <summary>
        /// Fired on message type 0x02C2 Magic_UpdateEnchantment_S2C. Apply an enchantment to your character.
        /// </summary>
        public event EventHandler<Magic_UpdateEnchantment_S2C_EventArgs> Magic_UpdateEnchantment_S2C;
    
        /// <summary>
        /// Fired on message type 0x02C3 Magic_RemoveEnchantment_S2C. Remove an enchantment from your character.
        /// </summary>
        public event EventHandler<Magic_RemoveEnchantment_S2C_EventArgs> Magic_RemoveEnchantment_S2C;
    
        /// <summary>
        /// Fired on message type 0x02C4 Magic_UpdateMultipleEnchantments_S2C. Update multiple enchantments from your character.
        /// </summary>
        public event EventHandler<Magic_UpdateMultipleEnchantments_S2C_EventArgs> Magic_UpdateMultipleEnchantments_S2C;
    
        /// <summary>
        /// Fired on message type 0x02C5 Magic_RemoveMultipleEnchantments_S2C. Remove multiple enchantments from your character.
        /// </summary>
        public event EventHandler<Magic_RemoveMultipleEnchantments_S2C_EventArgs> Magic_RemoveMultipleEnchantments_S2C;
    
        /// <summary>
        /// Fired on message type 0x02C6 Magic_PurgeEnchantments_S2C. Silently remove all enchantments from your character, e.g. when you die (no message in the chat window).
        /// </summary>
        public event EventHandler<Magic_PurgeEnchantments_S2C_EventArgs> Magic_PurgeEnchantments_S2C;
    
        /// <summary>
        /// Fired on message type 0x02C7 Magic_DispelEnchantment_S2C. Silently remove An enchantment from your character.
        /// </summary>
        public event EventHandler<Magic_DispelEnchantment_S2C_EventArgs> Magic_DispelEnchantment_S2C;
    
        /// <summary>
        /// Fired on message type 0x02C8 Magic_DispelMultipleEnchantments_S2C. Silently remove multiple enchantments from your character (no message in the chat window).
        /// </summary>
        public event EventHandler<Magic_DispelMultipleEnchantments_S2C_EventArgs> Magic_DispelMultipleEnchantments_S2C;
    
        /// <summary>
        /// Fired on message type 0x02C9 Misc_PortalStormBrewing_S2C. A portal storm is brewing.
        /// </summary>
        public event EventHandler<Misc_PortalStormBrewing_S2C_EventArgs> Misc_PortalStormBrewing_S2C;
    
        /// <summary>
        /// Fired on message type 0x02CA Misc_PortalStormImminent_S2C. A portal storm is imminent.
        /// </summary>
        public event EventHandler<Misc_PortalStormImminent_S2C_EventArgs> Misc_PortalStormImminent_S2C;
    
        /// <summary>
        /// Fired on message type 0x02CB Misc_PortalStorm_S2C. You have been portal stormed.
        /// </summary>
        public event EventHandler<Misc_PortalStorm_S2C_EventArgs> Misc_PortalStorm_S2C;
    
        /// <summary>
        /// Fired on message type 0x02CC Misc_PortalStormSubsided_S2C. The portal storm has subsided.
        /// </summary>
        public event EventHandler<Misc_PortalStormSubsided_S2C_EventArgs> Misc_PortalStormSubsided_S2C;
    
        /// <summary>
        /// Fired on message type 0x02CD Qualities_PrivateUpdateInt_S2C. Set or update a Character Int property value
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateInt_S2C_EventArgs> Qualities_PrivateUpdateInt_S2C;
    
        /// <summary>
        /// Fired on message type 0x02CE Qualities_UpdateInt_S2C. Set or update an Object Int property value
        /// </summary>
        public event EventHandler<Qualities_UpdateInt_S2C_EventArgs> Qualities_UpdateInt_S2C;
    
        /// <summary>
        /// Fired on message type 0x02CF Qualities_PrivateUpdateInt64_S2C. Set or update a Character Int64 property value
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateInt64_S2C_EventArgs> Qualities_PrivateUpdateInt64_S2C;
    
        /// <summary>
        /// Fired on message type 0x02D0 Qualities_UpdateInt64_S2C. Set or update a Character Int64 property value
        /// </summary>
        public event EventHandler<Qualities_UpdateInt64_S2C_EventArgs> Qualities_UpdateInt64_S2C;
    
        /// <summary>
        /// Fired on message type 0x02D1 Qualities_PrivateUpdateBool_S2C. Set or update a Character Boolean property value
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateBool_S2C_EventArgs> Qualities_PrivateUpdateBool_S2C;
    
        /// <summary>
        /// Fired on message type 0x02D2 Qualities_UpdateBool_S2C. Set or update an Object Boolean property value
        /// </summary>
        public event EventHandler<Qualities_UpdateBool_S2C_EventArgs> Qualities_UpdateBool_S2C;
    
        /// <summary>
        /// Fired on message type 0x02D3 Qualities_PrivateUpdateFloat_S2C. Set or update an Object float property value
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateFloat_S2C_EventArgs> Qualities_PrivateUpdateFloat_S2C;
    
        /// <summary>
        /// Fired on message type 0x02D4 Qualities_UpdateFloat_S2C. Set or update an Object float property value
        /// </summary>
        public event EventHandler<Qualities_UpdateFloat_S2C_EventArgs> Qualities_UpdateFloat_S2C;
    
        /// <summary>
        /// Fired on message type 0x02D5 Qualities_PrivateUpdateString_S2C. Set or update an Object String property value
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateString_S2C_EventArgs> Qualities_PrivateUpdateString_S2C;
    
        /// <summary>
        /// Fired on message type 0x02D6 Qualities_UpdateString_S2C. Set or update an Object String property value
        /// </summary>
        public event EventHandler<Qualities_UpdateString_S2C_EventArgs> Qualities_UpdateString_S2C;
    
        /// <summary>
        /// Fired on message type 0x02D7 Qualities_PrivateUpdateDataID_S2C. Set or update an Object DID property value
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateDataID_S2C_EventArgs> Qualities_PrivateUpdateDataID_S2C;
    
        /// <summary>
        /// Fired on message type 0x02D8 Qualities_UpdateDataID_S2C. Set or update an Object DID property value
        /// </summary>
        public event EventHandler<Qualities_UpdateDataID_S2C_EventArgs> Qualities_UpdateDataID_S2C;
    
        /// <summary>
        /// Fired on message type 0x02D9 Qualities_PrivateUpdateInstanceID_S2C. Set or update a IID property value
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateInstanceID_S2C_EventArgs> Qualities_PrivateUpdateInstanceID_S2C;
    
        /// <summary>
        /// Fired on message type 0x02DA Qualities_UpdateInstanceID_S2C. Set or update an Object IID property value
        /// </summary>
        public event EventHandler<Qualities_UpdateInstanceID_S2C_EventArgs> Qualities_UpdateInstanceID_S2C;
    
        /// <summary>
        /// Fired on message type 0x02DB Qualities_PrivateUpdatePosition_S2C. Set or update a Character Position property value
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdatePosition_S2C_EventArgs> Qualities_PrivateUpdatePosition_S2C;
    
        /// <summary>
        /// Fired on message type 0x02DC Qualities_UpdatePosition_S2C. Set or update a Character Position property value
        /// </summary>
        public event EventHandler<Qualities_UpdatePosition_S2C_EventArgs> Qualities_UpdatePosition_S2C;
    
        /// <summary>
        /// Fired on message type 0x02DD Qualities_PrivateUpdateSkill_S2C. Set or update a Character Skill value
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateSkill_S2C_EventArgs> Qualities_PrivateUpdateSkill_S2C;
    
        /// <summary>
        /// Fired on message type 0x02DE Qualities_UpdateSkill_S2C. Set or update a Character Skill value
        /// </summary>
        public event EventHandler<Qualities_UpdateSkill_S2C_EventArgs> Qualities_UpdateSkill_S2C;
    
        /// <summary>
        /// Fired on message type 0x02DF Qualities_PrivateUpdateSkillLevel_S2C. Set or update a Character Skill Level
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateSkillLevel_S2C_EventArgs> Qualities_PrivateUpdateSkillLevel_S2C;
    
        /// <summary>
        /// Fired on message type 0x02E0 Qualities_UpdateSkillLevel_S2C. Set or update a Character Skill Level
        /// </summary>
        public event EventHandler<Qualities_UpdateSkillLevel_S2C_EventArgs> Qualities_UpdateSkillLevel_S2C;
    
        /// <summary>
        /// Fired on message type 0x02E1 Qualities_PrivateUpdateSkillAC_S2C. Set or update a Character Skill state
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateSkillAC_S2C_EventArgs> Qualities_PrivateUpdateSkillAC_S2C;
    
        /// <summary>
        /// Fired on message type 0x02E2 Qualities_UpdateSkillAC_S2C. Set or update a Character Skill state
        /// </summary>
        public event EventHandler<Qualities_UpdateSkillAC_S2C_EventArgs> Qualities_UpdateSkillAC_S2C;
    
        /// <summary>
        /// Fired on message type 0x02E3 Qualities_PrivateUpdateAttribute_S2C. Set or update a Character Attribute value
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateAttribute_S2C_EventArgs> Qualities_PrivateUpdateAttribute_S2C;
    
        /// <summary>
        /// Fired on message type 0x02E4 Qualities_UpdateAttribute_S2C. Set or update a Character Attribute value
        /// </summary>
        public event EventHandler<Qualities_UpdateAttribute_S2C_EventArgs> Qualities_UpdateAttribute_S2C;
    
        /// <summary>
        /// Fired on message type 0x02E5 Qualities_PrivateUpdateAttributeLevel_S2C. Set or update a Character Attribute Level
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateAttributeLevel_S2C_EventArgs> Qualities_PrivateUpdateAttributeLevel_S2C;
    
        /// <summary>
        /// Fired on message type 0x02E6 Qualities_UpdateAttributeLevel_S2C. Set or update a Character Attribute Level
        /// </summary>
        public event EventHandler<Qualities_UpdateAttributeLevel_S2C_EventArgs> Qualities_UpdateAttributeLevel_S2C;
    
        /// <summary>
        /// Fired on message type 0x02E7 Qualities_PrivateUpdateAttribute2nd_S2C. Set or update a Character Vital value
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateAttribute2nd_S2C_EventArgs> Qualities_PrivateUpdateAttribute2nd_S2C;
    
        /// <summary>
        /// Fired on message type 0x02E8 Qualities_UpdateAttribute2nd_S2C. Set or update a Character Vital value
        /// </summary>
        public event EventHandler<Qualities_UpdateAttribute2nd_S2C_EventArgs> Qualities_UpdateAttribute2nd_S2C;
    
        /// <summary>
        /// Fired on message type 0x02E9 Qualities_PrivateUpdateAttribute2ndLevel_S2C. Set or update a Character Vital value
        /// </summary>
        public event EventHandler<Qualities_PrivateUpdateAttribute2ndLevel_S2C_EventArgs> Qualities_PrivateUpdateAttribute2ndLevel_S2C;
    
        /// <summary>
        /// Fired on message type 0x02EA Qualities_UpdateAttribute2ndLevel_S2C. Set or update a Character Vital value
        /// </summary>
        public event EventHandler<Qualities_UpdateAttribute2ndLevel_S2C_EventArgs> Qualities_UpdateAttribute2ndLevel_S2C;
    
        /// <summary>
        /// Fired on message type 0x02EB Communication_TransientString_S2C. Display a status message on the Action Viewscreen (the red text overlaid on the 3D area).
        /// </summary>
        public event EventHandler<Communication_TransientString_S2C_EventArgs> Communication_TransientString_S2C;
    
        /// <summary>
        /// Fired on message type 0x0311 Character_FinishBarber_C2S. Completes the barber interaction
        /// </summary>
        public event EventHandler<Character_FinishBarber_C2S_EventArgs> Character_FinishBarber_C2S;
    
        /// <summary>
        /// Fired on message type 0x0312 Magic_PurgeBadEnchantments_S2C. Remove all bad enchantments from your character.
        /// </summary>
        public event EventHandler<Magic_PurgeBadEnchantments_S2C_EventArgs> Magic_PurgeBadEnchantments_S2C;
    
        /// <summary>
        /// Fired on message type 0x0314 Social_SendClientContractTrackerTable_S2C. Sends all contract data
        /// </summary>
        public event EventHandler<Social_SendClientContractTrackerTable_S2C_EventArgs> Social_SendClientContractTrackerTable_S2C;
    
        /// <summary>
        /// Fired on message type 0x0315 Social_SendClientContractTracker_S2C. Updates a contract data
        /// </summary>
        public event EventHandler<Social_SendClientContractTracker_S2C_EventArgs> Social_SendClientContractTracker_S2C;
    
        /// <summary>
        /// Fired on message type 0x0316 Social_AbandonContract_C2S. Abandons a contract
        /// </summary>
        public event EventHandler<Social_AbandonContract_C2S_EventArgs> Social_AbandonContract_C2S;
    
        /// <summary>
        /// Fired on message type 0xEA60 Admin_Environs_S2C. This appears to be an admin command to change the environment (light, fog, sounds, colors)
        /// </summary>
        public event EventHandler<Admin_Environs_S2C_EventArgs> Admin_Environs_S2C;
    
        /// <summary>
        /// Fired on message type 0xF619 Movement_PositionAndMovementEvent_S2C. Sets both the position and movement, such as when materializing at a lifestone
        /// </summary>
        public event EventHandler<Movement_PositionAndMovementEvent_S2C_EventArgs> Movement_PositionAndMovementEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0xF61B Movement_Jump_C2S. Performs a jump
        /// </summary>
        public event EventHandler<Movement_Jump_C2S_EventArgs> Movement_Jump_C2S;
    
        /// <summary>
        /// Fired on message type 0xF61C Movement_MoveToState_C2S. Move to state data
        /// </summary>
        public event EventHandler<Movement_MoveToState_C2S_EventArgs> Movement_MoveToState_C2S;
    
        /// <summary>
        /// Fired on message type 0xF61E Movement_DoMovementCommand_C2S. Performs a movement based on input
        /// </summary>
        public event EventHandler<Movement_DoMovementCommand_C2S_EventArgs> Movement_DoMovementCommand_C2S;
    
        /// <summary>
        /// Fired on message type 0xF625 Item_ObjDescEvent_S2C. Sent whenever a character changes their clothes. It contains the entire description of what their wearing (and possibly their facial features as well). This message is only sent for changes, when the character is first created, the body of this message is included inside the creation message.
        /// </summary>
        public event EventHandler<Item_ObjDescEvent_S2C_EventArgs> Item_ObjDescEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0xF630 Character_SetPlayerVisualDesc_S2C. Sets the player visual desc, TODO confirm this
        /// </summary>
        public event EventHandler<Character_SetPlayerVisualDesc_S2C_EventArgs> Character_SetPlayerVisualDesc_S2C;
    
        /// <summary>
        /// Fired on message type 0xF643 Character_CharGenVerificationResponse_S2C. Character creation screen initilised.
        /// </summary>
        public event EventHandler<Character_CharGenVerificationResponse_S2C_EventArgs> Character_CharGenVerificationResponse_S2C;
    
        /// <summary>
        /// Fired on message type 0xF651 Login_AwaitingSubscriptionExpiration_S2C. Sent when your subsciption is about to expire
        /// </summary>
        public event EventHandler<Login_AwaitingSubscriptionExpiration_S2C_EventArgs> Login_AwaitingSubscriptionExpiration_S2C;
    
        /// <summary>
        /// Fired on message type 0xF653 Login_LogOffCharacter_C2S. Instructs the client to return to 2D mode - the character list.
        /// </summary>
        public event EventHandler<Login_LogOffCharacter_C2S_EventArgs> Login_LogOffCharacter_C2S;
    
        /// <summary>
        /// Fired on message type 0xF653 Login_LogOffCharacter_S2C. Instructs the client to return to 2D mode - the character list.
        /// </summary>
        public event EventHandler<Login_LogOffCharacter_S2C_EventArgs> Login_LogOffCharacter_S2C;
    
        /// <summary>
        /// Fired on message type 0xF655 Character_CharacterDelete_C2S. Mark a character for deletetion.
        /// </summary>
        public event EventHandler<Character_CharacterDelete_C2S_EventArgs> Character_CharacterDelete_C2S;
    
        /// <summary>
        /// Fired on message type 0xF655 Character_CharacterDelete_S2C. A character was marked for deletetion.
        /// </summary>
        public event EventHandler<Character_CharacterDelete_S2C_EventArgs> Character_CharacterDelete_S2C;
    
        /// <summary>
        /// Fired on message type 0xF656 Character_SendCharGenResult_C2S. Character creation result
        /// </summary>
        public event EventHandler<Character_SendCharGenResult_C2S_EventArgs> Character_SendCharGenResult_C2S;
    
        /// <summary>
        /// Fired on message type 0xF657 Login_SendEnterWorld_C2S. The character to log in.
        /// </summary>
        public event EventHandler<Login_SendEnterWorld_C2S_EventArgs> Login_SendEnterWorld_C2S;
    
        /// <summary>
        /// Fired on message type 0xF658 Login_LoginCharacterSet_S2C. The list of characters on the current account.
        /// </summary>
        public event EventHandler<Login_LoginCharacterSet_S2C_EventArgs> Login_LoginCharacterSet_S2C;
    
        /// <summary>
        /// Fired on message type 0xF659 Character_CharacterError_S2C. Failure to log in
        /// </summary>
        public event EventHandler<Character_CharacterError_S2C_EventArgs> Character_CharacterError_S2C;
    
        /// <summary>
        /// Fired on message type 0xF661 Movement_StopMovementCommand_C2S. Stops a movement
        /// </summary>
        public event EventHandler<Movement_StopMovementCommand_C2S_EventArgs> Movement_StopMovementCommand_C2S;
    
        /// <summary>
        /// Fired on message type 0xF6EA Object_SendForceObjdesc_C2S. Asks server for a new object description
        /// </summary>
        public event EventHandler<Object_SendForceObjdesc_C2S_EventArgs> Object_SendForceObjdesc_C2S;
    
        /// <summary>
        /// Fired on message type 0xF745 Item_CreateObject_S2C. Create an object somewhere in the world
        /// </summary>
        public event EventHandler<Item_CreateObject_S2C_EventArgs> Item_CreateObject_S2C;
    
        /// <summary>
        /// Fired on message type 0xF746 Login_CreatePlayer_S2C. Login of player
        /// </summary>
        public event EventHandler<Login_CreatePlayer_S2C_EventArgs> Login_CreatePlayer_S2C;
    
        /// <summary>
        /// Fired on message type 0xF747 Item_DeleteObject_S2C. Sent whenever an object is being deleted from the scene.
        /// </summary>
        public event EventHandler<Item_DeleteObject_S2C_EventArgs> Item_DeleteObject_S2C;
    
        /// <summary>
        /// Fired on message type 0xF748 Movement_PositionEvent_S2C. Sets the position/motion of an object
        /// </summary>
        public event EventHandler<Movement_PositionEvent_S2C_EventArgs> Movement_PositionEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0xF749 Item_ParentEvent_S2C. Sets the parent for an object, eg. equipting an object.
        /// </summary>
        public event EventHandler<Item_ParentEvent_S2C_EventArgs> Item_ParentEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0xF74A Inventory_PickupEvent_S2C. Sent when picking up an object
        /// </summary>
        public event EventHandler<Inventory_PickupEvent_S2C_EventArgs> Inventory_PickupEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0xF74B Item_SetState_S2C. Set's the current state of the object. Client appears to only process the following state changes post creation: NoDraw, LightingOn, Hidden
        /// </summary>
        public event EventHandler<Item_SetState_S2C_EventArgs> Item_SetState_S2C;
    
        /// <summary>
        /// Fired on message type 0xF74C Movement_SetObjectMovement_S2C. These are animations. Whenever a human, monster or object moves - one of these little messages is sent. Even idle emotes (like head scratching and nodding) are sent in this manner.
        /// </summary>
        public event EventHandler<Movement_SetObjectMovement_S2C_EventArgs> Movement_SetObjectMovement_S2C;
    
        /// <summary>
        /// Fired on message type 0xF74E Movement_VectorUpdate_S2C. Changes an objects vector, for things like jumping
        /// </summary>
        public event EventHandler<Movement_VectorUpdate_S2C_EventArgs> Movement_VectorUpdate_S2C;
    
        /// <summary>
        /// Fired on message type 0xF750 Effects_SoundEvent_S2C. Applies a sound effect.
        /// </summary>
        public event EventHandler<Effects_SoundEvent_S2C_EventArgs> Effects_SoundEvent_S2C;
    
        /// <summary>
        /// Fired on message type 0xF751 Effects_PlayerTeleport_S2C. Instructs the client to show the portal graphic.
        /// </summary>
        public event EventHandler<Effects_PlayerTeleport_S2C_EventArgs> Effects_PlayerTeleport_S2C;
    
        /// <summary>
        /// Fired on message type 0xF752 Movement_AutonomyLevel_C2S. Sets an autonomy level
        /// </summary>
        public event EventHandler<Movement_AutonomyLevel_C2S_EventArgs> Movement_AutonomyLevel_C2S;
    
        /// <summary>
        /// Fired on message type 0xF753 Movement_AutonomousPosition_C2S. Sends an autonomous position
        /// </summary>
        public event EventHandler<Movement_AutonomousPosition_C2S_EventArgs> Movement_AutonomousPosition_C2S;
    
        /// <summary>
        /// Fired on message type 0xF754 Effects_PlayScriptID_S2C. Instructs the client to play a script. (Not seen so far, maybe prefered PlayScriptType)
        /// </summary>
        public event EventHandler<Effects_PlayScriptID_S2C_EventArgs> Effects_PlayScriptID_S2C;
    
        /// <summary>
        /// Fired on message type 0xF755 Effects_PlayScriptType_S2C. Applies an effect with visual and sound by providing the type to be looked up in the Physics Script Table
        /// </summary>
        public event EventHandler<Effects_PlayScriptType_S2C_EventArgs> Effects_PlayScriptType_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7C1 Login_AccountBanned_S2C. Account has been banned
        /// </summary>
        public event EventHandler<Login_AccountBanned_S2C_EventArgs> Login_AccountBanned_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7C8 Login_SendEnterWorldRequest_C2S. The user has clicked 'Enter'. This message does not contain the ID of the character logging on; that comes later.
        /// </summary>
        public event EventHandler<Login_SendEnterWorldRequest_C2S_EventArgs> Login_SendEnterWorldRequest_C2S;
    
        /// <summary>
        /// Fired on message type 0xF7C9 Movement_Jump_NonAutonomous_C2S. Performs a non autonomous jump
        /// </summary>
        public event EventHandler<Movement_Jump_NonAutonomous_C2S_EventArgs> Movement_Jump_NonAutonomous_C2S;
    
        /// <summary>
        /// Fired on message type 0xF7CA Admin_ReceiveAccountData_S2C. Admin Receive Account Data
        /// </summary>
        public event EventHandler<Admin_ReceiveAccountData_S2C_EventArgs> Admin_ReceiveAccountData_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7CB Admin_ReceivePlayerData_S2C. Admin Receive Player Data
        /// </summary>
        public event EventHandler<Admin_ReceivePlayerData_S2C_EventArgs> Admin_ReceivePlayerData_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7CC Admin_SendAdminGetServerVersion_C2S. Sent if player is an PSR, I assume it displays the server version number
        /// </summary>
        public event EventHandler<Admin_SendAdminGetServerVersion_C2S_EventArgs> Admin_SendAdminGetServerVersion_C2S;
    
        /// <summary>
        /// Fired on message type 0xF7CD Social_SendFriendsCommand_C2S. Seems to be a legacy friends command, /friends old, for when Jan 2006 event changed the friends list
        /// </summary>
        public event EventHandler<Social_SendFriendsCommand_C2S_EventArgs> Social_SendFriendsCommand_C2S;
    
        /// <summary>
        /// Fired on message type 0xF7D9 Admin_SendAdminRestoreCharacter_C2S. Admin command to restore a character
        /// </summary>
        public event EventHandler<Admin_SendAdminRestoreCharacter_C2S_EventArgs> Admin_SendAdminRestoreCharacter_C2S;
    
        /// <summary>
        /// Fired on message type 0xF7DB Item_UpdateObject_S2C. Update an existing object's data.
        /// </summary>
        public event EventHandler<Item_UpdateObject_S2C_EventArgs> Item_UpdateObject_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7DC Login_AccountBooted_S2C. Account has been booted
        /// </summary>
        public event EventHandler<Login_AccountBooted_S2C_EventArgs> Login_AccountBooted_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7DE Communication_TurbineChat_S2C. Send or receive a message using Turbine Chat.
        /// </summary>
        public event EventHandler<Communication_TurbineChat_S2C_EventArgs> Communication_TurbineChat_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7DF Login_EnterGame_ServerReady_S2C. Switch from the character display to the game display.
        /// </summary>
        public event EventHandler<Login_EnterGame_ServerReady_S2C_EventArgs> Login_EnterGame_ServerReady_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7E0 Communication_TextboxString_S2C. Display a message in the chat window.
        /// </summary>
        public event EventHandler<Communication_TextboxString_S2C_EventArgs> Communication_TextboxString_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7E1 Login_WorldInfo_S2C. The name of the current world.
        /// </summary>
        public event EventHandler<Login_WorldInfo_S2C_EventArgs> Login_WorldInfo_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7E2 DDD_DataMessage_S2C. Add or update a dat file Resource.
        /// </summary>
        public event EventHandler<DDD_DataMessage_S2C_EventArgs> DDD_DataMessage_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7E3 DDD_RequestDataMessage_C2S. DDD request for data
        /// </summary>
        public event EventHandler<DDD_RequestDataMessage_C2S_EventArgs> DDD_RequestDataMessage_C2S;
    
        /// <summary>
        /// Fired on message type 0xF7E4 DDD_ErrorMessage_S2C. DDD error
        /// </summary>
        public event EventHandler<DDD_ErrorMessage_S2C_EventArgs> DDD_ErrorMessage_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7E5 DDD_InterrogationMessage_S2C. Add or update a dat file Resource.
        /// </summary>
        public event EventHandler<DDD_InterrogationMessage_S2C_EventArgs> DDD_InterrogationMessage_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7E6 DDD_InterrogationResponseMessage_C2S. TODO
        /// </summary>
        public event EventHandler<DDD_InterrogationResponseMessage_C2S_EventArgs> DDD_InterrogationResponseMessage_C2S;
    
        /// <summary>
        /// Fired on message type 0xF7E7 DDD_BeginDDDMessage_S2C. A list of dat files that need to be patched
        /// </summary>
        public event EventHandler<DDD_BeginDDDMessage_S2C_EventArgs> DDD_BeginDDDMessage_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7EA DDD_OnEndDDD_S2C. Ends DDD update
        /// </summary>
        public event EventHandler<DDD_OnEndDDD_S2C_EventArgs> DDD_OnEndDDD_S2C;
    
        /// <summary>
        /// Fired on message type 0xF7EB DDD_EndDDDMessage_C2S. Ends DDD update
        /// </summary>
        public event EventHandler<DDD_EndDDDMessage_C2S_EventArgs> DDD_EndDDDMessage_C2S;
    
        public void HandleIncomingMessageData(BinaryReader buffer) {
            IACDataType data;
            var opcode = (MessageType)buffer.ReadUInt32();
            Logger.Log($"Got opcode:{opcode} {((uint)opcode):X4}");

            switch (opcode) {
                case (MessageType)0xF7B0: // Value indicating this message has a sequencing header
                    var _objectId = buffer.ReadUInt32(); // Current unused
                    var _sequence = buffer.ReadUInt32(); // Currently unused
                    opcode = (MessageType)buffer.ReadUInt32();
                    Logger.Log($"Got 0xF7B0 event:{opcode} {((uint)opcode):X4}");
                    switch(opcode) {
                        case MessageType.Allegiance_AllegianceUpdateAborted: // System.Boolean True
                            data = new Allegiance_AllegianceUpdateAborted_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Allegiance_AllegianceUpdateAborted_S2C?.Invoke(this, new Allegiance_AllegianceUpdateAborted_S2C_EventArgs(data as Allegiance_AllegianceUpdateAborted_S2C));
                            break;
        
                        case MessageType.Communication_PopUpString: // System.Boolean True
                            data = new Communication_PopUpString_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Communication_PopUpString_S2C?.Invoke(this, new Communication_PopUpString_S2C_EventArgs(data as Communication_PopUpString_S2C));
                            break;
        
                        case MessageType.Login_PlayerDescription: // System.Boolean True
                            data = new Login_PlayerDescription_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Login_PlayerDescription_S2C?.Invoke(this, new Login_PlayerDescription_S2C_EventArgs(data as Login_PlayerDescription_S2C));
                            break;
        
                        case MessageType.Allegiance_AllegianceUpdate: // System.Boolean True
                            data = new Allegiance_AllegianceUpdate_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Allegiance_AllegianceUpdate_S2C?.Invoke(this, new Allegiance_AllegianceUpdate_S2C_EventArgs(data as Allegiance_AllegianceUpdate_S2C));
                            break;
        
                        case MessageType.Social_FriendsUpdate: // System.Boolean True
                            data = new Social_FriendsUpdate_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Social_FriendsUpdate_S2C?.Invoke(this, new Social_FriendsUpdate_S2C_EventArgs(data as Social_FriendsUpdate_S2C));
                            break;
        
                        case MessageType.Item_ServerSaysContainID: // System.Boolean True
                            data = new Item_ServerSaysContainID_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Item_ServerSaysContainID_S2C?.Invoke(this, new Item_ServerSaysContainID_S2C_EventArgs(data as Item_ServerSaysContainID_S2C));
                            break;
        
                        case MessageType.Item_WearItem: // System.Boolean True
                            data = new Item_WearItem_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Item_WearItem_S2C?.Invoke(this, new Item_WearItem_S2C_EventArgs(data as Item_WearItem_S2C));
                            break;
        
                        case MessageType.Social_CharacterTitleTable: // System.Boolean True
                            data = new Social_CharacterTitleTable_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Social_CharacterTitleTable_S2C?.Invoke(this, new Social_CharacterTitleTable_S2C_EventArgs(data as Social_CharacterTitleTable_S2C));
                            break;
        
                        case MessageType.Social_AddOrSetCharacterTitle: // System.Boolean True
                            data = new Social_AddOrSetCharacterTitle_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Social_AddOrSetCharacterTitle_S2C?.Invoke(this, new Social_AddOrSetCharacterTitle_S2C_EventArgs(data as Social_AddOrSetCharacterTitle_S2C));
                            break;
        
                        case MessageType.Item_StopViewingObjectContents: // System.Boolean True
                            data = new Item_StopViewingObjectContents_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Item_StopViewingObjectContents_S2C?.Invoke(this, new Item_StopViewingObjectContents_S2C_EventArgs(data as Item_StopViewingObjectContents_S2C));
                            break;
        
                        case MessageType.Vendor_VendorInfo: // System.Boolean True
                            data = new Vendor_VendorInfo_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Vendor_VendorInfo_S2C?.Invoke(this, new Vendor_VendorInfo_S2C_EventArgs(data as Vendor_VendorInfo_S2C));
                            break;
        
                        case MessageType.Character_StartBarber: // System.Boolean True
                            data = new Character_StartBarber_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Character_StartBarber_S2C?.Invoke(this, new Character_StartBarber_S2C_EventArgs(data as Character_StartBarber_S2C));
                            break;
        
                        case MessageType.Fellowship_Quit: // System.Boolean True
                            data = new Fellowship_Quit_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Fellowship_Quit_S2C?.Invoke(this, new Fellowship_Quit_S2C_EventArgs(data as Fellowship_Quit_S2C));
                            break;
        
                        case MessageType.Fellowship_Dismiss: // System.Boolean True
                            data = new Fellowship_Dismiss_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Fellowship_Dismiss_S2C?.Invoke(this, new Fellowship_Dismiss_S2C_EventArgs(data as Fellowship_Dismiss_S2C));
                            break;
        
                        case MessageType.Writing_BookOpen: // System.Boolean True
                            data = new Writing_BookOpen_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Writing_BookOpen_S2C?.Invoke(this, new Writing_BookOpen_S2C_EventArgs(data as Writing_BookOpen_S2C));
                            break;
        
                        case MessageType.Writing_BookAddPageResponse: // System.Boolean True
                            data = new Writing_BookAddPageResponse_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Writing_BookAddPageResponse_S2C?.Invoke(this, new Writing_BookAddPageResponse_S2C_EventArgs(data as Writing_BookAddPageResponse_S2C));
                            break;
        
                        case MessageType.Writing_BookDeletePageResponse: // System.Boolean True
                            data = new Writing_BookDeletePageResponse_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Writing_BookDeletePageResponse_S2C?.Invoke(this, new Writing_BookDeletePageResponse_S2C_EventArgs(data as Writing_BookDeletePageResponse_S2C));
                            break;
        
                        case MessageType.Writing_BookPageDataResponse: // System.Boolean True
                            data = new Writing_BookPageDataResponse_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Writing_BookPageDataResponse_S2C?.Invoke(this, new Writing_BookPageDataResponse_S2C_EventArgs(data as Writing_BookPageDataResponse_S2C));
                            break;
        
                        case MessageType.Item_GetInscriptionResponse: // System.Boolean True
                            data = new Item_GetInscriptionResponse_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Item_GetInscriptionResponse_S2C?.Invoke(this, new Item_GetInscriptionResponse_S2C_EventArgs(data as Item_GetInscriptionResponse_S2C));
                            break;
        
                        case MessageType.Item_SetAppraiseInfo: // System.Boolean True
                            data = new Item_SetAppraiseInfo_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Item_SetAppraiseInfo_S2C?.Invoke(this, new Item_SetAppraiseInfo_S2C_EventArgs(data as Item_SetAppraiseInfo_S2C));
                            break;
        
                        case MessageType.Communication_ChannelBroadcast: // System.Boolean True
                            data = new Communication_ChannelBroadcast_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Communication_ChannelBroadcast_S2C?.Invoke(this, new Communication_ChannelBroadcast_S2C_EventArgs(data as Communication_ChannelBroadcast_S2C));
                            break;
        
                        case MessageType.Communication_ChannelList: // System.Boolean True
                            data = new Communication_ChannelList_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Communication_ChannelList_S2C?.Invoke(this, new Communication_ChannelList_S2C_EventArgs(data as Communication_ChannelList_S2C));
                            break;
        
                        case MessageType.Communication_ChannelIndex: // System.Boolean True
                            data = new Communication_ChannelIndex_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Communication_ChannelIndex_S2C?.Invoke(this, new Communication_ChannelIndex_S2C_EventArgs(data as Communication_ChannelIndex_S2C));
                            break;
        
                        case MessageType.Item_OnViewContents: // System.Boolean True
                            data = new Item_OnViewContents_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Item_OnViewContents_S2C?.Invoke(this, new Item_OnViewContents_S2C_EventArgs(data as Item_OnViewContents_S2C));
                            break;
        
                        case MessageType.Item_ServerSaysMoveItem: // System.Boolean True
                            data = new Item_ServerSaysMoveItem_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Item_ServerSaysMoveItem_S2C?.Invoke(this, new Item_ServerSaysMoveItem_S2C_EventArgs(data as Item_ServerSaysMoveItem_S2C));
                            break;
        
                        case MessageType.Combat_HandleAttackDoneEvent: // System.Boolean True
                            data = new Combat_HandleAttackDoneEvent_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Combat_HandleAttackDoneEvent_S2C?.Invoke(this, new Combat_HandleAttackDoneEvent_S2C_EventArgs(data as Combat_HandleAttackDoneEvent_S2C));
                            break;
        
                        case MessageType.Magic_RemoveSpell: // System.Boolean True
                            data = new Magic_RemoveSpell_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Magic_RemoveSpell_S2C?.Invoke(this, new Magic_RemoveSpell_S2C_EventArgs(data as Magic_RemoveSpell_S2C));
                            break;
        
                        case MessageType.Combat_HandleVictimNotificationEventSelf: // System.Boolean True
                            data = new Combat_HandleVictimNotificationEventSelf_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Combat_HandleVictimNotificationEventSelf_S2C?.Invoke(this, new Combat_HandleVictimNotificationEventSelf_S2C_EventArgs(data as Combat_HandleVictimNotificationEventSelf_S2C));
                            break;
        
                        case MessageType.Combat_HandleVictimNotificationEventOther: // System.Boolean True
                            data = new Combat_HandleVictimNotificationEventOther_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Combat_HandleVictimNotificationEventOther_S2C?.Invoke(this, new Combat_HandleVictimNotificationEventOther_S2C_EventArgs(data as Combat_HandleVictimNotificationEventOther_S2C));
                            break;
        
                        case MessageType.Combat_HandleAttackerNotificationEvent: // System.Boolean True
                            data = new Combat_HandleAttackerNotificationEvent_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Combat_HandleAttackerNotificationEvent_S2C?.Invoke(this, new Combat_HandleAttackerNotificationEvent_S2C_EventArgs(data as Combat_HandleAttackerNotificationEvent_S2C));
                            break;
        
                        case MessageType.Combat_HandleDefenderNotificationEvent: // System.Boolean True
                            data = new Combat_HandleDefenderNotificationEvent_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Combat_HandleDefenderNotificationEvent_S2C?.Invoke(this, new Combat_HandleDefenderNotificationEvent_S2C_EventArgs(data as Combat_HandleDefenderNotificationEvent_S2C));
                            break;
        
                        case MessageType.Combat_HandleEvasionAttackerNotificationEvent: // System.Boolean True
                            data = new Combat_HandleEvasionAttackerNotificationEvent_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Combat_HandleEvasionAttackerNotificationEvent_S2C?.Invoke(this, new Combat_HandleEvasionAttackerNotificationEvent_S2C_EventArgs(data as Combat_HandleEvasionAttackerNotificationEvent_S2C));
                            break;
        
                        case MessageType.Combat_HandleEvasionDefenderNotificationEvent: // System.Boolean True
                            data = new Combat_HandleEvasionDefenderNotificationEvent_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Combat_HandleEvasionDefenderNotificationEvent_S2C?.Invoke(this, new Combat_HandleEvasionDefenderNotificationEvent_S2C_EventArgs(data as Combat_HandleEvasionDefenderNotificationEvent_S2C));
                            break;
        
                        case MessageType.Combat_HandleCommenceAttackEvent: // System.Boolean True
                            data = new Combat_HandleCommenceAttackEvent_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Combat_HandleCommenceAttackEvent_S2C?.Invoke(this, new Combat_HandleCommenceAttackEvent_S2C_EventArgs(data as Combat_HandleCommenceAttackEvent_S2C));
                            break;
        
                        case MessageType.Combat_QueryHealthResponse: // System.Boolean True
                            data = new Combat_QueryHealthResponse_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Combat_QueryHealthResponse_S2C?.Invoke(this, new Combat_QueryHealthResponse_S2C_EventArgs(data as Combat_QueryHealthResponse_S2C));
                            break;
        
                        case MessageType.Character_QueryAgeResponse: // System.Boolean True
                            data = new Character_QueryAgeResponse_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Character_QueryAgeResponse_S2C?.Invoke(this, new Character_QueryAgeResponse_S2C_EventArgs(data as Character_QueryAgeResponse_S2C));
                            break;
        
                        case MessageType.Item_UseDone: // System.Boolean True
                            data = new Item_UseDone_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Item_UseDone_S2C?.Invoke(this, new Item_UseDone_S2C_EventArgs(data as Item_UseDone_S2C));
                            break;
        
                        case MessageType.Fellowship_FellowUpdateDone: // System.Boolean True
                            data = new Fellowship_FellowUpdateDone_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Fellowship_FellowUpdateDone_S2C?.Invoke(this, new Fellowship_FellowUpdateDone_S2C_EventArgs(data as Fellowship_FellowUpdateDone_S2C));
                            break;
        
                        case MessageType.Fellowship_FellowStatsDone: // System.Boolean True
                            data = new Fellowship_FellowStatsDone_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Fellowship_FellowStatsDone_S2C?.Invoke(this, new Fellowship_FellowStatsDone_S2C_EventArgs(data as Fellowship_FellowStatsDone_S2C));
                            break;
        
                        case MessageType.Item_AppraiseDone: // System.Boolean True
                            data = new Item_AppraiseDone_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Item_AppraiseDone_S2C?.Invoke(this, new Item_AppraiseDone_S2C_EventArgs(data as Item_AppraiseDone_S2C));
                            break;
        
                        case MessageType.Character_ReturnPing: // System.Boolean True
                            data = new Character_ReturnPing_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Character_ReturnPing_S2C?.Invoke(this, new Character_ReturnPing_S2C_EventArgs(data as Character_ReturnPing_S2C));
                            break;
        
                        case MessageType.Communication_SetSquelchDB: // System.Boolean True
                            data = new Communication_SetSquelchDB_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Communication_SetSquelchDB_S2C?.Invoke(this, new Communication_SetSquelchDB_S2C_EventArgs(data as Communication_SetSquelchDB_S2C));
                            break;
        
                        case MessageType.Trade_RegisterTrade: // System.Boolean True
                            data = new Trade_RegisterTrade_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Trade_RegisterTrade_S2C?.Invoke(this, new Trade_RegisterTrade_S2C_EventArgs(data as Trade_RegisterTrade_S2C));
                            break;
        
                        case MessageType.Trade_OpenTrade: // System.Boolean True
                            data = new Trade_OpenTrade_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Trade_OpenTrade_S2C?.Invoke(this, new Trade_OpenTrade_S2C_EventArgs(data as Trade_OpenTrade_S2C));
                            break;
        
                        case MessageType.Trade_CloseTrade: // System.Boolean True
                            data = new Trade_CloseTrade_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Trade_CloseTrade_S2C?.Invoke(this, new Trade_CloseTrade_S2C_EventArgs(data as Trade_CloseTrade_S2C));
                            break;
        
                        case MessageType.Trade_AddToTrade: // System.Boolean True
                            data = new Trade_AddToTrade_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Trade_AddToTrade_S2C?.Invoke(this, new Trade_AddToTrade_S2C_EventArgs(data as Trade_AddToTrade_S2C));
                            break;
        
                        case MessageType.Trade_RemoveFromTrade: // System.Boolean True
                            data = new Trade_RemoveFromTrade_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Trade_RemoveFromTrade_S2C?.Invoke(this, new Trade_RemoveFromTrade_S2C_EventArgs(data as Trade_RemoveFromTrade_S2C));
                            break;
        
                        case MessageType.Trade_AcceptTrade: // System.Boolean True
                            data = new Trade_AcceptTrade_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Trade_AcceptTrade_S2C?.Invoke(this, new Trade_AcceptTrade_S2C_EventArgs(data as Trade_AcceptTrade_S2C));
                            break;
        
                        case MessageType.Trade_DeclineTrade: // System.Boolean True
                            data = new Trade_DeclineTrade_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Trade_DeclineTrade_S2C?.Invoke(this, new Trade_DeclineTrade_S2C_EventArgs(data as Trade_DeclineTrade_S2C));
                            break;
        
                        case MessageType.Trade_ResetTrade: // System.Boolean True
                            data = new Trade_ResetTrade_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Trade_ResetTrade_S2C?.Invoke(this, new Trade_ResetTrade_S2C_EventArgs(data as Trade_ResetTrade_S2C));
                            break;
        
                        case MessageType.Trade_TradeFailure: // System.Boolean True
                            data = new Trade_TradeFailure_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Trade_TradeFailure_S2C?.Invoke(this, new Trade_TradeFailure_S2C_EventArgs(data as Trade_TradeFailure_S2C));
                            break;
        
                        case MessageType.Trade_ClearTradeAcceptance: // System.Boolean True
                            data = new Trade_ClearTradeAcceptance_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Trade_ClearTradeAcceptance_S2C?.Invoke(this, new Trade_ClearTradeAcceptance_S2C_EventArgs(data as Trade_ClearTradeAcceptance_S2C));
                            break;
        
                        case MessageType.House_HouseProfile: // System.Boolean True
                            data = new House_HouseProfile_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            House_HouseProfile_S2C?.Invoke(this, new House_HouseProfile_S2C_EventArgs(data as House_HouseProfile_S2C));
                            break;
        
                        case MessageType.House_HouseData: // System.Boolean True
                            data = new House_HouseData_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            House_HouseData_S2C?.Invoke(this, new House_HouseData_S2C_EventArgs(data as House_HouseData_S2C));
                            break;
        
                        case MessageType.House_HouseStatus: // System.Boolean True
                            data = new House_HouseStatus_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            House_HouseStatus_S2C?.Invoke(this, new House_HouseStatus_S2C_EventArgs(data as House_HouseStatus_S2C));
                            break;
        
                        case MessageType.House_UpdateRentTime: // System.Boolean True
                            data = new House_UpdateRentTime_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            House_UpdateRentTime_S2C?.Invoke(this, new House_UpdateRentTime_S2C_EventArgs(data as House_UpdateRentTime_S2C));
                            break;
        
                        case MessageType.House_UpdateRentPayment: // System.Boolean True
                            data = new House_UpdateRentPayment_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            House_UpdateRentPayment_S2C?.Invoke(this, new House_UpdateRentPayment_S2C_EventArgs(data as House_UpdateRentPayment_S2C));
                            break;
        
                        case MessageType.House_UpdateRestrictions: // System.Boolean True
                            data = new House_UpdateRestrictions_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            House_UpdateRestrictions_S2C?.Invoke(this, new House_UpdateRestrictions_S2C_EventArgs(data as House_UpdateRestrictions_S2C));
                            break;
        
                        case MessageType.House_UpdateHAR: // System.Boolean True
                            data = new House_UpdateHAR_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            House_UpdateHAR_S2C?.Invoke(this, new House_UpdateHAR_S2C_EventArgs(data as House_UpdateHAR_S2C));
                            break;
        
                        case MessageType.House_HouseTransaction: // System.Boolean True
                            data = new House_HouseTransaction_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            House_HouseTransaction_S2C?.Invoke(this, new House_HouseTransaction_S2C_EventArgs(data as House_HouseTransaction_S2C));
                            break;
        
                        case MessageType.Item_QueryItemManaResponse: // System.Boolean True
                            data = new Item_QueryItemManaResponse_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Item_QueryItemManaResponse_S2C?.Invoke(this, new Item_QueryItemManaResponse_S2C_EventArgs(data as Item_QueryItemManaResponse_S2C));
                            break;
        
                        case MessageType.House_AvailableHouses: // System.Boolean True
                            data = new House_AvailableHouses_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            House_AvailableHouses_S2C?.Invoke(this, new House_AvailableHouses_S2C_EventArgs(data as House_AvailableHouses_S2C));
                            break;
        
                        case MessageType.Character_ConfirmationRequest: // System.Boolean True
                            data = new Character_ConfirmationRequest_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Character_ConfirmationRequest_S2C?.Invoke(this, new Character_ConfirmationRequest_S2C_EventArgs(data as Character_ConfirmationRequest_S2C));
                            break;
        
                        case MessageType.Character_ConfirmationDone: // System.Boolean True
                            data = new Character_ConfirmationDone_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Character_ConfirmationDone_S2C?.Invoke(this, new Character_ConfirmationDone_S2C_EventArgs(data as Character_ConfirmationDone_S2C));
                            break;
        
                        case MessageType.Allegiance_AllegianceLoginNotificationEvent: // System.Boolean True
                            data = new Allegiance_AllegianceLoginNotificationEvent_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Allegiance_AllegianceLoginNotificationEvent_S2C?.Invoke(this, new Allegiance_AllegianceLoginNotificationEvent_S2C_EventArgs(data as Allegiance_AllegianceLoginNotificationEvent_S2C));
                            break;
        
                        case MessageType.Allegiance_AllegianceInfoResponseEvent: // System.Boolean True
                            data = new Allegiance_AllegianceInfoResponseEvent_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Allegiance_AllegianceInfoResponseEvent_S2C?.Invoke(this, new Allegiance_AllegianceInfoResponseEvent_S2C_EventArgs(data as Allegiance_AllegianceInfoResponseEvent_S2C));
                            break;
        
                        case MessageType.Game_JoinGameResponse: // System.Boolean True
                            data = new Game_JoinGameResponse_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Game_JoinGameResponse_S2C?.Invoke(this, new Game_JoinGameResponse_S2C_EventArgs(data as Game_JoinGameResponse_S2C));
                            break;
        
                        case MessageType.Game_StartGame: // System.Boolean True
                            data = new Game_StartGame_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Game_StartGame_S2C?.Invoke(this, new Game_StartGame_S2C_EventArgs(data as Game_StartGame_S2C));
                            break;
        
                        case MessageType.Game_MoveResponse: // System.Boolean True
                            data = new Game_MoveResponse_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Game_MoveResponse_S2C?.Invoke(this, new Game_MoveResponse_S2C_EventArgs(data as Game_MoveResponse_S2C));
                            break;
        
                        case MessageType.Game_OpponentTurn: // System.Boolean True
                            data = new Game_OpponentTurn_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Game_OpponentTurn_S2C?.Invoke(this, new Game_OpponentTurn_S2C_EventArgs(data as Game_OpponentTurn_S2C));
                            break;
        
                        case MessageType.Game_OpponentStalemateState: // System.Boolean True
                            data = new Game_OpponentStalemateState_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Game_OpponentStalemateState_S2C?.Invoke(this, new Game_OpponentStalemateState_S2C_EventArgs(data as Game_OpponentStalemateState_S2C));
                            break;
        
                        case MessageType.Communication_WeenieError: // System.Boolean True
                            data = new Communication_WeenieError_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Communication_WeenieError_S2C?.Invoke(this, new Communication_WeenieError_S2C_EventArgs(data as Communication_WeenieError_S2C));
                            break;
        
                        case MessageType.Communication_WeenieErrorWithString: // System.Boolean True
                            data = new Communication_WeenieErrorWithString_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Communication_WeenieErrorWithString_S2C?.Invoke(this, new Communication_WeenieErrorWithString_S2C_EventArgs(data as Communication_WeenieErrorWithString_S2C));
                            break;
        
                        case MessageType.Game_GameOver: // System.Boolean True
                            data = new Game_GameOver_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Game_GameOver_S2C?.Invoke(this, new Game_GameOver_S2C_EventArgs(data as Game_GameOver_S2C));
                            break;
        
                        case MessageType.Communication_ChatRoomTracker: // System.Boolean True
                            data = new Communication_ChatRoomTracker_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Communication_ChatRoomTracker_S2C?.Invoke(this, new Communication_ChatRoomTracker_S2C_EventArgs(data as Communication_ChatRoomTracker_S2C));
                            break;
        
                        case MessageType.Admin_QueryPluginList: // System.Boolean True
                            data = new Admin_QueryPluginList_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Admin_QueryPluginList_S2C?.Invoke(this, new Admin_QueryPluginList_S2C_EventArgs(data as Admin_QueryPluginList_S2C));
                            break;
        
                        case MessageType.Admin_QueryPlugin: // System.Boolean True
                            data = new Admin_QueryPlugin_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Admin_QueryPlugin_S2C?.Invoke(this, new Admin_QueryPlugin_S2C_EventArgs(data as Admin_QueryPlugin_S2C));
                            break;
        
                        case MessageType.Admin_QueryPluginResponse2: // System.Boolean True
                            data = new Admin_QueryPluginResponse2_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Admin_QueryPluginResponse2_S2C?.Invoke(this, new Admin_QueryPluginResponse2_S2C_EventArgs(data as Admin_QueryPluginResponse2_S2C));
                            break;
        
                        case MessageType.Inventory_SalvageOperationsResultData: // System.Boolean True
                            data = new Inventory_SalvageOperationsResultData_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Inventory_SalvageOperationsResultData_S2C?.Invoke(this, new Inventory_SalvageOperationsResultData_S2C_EventArgs(data as Inventory_SalvageOperationsResultData_S2C));
                            break;
        
                        case MessageType.Communication_HearDirectSpeech: // System.Boolean True
                            data = new Communication_HearDirectSpeech_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Communication_HearDirectSpeech_S2C?.Invoke(this, new Communication_HearDirectSpeech_S2C_EventArgs(data as Communication_HearDirectSpeech_S2C));
                            break;
        
                        case MessageType.Fellowship_FullUpdate: // System.Boolean True
                            data = new Fellowship_FullUpdate_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Fellowship_FullUpdate_S2C?.Invoke(this, new Fellowship_FullUpdate_S2C_EventArgs(data as Fellowship_FullUpdate_S2C));
                            break;
        
                        case MessageType.Fellowship_Disband: // System.Boolean True
                            data = new Fellowship_Disband_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Fellowship_Disband_S2C?.Invoke(this, new Fellowship_Disband_S2C_EventArgs(data as Fellowship_Disband_S2C));
                            break;
        
                        case MessageType.Fellowship_UpdateFellow: // System.Boolean True
                            data = new Fellowship_UpdateFellow_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Fellowship_UpdateFellow_S2C?.Invoke(this, new Fellowship_UpdateFellow_S2C_EventArgs(data as Fellowship_UpdateFellow_S2C));
                            break;
        
                        case MessageType.Magic_UpdateSpell: // System.Boolean True
                            data = new Magic_UpdateSpell_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Magic_UpdateSpell_S2C?.Invoke(this, new Magic_UpdateSpell_S2C_EventArgs(data as Magic_UpdateSpell_S2C));
                            break;
        
                        case MessageType.Magic_UpdateEnchantment: // System.Boolean True
                            data = new Magic_UpdateEnchantment_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Magic_UpdateEnchantment_S2C?.Invoke(this, new Magic_UpdateEnchantment_S2C_EventArgs(data as Magic_UpdateEnchantment_S2C));
                            break;
        
                        case MessageType.Magic_RemoveEnchantment: // System.Boolean True
                            data = new Magic_RemoveEnchantment_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Magic_RemoveEnchantment_S2C?.Invoke(this, new Magic_RemoveEnchantment_S2C_EventArgs(data as Magic_RemoveEnchantment_S2C));
                            break;
        
                        case MessageType.Magic_UpdateMultipleEnchantments: // System.Boolean True
                            data = new Magic_UpdateMultipleEnchantments_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Magic_UpdateMultipleEnchantments_S2C?.Invoke(this, new Magic_UpdateMultipleEnchantments_S2C_EventArgs(data as Magic_UpdateMultipleEnchantments_S2C));
                            break;
        
                        case MessageType.Magic_RemoveMultipleEnchantments: // System.Boolean True
                            data = new Magic_RemoveMultipleEnchantments_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Magic_RemoveMultipleEnchantments_S2C?.Invoke(this, new Magic_RemoveMultipleEnchantments_S2C_EventArgs(data as Magic_RemoveMultipleEnchantments_S2C));
                            break;
        
                        case MessageType.Magic_PurgeEnchantments: // System.Boolean True
                            data = new Magic_PurgeEnchantments_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Magic_PurgeEnchantments_S2C?.Invoke(this, new Magic_PurgeEnchantments_S2C_EventArgs(data as Magic_PurgeEnchantments_S2C));
                            break;
        
                        case MessageType.Magic_DispelEnchantment: // System.Boolean True
                            data = new Magic_DispelEnchantment_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Magic_DispelEnchantment_S2C?.Invoke(this, new Magic_DispelEnchantment_S2C_EventArgs(data as Magic_DispelEnchantment_S2C));
                            break;
        
                        case MessageType.Magic_DispelMultipleEnchantments: // System.Boolean True
                            data = new Magic_DispelMultipleEnchantments_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Magic_DispelMultipleEnchantments_S2C?.Invoke(this, new Magic_DispelMultipleEnchantments_S2C_EventArgs(data as Magic_DispelMultipleEnchantments_S2C));
                            break;
        
                        case MessageType.Misc_PortalStormBrewing: // System.Boolean True
                            data = new Misc_PortalStormBrewing_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Misc_PortalStormBrewing_S2C?.Invoke(this, new Misc_PortalStormBrewing_S2C_EventArgs(data as Misc_PortalStormBrewing_S2C));
                            break;
        
                        case MessageType.Misc_PortalStormImminent: // System.Boolean True
                            data = new Misc_PortalStormImminent_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Misc_PortalStormImminent_S2C?.Invoke(this, new Misc_PortalStormImminent_S2C_EventArgs(data as Misc_PortalStormImminent_S2C));
                            break;
        
                        case MessageType.Misc_PortalStorm: // System.Boolean True
                            data = new Misc_PortalStorm_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Misc_PortalStorm_S2C?.Invoke(this, new Misc_PortalStorm_S2C_EventArgs(data as Misc_PortalStorm_S2C));
                            break;
        
                        case MessageType.Misc_PortalStormSubsided: // System.Boolean True
                            data = new Misc_PortalStormSubsided_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Misc_PortalStormSubsided_S2C?.Invoke(this, new Misc_PortalStormSubsided_S2C_EventArgs(data as Misc_PortalStormSubsided_S2C));
                            break;
        
                        case MessageType.Communication_TransientString: // System.Boolean True
                            data = new Communication_TransientString_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Communication_TransientString_S2C?.Invoke(this, new Communication_TransientString_S2C_EventArgs(data as Communication_TransientString_S2C));
                            break;
        
                        case MessageType.Magic_PurgeBadEnchantments: // System.Boolean True
                            data = new Magic_PurgeBadEnchantments_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Magic_PurgeBadEnchantments_S2C?.Invoke(this, new Magic_PurgeBadEnchantments_S2C_EventArgs(data as Magic_PurgeBadEnchantments_S2C));
                            break;
        
                        case MessageType.Social_SendClientContractTrackerTable: // System.Boolean True
                            data = new Social_SendClientContractTrackerTable_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Social_SendClientContractTrackerTable_S2C?.Invoke(this, new Social_SendClientContractTrackerTable_S2C_EventArgs(data as Social_SendClientContractTrackerTable_S2C));
                            break;
        
                        case MessageType.Social_SendClientContractTracker: // System.Boolean True
                            data = new Social_SendClientContractTracker_S2C();
                            data.ReadFromBuffer(buffer);
                            Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                            Social_SendClientContractTracker_S2C?.Invoke(this, new Social_SendClientContractTracker_S2C_EventArgs(data as Social_SendClientContractTracker_S2C));
                            break;
        
                    }
                    break; // end 0xF7B0

                    case MessageType.Item_ServerSaysRemove:
                        data = new Item_ServerSaysRemove_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Item_ServerSaysRemove_S2C?.Invoke(this, new Item_ServerSaysRemove_S2C_EventArgs(data as Item_ServerSaysRemove_S2C));
                        break;
    
                    case MessageType.Character_ServerSaysAttemptFailed:
                        data = new Character_ServerSaysAttemptFailed_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Character_ServerSaysAttemptFailed_S2C?.Invoke(this, new Character_ServerSaysAttemptFailed_S2C_EventArgs(data as Character_ServerSaysAttemptFailed_S2C));
                        break;
    
                    case MessageType.Item_UpdateStackSize:
                        data = new Item_UpdateStackSize_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Item_UpdateStackSize_S2C?.Invoke(this, new Item_UpdateStackSize_S2C_EventArgs(data as Item_UpdateStackSize_S2C));
                        break;
    
                    case MessageType.Combat_HandlePlayerDeathEvent:
                        data = new Combat_HandlePlayerDeathEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Combat_HandlePlayerDeathEvent_S2C?.Invoke(this, new Combat_HandlePlayerDeathEvent_S2C_EventArgs(data as Combat_HandlePlayerDeathEvent_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateRemoveIntEvent:
                        data = new Qualities_PrivateRemoveIntEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateRemoveIntEvent_S2C?.Invoke(this, new Qualities_PrivateRemoveIntEvent_S2C_EventArgs(data as Qualities_PrivateRemoveIntEvent_S2C));
                        break;
    
                    case MessageType.Qualities_RemoveIntEvent:
                        data = new Qualities_RemoveIntEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_RemoveIntEvent_S2C?.Invoke(this, new Qualities_RemoveIntEvent_S2C_EventArgs(data as Qualities_RemoveIntEvent_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateRemoveBoolEvent:
                        data = new Qualities_PrivateRemoveBoolEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateRemoveBoolEvent_S2C?.Invoke(this, new Qualities_PrivateRemoveBoolEvent_S2C_EventArgs(data as Qualities_PrivateRemoveBoolEvent_S2C));
                        break;
    
                    case MessageType.Qualities_RemoveBoolEvent:
                        data = new Qualities_RemoveBoolEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_RemoveBoolEvent_S2C?.Invoke(this, new Qualities_RemoveBoolEvent_S2C_EventArgs(data as Qualities_RemoveBoolEvent_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateRemoveFloatEvent:
                        data = new Qualities_PrivateRemoveFloatEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateRemoveFloatEvent_S2C?.Invoke(this, new Qualities_PrivateRemoveFloatEvent_S2C_EventArgs(data as Qualities_PrivateRemoveFloatEvent_S2C));
                        break;
    
                    case MessageType.Qualities_RemoveFloatEvent:
                        data = new Qualities_RemoveFloatEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_RemoveFloatEvent_S2C?.Invoke(this, new Qualities_RemoveFloatEvent_S2C_EventArgs(data as Qualities_RemoveFloatEvent_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateRemoveStringEvent:
                        data = new Qualities_PrivateRemoveStringEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateRemoveStringEvent_S2C?.Invoke(this, new Qualities_PrivateRemoveStringEvent_S2C_EventArgs(data as Qualities_PrivateRemoveStringEvent_S2C));
                        break;
    
                    case MessageType.Qualities_RemoveStringEvent:
                        data = new Qualities_RemoveStringEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_RemoveStringEvent_S2C?.Invoke(this, new Qualities_RemoveStringEvent_S2C_EventArgs(data as Qualities_RemoveStringEvent_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateRemoveDataIDEvent:
                        data = new Qualities_PrivateRemoveDataIDEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateRemoveDataIDEvent_S2C?.Invoke(this, new Qualities_PrivateRemoveDataIDEvent_S2C_EventArgs(data as Qualities_PrivateRemoveDataIDEvent_S2C));
                        break;
    
                    case MessageType.Qualities_RemoveDataIDEvent:
                        data = new Qualities_RemoveDataIDEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_RemoveDataIDEvent_S2C?.Invoke(this, new Qualities_RemoveDataIDEvent_S2C_EventArgs(data as Qualities_RemoveDataIDEvent_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateRemoveInstanceIDEvent:
                        data = new Qualities_PrivateRemoveInstanceIDEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateRemoveInstanceIDEvent_S2C?.Invoke(this, new Qualities_PrivateRemoveInstanceIDEvent_S2C_EventArgs(data as Qualities_PrivateRemoveInstanceIDEvent_S2C));
                        break;
    
                    case MessageType.Qualities_RemoveInstanceIDEvent:
                        data = new Qualities_RemoveInstanceIDEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_RemoveInstanceIDEvent_S2C?.Invoke(this, new Qualities_RemoveInstanceIDEvent_S2C_EventArgs(data as Qualities_RemoveInstanceIDEvent_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateRemovePositionEvent:
                        data = new Qualities_PrivateRemovePositionEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateRemovePositionEvent_S2C?.Invoke(this, new Qualities_PrivateRemovePositionEvent_S2C_EventArgs(data as Qualities_PrivateRemovePositionEvent_S2C));
                        break;
    
                    case MessageType.Qualities_RemovePositionEvent:
                        data = new Qualities_RemovePositionEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_RemovePositionEvent_S2C?.Invoke(this, new Qualities_RemovePositionEvent_S2C_EventArgs(data as Qualities_RemovePositionEvent_S2C));
                        break;
    
                    case MessageType.Communication_HearEmote:
                        data = new Communication_HearEmote_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Communication_HearEmote_S2C?.Invoke(this, new Communication_HearEmote_S2C_EventArgs(data as Communication_HearEmote_S2C));
                        break;
    
                    case MessageType.Communication_HearSoulEmote:
                        data = new Communication_HearSoulEmote_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Communication_HearSoulEmote_S2C?.Invoke(this, new Communication_HearSoulEmote_S2C_EventArgs(data as Communication_HearSoulEmote_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateRemoveInt64Event:
                        data = new Qualities_PrivateRemoveInt64Event_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateRemoveInt64Event_S2C?.Invoke(this, new Qualities_PrivateRemoveInt64Event_S2C_EventArgs(data as Qualities_PrivateRemoveInt64Event_S2C));
                        break;
    
                    case MessageType.Qualities_RemoveInt64Event:
                        data = new Qualities_RemoveInt64Event_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_RemoveInt64Event_S2C?.Invoke(this, new Qualities_RemoveInt64Event_S2C_EventArgs(data as Qualities_RemoveInt64Event_S2C));
                        break;
    
                    case MessageType.Communication_HearSpeech:
                        data = new Communication_HearSpeech_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Communication_HearSpeech_S2C?.Invoke(this, new Communication_HearSpeech_S2C_EventArgs(data as Communication_HearSpeech_S2C));
                        break;
    
                    case MessageType.Communication_HearRangedSpeech:
                        data = new Communication_HearRangedSpeech_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Communication_HearRangedSpeech_S2C?.Invoke(this, new Communication_HearRangedSpeech_S2C_EventArgs(data as Communication_HearRangedSpeech_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateInt:
                        data = new Qualities_PrivateUpdateInt_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateInt_S2C?.Invoke(this, new Qualities_PrivateUpdateInt_S2C_EventArgs(data as Qualities_PrivateUpdateInt_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateInt:
                        data = new Qualities_UpdateInt_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateInt_S2C?.Invoke(this, new Qualities_UpdateInt_S2C_EventArgs(data as Qualities_UpdateInt_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateInt64:
                        data = new Qualities_PrivateUpdateInt64_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateInt64_S2C?.Invoke(this, new Qualities_PrivateUpdateInt64_S2C_EventArgs(data as Qualities_PrivateUpdateInt64_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateInt64:
                        data = new Qualities_UpdateInt64_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateInt64_S2C?.Invoke(this, new Qualities_UpdateInt64_S2C_EventArgs(data as Qualities_UpdateInt64_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateBool:
                        data = new Qualities_PrivateUpdateBool_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateBool_S2C?.Invoke(this, new Qualities_PrivateUpdateBool_S2C_EventArgs(data as Qualities_PrivateUpdateBool_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateBool:
                        data = new Qualities_UpdateBool_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateBool_S2C?.Invoke(this, new Qualities_UpdateBool_S2C_EventArgs(data as Qualities_UpdateBool_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateFloat:
                        data = new Qualities_PrivateUpdateFloat_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateFloat_S2C?.Invoke(this, new Qualities_PrivateUpdateFloat_S2C_EventArgs(data as Qualities_PrivateUpdateFloat_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateFloat:
                        data = new Qualities_UpdateFloat_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateFloat_S2C?.Invoke(this, new Qualities_UpdateFloat_S2C_EventArgs(data as Qualities_UpdateFloat_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateString:
                        data = new Qualities_PrivateUpdateString_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateString_S2C?.Invoke(this, new Qualities_PrivateUpdateString_S2C_EventArgs(data as Qualities_PrivateUpdateString_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateString:
                        data = new Qualities_UpdateString_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateString_S2C?.Invoke(this, new Qualities_UpdateString_S2C_EventArgs(data as Qualities_UpdateString_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateDataID:
                        data = new Qualities_PrivateUpdateDataID_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateDataID_S2C?.Invoke(this, new Qualities_PrivateUpdateDataID_S2C_EventArgs(data as Qualities_PrivateUpdateDataID_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateDataID:
                        data = new Qualities_UpdateDataID_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateDataID_S2C?.Invoke(this, new Qualities_UpdateDataID_S2C_EventArgs(data as Qualities_UpdateDataID_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateInstanceID:
                        data = new Qualities_PrivateUpdateInstanceID_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateInstanceID_S2C?.Invoke(this, new Qualities_PrivateUpdateInstanceID_S2C_EventArgs(data as Qualities_PrivateUpdateInstanceID_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateInstanceID:
                        data = new Qualities_UpdateInstanceID_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateInstanceID_S2C?.Invoke(this, new Qualities_UpdateInstanceID_S2C_EventArgs(data as Qualities_UpdateInstanceID_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdatePosition:
                        data = new Qualities_PrivateUpdatePosition_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdatePosition_S2C?.Invoke(this, new Qualities_PrivateUpdatePosition_S2C_EventArgs(data as Qualities_PrivateUpdatePosition_S2C));
                        break;
    
                    case MessageType.Qualities_UpdatePosition:
                        data = new Qualities_UpdatePosition_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdatePosition_S2C?.Invoke(this, new Qualities_UpdatePosition_S2C_EventArgs(data as Qualities_UpdatePosition_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateSkill:
                        data = new Qualities_PrivateUpdateSkill_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateSkill_S2C?.Invoke(this, new Qualities_PrivateUpdateSkill_S2C_EventArgs(data as Qualities_PrivateUpdateSkill_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateSkill:
                        data = new Qualities_UpdateSkill_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateSkill_S2C?.Invoke(this, new Qualities_UpdateSkill_S2C_EventArgs(data as Qualities_UpdateSkill_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateSkillLevel:
                        data = new Qualities_PrivateUpdateSkillLevel_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateSkillLevel_S2C?.Invoke(this, new Qualities_PrivateUpdateSkillLevel_S2C_EventArgs(data as Qualities_PrivateUpdateSkillLevel_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateSkillLevel:
                        data = new Qualities_UpdateSkillLevel_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateSkillLevel_S2C?.Invoke(this, new Qualities_UpdateSkillLevel_S2C_EventArgs(data as Qualities_UpdateSkillLevel_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateSkillAC:
                        data = new Qualities_PrivateUpdateSkillAC_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateSkillAC_S2C?.Invoke(this, new Qualities_PrivateUpdateSkillAC_S2C_EventArgs(data as Qualities_PrivateUpdateSkillAC_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateSkillAC:
                        data = new Qualities_UpdateSkillAC_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateSkillAC_S2C?.Invoke(this, new Qualities_UpdateSkillAC_S2C_EventArgs(data as Qualities_UpdateSkillAC_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateAttribute:
                        data = new Qualities_PrivateUpdateAttribute_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateAttribute_S2C?.Invoke(this, new Qualities_PrivateUpdateAttribute_S2C_EventArgs(data as Qualities_PrivateUpdateAttribute_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateAttribute:
                        data = new Qualities_UpdateAttribute_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateAttribute_S2C?.Invoke(this, new Qualities_UpdateAttribute_S2C_EventArgs(data as Qualities_UpdateAttribute_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateAttributeLevel:
                        data = new Qualities_PrivateUpdateAttributeLevel_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateAttributeLevel_S2C?.Invoke(this, new Qualities_PrivateUpdateAttributeLevel_S2C_EventArgs(data as Qualities_PrivateUpdateAttributeLevel_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateAttributeLevel:
                        data = new Qualities_UpdateAttributeLevel_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateAttributeLevel_S2C?.Invoke(this, new Qualities_UpdateAttributeLevel_S2C_EventArgs(data as Qualities_UpdateAttributeLevel_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateAttribute2nd:
                        data = new Qualities_PrivateUpdateAttribute2nd_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateAttribute2nd_S2C?.Invoke(this, new Qualities_PrivateUpdateAttribute2nd_S2C_EventArgs(data as Qualities_PrivateUpdateAttribute2nd_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateAttribute2nd:
                        data = new Qualities_UpdateAttribute2nd_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateAttribute2nd_S2C?.Invoke(this, new Qualities_UpdateAttribute2nd_S2C_EventArgs(data as Qualities_UpdateAttribute2nd_S2C));
                        break;
    
                    case MessageType.Qualities_PrivateUpdateAttribute2ndLevel:
                        data = new Qualities_PrivateUpdateAttribute2ndLevel_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_PrivateUpdateAttribute2ndLevel_S2C?.Invoke(this, new Qualities_PrivateUpdateAttribute2ndLevel_S2C_EventArgs(data as Qualities_PrivateUpdateAttribute2ndLevel_S2C));
                        break;
    
                    case MessageType.Qualities_UpdateAttribute2ndLevel:
                        data = new Qualities_UpdateAttribute2ndLevel_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Qualities_UpdateAttribute2ndLevel_S2C?.Invoke(this, new Qualities_UpdateAttribute2ndLevel_S2C_EventArgs(data as Qualities_UpdateAttribute2ndLevel_S2C));
                        break;
    
                    case MessageType.Admin_Environs:
                        data = new Admin_Environs_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Admin_Environs_S2C?.Invoke(this, new Admin_Environs_S2C_EventArgs(data as Admin_Environs_S2C));
                        break;
    
                    case MessageType.Movement_PositionAndMovementEvent:
                        data = new Movement_PositionAndMovementEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Movement_PositionAndMovementEvent_S2C?.Invoke(this, new Movement_PositionAndMovementEvent_S2C_EventArgs(data as Movement_PositionAndMovementEvent_S2C));
                        break;
    
                    case MessageType.Item_ObjDescEvent:
                        data = new Item_ObjDescEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Item_ObjDescEvent_S2C?.Invoke(this, new Item_ObjDescEvent_S2C_EventArgs(data as Item_ObjDescEvent_S2C));
                        break;
    
                    case MessageType.Character_SetPlayerVisualDesc:
                        data = new Character_SetPlayerVisualDesc_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Character_SetPlayerVisualDesc_S2C?.Invoke(this, new Character_SetPlayerVisualDesc_S2C_EventArgs(data as Character_SetPlayerVisualDesc_S2C));
                        break;
    
                    case MessageType.Character_CharGenVerificationResponse:
                        data = new Character_CharGenVerificationResponse_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Character_CharGenVerificationResponse_S2C?.Invoke(this, new Character_CharGenVerificationResponse_S2C_EventArgs(data as Character_CharGenVerificationResponse_S2C));
                        break;
    
                    case MessageType.Login_AwaitingSubscriptionExpiration:
                        data = new Login_AwaitingSubscriptionExpiration_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Login_AwaitingSubscriptionExpiration_S2C?.Invoke(this, new Login_AwaitingSubscriptionExpiration_S2C_EventArgs(data as Login_AwaitingSubscriptionExpiration_S2C));
                        break;
    
                    case MessageType.Login_LogOffCharacter:
                        data = new Login_LogOffCharacter_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Login_LogOffCharacter_S2C?.Invoke(this, new Login_LogOffCharacter_S2C_EventArgs(data as Login_LogOffCharacter_S2C));
                        break;
    
                    case MessageType.Character_CharacterDelete:
                        data = new Character_CharacterDelete_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Character_CharacterDelete_S2C?.Invoke(this, new Character_CharacterDelete_S2C_EventArgs(data as Character_CharacterDelete_S2C));
                        break;
    
                    case MessageType.Login_LoginCharacterSet:
                        data = new Login_LoginCharacterSet_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Login_LoginCharacterSet_S2C?.Invoke(this, new Login_LoginCharacterSet_S2C_EventArgs(data as Login_LoginCharacterSet_S2C));
                        break;
    
                    case MessageType.Character_CharacterError:
                        data = new Character_CharacterError_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Character_CharacterError_S2C?.Invoke(this, new Character_CharacterError_S2C_EventArgs(data as Character_CharacterError_S2C));
                        break;
    
                    case MessageType.Item_CreateObject:
                        data = new Item_CreateObject_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Item_CreateObject_S2C?.Invoke(this, new Item_CreateObject_S2C_EventArgs(data as Item_CreateObject_S2C));
                        break;
    
                    case MessageType.Login_CreatePlayer:
                        data = new Login_CreatePlayer_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Login_CreatePlayer_S2C?.Invoke(this, new Login_CreatePlayer_S2C_EventArgs(data as Login_CreatePlayer_S2C));
                        break;
    
                    case MessageType.Item_DeleteObject:
                        data = new Item_DeleteObject_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Item_DeleteObject_S2C?.Invoke(this, new Item_DeleteObject_S2C_EventArgs(data as Item_DeleteObject_S2C));
                        break;
    
                    case MessageType.Movement_PositionEvent:
                        data = new Movement_PositionEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Movement_PositionEvent_S2C?.Invoke(this, new Movement_PositionEvent_S2C_EventArgs(data as Movement_PositionEvent_S2C));
                        break;
    
                    case MessageType.Item_ParentEvent:
                        data = new Item_ParentEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Item_ParentEvent_S2C?.Invoke(this, new Item_ParentEvent_S2C_EventArgs(data as Item_ParentEvent_S2C));
                        break;
    
                    case MessageType.Inventory_PickupEvent:
                        data = new Inventory_PickupEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Inventory_PickupEvent_S2C?.Invoke(this, new Inventory_PickupEvent_S2C_EventArgs(data as Inventory_PickupEvent_S2C));
                        break;
    
                    case MessageType.Item_SetState:
                        data = new Item_SetState_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Item_SetState_S2C?.Invoke(this, new Item_SetState_S2C_EventArgs(data as Item_SetState_S2C));
                        break;
    
                    case MessageType.Movement_SetObjectMovement:
                        data = new Movement_SetObjectMovement_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Movement_SetObjectMovement_S2C?.Invoke(this, new Movement_SetObjectMovement_S2C_EventArgs(data as Movement_SetObjectMovement_S2C));
                        break;
    
                    case MessageType.Movement_VectorUpdate:
                        data = new Movement_VectorUpdate_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Movement_VectorUpdate_S2C?.Invoke(this, new Movement_VectorUpdate_S2C_EventArgs(data as Movement_VectorUpdate_S2C));
                        break;
    
                    case MessageType.Effects_SoundEvent:
                        data = new Effects_SoundEvent_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Effects_SoundEvent_S2C?.Invoke(this, new Effects_SoundEvent_S2C_EventArgs(data as Effects_SoundEvent_S2C));
                        break;
    
                    case MessageType.Effects_PlayerTeleport:
                        data = new Effects_PlayerTeleport_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Effects_PlayerTeleport_S2C?.Invoke(this, new Effects_PlayerTeleport_S2C_EventArgs(data as Effects_PlayerTeleport_S2C));
                        break;
    
                    case MessageType.Effects_PlayScriptID:
                        data = new Effects_PlayScriptID_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Effects_PlayScriptID_S2C?.Invoke(this, new Effects_PlayScriptID_S2C_EventArgs(data as Effects_PlayScriptID_S2C));
                        break;
    
                    case MessageType.Effects_PlayScriptType:
                        data = new Effects_PlayScriptType_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Effects_PlayScriptType_S2C?.Invoke(this, new Effects_PlayScriptType_S2C_EventArgs(data as Effects_PlayScriptType_S2C));
                        break;
    
                    case MessageType.Login_AccountBanned:
                        data = new Login_AccountBanned_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Login_AccountBanned_S2C?.Invoke(this, new Login_AccountBanned_S2C_EventArgs(data as Login_AccountBanned_S2C));
                        break;
    
                    case MessageType.Admin_ReceiveAccountData:
                        data = new Admin_ReceiveAccountData_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Admin_ReceiveAccountData_S2C?.Invoke(this, new Admin_ReceiveAccountData_S2C_EventArgs(data as Admin_ReceiveAccountData_S2C));
                        break;
    
                    case MessageType.Admin_ReceivePlayerData:
                        data = new Admin_ReceivePlayerData_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Admin_ReceivePlayerData_S2C?.Invoke(this, new Admin_ReceivePlayerData_S2C_EventArgs(data as Admin_ReceivePlayerData_S2C));
                        break;
    
                    case MessageType.Item_UpdateObject:
                        data = new Item_UpdateObject_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Item_UpdateObject_S2C?.Invoke(this, new Item_UpdateObject_S2C_EventArgs(data as Item_UpdateObject_S2C));
                        break;
    
                    case MessageType.Login_AccountBooted:
                        data = new Login_AccountBooted_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Login_AccountBooted_S2C?.Invoke(this, new Login_AccountBooted_S2C_EventArgs(data as Login_AccountBooted_S2C));
                        break;
    
                    case MessageType.Communication_TurbineChat:
                        data = new Communication_TurbineChat_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Communication_TurbineChat_S2C?.Invoke(this, new Communication_TurbineChat_S2C_EventArgs(data as Communication_TurbineChat_S2C));
                        break;
    
                    case MessageType.Login_EnterGame_ServerReady:
                        data = new Login_EnterGame_ServerReady_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Login_EnterGame_ServerReady_S2C?.Invoke(this, new Login_EnterGame_ServerReady_S2C_EventArgs(data as Login_EnterGame_ServerReady_S2C));
                        break;
    
                    case MessageType.Communication_TextboxString:
                        data = new Communication_TextboxString_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Communication_TextboxString_S2C?.Invoke(this, new Communication_TextboxString_S2C_EventArgs(data as Communication_TextboxString_S2C));
                        break;
    
                    case MessageType.Login_WorldInfo:
                        data = new Login_WorldInfo_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        Login_WorldInfo_S2C?.Invoke(this, new Login_WorldInfo_S2C_EventArgs(data as Login_WorldInfo_S2C));
                        break;
    
                    case MessageType.DDD_DataMessage:
                        data = new DDD_DataMessage_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        DDD_DataMessage_S2C?.Invoke(this, new DDD_DataMessage_S2C_EventArgs(data as DDD_DataMessage_S2C));
                        break;
    
                    case MessageType.DDD_ErrorMessage:
                        data = new DDD_ErrorMessage_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        DDD_ErrorMessage_S2C?.Invoke(this, new DDD_ErrorMessage_S2C_EventArgs(data as DDD_ErrorMessage_S2C));
                        break;
    
                    case MessageType.DDD_InterrogationMessage:
                        data = new DDD_InterrogationMessage_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        DDD_InterrogationMessage_S2C?.Invoke(this, new DDD_InterrogationMessage_S2C_EventArgs(data as DDD_InterrogationMessage_S2C));
                        break;
    
                    case MessageType.DDD_BeginDDDMessage:
                        data = new DDD_BeginDDDMessage_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        DDD_BeginDDDMessage_S2C?.Invoke(this, new DDD_BeginDDDMessage_S2C_EventArgs(data as DDD_BeginDDDMessage_S2C));
                        break;
    
                    case MessageType.DDD_OnEndDDD:
                        data = new DDD_OnEndDDD_S2C();
                        data.ReadFromBuffer(buffer);
                        Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                        DDD_OnEndDDD_S2C?.Invoke(this, new DDD_OnEndDDD_S2C_EventArgs(data as DDD_OnEndDDD_S2C));
                        break;
    
                default:
                    var rawData = buffer.ReadBytes((int)(buffer.BaseStream.Length - buffer.BaseStream.Position));
                    UnknownMessageType?.Invoke(this, new UnknownMessageTypeEventArgs(MessageDirection.S2C, opcode, rawData));
                    break;
            }
        }
    }
}
