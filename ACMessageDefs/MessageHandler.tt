﻿<#@ assembly name="System.Core" #>
<#@ assembly name="System.Linq" #>
<#@ assembly name="$(TargetPath)" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ template language="c#" hostspecific="true" inherits="T4RuntimeTemplates.TextTemplateBase" #>
<#@ output extension=".cs" #>
<#
    SetupMessageParser(this.Host.ResolvePath("messages.xml"));
    PrintLocalModificationWarning();
#>
using ACMessageDefs.Enums;
using ACMessageDefs.Lib;
using ACMessageDefs.Messages;
using ACMessageDefs.HandlerEventArgs;
using System;
using System.IO;

namespace ACMessageDefs.HandlerEventArgs {
    /// <summary>
    /// Contains data about a message
    /// </summary>
    public class MessageEventArgs {
        /// <summary>
        /// Direction the message was sent in (S2C or C2S)
        /// </summary>
        public MessageDirection Direction;

        /// <summary>
        /// The type of message this is
        /// </summary>
        public MessageType Type;

        /// <summary>
        /// The actual message data
        /// </summary>
        public IACDataType Data;

        public MessageEventArgs(MessageDirection direction, MessageType type, IACDataType data) {
            Direction = direction;
            Type = type;
            Data = data;
        }
    }

    public class UnknownMessageTypeEventArgs {
        public MessageDirection Direction;
        public MessageType Type;
        public byte[] RawData;

        public UnknownMessageTypeEventArgs(MessageDirection direction, MessageType type, byte[] rawData) {
            Direction = direction;
            Type = type;
            RawData = rawData;
        }
    }

<#
    using (new IndentHelper(this)) {
        foreach(var kv in this.MessageReader.ACMessages) {
            var message = kv.Value;
#>
/// <summary>
/// <#= kv.Key #> Event Args
/// </summary>
public class <#= kv.Key #>_EventArgs : EventArgs {
    /// <summary>
    /// <#= kv.Key #> Message Data
    /// </summary>
    public <#= kv.Key #> Data;

    public <#= kv.Key #>_EventArgs(<#= kv.Key #> data) {
        Data = data;
    }
}

<#
        }
    }
#>
}

namespace ACMessageDefs {
    public class MessageHandler {
        /// <summary>
        /// Fired for every valid parsed message
        /// </summary>
        public event EventHandler<MessageEventArgs> Message;

        /// <summary>
        /// Fired when an unknown message type was encountered
        /// </summary>
        public event EventHandler<UnknownMessageTypeEventArgs> UnknownMessageType;
<#
    using (new IndentHelper(this)) {
        foreach(var kv in this.MessageReader.ACMessages) {
            var message = kv.Value;
#>
    /// <summary>
    /// Fired on message type <#= message.Type #> <#= kv.Key #>. <#= (string.IsNullOrWhiteSpace(message.Text) ? "" : message.Text) #>
    /// </summary>
    public event EventHandler<<#= kv.Key #>_EventArgs> <#= kv.Key #>;

<#
        }
    }
#>
        public void HandleIncomingMessageData(BinaryReader buffer) {
            IACDataType data;
            var opcode = (MessageType)buffer.ReadUInt32();
            Logger.Log($"Got opcode:{opcode} {((uint)opcode):X4}");

            switch (opcode) {
                case (MessageType)0xF7B0: // Value indicating this message has a sequencing header
                    var _objectId = buffer.ReadUInt32(); // Current unused
                    var _sequence = buffer.ReadUInt32(); // Currently unused
                    opcode = (MessageType)buffer.ReadUInt32();
                    Logger.Log($"Got 0xF7B0 event:{opcode} {((uint)opcode):X4}");
                    switch(opcode) {
<#
    using (new IndentHelper(this))
    using (new IndentHelper(this)) {
        foreach(var kv in this.MessageReader.ACMessages) {
            if (!kv.Key.EndsWith("_S2C") || kv.Value.Ordered == false)
                continue;
#>
                case MessageType.<#= kv.Key.Replace("_S2C", "") #>: // <#= kv.Value.Ordered.GetType() #> <#= kv.Value.Ordered #>
                    data = new <#= kv.Key #>();
                    data.ReadFromBuffer(buffer);
                    Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                    <#= kv.Key #>?.Invoke(this, new <#= kv.Key #>_EventArgs(data as <#= kv.Key #>));
                    break;

<#
        }
    }
#>
                    }
                    break; // end 0xF7B0

<#
    using (new IndentHelper(this)) {
        foreach(var kv in this.MessageReader.ACMessages) {
            if (!kv.Key.EndsWith("_S2C") || kv.Value.Ordered == true)
                continue;
#>
                case MessageType.<#= kv.Key.Replace("_S2C", "") #>:
                    data = new <#= kv.Key #>();
                    data.ReadFromBuffer(buffer);
                    Message?.Invoke(this, new MessageEventArgs(MessageDirection.S2C, opcode, null));
                    <#= kv.Key #>?.Invoke(this, new <#= kv.Key #>_EventArgs(data as <#= kv.Key #>));
                    break;

<#
        }
    }
#>
                default:
                    var rawData = buffer.ReadBytes((int)(buffer.BaseStream.Length - buffer.BaseStream.Position));
                    UnknownMessageType?.Invoke(this, new UnknownMessageTypeEventArgs(MessageDirection.S2C, opcode, rawData));
                    break;
            }
        }
    }
}
