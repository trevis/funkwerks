﻿<#@ assembly name="$(TargetPath)" #>
<#@ template language="c#" hostspecific="true" inherits="T4RuntimeTemplates.TextTemplateBase" #>
<#@ output extension=".cs" #>
<#
    PrintLocalModificationWarning();
#>
using System;

namespace ACMessageDefs.Enums {
    /// <summary>
    /// Represents the direction of a network message
    /// </summary>
    public enum MessageDirection : byte {
        /// <summary>
        /// Server to Client
        /// </summary>
        S2C = 1,

        /// <summary>
        /// Client to Server
        /// </summary>
        C2S = 2
    }

<#
    SetupMessageParser(this.Host.ResolvePath("messages.xml"));
    
    Indent();

    // loop through enums and output definitions
    foreach (var kv in this.MessageReader.ACEnums) {
        WriteSummary(kv.Value.Text);

        if (kv.Value.IsMask)
            WriteLine("[Flags]");
        
        WriteLine("public enum " + kv.Key + " : " + kv.Value.ParentType + " {");
        
        Indent();
        foreach (var v in kv.Value.Values) {
            WriteSummary(v.Text);
            WriteLine(v.Name + " = " + v.Value + ",\n");
        }
        Outdent();
        
        WriteLine("}\n"); 
    }

    // primitives enum
    WriteSummary("List of primitive data types used in ac messages");
    WriteLine("public enum PrimitiveDataType : ushort {");

    Indent();
    var i = 1;
    
    WriteSummary("Unknown ac primitive type");
    WriteLine("Unknown = 0,");
    
    foreach (var kv in this.MessageReader.ACPrimitives) {
        WriteSummary(kv.Value.Name + " ac primitive type");
        WriteLine("@" + kv.Key + " = " + i + ",\n");
        i++;
    }
    Outdent();

    WriteLine("}");
    Outdent();
#>
}
