﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACMessageDefs.Lib {
    public interface IACDataType {
        /// <summary>
        /// Populates this data type instance from a BinaryReader buffer
        /// </summary>
        /// <param name="buffer">BinaryReader instance to read from</param>
        void ReadFromBuffer(BinaryReader buffer);
    }
}
