﻿using System;
using System.Collections.Generic;
using System.Text;

using System.CodeDom.Compiler;
using System.Globalization;
using System.Reflection;
using ACMessageDefs;
using System.IO;
using System.Linq;
using System.Net;
using ACMessageDefs.Models;

namespace T4RuntimeTemplates {

    /// <summary>
    /// Base TextTemplate class with built-in helpers for managing messages.xml data
    /// </summary>
    public abstract class TextTemplateBase {
        public class IndentHelper : IDisposable {
            public TextTemplateBase Template { get; }

            public IndentHelper(TextTemplateBase template) {
                Template = template;
                Template.Indent();
            }

            public void Dispose() {
                Template.Outdent();
            }
        }

        const string _defaultIndentStr = "    ";
        string _currentIndent = "";
        bool _endsWithNewline;
        CompilerErrorCollection _errors;
        StringBuilder _generationEnvironment;
        List<int> _indentLengths;

        /// <summary>
        /// Messages.xml Reader class
        /// </summary>
        public MessagesReader MessageReader;

        /// <summary>
        /// Create a new TextTemplateBase
        /// </summary>
        public TextTemplateBase() {
            ToStringHelper = new ToStringInstanceHelper();
        }

        public void SetupMessageParser(string path) {
            MessageReader = new MessagesReader(path);
        }

        #region TemplateHelpers
        public void GenerateManualConstructorContents(ACMessageDefs.Models.ACDataType dataType) {
            // build out all the assign statements for each struct field
            foreach (var child in dataType.Children) {
                if (child is ACMessageDefs.Models.ACDataMember) {
                    var member = child as ACMessageDefs.Models.ACDataMember;
                    WriteLine("this." + member.Name + " = " + member.Name + ";");
                }
                else if (child is ACMessageDefs.Models.ACVector) {
                    var vector = child as ACMessageDefs.Models.ACVector;
                    WriteLine("this." + vector.Name + " = " + vector.Name + ";");
                }
            }
        }

        public void GenerateBinaryConstructorContents(ACMessageDefs.Models.ACBaseModel dataType) {
            // build out all the assign statements for each struct field
            foreach (var child in dataType.Children) {
                GenerateChildReader(child, false);
            }
        }

        public void GenerateChildReader(ACMessageDefs.Models.ACBaseModel child, bool isVector, int depth = 0) {
            if (child is ACMessageDefs.Models.ACDataMember) {
                var member = child as ACMessageDefs.Models.ACDataMember;
                GenerateMemberReader(member, isVector);
            }
            if (child is ACMessageDefs.Models.ACField) {
                var member = child as ACMessageDefs.Models.ACField;
                GenerateMemberReader(member, isVector);
            }
            else if (child is ACMessageDefs.Models.ACIf) {
                var acif = child as ACMessageDefs.Models.ACIf;
                WriteLine("if (" + acif.Test + ") {");
                using (new IndentHelper(this)) {
                    foreach (var member in acif.TrueMembers) {
                        GenerateChildReader(member, isVector);
                    }
                }
                WriteLine("}");
                if (acif.FalseMembers.Count > 0) {
                    WriteLine("else {");
                    using (new IndentHelper(this)) {
                        foreach (var member in acif.FalseMembers) {
                            GenerateChildReader(member, isVector);
                        }
                    }
                    WriteLine("}");
                }
            }
            else if (child is ACMessageDefs.Models.ACMaskMap) {
                var maskMap = child as ACMessageDefs.Models.ACMaskMap;
                foreach (var mask in maskMap.Masks) {
                    WriteLine("if (((uint)" + maskMap.Name + " & " + mask.Value + ") != 0) {");
                    using (new IndentHelper(this)) {
                        foreach (var maskChild in mask.Children) {
                            GenerateChildReader(maskChild, isVector);
                        }
                    }
                    WriteLine("}");
                }
            }
            else if (child is ACMessageDefs.Models.ACAlign) {
                var align = child as ACMessageDefs.Models.ACAlign;
                WriteLine("if ((buffer.BaseStream.Position % 4) != 0) { // align " + align.Type);
                using (new IndentHelper(this)) {
                    WriteLine("buffer.BaseStream.Position += 4 - (buffer.BaseStream.Position % 4);");
                }
                WriteLine("}");
            }
            else if (child is ACMessageDefs.Models.ACSwitch) {
                var acswitch = child as ACMessageDefs.Models.ACSwitch;
                WriteLine("switch((int)" + acswitch.Name + ") {");
                using (new IndentHelper(this)) {
                    foreach (var scase in acswitch.Cases) {
                        var cases = scase.Value.Split(new string[] { " | " }, StringSplitOptions.None);
                        foreach (var s in cases) {
                            WriteLine("case " + s + ":");
                        }
                        using (new IndentHelper(this)) {
                            foreach (var cchild in scase.Children) {
                                GenerateChildReader(cchild, isVector);
                            }
                            WriteLine("break;");
                        }
                    }
                }
                WriteLine("}");
            }
            else if (child is ACMessageDefs.Models.ACVector) {
                var vector = child as ACMessageDefs.Models.ACVector;
                if (depth == 1)
                    WriteLine("for (var x=0; x < " + vector.Length + "; x++) {");
                else
                    WriteLine("for (var i=0; i < " + vector.Length + "; i++) {");
                using (new IndentHelper(this)) {
                    foreach (var vmember in vector.Children) {
                        if (vmember is ACVector) {
                            var cvector = vmember as ACVector;
                            var vType = SimplifyType((cvector.Children.First() as ACField).MemberType);
                            WriteLine("var " + cvector.Name + " = new List<" + vType + ">();");
                        }
                        else {
                            Write("var ");
                        }
                        GenerateChildReader(vmember, false, 1);
                    }
                    if (vector.Children.Count == 1) {
                        var fchild = vector.Children.First();
                        if (fchild is ACDataMember)
                            WriteLine(vector.Name + ".Add(" + (fchild as ACDataMember).Name + ");");
                        else if (fchild is ACField)
                            WriteLine(vector.Name + ".Add(" + (fchild as ACField).Name + ");");
                    }
                    else {
                        WriteLine(vector.Name + ".Add(new " + GetVectorClassName(vector) + "() {");
                        using (new IndentHelper(this)) {
                            foreach (var vchild in vector.Children) {
                                if (vchild is ACMessageDefs.Models.ACDataMember) {
                                    var vmember = vchild as ACMessageDefs.Models.ACDataMember;
                                    WriteLine(vmember.Name + " = " + vmember.Name + ",");
                                }
                                else if (vchild is ACMessageDefs.Models.ACField) {
                                    var vmember = vchild as ACField;
                                    WriteLine(vmember.Name + " = " + vmember.Name + ",");
                                }
                                else if (vchild is ACMessageDefs.Models.ACVector) {
                                    var vmember = vchild as ACVector;
                                    WriteLine(vmember.Name + " = " + vmember.Name + ",");
                                }
                                else {
                                    WriteLine("Unhandled Vector Child: " + vchild.GetType());
                                }
                            }
                        }
                        WriteLine("});");
                    }
                }
                WriteLine("}");
            }
            else {
                WriteLine("// Unhandled child type: " + child.GetType());
            }
        }

        public void GenerateMemberReader(ACMessageDefs.Models.ACDataMember member, bool isVector) {
            if (this.MessageReader.ACEnums.ContainsKey(member.MemberType)) {
                WriteLine(CleanName(member.Name) + " = (" + member.MemberType + ")" + GetBinaryReaderForEnum(member.MemberType) + (isVector ? "," : ";"));
            }
            else if (this.MessageReader.ACDataTypes.ContainsKey(member.MemberType)) {
                WriteLine(CleanName(member.Name) + " = new " + member.TypeDeclaration + "()" + (isVector ? "," : ";"));
                WriteLine(CleanName(member.Name) + ".ReadFromBuffer(buffer);");
            }
            else {
                WriteLine(CleanName(member.Name) + " = " + GetBinaryReaderForType(member.MemberType) + (isVector ? "," : ";") + " // " + member.MemberType);
            }
            WriteLine("Logger.Log($\"" + CleanName(member.Name) + " = {" + CleanName(member.Name) + "}\");");
        }

        public void GenerateMemberReader(ACMessageDefs.Models.ACField member, bool isVector) {
            if (this.MessageReader.ACEnums.ContainsKey(member.MemberType)) {
                WriteLine(CleanName(member.Name) + " = (" + member.MemberType + ")" + GetBinaryReaderForEnum(member.MemberType) + (isVector ? "," : ";"));
            }
            else if (this.MessageReader.ACDataTypes.ContainsKey(member.MemberType)) {
                WriteLine(CleanName(member.Name) + " = new " + member.TypeDeclaration + "()" + (isVector ? "," : ";"));
                WriteLine(CleanName(member.Name) + ".ReadFromBuffer(buffer);");
            }
            else {
                WriteLine(CleanName(member.Name) + " = " + GetBinaryReaderForType(member.MemberType) + (isVector ? "," : ";") + " // " + member.MemberType);
            }
            WriteLine("Logger.Log($\"" + CleanName(member.Name) + " = {" + CleanName(member.Name) + "}\");");
        }

        public string GetBinaryReaderForEnum(string type) {
            return GetBinaryReaderForType(this.MessageReader.ACEnums[type].ParentType);
        }

        public string GetBinaryReaderForType(string type) {
            switch (type) {
                case "WORD":
                case "SpellID":
                case "ushort":
                    return "BinaryHelpers.ReadUInt16(buffer)";
                case "short":
                    return "BinaryHelpers.ReadInt16(buffer)";
                case "DWORD":
                case "ObjectID":
                case "uint":
                    return "BinaryHelpers.ReadUInt32(buffer)";
                case "int":
                    return "BinaryHelpers.ReadInt32(buffer)";
                case "ulong":
                    return "BinaryHelpers.ReadUInt64(buffer)";
                case "long":
                    return "BinaryHelpers.ReadInt64(buffer)";
                case "float":
                    return "BinaryHelpers.ReadSingle(buffer)";
                case "double":
                    return "BinaryHelpers.ReadDouble(buffer)";
                case "bool":
                    return "BinaryHelpers.ReadBool(buffer)";
                case "byte":
                    return "BinaryHelpers.ReadByte(buffer)";
                case "string":
                    return "BinaryHelpers.ReadString(buffer)";
                case "WString":
                    return "BinaryHelpers.ReadWString(buffer)";
                case "PackedWORD":
                    return "BinaryHelpers.ReadPackedWORD(buffer)";
                case "DataID":
                case "PackedDWORD":
                    return "BinaryHelpers.ReadPackedDWORD(buffer)";
                default:
                    return "UnknownType:" + type;
            }
        }

        public string CleanName(string name) {
            if (name == "params" || name == "byte")
                name = "@" + name;

            return name;
        }

        public void GenerateStructFields(ACMessageDefs.Models.ACBaseModel baseModel) {
            var dataType = (baseModel.Parent as ACMessageDefs.Models.ACDataType);

            var _switch = new Dictionary<Type, Action> {
            // handles normal properties
            {typeof(ACMessageDefs.Models.ACDataMember), () => {
                    var member = baseModel as ACMessageDefs.Models.ACDataMember;
                    var name = CleanName(member.Name);
                    var simplifiedType = SimplifyType(member.MemberType);
                    WriteSummary(member.Text);
                    WriteLine("public " + simplifiedType + member.VectorTypeString + " " + name + "; // " + member.MemberType + "\n");
                    foreach (var subMember in member.SubMembers) {
                        GenerateStructFields(subMember);
                    }
            }},
            // handles submember properties
            {typeof(ACMessageDefs.Models.ACSubMember), () => {
                    var member = baseModel as ACMessageDefs.Models.ACSubMember;
                    var parent = baseModel.Parent as ACMessageDefs.Models.ACDataMember;
                    var name = CleanName(member.Name);
                    var simplifiedType = SimplifyType(member.Type);
                    var getter = member.Value;
                    if (!string.IsNullOrWhiteSpace(member.Shift))
                        getter = "(" + parent.Name + " >> " + member.Shift + ")";
                    if (!string.IsNullOrWhiteSpace(member.And))
                        getter = "(" + (string.IsNullOrWhiteSpace(getter) ? parent.Name : getter) + " & " + member.And + ")";
                    WriteSummary("Derived from " + parent.Name + ". " + member.Text);
                    WriteLine("public " + simplifiedType + " " + name + " { get => (" + simplifiedType + ")" + getter + "; } // " + member.Type + "\n");
            }},
            // handles message properties
            {typeof(ACMessageDefs.Models.ACField), () => {
                    var member = baseModel as ACMessageDefs.Models.ACField;
                    var name = CleanName(member.Name);
                    var simplifiedType = SimplifyType(member.MemberType);
                    WriteSummary(member.Text);
                    WriteLine("public " + simplifiedType + member.VectorTypeString + " " + name + "; // " + member.MemberType + "\n");
                    foreach (var subMember in member.SubMembers) {
                        GenerateStructFields(subMember);
                    }
            }},
            // handles message subfield properties
            {typeof(ACMessageDefs.Models.ACSubField), () => {
                    var member = baseModel as ACMessageDefs.Models.ACSubField;
                    var parent = baseModel.Parent as ACMessageDefs.Models.ACField;
                    var name = CleanName(member.Name);
                    var simplifiedType = SimplifyType(member.Type);
                    var getter = member.Value;
                    if (!string.IsNullOrWhiteSpace(member.Shift))
                        getter = "(" + parent.Name + " >> " + member.Shift + ")";
                    if (!string.IsNullOrWhiteSpace(member.And))
                        getter = "(" + (string.IsNullOrWhiteSpace(getter) ? parent.Name : getter) + " & " + member.And + ")";
                    WriteSummary("Derived from " + parent.Name + ". " + member.Text);
                    WriteLine("public " + simplifiedType + " " + name + " { get => (" + simplifiedType + ")" + getter + "; } // " + member.Type + "\n");
            }},
            // handles vector properties
            {typeof(ACMessageDefs.Models.ACVector), () => {
                    var vector = baseModel as ACMessageDefs.Models.ACVector;
                    var typeBase = dataType == null ? "" : dataType.BaseType;
                    var summary = vector.Text;
                    var typeDeclaration = dataType == null ? "" : (dataType.BaseType + dataType.VectorTypeString);

                    if (vector.Children.Count == 1) {
                        typeBase = GetVectorClassName(vector);
                        typeDeclaration = "List<" + SimplifyType(typeBase) + ">";
                    }
                    else {
                        // create a new sub type to hold this vector
                        typeBase = GetVectorClassName(vector);
                        summary = "Subclass to hold " + vector.Name + " data";
                        typeDeclaration = "List<" + typeBase + ">";
                        WriteLine("public class " + typeBase + " {");

                        // vector member properties
                        using (new IndentHelper(this)) {
                            foreach (var child in vector.Children) {
                                if (child is ACMessageDefs.Models.ACDataMember) {
                                    var member = (child as ACMessageDefs.Models.ACDataMember);
                                    var simplifiedType = SimplifyType(member.MemberType);
                                    WriteSummary(member.Text);
                                    WriteLine("public " + simplifiedType + member.VectorTypeString + " " + member.Name + ";\n");
                                }
                                else if (child is ACMessageDefs.Models.ACField) {
                                    var member = (child as ACMessageDefs.Models.ACField);
                                    var simplifiedType = SimplifyType(member.MemberType);
                                    WriteSummary(member.Text);
                                    WriteLine("public " + simplifiedType + member.VectorTypeString + " " + member.Name + ";\n");
                                }
                                else if (child is ACMessageDefs.Models.ACVector) {
                                    GenerateStructFields(child);
                                }
                                else {
                                    WriteLine("// Unknown vector member property: " + child.GetType());
                                }
                            }
                        }

                        WriteLine("}\n");
                    }

                    // define vector
                    WriteSummary(summary);
                    WriteLine("public " + typeDeclaration + " " + vector.Name + " { get; set; } = new " + typeDeclaration + "(); // test\n");
            }},
        };

            if (_switch.ContainsKey(baseModel.GetType())) {
                _switch[baseModel.GetType()]();
            }
            else {
                WriteLine("// !!");
                WriteLine("// !! Found unhandled child: " + (baseModel.GetType()).ToString());
                WriteLine("// !!\n");
            }
        }

        public string GetVectorClassName(ACMessageDefs.Models.ACVector vector) {
            if (vector.Children.Count == 1) {
                if (vector.Children.First() is ACDataMember)
                    return (vector.Children.First() as ACDataMember).MemberType;
                if (vector.Children.First() is ACField)
                    return (vector.Children.First() as ACField).MemberType;
            }
            return vector.Name[0].ToString().ToUpper() + string.Join("", vector.Name.TrimEnd(new char[] { 's' }).Skip(1)) + "VectorItem";
        }
        #endregion

        public string SimplifyType(string type) {
            return MessagesReader.SimplifyType(type);
        }

        public string EnumifyText(string text) {
            char[] arr = text.Replace(" ", "_").Where(c => (char.IsLetterOrDigit(c) ||
                                 char.IsWhiteSpace(c) ||
                                 c == '_')).ToArray();

            return new string(arr);
        }

        public string GetParserString(Dictionary<string, string> enumLookup, string memberName, string memberType, string valueCast, string memberTypeString) {
            string ret = "";
            switch (memberType) {
                case "byte":
                    ret += ("\t\t\t" + memberName + " = " + valueCast + "Buffer.ReadByte();\n");
                    break;
                case "ushort":
                    ret += ("\t\t\t" + memberName + " = " + valueCast + "Buffer.ReadUInt16();\n");
                    break;
                case "short":
                    ret += ("\t\t\t" + memberName + " = " + valueCast + "Buffer.ReadInt16();\n");
                    break;
                case "uint":
                    ret += ("\t\t\t" + memberName + " = " + valueCast + "Buffer.ReadUInt32();\n");
                    break;
                case "int":
                    ret += ("\t\t\t" + memberName + " = " + valueCast + "Buffer.ReadInt32();\n");
                    break;
                case "float":
                    ret += ("\t\t\t" + memberName + " = " + valueCast + "Buffer.ReadSingle();\n");
                    break;
                case "double":
                    ret += ("\t\t\t" + memberName + " = " + valueCast + "Buffer.ReadDouble();\n");
                    break;
                case "ulong":
                    ret += ("\t\t\t" + memberName + " = " + valueCast + "Buffer.ReadUInt64();\n");
                    break;
                case "long":
                    ret += ("\t\t\t" + memberName + " = " + valueCast + "Buffer.ReadInt64();\n");
                    break;
                case "bool":
                    ret += ("\t\t\t" + memberName + " = " + valueCast + "Buffer.ReadBoolean();\n");
                    break;
                case "string":
                    ret += ("\t\t\t" + memberName + " = " + valueCast + "Buffer.ReadString(); // TODO: AC string support\n");
                    break;
                default:
                    if (enumLookup.ContainsKey(memberType)) {
                        ret += GetParserString(enumLookup, memberName, enumLookup[memberType], "(" + memberType + ")", "");
                    }
                    else {
                        ret += ("\t\t\t" + memberName + " = new " + memberType + memberTypeString + "(\"" + memberName + "\", this, Buffer);\n");
                    }
                    break;
            }
            return ret;
        }

        /// <summary>
        /// Increases the internal indent amount by one
        /// </summary>
        public void Indent(string indentStr=_defaultIndentStr) {
            PushIndent(indentStr);
        }

        /// <summary>
        /// Decreases the internal indent amount by one
        /// </summary>
        public void Outdent() {
            PopIndent();
        }

        /// <summary>
        /// Prints a warning not to modify this file, but to modify the template instead
        /// </summary>
        public void PrintLocalModificationWarning() {
            WriteLine("//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//");
            WriteLine("//                                                            //");
            WriteLine("//                          WARNING                           //");
            WriteLine("//                                                            //");
            WriteLine("//           DO NOT MAKE LOCAL CHANGES TO THIS FILE           //");
            WriteLine("//               EDIT THE .tt TEMPLATE INSTEAD                //");
            WriteLine("//                                                            //");
            WriteLine("//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//");
            WriteLine("\n");
        }

        /// <summary>
        /// Writes out a csharm summary comment
        /// </summary>
        /// <param name="enumText"></param>
        public void WriteSummary(string enumText) {
            if (string.IsNullOrWhiteSpace(enumText))
                return;
            
            WriteLine("/// <summary>");
            WriteLine("/// " + WebUtility.HtmlEncode(enumText));
            WriteLine("/// </summary>");
        }

        #region Base Implementation
        /// <summary>
        /// Required to make this class a base class for T4 templates
        /// </summary>
        public abstract string TransformText();

        /// <summary>
        /// The string builder that generation-time code is using to assemble generated output
        /// </summary>
        protected StringBuilder GenerationEnvironment {
            get { return _generationEnvironment ?? (_generationEnvironment = new StringBuilder()); }
            set { _generationEnvironment = value; }
        }

        /// <summary>
        /// The error collection for the generation process
        /// </summary>
        public CompilerErrorCollection Errors {
            get { return _errors ?? (_errors = new CompilerErrorCollection()); }
        }

        /// <summary>
        /// A list of the lengths of each indent that was added with PushIndent
        /// </summary>
        List<int> IndentLengths {
            get { return _indentLengths ?? (_indentLengths = new List<int>()); }
        }

        /// <summary>
        /// Gets the current indent we use when adding lines to the output
        /// </summary>
        public string CurrentIndent {
            get { return _currentIndent; }
        }

        /// <summary>
        /// Current transformation session
        /// </summary>
        public virtual IDictionary<string, object> Session { get; set; }

        public void Initialize() {
        
        }

        /// <summary>
        /// Write text directly into the generated output
        /// </summary>
        public void Write(string textToAppend) {
            if (string.IsNullOrEmpty(textToAppend))
                return;
            // If we're starting off, or if the previous text ended with a newline,
            // we have to append the current indent first.
            if (GenerationEnvironment.Length == 0 || _endsWithNewline) {
                GenerationEnvironment.Append(_currentIndent);
                _endsWithNewline = false;
            }
            // Check if the current text ends with a newline
            if (textToAppend.EndsWith(Environment.NewLine, StringComparison.CurrentCulture))
                _endsWithNewline = true;
            // This is an optimization. If the current indent is "", then we don't have to do any
            // of the more complex stuff further down.
            if (_currentIndent.Length == 0) {
                GenerationEnvironment.Append(textToAppend);
                return;
            }
            // Everywhere there is a newline in the text, add an indent after it
            textToAppend = textToAppend.Replace(Environment.NewLine, Environment.NewLine + _currentIndent);
            // If the text ends with a newline, then we should strip off the indent added at the very end
            // because the appropriate indent will be added when the next time Write() is called
            if (_endsWithNewline)
                GenerationEnvironment.Append(textToAppend, 0, textToAppend.Length - _currentIndent.Length);
            else
                GenerationEnvironment.Append(textToAppend);
        }

        /// <summary>
        /// Write text directly into the generated output
        /// </summary>
        public void WriteLine(string textToAppend) {
            Write(textToAppend);
            GenerationEnvironment.AppendLine();
            _endsWithNewline = true;
        }

        /// <summary>
        /// Write formatted text directly into the generated output
        /// </summary>
        public void Write(string format, params object[] args) {
            Write(string.Format(CultureInfo.CurrentCulture, format, args));
        }

        /// <summary>
        /// Write formatted text directly into the generated output
        /// </summary>
        public void WriteLine(string format, params object[] args) {
            WriteLine(string.Format(CultureInfo.CurrentCulture, format, args));
        }

        /// <summary>
        /// Raise an error
        /// </summary>
        public void Error(string message) {
            Errors.Add(new CompilerError { ErrorText = message });
        }

        /// <summary>
        /// Raise a warning
        /// </summary>
        public void Warning(string message) {
            Errors.Add(new CompilerError { ErrorText = message, IsWarning = true });
        }

        /// <summary>
        /// Increase the indent
        /// </summary>
        public void PushIndent(string indent) {
            if (indent == null)
                throw new ArgumentNullException("indent");
            _currentIndent = _currentIndent + indent;
            IndentLengths.Add(indent.Length);
        }

        /// <summary>
        /// Remove the last indent that was added with PushIndent
        /// </summary>
        public string PopIndent() {
            string returnValue = "";
            if (IndentLengths.Count > 0) {
                int indentLength = IndentLengths[IndentLengths.Count - 1];
                IndentLengths.RemoveAt(IndentLengths.Count - 1);
                if (indentLength > 0) {
                    returnValue = _currentIndent.Substring(_currentIndent.Length - indentLength);
                    _currentIndent = _currentIndent.Remove(_currentIndent.Length - indentLength);
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Remove any indentation
        /// </summary>
        public void ClearIndent() {
            IndentLengths.Clear();
            _currentIndent = "";
        }

        /// <summary>
        /// Helper to produce culture-oriented representation of an object as a string
        /// </summary>
        public ToStringInstanceHelper ToStringHelper { get; private set; }

        /// <summary>
        /// Utility class to produce culture-oriented representation of an object as a string.
        /// </summary>
        public class ToStringInstanceHelper {
            IFormatProvider _formatProvider = CultureInfo.InvariantCulture;

            /// <summary>
            /// Gets or sets format provider to be used by ToStringWithCulture method.
            /// </summary>
            public IFormatProvider FormatProvider {
                get { return _formatProvider; }
                set {
                    if (value != null)
                        _formatProvider = value;
                }
            }

            /// <summary>
            /// This is called from the compile/run appdomain to convert objects within an expression block to a string
            /// </summary>
            public string ToStringWithCulture(object objectToConvert) {
                if (objectToConvert == null)
                    throw new ArgumentNullException("objectToConvert");
                Type t = objectToConvert.GetType();
                MethodInfo method = t.GetMethod("ToString", new[]
                                                                {
                                                                    typeof (IFormatProvider)
                                                                });
                if (method == null)
                    return objectToConvert.ToString();
                else {
                    return (string)(method.Invoke(objectToConvert, new object[]
                                                                        {
                                                                            _formatProvider
                                                                        }));
                }
            }
        }
        #endregion
    }
}
