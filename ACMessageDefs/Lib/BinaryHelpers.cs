﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACMessageDefs.Lib {
	public static class BinaryHelpers {
		public static bool Debug = true;

		public static object FromType(BinaryReader buffer, Type type) {
			if (type == typeof(ushort))
				return ReadUInt16(buffer);
			else if (type == typeof(short))
				return ReadInt16(buffer);
			else if (type == typeof(uint))
				return ReadUInt32(buffer);
			else if (type == typeof(int))
				return ReadInt32(buffer);
			else if (type == typeof(ulong))
				return ReadUInt64(buffer);
			else if (type == typeof(long))
				return ReadInt64(buffer);
			else if (type == typeof(float))
				return ReadSingle(buffer);
			else if (type == typeof(double))
				return ReadDouble(buffer);
			else if (type == typeof(byte))
				return ReadByte(buffer);
			else if (type == typeof(bool))
				return ReadBool(buffer);
			else if (type == typeof(string))
				return ReadString(buffer);
			else {
				Logger.Log($"Tried to BinaryHelper.FromType bad type: {type.ToString()}");
				Logger.Log(Environment.StackTrace.ToString());
				return null;
			}
		}

		public static ushort ReadUInt16(BinaryReader buffer) {
			var val = buffer.ReadUInt16();
			if (Debug) Logger.Log($"ReadUInt16 {val} {val:X4}");
			return val;
		}
		public static short ReadInt16(BinaryReader buffer) {
			var val = buffer.ReadInt16();
			if (Debug) Logger.Log($"ReadInt16 {val} {val:X4}");
			return val;
		}

		public static uint ReadUInt32(BinaryReader buffer) {
			var val = buffer.ReadUInt32();
			if (Debug) Logger.Log($"ReadUInt32 {val} {val:X8}");
			return val;
		}

		public static int ReadInt32(BinaryReader buffer) {
			var val = buffer.ReadInt32();
			if (Debug) Logger.Log($"ReadInt32 {val} {val:X8}");
			return val;
		}

		public static ulong ReadUInt64(BinaryReader buffer) {
			var val = buffer.ReadUInt64();
			if (Debug) Logger.Log($"ReadUInt64 {val} {val:X16}");
			return val;
		}

		public static long ReadInt64(BinaryReader buffer) {
			var val = buffer.ReadInt64();
			if (Debug) Logger.Log($"ReadInt64 {val} {val:X16}");
			return val;
		}

		public static float ReadSingle(BinaryReader buffer) {
			var val = buffer.ReadSingle();
			if (Debug) Logger.Log($"ReadSingle {val}");
			return val;
		}

		public static double ReadDouble(BinaryReader buffer) {
			var val = buffer.ReadDouble();
			if (Debug) Logger.Log($"ReadDouble {val}");
			return val;
		}

		public static bool ReadBool(BinaryReader buffer) {
			var val = buffer.ReadInt32();
			if (Debug) Logger.Log($"ReadInt32 {val}");
			return val == 1;
		}

		public static byte ReadByte(BinaryReader buffer) {
			var val = buffer.ReadByte();
			if (Debug) Logger.Log($"ReadByte {val}");
			return val;
		}

		public static short ReadPackedWORD(BinaryReader buffer) {
			short tmp = buffer.ReadByte();
			if ((tmp & 0x80) != 0)
				tmp = (short)(((tmp & 0x7f) << 8) | buffer.ReadByte());

			if (Debug) Logger.Log($"ReadPackedWORD {tmp}");
			return tmp;
		}

		public static uint ReadPackedDWORD(BinaryReader buffer) {
			int tmp = buffer.ReadInt16();
			if ((tmp & 0x8000) != 0)
				tmp = ((tmp & 0x7fff) << 16) + buffer.ReadInt16();

			if (Debug) Logger.Log($"ReadPackedDWORD {tmp}");
			return (uint)tmp;
		}

		public static string ReadString(BinaryReader buffer) {
			long start = buffer.BaseStream.Position;
			int length = buffer.ReadInt16();
			if (length == -1) {
				length = buffer.ReadInt32();
			}

			byte[] tmp = buffer.ReadBytes(length);

			int align = (int)((buffer.BaseStream.Position - start) % 4);
			if (align > 0)
				buffer.ReadBytes(4 - align);

			var val = System.Text.Encoding.ASCII.GetString(tmp);

			if (Debug) Logger.Log($"ReadString <<{val}>>");
			return val;
		}

		public static string ReadWString(BinaryReader buffer) {
			int length = buffer.ReadByte();
			if ((length & 0x80) != 0) {
				length = ((length & 0x7f) << 8) | buffer.ReadByte();
			}

			byte[] tmp = buffer.ReadBytes(length * 2);

			var val = System.Text.Encoding.Unicode.GetString(tmp);

			if (Debug) Logger.Log($"ReadWString {val}");
			return val;
		}
	}
}
