﻿using ACMessageDefs.Lib;
using ACMessageDefs.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;

namespace ACMessageDefs {
    public class MessagesReader {
        private string messagesXmlPath;
        public XElement Xml;

        public static Dictionary<string, string> ACPrimitiveAliases = new Dictionary<string, string>();
        public Dictionary<string, ACPrimitive> ACPrimitives = new Dictionary<string, ACPrimitive>();
        public Dictionary<string, ACEnum> ACEnums = new Dictionary<string, ACEnum>();
        public Dictionary<string, ACDataType> ACDataTypes = new Dictionary<string, ACDataType>();
        public Dictionary<string, ACMessage> ACMessages = new Dictionary<string, ACMessage>();

        public static Dictionary<string, string> PrimitiveTypeLookup = new Dictionary<string, string>() {
            { "byte", "System.Byte" },
            { "short", "System.Int16" },
            { "ushort", "System.UInt16" },
            { "int", "System.Int32" },
            { "uint", "System.UInt32" },
        };

        public MessagesReader(string messagesXmlPath) {
            Logger.Log("---");

            using (var stream = new FileStream(messagesXmlPath, FileMode.Open)) {
                Xml = XElement.Load(stream);
            }

            ACPrimitiveAliases.Clear();
            ACPrimitiveAliases.Add("WString", "string");

            ParsePrimitives();
            ParseEnums();
            ParseTypes();
            ParseMessages();
        }

        /// <summary>
        /// Converts an ACData type to its primitive type
        /// </summary>
        /// <param name="type">the type to convert</param>
        /// <returns>a primitive type string</returns>
        public static string SimplifyType(string type) {
            while (ACPrimitiveAliases.ContainsKey(type))
                type = ACPrimitiveAliases[type];
            return type;
        }

        /// <summary>
        /// Converts an ac primitive to its system data type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public string SimplifyPrimitive(string type) {
            while (ACPrimitiveAliases.ContainsKey(type))
                type = ACPrimitiveAliases[type];

            if (PrimitiveTypeLookup.ContainsKey(type))
                type = PrimitiveTypeLookup[type];

            return type;
        }

        private void ParsePrimitives() {
            var nodes = Xml.XPathSelectElements("/datatypes/types/type[@primitive='true']");
            foreach (var node in nodes) {
                var acPrimitive = ACPrimitive.FromXElement(null, node);
                ACPrimitives.Add(acPrimitive.Name, acPrimitive);
            }
        }

        private void ParseEnums() {
            var nodes = Xml.XPathSelectElements("/datatypes/enums/enum");
            foreach (var node in nodes) {
                var acEnum = ACEnum.FromXElement(null, node);
                ACEnums.Add(acEnum.Name, acEnum);
            }
        }

        private void ParseTypes() {
            var nodes = Xml.XPathSelectElements("/datatypes/types/type");
            foreach (var node in nodes) {
                var acDataType = ACDataType.FromXElement(null, node);

                if (!string.IsNullOrWhiteSpace(acDataType.ParentType)) {
                    ACPrimitiveAliases.Add(acDataType.Name, SimplifyType(acDataType.ParentType));
                    continue;
                }

                if (!string.IsNullOrWhiteSpace(acDataType.Primitive)) {
                    if (acDataType.Primitive != "true")
                        ACPrimitiveAliases.Add(acDataType.Name, SimplifyType(acDataType.Primitive));
                    continue;
                }

                ACDataTypes.Add(acDataType.Name, acDataType);
            }
        }

        private void ParseMessages() {
            var nodes = Xml.XPathSelectElements("/messages/message");
            foreach (var node in nodes) {
                var acMessage = ACMessage.FromXElement(null, node);
                var opcode = UInt32.Parse(acMessage.Type.Replace("0x", ""), System.Globalization.NumberStyles.HexNumber);
                var name = acMessage.Type;
                foreach (uint e in Enum.GetValues(typeof(Enums.MessageType))) {
                    if (e == opcode) {
                        name = ((Enums.MessageType)opcode).ToString();
                        acMessage.MessageType = name;
                        break;
                    }
                }

                if (!name.EndsWith(acMessage.Direction))
                    name += "_" + acMessage.Direction;

                Logger.Log($"Parsed Message: {acMessage.Type} :: {name}");
                ACMessages.Add(name, acMessage);
            }
        }
    }
}
