﻿using ACMessageDefs.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Xml.Linq;
using System.Xml.XPath;

namespace ACMessageDefs.Models {
    public class ACDataType : ACBaseModel {
        public string Name { get; set; }
        public string Primitive { get; set; }
        public string ParentType { get; private set; }
        public string BaseType { get; set; }
        public string Param { get; set; }
        public string Text { get; set; }
        public bool IsTemplated { get; set; }

        public string TypeDeclaration { get => Name + VectorTypeString; }
        public string VectorTypeString {
            get {
                if (!IsTemplated)
                    return "";

                foreach (var child in Children) {
                    if (child is ACVector) {
                        var memberTypes = child.Children.Select(
                            c => MessagesReader.SimplifyType((c as ACDataMember).MemberType)
                        );
                        return "<" + string.Join(", ", memberTypes) + ">";
                    }
                }

                return "";
            }
        }

        public string ArgumentsString {
            get {
                var args = new List<string>();
                foreach (var child in Children) {
                    if (child is ACDataMember) {
                        var member = child as ACDataMember;
                        args.Add($"{MessagesReader.SimplifyType(member.MemberType)}{member.VectorTypeString} {member.Name}");
                    }
                    if (child is ACVector) {
                        var vector = child as ACVector;
                        args.Add($"{vector.TypeDeclaration} {vector.Name}");
                    }
                }
                return string.Join(", ", args);
            }
        }

        public ACDataType(ACBaseModel parent, XElement element) : base(parent, element) {

        }

        public static ACDataType FromXElement(ACBaseModel parent, XElement element) {
            var name = (string)element.Attribute("name");
            var primitive = ((string)element.Attribute("primitive"));
            var parentType = ((string)element.Attribute("parent"));
            var baseType = ((string)element.Attribute("baseType"));
            var param = ((string)element.Attribute("param"));
            var text = ((string)element.Attribute("text"));
            var templated = ((string)element.Attribute("templated"));

            return new ACDataType(parent, element) {
                Name = name,
                Primitive = primitive,
                ParentType = parentType,
                BaseType = baseType,
                Param = param,
                Text = text,
                IsTemplated = (templated == "true")
            };
        }
    }
}
