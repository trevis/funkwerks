﻿using ACMessageDefs.Lib;
using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.XPath;

namespace ACMessageDefs.Models {
    public class ACMessage : ACBaseModel {
        public string Type { get; set; } = "";
        
        public string Direction { get; set; } = "";

        public bool Ordered { get; set; }

        public string Queue { get; set; } = "";

        public string Text { get; set; } = "";

        public List<ACSubField> Fields { get; set; } = new List<ACSubField>();
        public string MessageType { get; internal set; }

        public ACMessage(ACBaseModel parent, XElement element) : base(parent, element) {

        }

        public static ACMessage FromXElement(ACBaseModel parent, XElement element) {
            var type = (string)element.Attribute("type");
            var direction = (string)element.Attribute("direction");
            var ordered = ((string)element.Attribute("ordered") ?? "").ToLower() == "true";
            var queue = (string)element.Attribute("queue");
            var text = (string)element.Attribute("text");

            var message = new ACMessage(parent, element) {
                Type = type,
                Direction = direction,
                Ordered = ordered,
                Queue = queue,
                Text = text
            };

            var subFieldNodes = element.XPathSelectElements("./field");
            foreach (var valueNode in subFieldNodes) {
                message.Fields.Add(ACSubField.FromXElement(message, valueNode));
            }

            return message;
        }
    }
}