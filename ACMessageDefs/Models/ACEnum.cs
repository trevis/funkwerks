﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using System.Xml.Linq;
using System.Xml.XPath;

namespace ACMessageDefs.Models {
    public class ACEnum : ACBaseModel {
        public string Name { get; set; }
        public string ParentType { get; set; }
        public string Text { get; set; }
        public bool IsMask { get; set; }

        public List<ACEnumValue> Values { get; set; } = new List<ACEnumValue>();

        public ACEnum(ACBaseModel parent, XElement element) : base(parent, element) {

        }

        public static ACEnum FromXElement(ACBaseModel parent, XElement element) {
            var acenum = new ACEnum(parent, element) {
                Name = (string)element.Attribute("name"),
                ParentType = (string)element.Attribute("parent"),
                Text = (string)element.Attribute("text")
            };

            var valueNodes = element.XPathSelectElements("./value");
            foreach (var valueNode in valueNodes) {
                acenum.Values.Add(ACEnumValue.FromXElement(acenum, valueNode));
            }

            var maskNodes = element.XPathSelectElements("./mask");
            foreach (var maskNode in maskNodes) {
                acenum.Values.Add(ACEnumValue.FromXElement(acenum, maskNode));
            }

            acenum.IsMask = maskNodes.Count() > 0;

            return acenum;
        }
    }
}
