﻿using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.XPath;

namespace ACMessageDefs.Models {
    public class ACField : ACBaseModel {
        public string Name { get; set; } = "";
        public string MemberType { get; set; } = "";
        public string Text { get; set; } = "";
        public string GenericKey { get; set; } = "";
        public string GenericValue { get; set; } = "";
        public string GenericType { get; set; } = "";

        public List<ACSubField> SubMembers { get; set; } = new List<ACSubField>();

        public string TypeDeclaration { get => MemberType + VectorTypeString; }

        public string VectorTypeString {
            get {
                if (!string.IsNullOrWhiteSpace(GenericKey) && !string.IsNullOrWhiteSpace(GenericValue))
                    return $"<{MessagesReader.SimplifyType(GenericKey)}, {MessagesReader.SimplifyType(GenericValue)}>";
                if (!string.IsNullOrWhiteSpace(GenericType))
                    return $"<{MessagesReader.SimplifyType(GenericType)}>";
                return "";
            }
        }

        public ACField(ACBaseModel parent, XElement element) : base(parent, element) {

        }

        public static ACField FromXElement(ACBaseModel parent, XElement element) {
            var name = (string)element.Attribute("name");
            var memberType = (string)element.Attribute("type");
            var text = (string)element.Attribute("text");
            var genericKey = (string)element.Attribute("genericKey");
            var genericValue = (string)element.Attribute("genericValue");
            var genericType = (string)element.Attribute("genericType");

            var dataMember = new ACField(parent, element) {
                Name = name,
                Text = text,
                MemberType = memberType,
                GenericKey = genericKey,
                GenericType = genericType,
                GenericValue = genericValue
            };

            var subMemberNodes = element.XPathSelectElements("./subfield");
            foreach (var valueNode in subMemberNodes) {
                dataMember.SubMembers.Add(ACSubField.FromXElement(dataMember, valueNode));
            }

            return dataMember;
        }
    }
}