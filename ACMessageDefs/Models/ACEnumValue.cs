﻿using System;
using System.Xml.Linq;

namespace ACMessageDefs.Models {
    public class ACEnumValue : ACBaseModel {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }

        public ACEnumValue(ACBaseModel parent, XElement element) : base(parent, element) {

        }

        internal static ACEnumValue FromXElement(ACBaseModel parent, XElement element) {
            var name = (string)element.Attribute("name");
            var value = (string)element.Attribute("value");

            if (string.IsNullOrWhiteSpace(name))
                name = "OPCODE_" + value;

            return new ACEnumValue(parent, element) {
                Name = name,
                Value = value,
                Text = (string)element.Attribute("text")
            };
        }
    }
}