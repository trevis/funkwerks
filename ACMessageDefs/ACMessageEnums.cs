﻿//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                                            //
//                          WARNING                           //
//                                                            //
//           DO NOT MAKE LOCAL CHANGES TO THIS FILE           //
//               EDIT THE .tt TEMPLATE INSTEAD                //
//                                                            //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//


using System;

namespace ACMessageDefs.Enums {
    /// <summary>
    /// Represents the direction of a network message
    /// </summary>
    public enum MessageDirection : byte {
        /// <summary>
        /// Server to Client
        /// </summary>
        S2C = 1,

        /// <summary>
        /// Client to Server
        /// </summary>
        C2S = 2
    }

    public enum Queues : uint {
        /// <summary>
        /// C2S, small number of admin messages and a few others
        /// </summary>
        Control = 0x0002,

        /// <summary>
        /// C2S, most all game actions, all ordered
        /// </summary>
        Weenie = 0x0003,

        /// <summary>
        /// Bidirectional, login messages, turbine chat
        /// </summary>
        Logon = 0x0004,

        /// <summary>
        /// Bidirectional, DAT patching
        /// </summary>
        CLCache = 0x0005,

        /// <summary>
        /// S2C, game events (ordered) and other character related messages
        /// </summary>
        UIQueue = 0x0009,

        /// <summary>
        /// S2C, messages mostly related to object creation/deletion and their motion, effects
        /// </summary>
        SmartBox = 0x000A,

    }

    /// <summary>
    /// All message types of game messages
    /// </summary>
    public enum MessageType : uint {
        Allegiance_AllegianceUpdateAborted = 0x0003,

        Communication_PopUpString = 0x0004,

        Character_PlayerOptionChangedEvent = 0x0005,

        Combat_TargetedMeleeAttack = 0x0008,

        Combat_TargetedMissileAttack = 0x000A,

        Communication_SetAFKMode = 0x000F,

        Communication_SetAFKMessage = 0x0010,

        Login_PlayerDescription = 0x0013,

        Communication_Talk = 0x0015,

        Social_RemoveFriend = 0x0017,

        Social_AddFriend = 0x0018,

        Inventory_PutItemInContainer = 0x0019,

        Inventory_GetAndWieldItem = 0x001A,

        Inventory_DropItem = 0x001B,

        Allegiance_SwearAllegiance = 0x001D,

        Allegiance_BreakAllegiance = 0x001E,

        Allegiance_UpdateRequest = 0x001F,

        Allegiance_AllegianceUpdate = 0x0020,

        Social_FriendsUpdate = 0x0021,

        Item_ServerSaysContainID = 0x0022,

        Item_WearItem = 0x0023,

        Item_ServerSaysRemove = 0x0024,

        Social_ClearFriends = 0x0025,

        Character_TeleToPKLArena = 0x0026,

        Character_TeleToPKArena = 0x0027,

        Social_CharacterTitleTable = 0x0029,

        Social_AddOrSetCharacterTitle = 0x002B,

        Social_SetDisplayCharacterTitle = 0x002C,

        Allegiance_QueryAllegianceName = 0x0030,

        Allegiance_ClearAllegianceName = 0x0031,

        Communication_TalkDirect = 0x0032,

        Allegiance_SetAllegianceName = 0x0033,

        Inventory_UseWithTargetEvent = 0x0035,

        Inventory_UseEvent = 0x0036,

        Allegiance_SetAllegianceOfficer = 0x003B,

        Allegiance_SetAllegianceOfficerTitle = 0x003C,

        Allegiance_ListAllegianceOfficerTitles = 0x003D,

        Allegiance_ClearAllegianceOfficerTitles = 0x003E,

        Allegiance_DoAllegianceLockAction = 0x003F,

        Allegiance_SetAllegianceApprovedVassal = 0x0040,

        Allegiance_AllegianceChatGag = 0x0041,

        Allegiance_DoAllegianceHouseAction = 0x0042,

        Train_TrainAttribute2nd = 0x0044,

        Train_TrainAttribute = 0x0045,

        Train_TrainSkill = 0x0046,

        Train_TrainSkillAdvancementClass = 0x0047,

        Magic_CastUntargetedSpell = 0x0048,

        Magic_CastTargetedSpell = 0x004A,

        Item_StopViewingObjectContents = 0x0052,

        Combat_ChangeCombatMode = 0x0053,

        Inventory_StackableMerge = 0x0054,

        Inventory_StackableSplitToContainer = 0x0055,

        Inventory_StackableSplitTo3D = 0x0056,

        Communication_ModifyCharacterSquelch = 0x0058,

        Communication_ModifyAccountSquelch = 0x0059,

        Communication_ModifyGlobalSquelch = 0x005B,

        Communication_TalkDirectByName = 0x005D,

        Vendor_Buy = 0x005F,

        Vendor_Sell = 0x0060,

        Vendor_VendorInfo = 0x0062,

        Character_TeleToLifestone = 0x0063,

        Character_StartBarber = 0x0075,

        Character_ServerSaysAttemptFailed = 0x00A0,

        Character_LoginCompleteNotification = 0x00A1,

        Fellowship_Create = 0x00A2,

        Fellowship_Quit = 0x00A3,

        Fellowship_Dismiss = 0x00A4,

        Fellowship_Recruit = 0x00A5,

        Fellowship_UpdateRequest = 0x00A6,

        Writing_BookAddPage = 0x00AA,

        Writing_BookModifyPage = 0x00AB,

        Writing_BookData = 0x00AC,

        Writing_BookDeletePage = 0x00AD,

        Writing_BookPageData = 0x00AE,

        Writing_BookOpen = 0x00B4,

        Writing_BookAddPageResponse = 0x00B6,

        Writing_BookDeletePageResponse = 0x00B7,

        Writing_BookPageDataResponse = 0x00B8,

        Writing_SetInscription = 0x00BF,

        Item_GetInscriptionResponse = 0x00C3,

        Item_Appraise = 0x00C8,

        Item_SetAppraiseInfo = 0x00C9,

        Inventory_GiveObjectRequest = 0x00CD,

        Advocate_Teleport = 0x00D6,

        Character_AbuseLogRequest = 0x0140,

        Communication_AddToChannel = 0x0145,

        Communication_RemoveFromChannel = 0x0146,

        Communication_ChannelBroadcast = 0x0147,

        Communication_ChannelList = 0x0148,

        Communication_ChannelIndex = 0x0149,

        Inventory_NoLongerViewingContents = 0x0195,

        Item_OnViewContents = 0x0196,

        Item_UpdateStackSize = 0x0197,

        Item_ServerSaysMoveItem = 0x019A,

        Inventory_StackableSplitToWield = 0x019B,

        Character_AddShortCut = 0x019C,

        Character_RemoveShortCut = 0x019D,

        Combat_HandlePlayerDeathEvent = 0x019E,

        Character_CharacterOptionsEvent = 0x01A1,

        Combat_HandleAttackDoneEvent = 0x01A7,

        Magic_RemoveSpell = 0x01A8,

        Combat_HandleVictimNotificationEventSelf = 0x01AC,

        Combat_HandleVictimNotificationEventOther = 0x01AD,

        Combat_HandleAttackerNotificationEvent = 0x01B1,

        Combat_HandleDefenderNotificationEvent = 0x01B2,

        Combat_HandleEvasionAttackerNotificationEvent = 0x01B3,

        Combat_HandleEvasionDefenderNotificationEvent = 0x01B4,

        Combat_CancelAttack = 0x01B7,

        Combat_HandleCommenceAttackEvent = 0x01B8,

        Combat_QueryHealth = 0x01BF,

        Combat_QueryHealthResponse = 0x01C0,

        Character_QueryAge = 0x01C2,

        Character_QueryAgeResponse = 0x01C3,

        Character_QueryBirth = 0x01C4,

        Item_UseDone = 0x01C7,

        Fellowship_FellowUpdateDone = 0x01C9,

        Fellowship_FellowStatsDone = 0x01CA,

        Item_AppraiseDone = 0x01CB,

        Qualities_PrivateRemoveIntEvent = 0x01D1,

        Qualities_RemoveIntEvent = 0x01D2,

        Qualities_PrivateRemoveBoolEvent = 0x01D3,

        Qualities_RemoveBoolEvent = 0x01D4,

        Qualities_PrivateRemoveFloatEvent = 0x01D5,

        Qualities_RemoveFloatEvent = 0x01D6,

        Qualities_PrivateRemoveStringEvent = 0x01D7,

        Qualities_RemoveStringEvent = 0x01D8,

        Qualities_PrivateRemoveDataIDEvent = 0x01D9,

        Qualities_RemoveDataIDEvent = 0x01DA,

        Qualities_PrivateRemoveInstanceIDEvent = 0x01DB,

        Qualities_RemoveInstanceIDEvent = 0x01DC,

        Qualities_PrivateRemovePositionEvent = 0x01DD,

        Qualities_RemovePositionEvent = 0x01DE,

        Communication_Emote = 0x01DF,

        Communication_HearEmote = 0x01E0,

        Communication_SoulEmote = 0x01E1,

        Communication_HearSoulEmote = 0x01E2,

        Character_AddSpellFavorite = 0x01E3,

        Character_RemoveSpellFavorite = 0x01E4,

        Character_RequestPing = 0x01E9,

        Character_ReturnPing = 0x01EA,

        Communication_SetSquelchDB = 0x01F4,

        Trade_OpenTradeNegotiations = 0x01F6,

        Trade_CloseTradeNegotiations = 0x01F7,

        Trade_AddToTrade_C2S = 0x01F8,

        Trade_AcceptTrade_C2S = 0x01FA,

        Trade_DeclineTrade_C2S = 0x01FB,

        Trade_RegisterTrade = 0x01FD,

        Trade_OpenTrade = 0x01FE,

        Trade_CloseTrade = 0x01FF,

        Trade_AddToTrade = 0x0200,

        Trade_RemoveFromTrade = 0x0201,

        Trade_AcceptTrade = 0x0202,

        Trade_DeclineTrade = 0x0203,

        Trade_ResetTrade_C2S = 0x0204,

        Trade_ResetTrade = 0x0205,

        Trade_TradeFailure = 0x0207,

        Trade_ClearTradeAcceptance = 0x0208,

        Character_ClearPlayerConsentList = 0x0216,

        Character_DisplayPlayerConsentList = 0x0217,

        Character_RemoveFromPlayerConsentList = 0x0218,

        Character_AddPlayerPermission = 0x0219,

        House_BuyHouse = 0x021C,

        House_HouseProfile = 0x021D,

        House_QueryHouse = 0x021E,

        House_AbandonHouse = 0x021F,

        Character_RemovePlayerPermission = 0x0220,

        House_RentHouse = 0x0221,

        Character_SetDesiredComponentLevel = 0x0224,

        House_HouseData = 0x0225,

        House_HouseStatus = 0x0226,

        House_UpdateRentTime = 0x0227,

        House_UpdateRentPayment = 0x0228,

        House_AddPermanentGuest = 0x0245,

        House_RemovePermanentGuest = 0x0246,

        House_SetOpenHouseStatus = 0x0247,

        House_UpdateRestrictions = 0x0248,

        House_ChangeStoragePermission = 0x0249,

        House_BootSpecificHouseGuest = 0x024A,

        House_RemoveAllStoragePermission = 0x024C,

        House_RequestFullGuestList = 0x024D,

        Allegiance_SetMotd = 0x0254,

        Allegiance_QueryMotd = 0x0255,

        Allegiance_ClearMotd = 0x0256,

        House_UpdateHAR = 0x0257,

        House_QueryLord = 0x0258,

        House_HouseTransaction = 0x0259,

        House_AddAllStoragePermission = 0x025C,

        House_RemoveAllPermanentGuests = 0x025E,

        House_BootEveryone = 0x025F,

        House_TeleToHouse = 0x0262,

        Item_QueryItemMana = 0x0263,

        Item_QueryItemManaResponse = 0x0264,

        House_SetHooksVisibility = 0x0266,

        House_ModifyAllegianceGuestPermission = 0x0267,

        House_ModifyAllegianceStoragePermission = 0x0268,

        Game_Join = 0x0269,

        Game_Quit = 0x026A,

        Game_Move = 0x026B,

        Game_MovePass = 0x026D,

        Game_Stalemate = 0x026E,

        House_ListAvailableHouses = 0x0270,

        House_AvailableHouses = 0x0271,

        Character_ConfirmationRequest = 0x0274,

        Character_ConfirmationResponse = 0x0275,

        Character_ConfirmationDone = 0x0276,

        Allegiance_BreakAllegianceBoot = 0x0277,

        House_TeleToMansion = 0x0278,

        Character_Suicide = 0x0279,

        Allegiance_AllegianceLoginNotificationEvent = 0x027A,

        Allegiance_AllegianceInfoRequest = 0x027B,

        Allegiance_AllegianceInfoResponseEvent = 0x027C,

        Inventory_CreateTinkeringTool = 0x027D,

        Game_JoinGameResponse = 0x0281,

        Game_StartGame = 0x0282,

        Game_MoveResponse = 0x0283,

        Game_OpponentTurn = 0x0284,

        Game_OpponentStalemateState = 0x0285,

        Character_SpellbookFilterEvent = 0x0286,

        Communication_WeenieError = 0x028A,

        Communication_WeenieErrorWithString = 0x028B,

        Game_GameOver = 0x028C,

        Character_TeleToMarketplace = 0x028D,

        Character_EnterPKLite = 0x028F,

        Fellowship_AssignNewLeader = 0x0290,

        Fellowship_ChangeFellowOpeness = 0x0291,

        Communication_ChatRoomTracker = 0x0295,

        Allegiance_AllegianceChatBoot = 0x02A0,

        Allegiance_AddAllegianceBan = 0x02A1,

        Allegiance_RemoveAllegianceBan = 0x02A2,

        Allegiance_ListAllegianceBans = 0x02A3,

        Allegiance_RemoveAllegianceOfficer = 0x02A5,

        Allegiance_ListAllegianceOfficers = 0x02A6,

        Allegiance_ClearAllegianceOfficers = 0x02A7,

        Allegiance_RecallAllegianceHometown = 0x02AB,

        Admin_QueryPluginList = 0x02AE,

        Admin_QueryPluginListResponse = 0x02AF,

        Admin_QueryPlugin = 0x02B1,

        Admin_QueryPluginResponse = 0x02B2,

        Admin_QueryPluginResponse2 = 0x02B3,

        Inventory_SalvageOperationsResultData = 0x02B4,

        Qualities_PrivateRemoveInt64Event = 0x02B8,

        Qualities_RemoveInt64Event = 0x02B9,

        Communication_HearSpeech = 0x02BB,

        Communication_HearRangedSpeech = 0x02BC,

        Communication_HearDirectSpeech = 0x02BD,

        Fellowship_FullUpdate = 0x02BE,

        Fellowship_Disband = 0x02BF,

        Fellowship_UpdateFellow = 0x02C0,

        Magic_UpdateSpell = 0x02C1,

        Magic_UpdateEnchantment = 0x02C2,

        Magic_RemoveEnchantment = 0x02C3,

        Magic_UpdateMultipleEnchantments = 0x02C4,

        Magic_RemoveMultipleEnchantments = 0x02C5,

        Magic_PurgeEnchantments = 0x02C6,

        Magic_DispelEnchantment = 0x02C7,

        Magic_DispelMultipleEnchantments = 0x02C8,

        Misc_PortalStormBrewing = 0x02C9,

        Misc_PortalStormImminent = 0x02CA,

        Misc_PortalStorm = 0x02CB,

        Misc_PortalStormSubsided = 0x02CC,

        Qualities_PrivateUpdateInt = 0x02CD,

        Qualities_UpdateInt = 0x02CE,

        Qualities_PrivateUpdateInt64 = 0x02CF,

        Qualities_UpdateInt64 = 0x02D0,

        Qualities_PrivateUpdateBool = 0x02D1,

        Qualities_UpdateBool = 0x02D2,

        Qualities_PrivateUpdateFloat = 0x02D3,

        Qualities_UpdateFloat = 0x02D4,

        Qualities_PrivateUpdateString = 0x02D5,

        Qualities_UpdateString = 0x02D6,

        Qualities_PrivateUpdateDataID = 0x02D7,

        Qualities_UpdateDataID = 0x02D8,

        Qualities_PrivateUpdateInstanceID = 0x02D9,

        Qualities_UpdateInstanceID = 0x02DA,

        Qualities_PrivateUpdatePosition = 0x02DB,

        Qualities_UpdatePosition = 0x02DC,

        Qualities_PrivateUpdateSkill = 0x02DD,

        Qualities_UpdateSkill = 0x02DE,

        Qualities_PrivateUpdateSkillLevel = 0x02DF,

        Qualities_UpdateSkillLevel = 0x02E0,

        Qualities_PrivateUpdateSkillAC = 0x02E1,

        Qualities_UpdateSkillAC = 0x02E2,

        Qualities_PrivateUpdateAttribute = 0x02E3,

        Qualities_UpdateAttribute = 0x02E4,

        Qualities_PrivateUpdateAttributeLevel = 0x02E5,

        Qualities_UpdateAttributeLevel = 0x02E6,

        Qualities_PrivateUpdateAttribute2nd = 0x02E7,

        Qualities_UpdateAttribute2nd = 0x02E8,

        Qualities_PrivateUpdateAttribute2ndLevel = 0x02E9,

        Qualities_UpdateAttribute2ndLevel = 0x02EA,

        Communication_TransientString = 0x02EB,

        Character_FinishBarber = 0x0311,

        Magic_PurgeBadEnchantments = 0x0312,

        Social_SendClientContractTrackerTable = 0x0314,

        Social_SendClientContractTracker = 0x0315,

        Social_AbandonContract = 0x0316,

        Admin_Environs = 0xEA60,

        Movement_PositionAndMovementEvent = 0xF619,

        Movement_Jump = 0xF61B,

        Movement_MoveToState = 0xF61C,

        Movement_DoMovementCommand = 0xF61E,

        Item_ObjDescEvent = 0xF625,

        Character_SetPlayerVisualDesc = 0xF630,

        Character_CharGenVerificationResponse = 0xF643,

        Movement_TurnToEvent = 0xF649,

        Login_AwaitingSubscriptionExpiration = 0xF651,

        Login_LogOffCharacter = 0xF653,

        Login_ExecuteLogOff = 0xF653,

        Character_CharacterDelete = 0xF655,

        Character_SendCharGenResult = 0xF656,

        Login_SendEnterWorld = 0xF657,

        Login_LoginCharacterSet = 0xF658,

        Character_CharacterError = 0xF659,

        Movement_StopMovementCommand = 0xF661,

        Object_SendForceObjdesc = 0xF6EA,

        Item_CreateObject = 0xF745,

        Login_CreatePlayer = 0xF746,

        Item_DeleteObject = 0xF747,

        Movement_PositionEvent = 0xF748,

        Item_ParentEvent = 0xF749,

        Inventory_PickupEvent = 0xF74A,

        Item_SetState = 0xF74B,

        Movement_SetObjectMovement = 0xF74C,

        Movement_VectorUpdate = 0xF74E,

        Effects_SoundEvent = 0xF750,

        Effects_PlayerTeleport = 0xF751,

        Movement_AutonomyLevel = 0xF752,

        Movement_AutonomousPosition = 0xF753,

        Effects_PlayScriptID = 0xF754,

        Effects_PlayScriptType = 0xF755,

        Network_WOrderHdr = 0xF7B0,

        Network_OrderHdr = 0xF7B1,

        Login_AccountBanned = 0xF7C1,

        Login_SendEnterWorldRequest = 0xF7C8,

        Movement_Jump_NonAutonomous = 0xF7C9,

        Admin_ReceiveAccountData = 0xF7CA,

        Admin_ReceivePlayerData = 0xF7CB,

        Admin_SendAdminGetServerVersion = 0xF7CC,

        Social_SendFriendsCommand = 0xF7CD,

        Admin_SendAdminRestoreCharacter = 0xF7D9,

        Item_UpdateObject = 0xF7DB,

        Login_AccountBooted = 0xF7DC,

        Communication_TurbineChat = 0xF7DE,

        Login_EnterGame_ServerReady = 0xF7DF,

        Communication_TextboxString = 0xF7E0,

        Login_WorldInfo = 0xF7E1,

        DDD_DataMessage = 0xF7E2,

        DDD_RequestDataMessage = 0xF7E3,

        DDD_ErrorMessage = 0xF7E4,

        DDD_InterrogationMessage = 0xF7E5,

        DDD_InterrogationResponseMessage = 0xF7E6,

        DDD_BeginDDDMessage = 0xF7E7,

        DDD_OnEndDDD = 0xF7EA,

        DDD_EndDDDMessage = 0xF7EB,

    }

    /// <summary>
    /// Set of predefined status messages, some of which take additional data as input
    /// </summary>
    public enum StatusMessage : uint {
        /// <summary>
        /// You failed to go to non-combat mode.
        /// </summary>
        OPCODE_0x17 = 0x17,

        /// <summary>
        /// You&#39;re too busy!
        /// </summary>
        OPCODE_0x1D = 0x1D,

        /// <summary>
        ///  is too busy to accept gifts right now.\n
        /// </summary>
        OPCODE_0x1E = 0x1E,

        /// <summary>
        /// You must control both objects!
        /// </summary>
        OPCODE_0x20 = 0x20,

        /// <summary>
        /// Unable to move to object!
        /// </summary>
        OPCODE_0x23 = 0x23,

        /// <summary>
        /// You can&#39;t jump while in the air
        /// </summary>
        OPCODE_0x24 = 0x24,

        /// <summary>
        /// That is not a valid command.
        /// </summary>
        OPCODE_0x26 = 0x26,

        /// <summary>
        /// The item is under someone else&#39;s control!
        /// </summary>
        OPCODE_0x28 = 0x28,

        /// <summary>
        /// You cannot pick that up!
        /// </summary>
        OPCODE_0x29 = 0x29,

        /// <summary>
        /// You are too encumbered to carry that!
        /// </summary>
        OPCODE_0x2A = 0x2A,

        /// <summary>
        ///  cannot carry anymore.\n
        /// </summary>
        OPCODE_0x2B = 0x2B,

        /// <summary>
        /// Action cancelled!
        /// </summary>
        OPCODE_0x36 = 0x36,

        /// <summary>
        /// Unable to move to object!
        /// </summary>
        Unable_to_move_to_object_1 = 0x37,

        /// <summary>
        /// Unable to move to object!
        /// </summary>
        Unable_to_move_to_object_2 = 0x38,

        /// <summary>
        /// Unable to move to object!
        /// </summary>
        Unable_to_move_to_object_3 = 0x39,

        /// <summary>
        /// You can&#39;t do that... you&#39;re dead!
        /// </summary>
        OPCODE_0x3A = 0x3A,

        /// <summary>
        /// You charged too far!
        /// </summary>
        OPCODE_0x3D = 0x3D,

        /// <summary>
        /// You are too tired to do that!
        /// </summary>
        OPCODE_0x3E = 0x3E,

        /// <summary>
        /// The container is closed!
        /// </summary>
        OPCODE_0x3EE = 0x3EE,

        /// <summary>
        ///  is not accepting gifts right now.\n
        /// </summary>
        OPCODE_0x3EF = 0x3EF,

        /// <summary>
        /// You failed to go to non-combat mode.
        /// </summary>
        You_failed_to_go_to_noncombat_mode_2 = 0x3F1,

        /// <summary>
        /// You are too fatigued to attack!
        /// </summary>
        OPCODE_0x3F7 = 0x3F7,

        /// <summary>
        /// You are out of ammunition!
        /// </summary>
        OPCODE_0x3F8 = 0x3F8,

        /// <summary>
        /// Your missile attack misfired!
        /// </summary>
        OPCODE_0x3F9 = 0x3F9,

        /// <summary>
        /// You&#39;ve attempted an impossible spell path!
        /// </summary>
        OPCODE_0x3FA = 0x3FA,

        /// <summary>
        /// You don&#39;t know that spell!
        /// </summary>
        OPCODE_0x3FE = 0x3FE,

        /// <summary>
        /// Incorrect target type
        /// </summary>
        OPCODE_0x3FF = 0x3FF,

        /// <summary>
        /// You don&#39;t have all the components for this spell.
        /// </summary>
        OPCODE_0x400 = 0x400,

        /// <summary>
        /// You don&#39;t have enough Mana to cast this spell.
        /// </summary>
        OPCODE_0x401 = 0x401,

        /// <summary>
        /// Your spell fizzled.\n
        /// </summary>
        OPCODE_0x402 = 0x402,

        /// <summary>
        /// Your spell&#39;s target is missing!
        /// </summary>
        OPCODE_0x403 = 0x403,

        /// <summary>
        /// Your projectile spell mislaunched!
        /// </summary>
        OPCODE_0x404 = 0x404,

        /// <summary>
        /// Your spell cannot be cast outside
        /// </summary>
        OPCODE_0x407 = 0x407,

        /// <summary>
        /// Your spell cannot be cast inside
        /// </summary>
        OPCODE_0x408 = 0x408,

        /// <summary>
        /// You are unprepared to cast a spell
        /// </summary>
        OPCODE_0x40A = 0x40A,

        /// <summary>
        /// You&#39;ve already sworn your Allegiance
        /// </summary>
        OPCODE_0x40B = 0x40B,

        /// <summary>
        /// You don&#39;t have enough experience available to swear Allegiance
        /// </summary>
        OPCODE_0x40C = 0x40C,

        /// <summary>
        /// %s is already one of your followers
        /// </summary>
        OPCODE_0x413 = 0x413,

        /// <summary>
        /// You are not in an allegiance!
        /// </summary>
        OPCODE_0x414 = 0x414,

        /// <summary>
        /// %s cannot have any more Vassals
        /// </summary>
        OPCODE_0x416 = 0x416,

        /// <summary>
        /// You must be the leader of a Fellowship
        /// </summary>
        OPCODE_0x41D = 0x41D,

        /// <summary>
        /// Your Fellowship is full
        /// </summary>
        OPCODE_0x41E = 0x41E,

        /// <summary>
        /// That Fellowship name is not permitted
        /// </summary>
        OPCODE_0x41F = 0x41F,

        /// <summary>
        /// That channel doesn&#39;t exist.
        /// </summary>
        OPCODE_0x422 = 0x422,

        /// <summary>
        /// You can&#39;t use that channel.
        /// </summary>
        OPCODE_0x423 = 0x423,

        /// <summary>
        /// You&#39;re already on that channel.
        /// </summary>
        OPCODE_0x424 = 0x424,

        /// <summary>
        /// You&#39;re not currently on that channel.
        /// </summary>
        OPCODE_0x425 = 0x425,

        /// <summary>
        /// You cannot merge different stacks!
        /// </summary>
        OPCODE_0x427 = 0x427,

        /// <summary>
        /// You cannot merge enchanted items!
        /// </summary>
        OPCODE_0x428 = 0x428,

        /// <summary>
        /// You must control at least one stack!
        /// </summary>
        OPCODE_0x429 = 0x429,

        /// <summary>
        /// Your craft attempt fails.
        /// </summary>
        OPCODE_0x432 = 0x432,

        /// <summary>
        /// Your craft attempt fails.
        /// </summary>
        Your_craft_attempt_fails_2 = 0x433,

        /// <summary>
        /// Given that number of items, you cannot craft anything.
        /// </summary>
        OPCODE_0x434 = 0x434,

        /// <summary>
        /// Your craft attempt fails.
        /// </summary>
        Your_craft_attempt_fails_3 = 0x435,

        /// <summary>
        /// Either you or one of the items involved does not pass the requirements for this craft interaction.
        /// </summary>
        OPCODE_0x437 = 0x437,

        /// <summary>
        /// You do not have all the neccessary items.
        /// </summary>
        OPCODE_0x438 = 0x438,

        /// <summary>
        /// Not all the items are avaliable.
        /// </summary>
        OPCODE_0x439 = 0x439,

        /// <summary>
        /// You must be at rest in peace mode to do trade skills.
        /// </summary>
        OPCODE_0x43A = 0x43A,

        /// <summary>
        /// You are not trained in that trade skill.
        /// </summary>
        OPCODE_0x43B = 0x43B,

        /// <summary>
        /// Your hands must be free.
        /// </summary>
        OPCODE_0x43C = 0x43C,

        /// <summary>
        /// You cannot link to that portal!\n
        /// </summary>
        OPCODE_0x43D = 0x43D,

        /// <summary>
        /// You have solved this quest too recently!\n
        /// </summary>
        OPCODE_0x43E = 0x43E,

        /// <summary>
        /// You have solved this quest too many times!\n
        /// </summary>
        OPCODE_0x43F = 0x43F,

        /// <summary>
        /// This item requires you to complete a specific quest before you can pick it up!\n
        /// </summary>
        OPCODE_0x445 = 0x445,

        /// <summary>
        /// Player killers may not interact with that portal!\n
        /// </summary>
        OPCODE_0x45C = 0x45C,

        /// <summary>
        /// Non-player killers may not interact with that portal!\n
        /// </summary>
        OPCODE_0x45D = 0x45D,

        /// <summary>
        /// You do not own a house!
        /// </summary>
        OPCODE_0x45E = 0x45E,

        /// <summary>
        /// You do not own a house!
        /// </summary>
        You_do_not_own_a_house_2 = 0x45F,

        /// <summary>
        /// You must purchase Asheron&#39;s Call: Dark Majesty to interact with that portal.\n
        /// </summary>
        OPCODE_0x466 = 0x466,

        /// <summary>
        /// You have used all the hooks you are allowed to use for this house.\n
        /// </summary>
        OPCODE_0x469 = 0x469,

        /// <summary>
        ///  doesn&#39;t know what to do with that.\n
        /// </summary>
        OPCODE_0x46A = 0x46A,

        /// <summary>
        /// You must complete a quest to interact with that portal.\n
        /// </summary>
        OPCODE_0x474 = 0x474,

        /// <summary>
        /// You must own a house to use this command.
        /// </summary>
        OPCODE_0x47F = 0x47F,

        /// <summary>
        /// You can&#39;t jump from this position
        /// </summary>
        OPCODE_0x48 = 0x48,

        /// <summary>
        /// Your monarch does not own a mansion or a villa!
        /// </summary>
        OPCODE_0x480 = 0x480,

        /// <summary>
        /// Your monarch does not own a mansion or a villa!
        /// </summary>
        Your_monarch_does_not_own_a_mansion_or_a_villa_2 = 0x481,

        /// <summary>
        /// Your monarch has closed the mansion to the Allegiance.
        /// </summary>
        OPCODE_0x482 = 0x482,

        /// <summary>
        /// You must be above level %s to purchase this dwelling.\n
        /// </summary>
        OPCODE_0x488 = 0x488,

        /// <summary>
        /// You must be at or below level %s to purchase this dwelling.\n
        /// </summary>
        OPCODE_0x489 = 0x489,

        /// <summary>
        /// You must be a monarch to purchase this dwelling.\n
        /// </summary>
        OPCODE_0x48A = 0x48A,

        /// <summary>
        /// You must be above allegiance rank %s to purchase this dwelling.\n
        /// </summary>
        OPCODE_0x48B = 0x48B,

        /// <summary>
        /// You must be at or below allegiance rank %s to purchase this dwelling.\n
        /// </summary>
        OPCODE_0x48C = 0x48C,

        /// <summary>
        /// Your offer of Allegiance has been ignored.
        /// </summary>
        OPCODE_0x48E = 0x48E,

        /// <summary>
        /// You are already involved in something!
        /// </summary>
        OPCODE_0x48F = 0x48F,

        /// <summary>
        /// You&#39;re too loaded down to jump
        /// </summary>
        OPCODE_0x49 = 0x49,

        /// <summary>
        /// You must be a monarch to use this command.
        /// </summary>
        OPCODE_0x490 = 0x490,

        /// <summary>
        /// You must specify a character to boot.
        /// </summary>
        OPCODE_0x491 = 0x491,

        /// <summary>
        /// You can&#39;t boot yourself!
        /// </summary>
        OPCODE_0x492 = 0x492,

        /// <summary>
        /// That character does not exist.
        /// </summary>
        OPCODE_0x493 = 0x493,

        /// <summary>
        /// That person is not a member of your Allegiance!
        /// </summary>
        OPCODE_0x494 = 0x494,

        /// <summary>
        /// No patron from which to break!
        /// </summary>
        OPCODE_0x495 = 0x495,

        /// <summary>
        /// Your Allegiance has been dissolved!\n
        /// </summary>
        OPCODE_0x496 = 0x496,

        /// <summary>
        /// Your patron&#39;s Allegiance to you has been broken!\n
        /// </summary>
        OPCODE_0x497 = 0x497,

        /// <summary>
        /// You have moved too far!
        /// </summary>
        OPCODE_0x498 = 0x498,

        /// <summary>
        /// That is not a valid destination!
        /// </summary>
        OPCODE_0x499 = 0x499,

        /// <summary>
        /// You must purchase Asheron&#39;s Call -- Dark Majesty to use this function.
        /// </summary>
        OPCODE_0x49A = 0x49A,

        /// <summary>
        /// You fail to link with the lifestone!\n
        /// </summary>
        OPCODE_0x49B = 0x49B,

        /// <summary>
        /// You wandered too far to link with the lifestone!\n
        /// </summary>
        OPCODE_0x49C = 0x49C,

        /// <summary>
        /// You successfully link with the lifestone!\n
        /// </summary>
        OPCODE_0x49D = 0x49D,

        /// <summary>
        /// You must have linked with a lifestone in order to recall to it!\n
        /// </summary>
        OPCODE_0x49E = 0x49E,

        /// <summary>
        /// You fail to recall to the lifestone!\n
        /// </summary>
        OPCODE_0x49F = 0x49F,

        /// <summary>
        /// Ack! You killed yourself!\n
        /// </summary>
        OPCODE_0x4A = 0x4A,

        /// <summary>
        /// You fail to link with the portal!\n
        /// </summary>
        OPCODE_0x4A0 = 0x4A0,

        /// <summary>
        /// You successfully link with the portal!\n
        /// </summary>
        OPCODE_0x4A1 = 0x4A1,

        /// <summary>
        /// You fail to recall to the portal!\n
        /// </summary>
        OPCODE_0x4A2 = 0x4A2,

        /// <summary>
        /// You must have linked with a portal in order to recall to it!\n
        /// </summary>
        OPCODE_0x4A3 = 0x4A3,

        /// <summary>
        /// You fail to summon the portal!\n
        /// </summary>
        OPCODE_0x4A4 = 0x4A4,

        /// <summary>
        /// You must have linked with a portal in order to summon it!\n
        /// </summary>
        OPCODE_0x4A5 = 0x4A5,

        /// <summary>
        /// You fail to teleport!\n
        /// </summary>
        OPCODE_0x4A6 = 0x4A6,

        /// <summary>
        /// You have been teleported too recently!\n
        /// </summary>
        OPCODE_0x4A7 = 0x4A7,

        /// <summary>
        /// You must be an Advocate to interact with that portal.\n
        /// </summary>
        OPCODE_0x4A8 = 0x4A8,

        /// <summary>
        /// Players may not interact with that portal.\n
        /// </summary>
        OPCODE_0x4AA = 0x4AA,

        /// <summary>
        /// You are not powerful enough to interact with that portal!\n
        /// </summary>
        OPCODE_0x4AB = 0x4AB,

        /// <summary>
        /// You are too powerful to interact with that portal!\n
        /// </summary>
        OPCODE_0x4AC = 0x4AC,

        /// <summary>
        /// You cannot recall to that portal!\n
        /// </summary>
        OPCODE_0x4AD = 0x4AD,

        /// <summary>
        /// You cannot summon that portal!\n
        /// </summary>
        OPCODE_0x4AE = 0x4AE,

        /// <summary>
        /// The lock is already unlocked.
        /// </summary>
        OPCODE_0x4AF = 0x4AF,

        /// <summary>
        /// You can&#39;t lock or unlock that!
        /// </summary>
        OPCODE_0x4B0 = 0x4B0,

        /// <summary>
        /// You can&#39;t lock or unlock what is open!
        /// </summary>
        OPCODE_0x4B1 = 0x4B1,

        /// <summary>
        /// The key doesn&#39;t fit this lock.\n
        /// </summary>
        OPCODE_0x4B2 = 0x4B2,

        /// <summary>
        /// The lock has been used too recently.
        /// </summary>
        OPCODE_0x4B3 = 0x4B3,

        /// <summary>
        /// You aren&#39;t trained in lockpicking!
        /// </summary>
        OPCODE_0x4B4 = 0x4B4,

        /// <summary>
        /// You must specify a character to query.
        /// </summary>
        OPCODE_0x4B5 = 0x4B5,

        /// <summary>
        /// Please use the allegiance panel to view your own information.
        /// </summary>
        OPCODE_0x4B6 = 0x4B6,

        /// <summary>
        /// You have used that command too recently.
        /// </summary>
        OPCODE_0x4B7 = 0x4B7,

        /// <summary>
        /// SendNotice_AbuseReportResponse
        /// </summary>
        OPCODE_0x4B8 = 0x4B8,

        /// <summary>
        /// SendNotice_AbuseReportResponse
        /// </summary>
        SendNotice_AbuseReportResponse_2 = 0x4B9,

        /// <summary>
        /// SendNotice_AbuseReportResponse
        /// </summary>
        SendNotice_AbuseReportResponse_3 = 0x4BA,

        /// <summary>
        /// You do not own that salvage tool!\n
        /// </summary>
        OPCODE_0x4BD = 0x4BD,

        /// <summary>
        /// You do not own that item!\n
        /// </summary>
        OPCODE_0x4BE = 0x4BE,

        /// <summary>
        /// The %s was not suitable for salvaging.
        /// </summary>
        OPCODE_0x4BF = 0x4BF,

        /// <summary>
        /// The %s contains the wrong material.
        /// </summary>
        OPCODE_0x4C0 = 0x4C0,

        /// <summary>
        /// The material cannot be created.\n
        /// </summary>
        OPCODE_0x4C1 = 0x4C1,

        /// <summary>
        /// The list of items you are attempting to salvage is invalid.\n
        /// </summary>
        OPCODE_0x4C2 = 0x4C2,

        /// <summary>
        /// You cannot salvage items that you are trading!\n
        /// </summary>
        OPCODE_0x4C3 = 0x4C3,

        /// <summary>
        /// You must be a guest in this house to interact with that portal.\n
        /// </summary>
        OPCODE_0x4C4 = 0x4C4,

        /// <summary>
        /// Your Allegiance Rank is too low to use that item&#39;s magic.
        /// </summary>
        OPCODE_0x4C5 = 0x4C5,

        /// <summary>
        /// You must be %s to use that item&#39;s magic.
        /// </summary>
        OPCODE_0x4C6 = 0x4C6,

        /// <summary>
        /// Your Arcane Lore skill is too low to use that item&#39;s magic.
        /// </summary>
        OPCODE_0x4C7 = 0x4C7,

        /// <summary>
        /// That item doesn&#39;t have enough Mana.
        /// </summary>
        OPCODE_0x4C8 = 0x4C8,

        /// <summary>
        /// Your %s is too low to use that item&#39;s magic.
        /// </summary>
        OPCODE_0x4C9 = 0x4C9,

        /// <summary>
        /// Only %s may use that item&#39;s magic.
        /// </summary>
        OPCODE_0x4CA = 0x4CA,

        /// <summary>
        /// You must have %s specialized to use that item&#39;s magic.
        /// </summary>
        OPCODE_0x4CB = 0x4CB,

        /// <summary>
        /// You have been involved in a player killer battle too recently to do that!\n
        /// </summary>
        OPCODE_0x4CC = 0x4CC,

        /// <summary>
        /// %s is too busy to accept gifts right now.\n
        /// </summary>
        s_is_too_busy_to_accept_gifts_right_nown = 0x4CE,

        /// <summary>
        ///  cannot accept stacked objects. Try giving one at a time.\n
        /// </summary>
        OPCODE_0x4CF = 0x4CF,

        /// <summary>
        /// Invalid PK status!
        /// </summary>
        OPCODE_0x4D = 0x4D,

        /// <summary>
        /// You have failed to alter your skill.\n
        /// </summary>
        OPCODE_0x4D0 = 0x4D0,

        /// <summary>
        /// Your %s skill must be trained, not untrained or specialized, in order to be altered in this way!\n
        /// </summary>
        OPCODE_0x4D1 = 0x4D1,

        /// <summary>
        /// You do not have enough skill credits to specialize your %s skill.\n
        /// </summary>
        OPCODE_0x4D2 = 0x4D2,

        /// <summary>
        /// You have too many available experience points to be able to absorb the experience points from your %s skill. Please spend some of your experience points and try again.\n
        /// </summary>
        OPCODE_0x4D3 = 0x4D3,

        /// <summary>
        /// Your %s skill is already untrained!\n
        /// </summary>
        OPCODE_0x4D4 = 0x4D4,

        /// <summary>
        /// You are currently wielding items which require a certain level of %s.  Your %s skill cannot be lowered while you are wielding these items.  Please remove these items and try again.\n
        /// </summary>
        OPCODE_0x4D5 = 0x4D5,

        /// <summary>
        /// You have succeeded in specializing your %s skill!\n
        /// </summary>
        OPCODE_0x4D6 = 0x4D6,

        /// <summary>
        /// You have succeeded in lowering your %s skill from specialized to trained!\n
        /// </summary>
        OPCODE_0x4D7 = 0x4D7,

        /// <summary>
        /// You have succeeded in untraining your %s skill!\n
        /// </summary>
        OPCODE_0x4D8 = 0x4D8,

        /// <summary>
        /// Although you cannot untrain your %s skill, you have succeeded in recovering all the experience you had invested in it.\n
        /// </summary>
        OPCODE_0x4D9 = 0x4D9,

        /// <summary>
        /// You have too many credits invested in specialized skills already! Before you can specialize your %s skill, you will need to unspecialize some other skill.\n
        /// </summary>
        OPCODE_0x4DA = 0x4DA,

        /// <summary>
        /// You have failed to alter your attributes.\n
        /// </summary>
        OPCODE_0x4DD = 0x4DD,

        /// <summary>
        /// \n
        /// </summary>
        blank_1 = 0x4DE,

        /// <summary>
        /// \n
        /// </summary>
        blank_2 = 0x4DF,

        /// <summary>
        /// You fail to affect %s because you cannot affect anyone!\n
        /// </summary>
        OPCODE_0x4E = 0x4E,

        /// <summary>
        /// You are currently wielding items which require a certain level of skill. Your attributes cannot be transferred while you are wielding these items. Please remove these items and try again.\n
        /// </summary>
        OPCODE_0x4E0 = 0x4E0,

        /// <summary>
        /// You have succeeded in transferring your attributes!\n
        /// </summary>
        OPCODE_0x4E1 = 0x4E1,

        /// <summary>
        /// This hook is a duplicated housing object. You may not add items to a duplicated housing object. Please empty the hook and allow it to reset.\n
        /// </summary>
        OPCODE_0x4E2 = 0x4E2,

        /// <summary>
        /// That item is of the wrong type to be placed on this hook.\n
        /// </summary>
        OPCODE_0x4E3 = 0x4E3,

        /// <summary>
        /// This chest is a duplicated housing object. You may not add items to a duplicated housing object. Please empty everything -- including backpacks -- out of the chest and allow the chest to reset.\n
        /// </summary>
        OPCODE_0x4E4 = 0x4E4,

        /// <summary>
        /// This hook was a duplicated housing object. Since it is now empty, it will be deleted momentarily. Once it is gone, it is safe to use the other, non-duplicated hook that is here.\n
        /// </summary>
        OPCODE_0x4E5 = 0x4E5,

        /// <summary>
        /// This chest was a duplicated housing object. Since it is now empty, it will be deleted momentarily. Once it is gone, it is safe to use the other, non-duplicated chest that is here.\n
        /// </summary>
        OPCODE_0x4E6 = 0x4E6,

        /// <summary>
        /// You cannot swear allegiance to anyone because you own a monarch-only house. Please abandon your house and try again.\n
        /// </summary>
        OPCODE_0x4E7 = 0x4E7,

        /// <summary>
        /// The %s cannot be used while on a hook and only the owner may open the hook.\n
        /// </summary>
        OPCODE_0x4E8 = 0x4E8,

        /// <summary>
        /// The %s cannot be used while on a hook, use the &#39;@house hooks on&#39; command to make the hook openable.\n
        /// </summary>
        OPCODE_0x4E9 = 0x4E9,

        /// <summary>
        /// The %s can only be used while on a hook.\n
        /// </summary>
        OPCODE_0x4EA = 0x4EA,

        /// <summary>
        /// You can&#39;t do that while in the air!
        /// </summary>
        OPCODE_0x4EB = 0x4EB,

        /// <summary>
        /// You cannot modify your player killer status while you are recovering from a PK death.\n
        /// </summary>
        OPCODE_0x4EC = 0x4EC,

        /// <summary>
        /// Advocates may not change their player killer status!\n
        /// </summary>
        OPCODE_0x4ED = 0x4ED,

        /// <summary>
        /// Your level is too low to change your player killer status with this object.\n
        /// </summary>
        OPCODE_0x4EE = 0x4EE,

        /// <summary>
        /// Your level is too high to change your player killer status with this object.\n
        /// </summary>
        OPCODE_0x4EF = 0x4EF,

        /// <summary>
        /// You fail to affect %s because $s cannot be harmed!\n
        /// </summary>
        OPCODE_0x4F = 0x4F,

        /// <summary>
        /// You feel a harsh dissonance, and you sense that an act of killing you have committed recently is interfering with the conversion.\n
        /// </summary>
        OPCODE_0x4F0 = 0x4F0,

        /// <summary>
        /// Bael&#39;Zharon&#39;s power flows through you again. You are once more a player killer.\n
        /// </summary>
        OPCODE_0x4F1 = 0x4F1,

        /// <summary>
        /// Bael&#39;Zharon has granted you respite after your moment of weakness. You are temporarily no longer a player killer.\n
        /// </summary>
        OPCODE_0x4F2 = 0x4F2,

        /// <summary>
        /// Lite Player Killers may not interact with that portal!\n
        /// </summary>
        OPCODE_0x4F3 = 0x4F3,

        /// <summary>
        /// %s fails to affect you because $s cannot affect anyone!\n
        /// </summary>
        OPCODE_0x4F4 = 0x4F4,

        /// <summary>
        /// %s fails to affect you because you cannot be harmed!\n
        /// </summary>
        OPCODE_0x4F5 = 0x4F5,

        /// <summary>
        /// %s fails to affect you because %s is not a player killer!\n
        /// </summary>
        OPCODE_0x4F6 = 0x4F6,

        /// <summary>
        ///  fails to affect you because you are not a player killer!\n
        /// </summary>
        OPCODE_0x4F7 = 0x4F7,

        /// <summary>
        ///  fails to affect you because you are not the same sort of player killer as 
        /// </summary>
        OPCODE_0x4F8 = 0x4F8,

        /// <summary>
        ///  fails to affect you across a house boundary!\n
        /// </summary>
        OPCODE_0x4F9 = 0x4F9,

        /// <summary>
        ///  is an invalid target.\n
        /// </summary>
        OPCODE_0x4FA = 0x4FA,

        /// <summary>
        /// You are an invalid target for the spell of %s.\n
        /// </summary>
        OPCODE_0x4FB = 0x4FB,

        /// <summary>
        /// You aren&#39;t trained in healing!
        /// </summary>
        OPCODE_0x4FC = 0x4FC,

        /// <summary>
        /// You don&#39;t own that healing kit!
        /// </summary>
        OPCODE_0x4FD = 0x4FD,

        /// <summary>
        /// You can&#39;t heal that!
        /// </summary>
        OPCODE_0x4FE = 0x4FE,

        /// <summary>
        ///  is already at full health!
        /// </summary>
        OPCODE_0x4FF = 0x4FF,

        /// <summary>
        /// You fail to affect %s because beneficial spells do not affect %s!\n
        /// </summary>
        OPCODE_0x50 = 0x50,

        /// <summary>
        /// You aren&#39;t ready to heal!
        /// </summary>
        OPCODE_0x500 = 0x500,

        /// <summary>
        /// You can only use Healing Kits on player characters.
        /// </summary>
        OPCODE_0x501 = 0x501,

        /// <summary>
        /// The Lifestone&#39;s magic protects you from the attack!\n
        /// </summary>
        OPCODE_0x502 = 0x502,

        /// <summary>
        /// The portal&#39;s residual energy protects you from the attack!\n
        /// </summary>
        OPCODE_0x503 = 0x503,

        /// <summary>
        /// You are enveloped in a feeling of warmth as you are brought back into the protection of the Light. You are once again a Non-Player Killer.\n
        /// </summary>
        OPCODE_0x504 = 0x504,

        /// <summary>
        /// You&#39;re too close to your sanctuary!
        /// </summary>
        OPCODE_0x505 = 0x505,

        /// <summary>
        /// You can&#39;t do that -- you&#39;re trading!
        /// </summary>
        OPCODE_0x506 = 0x506,

        /// <summary>
        /// Only Non-Player Killers may enter PK Lite. Please see @help pklite for more details about this command.\n
        /// </summary>
        OPCODE_0x507 = 0x507,

        /// <summary>
        /// A cold wind touches your heart. You are now a Player Killer Lite.\n
        /// </summary>
        OPCODE_0x508 = 0x508,

        /// <summary>
        ///  has no appropriate targets equipped for this spell.\n
        /// </summary>
        OPCODE_0x509 = 0x509,

        /// <summary>
        /// You have no appropriate targets equipped for %s&#39;s spell.\n
        /// </summary>
        OPCODE_0x50A = 0x50A,

        /// <summary>
        ///  is now an open fellowship; anyone may recruit new members.\n
        /// </summary>
        OPCODE_0x50B = 0x50B,

        /// <summary>
        ///  is now a closed fellowship.\n
        /// </summary>
        OPCODE_0x50C = 0x50C,

        /// <summary>
        ///  is now the leader of this fellowship.\n
        /// </summary>
        OPCODE_0x50D = 0x50D,

        /// <summary>
        /// You have passed leadership of the fellowship to %s\n
        /// </summary>
        OPCODE_0x50E = 0x50E,

        /// <summary>
        /// You do not belong to a Fellowship.
        /// </summary>
        OPCODE_0x50F = 0x50F,

        /// <summary>
        /// You fail to affect %s because you are not a player killer!\n
        /// </summary>
        OPCODE_0x51 = 0x51,

        /// <summary>
        /// You may not hook any more %s on your house.  You already have the maximum number of %s hooked or you are not permitted to hook any on your type of house.\n
        /// </summary>
        OPCODE_0x510 = 0x510,

        /// <summary>
        /// You are now using the maximum number of hooks.  You cannot use another hook until you take an item off one of your hooks.\n
        /// </summary>
        OPCODE_0x512 = 0x512,

        /// <summary>
        /// You are no longer using the maximum number of hooks.  You may again add items to your hooks.\n
        /// </summary>
        OPCODE_0x513 = 0x513,

        /// <summary>
        /// You now have the maximum number of %s hooked.  You cannot hook any additional %s until you remove one or more from your house.\n
        /// </summary>
        OPCODE_0x514 = 0x514,

        /// <summary>
        /// You no longer have the maximum number of %s hooked.  You may hook additional %s.\n
        /// </summary>
        OPCODE_0x515 = 0x515,

        /// <summary>
        /// You are not permitted to use that hook.\n
        /// </summary>
        OPCODE_0x516 = 0x516,

        /// <summary>
        ///  is not close enough to your level.\n
        /// </summary>
        OPCODE_0x517 = 0x517,

        /// <summary>
        /// This fellowship is locked; %s cannot be recruited into the fellowship.\n
        /// </summary>
        OPCODE_0x518 = 0x518,

        /// <summary>
        /// The fellowship is locked, you were not added to the fellowship.\n
        /// </summary>
        OPCODE_0x519 = 0x519,

        /// <summary>
        /// Only the original owner may use that item&#39;s magic.
        /// </summary>
        OPCODE_0x51A = 0x51A,

        /// <summary>
        /// You have entered the %s channel.\n
        /// </summary>
        OPCODE_0x51B = 0x51B,

        /// <summary>
        /// You have left the %s channel.\n
        /// </summary>
        OPCODE_0x51C = 0x51C,

        /// <summary>
        ///  will not receive your message, please use urgent assistance to speak with an in-game representative\n
        /// </summary>
        OPCODE_0x51E = 0x51E,

        /// <summary>
        /// Message Blocked: %s
        /// </summary>
        OPCODE_0x51F = 0x51F,

        /// <summary>
        /// You fail to affect %s because %s is not a player killer!\n
        /// </summary>
        OPCODE_0x52 = 0x52,

        /// <summary>
        /// You cannot add anymore people to the list of players that you can hear.\n
        /// </summary>
        OPCODE_0x520 = 0x520,

        /// <summary>
        ///  has been added to the list of people you can hear.\n
        /// </summary>
        OPCODE_0x521 = 0x521,

        /// <summary>
        ///  has been removed from the list of people you can hear.\n
        /// </summary>
        OPCODE_0x522 = 0x522,

        /// <summary>
        /// You are now deaf to player&#39;s screams.\n
        /// </summary>
        OPCODE_0x523 = 0x523,

        /// <summary>
        /// You can hear all players once again.\n
        /// </summary>
        OPCODE_0x524 = 0x524,

        /// <summary>
        /// You fail to remove %s from your loud list.\n
        /// </summary>
        OPCODE_0x525 = 0x525,

        /// <summary>
        /// You chicken out.
        /// </summary>
        OPCODE_0x526 = 0x526,

        /// <summary>
        /// You cannot posssibly succeed.
        /// </summary>
        OPCODE_0x527 = 0x527,

        /// <summary>
        /// The fellowship is locked; you cannot open locked fellowships.\n
        /// </summary>
        OPCODE_0x528 = 0x528,

        /// <summary>
        /// Trade Complete!
        /// </summary>
        OPCODE_0x529 = 0x529,

        /// <summary>
        /// That is not a salvaging tool.
        /// </summary>
        OPCODE_0x52A = 0x52A,

        /// <summary>
        /// That person is not available now.
        /// </summary>
        OPCODE_0x52B = 0x52B,

        /// <summary>
        /// You are now snooping on %s.\n
        /// </summary>
        OPCODE_0x52C = 0x52C,

        /// <summary>
        /// You are no longer snooping on %s.\n
        /// </summary>
        OPCODE_0x52D = 0x52D,

        /// <summary>
        /// You fail to snoop on %s.\n
        /// </summary>
        OPCODE_0x52E = 0x52E,

        /// <summary>
        /// %s attempted to snoop on you.\n
        /// </summary>
        OPCODE_0x52F = 0x52F,

        /// <summary>
        /// You fail to affect %s because you are not the same sort of player killer as %s!\n
        /// </summary>
        OPCODE_0x53 = 0x53,

        /// <summary>
        /// %s is already being snooped on, only one person may snoop on another at a time.\n
        /// </summary>
        OPCODE_0x530 = 0x530,

        /// <summary>
        /// %s is in limbo and cannot receive your message.\n
        /// </summary>
        OPCODE_0x531 = 0x531,

        /// <summary>
        /// You must wait 30 days after purchasing a house before you may purchase another with any character on the same account. This applies to all housing except apartments.\n
        /// </summary>
        OPCODE_0x532 = 0x532,

        /// <summary>
        /// You have been booted from your allegiance chat room. Use &quot;@allegiance chat on&quot; to rejoin. (%s).\n
        /// </summary>
        OPCODE_0x533 = 0x533,

        /// <summary>
        /// %s has been booted from the allegiance chat room.\n
        /// </summary>
        OPCODE_0x534 = 0x534,

        /// <summary>
        /// You do not have the authority within your allegiance to do that.\n
        /// </summary>
        OPCODE_0x535 = 0x535,

        /// <summary>
        /// The account of %s is already banned from the allegiance.\n
        /// </summary>
        OPCODE_0x536 = 0x536,

        /// <summary>
        /// The account of %s is not banned from the allegiance.\n
        /// </summary>
        OPCODE_0x537 = 0x537,

        /// <summary>
        /// The account of %s was not unbanned from the allegiance.\n
        /// </summary>
        OPCODE_0x538 = 0x538,

        /// <summary>
        /// The account of %s has been banned from the allegiance.\n
        /// </summary>
        OPCODE_0x539 = 0x539,

        /// <summary>
        /// The account of %s is no longer banned from the allegiance.\n
        /// </summary>
        OPCODE_0x53A = 0x53A,

        /// <summary>
        /// Banned Characters: 
        /// </summary>
        OPCODE_0x53B = 0x53B,

        /// <summary>
        /// %s is banned from the allegiance!\n
        /// </summary>
        OPCODE_0x53E = 0x53E,

        /// <summary>
        /// You are banned from %s&#39;s allegiance!\n
        /// </summary>
        OPCODE_0x53F = 0x53F,

        /// <summary>
        /// You fail to affect %s because you are acting across a house boundary!\n
        /// </summary>
        OPCODE_0x54 = 0x54,

        /// <summary>
        /// You have the maximum number of accounts banned.!\n
        /// </summary>
        OPCODE_0x540 = 0x540,

        /// <summary>
        /// %s is now an allegiance officer.\n
        /// </summary>
        OPCODE_0x541 = 0x541,

        /// <summary>
        /// An unspecified error occurred while attempting to set %s as an allegiance officer.\n
        /// </summary>
        OPCODE_0x542 = 0x542,

        /// <summary>
        /// %s is no longer an allegiance officer.\n
        /// </summary>
        OPCODE_0x543 = 0x543,

        /// <summary>
        /// An unspecified error occurred while attempting to remove %s as an allegiance officer.\n
        /// </summary>
        OPCODE_0x544 = 0x544,

        /// <summary>
        /// You already have the maximum number of allegiance officers. You must remove some before you add any more.\n
        /// </summary>
        OPCODE_0x545 = 0x545,

        /// <summary>
        /// Your allegiance officers have been cleared.\n
        /// </summary>
        OPCODE_0x546 = 0x546,

        /// <summary>
        /// You must wait %s before communicating again!\n
        /// </summary>
        OPCODE_0x547 = 0x547,

        /// <summary>
        /// You cannot join any chat channels while gagged.\n
        /// </summary>
        OPCODE_0x548 = 0x548,

        /// <summary>
        /// Your allegiance officer status has been modified. You now hold the position of: %s.\n
        /// </summary>
        OPCODE_0x549 = 0x549,

        /// <summary>
        /// You are no longer an allegiance officer.\n
        /// </summary>
        OPCODE_0x54A = 0x54A,

        /// <summary>
        /// %s is already an allegiance officer of that level.\n
        /// </summary>
        OPCODE_0x54B = 0x54B,

        /// <summary>
        /// Your allegiance does not have a hometown.\n
        /// </summary>
        OPCODE_0x54C = 0x54C,

        /// <summary>
        /// The %s is currently in use.\n
        /// </summary>
        OPCODE_0x54D = 0x54D,

        /// <summary>
        /// The hook does not contain a usable item. You cannot open the hook because you do not own the house to which it belongs.\n
        /// </summary>
        OPCODE_0x54E = 0x54E,

        /// <summary>
        /// The hook does not contain a usable item. Use the &#39;@house hooks on&#39;command to make the hook openable.\n
        /// </summary>
        OPCODE_0x54F = 0x54F,

        /// <summary>
        /// Out of Range!
        /// </summary>
        OPCODE_0x550 = 0x550,

        /// <summary>
        /// You are not listening to the %s channel!\n
        /// </summary>
        OPCODE_0x551 = 0x551,

        /// <summary>
        /// You must purchase Asheron&#39;s Call -- Throne of Destiny to use this function.
        /// </summary>
        OPCODE_0x552 = 0x552,

        /// <summary>
        /// You must purchase Asheron&#39;s Call -- Throne of Destiny to use this item.
        /// </summary>
        OPCODE_0x553 = 0x553,

        /// <summary>
        /// You must purchase Asheron&#39;s Call -- Throne of Destiny to use this portal.
        /// </summary>
        OPCODE_0x554 = 0x554,

        /// <summary>
        /// You must purchase Asheron&#39;s Call -- Throne of Destiny to access this quest.
        /// </summary>
        OPCODE_0x555 = 0x555,

        /// <summary>
        /// You have failed to complete the augmentation.\n
        /// </summary>
        OPCODE_0x556 = 0x556,

        /// <summary>
        /// You have used this augmentation too many times already.\n
        /// </summary>
        OPCODE_0x557 = 0x557,

        /// <summary>
        /// You have used augmentations of this type too many times already.\n
        /// </summary>
        OPCODE_0x558 = 0x558,

        /// <summary>
        /// You do not have enough unspent experience available to purchase this augmentation.\n
        /// </summary>
        OPCODE_0x559 = 0x559,

        /// <summary>
        /// %s\n
        /// </summary>
        OPCODE_0x55A = 0x55A,

        /// <summary>
        /// Congratulations! You have succeeded in acquiring the %s augmentation.\n
        /// </summary>
        OPCODE_0x55B = 0x55B,

        /// <summary>
        /// Although your augmentation will not allow you to untrain your %s skill, you have succeeded in recovering all the experience you had invested in it.\n
        /// </summary>
        OPCODE_0x55C = 0x55C,

        /// <summary>
        /// You must exit the Training Academy before that command will be available to you.\n
        /// </summary>
        OPCODE_0x55D = 0x55D,

        /// <summary>
        /// {arbitrary string sent by server}
        /// </summary>
        OPCODE_0x55E = 0x55E,

        /// <summary>
        /// Only Player Killer characters may use this command!\n
        /// </summary>
        OPCODE_0x55F = 0x55F,

        /// <summary>
        /// Only Player Killer Lite characters may use this command!\n
        /// </summary>
        OPCODE_0x560 = 0x560,

        /// <summary>
        /// You may only have a maximum of 50 friends at once. If you wish to add more friends, you must first remove some.
        /// </summary>
        OPCODE_0x561 = 0x561,

        /// <summary>
        /// %s is already on your friends list!\n
        /// </summary>
        OPCODE_0x562 = 0x562,

        /// <summary>
        /// That character is not on your friends list!\n
        /// </summary>
        OPCODE_0x563 = 0x563,

        /// <summary>
        /// Only the character who owns the house may use this command.
        /// </summary>
        OPCODE_0x564 = 0x564,

        /// <summary>
        /// That allegiance name is invalid because it is empty. Please use the @allegiance name clear command to clear your allegiance name.\n
        /// </summary>
        OPCODE_0x565 = 0x565,

        /// <summary>
        /// That allegiance name is too long. Please choose another name.\n
        /// </summary>
        OPCODE_0x566 = 0x566,

        /// <summary>
        /// That allegiance name contains illegal characters. Please choose another name using only letters, spaces, - and &#39;.\n
        /// </summary>
        OPCODE_0x567 = 0x567,

        /// <summary>
        /// That allegiance name is not appropriate. Please choose another name.\n
        /// </summary>
        OPCODE_0x568 = 0x568,

        /// <summary>
        /// That allegiance name is already in use. Please choose another name.\n
        /// </summary>
        OPCODE_0x569 = 0x569,

        /// <summary>
        /// You may only change your allegiance name once every 24 hours. You may change your allegiance name again in %s.\n
        /// </summary>
        OPCODE_0x56A = 0x56A,

        /// <summary>
        /// Your allegiance name has been cleared.\n
        /// </summary>
        OPCODE_0x56B = 0x56B,

        /// <summary>
        /// That is already the name of your allegiance!\n
        /// </summary>
        OPCODE_0x56C = 0x56C,

        /// <summary>
        /// %s is the monarch and cannot be promoted or demoted.\n
        /// </summary>
        OPCODE_0x56D = 0x56D,

        /// <summary>
        /// That level of allegiance officer is now known as: %s.\n
        /// </summary>
        OPCODE_0x56E = 0x56E,

        /// <summary>
        /// That is an invalid officer level.\n
        /// </summary>
        OPCODE_0x56F = 0x56F,

        /// <summary>
        /// That allegiance officer title is not appropriate.\n
        /// </summary>
        OPCODE_0x570 = 0x570,

        /// <summary>
        /// That allegiance name is too long. Please choose another name.\n
        /// </summary>
        That_allegiance_name_is_too_long_Please_choose_another_namen_2 = 0x571,

        /// <summary>
        /// All of your allegiance officer titles have been cleared.\n
        /// </summary>
        OPCODE_0x572 = 0x572,

        /// <summary>
        /// That allegiance title contains illegal characters. Please choose another name using only letters, spaces, - and &#39;.\n
        /// </summary>
        OPCODE_0x573 = 0x573,

        /// <summary>
        /// Your allegiance is currently: %s.\n
        /// </summary>
        OPCODE_0x574 = 0x574,

        /// <summary>
        /// Your allegiance is now: %s.\n
        /// </summary>
        OPCODE_0x575 = 0x575,

        /// <summary>
        /// You may not accept the offer of allegiance from %s because your allegiance is locked.\n
        /// </summary>
        OPCODE_0x576 = 0x576,

        /// <summary>
        /// You may not swear allegiance at this time because the allegiance of %s is locked.\n
        /// </summary>
        OPCODE_0x577 = 0x577,

        /// <summary>
        /// You have pre-approved %s to join your allegiance.\n
        /// </summary>
        OPCODE_0x578 = 0x578,

        /// <summary>
        /// You have not pre-approved any vassals to join your allegiance.\n
        /// </summary>
        OPCODE_0x579 = 0x579,

        /// <summary>
        /// %s is already a member of your allegiance!\n
        /// </summary>
        OPCODE_0x57A = 0x57A,

        /// <summary>
        /// %s has been pre-approved to join your allegiance.\n
        /// </summary>
        OPCODE_0x57B = 0x57B,

        /// <summary>
        /// You have cleared the pre-approved vassal for your allegiance.\n
        /// </summary>
        OPCODE_0x57C = 0x57C,

        /// <summary>
        /// That character is already gagged!\n
        /// </summary>
        OPCODE_0x57D = 0x57D,

        /// <summary>
        /// That character is not currently gagged!\n
        /// </summary>
        OPCODE_0x57E = 0x57E,

        /// <summary>
        /// Your allegiance chat privileges have been temporarily removed by %s. Until they are restored, you may not view or speak in the allegiance chat channel.
        /// </summary>
        OPCODE_0x57F = 0x57F,

        /// <summary>
        /// %s is now temporarily unable to view or speak in allegiance chat. The gag will run out in 5 minutes, or %s may be explicitly ungagged before then.
        /// </summary>
        OPCODE_0x580 = 0x580,

        /// <summary>
        /// Your allegiance chat privileges have been restored.\n
        /// </summary>
        OPCODE_0x581 = 0x581,

        /// <summary>
        /// Your allegiance chat privileges have been restored by %s.
        /// </summary>
        OPCODE_0x582 = 0x582,

        /// <summary>
        /// You have restored allegiance chat privileges to %s.
        /// </summary>
        OPCODE_0x583 = 0x583,

        /// <summary>
        /// You cannot pick up more of that item!
        /// </summary>
        OPCODE_0x584 = 0x584,

        /// <summary>
        /// You are restricted to clothes and armor created for your race.
        /// </summary>
        OPCODE_0x585 = 0x585,

        /// <summary>
        /// That item was specifically created for another race.
        /// </summary>
        OPCODE_0x586 = 0x586,

        /// <summary>
        /// Olthoi cannot interact with that!\n
        /// </summary>
        OPCODE_0x587 = 0x587,

        /// <summary>
        /// Olthoi cannot use regular lifestones! Asheron would not allow it!\n
        /// </summary>
        OPCODE_0x588 = 0x588,

        /// <summary>
        /// The vendor looks at you in horror!\n
        /// </summary>
        OPCODE_0x589 = 0x589,

        /// <summary>
        /// %s cowers from you!\n
        /// </summary>
        OPCODE_0x58A = 0x58A,

        /// <summary>
        /// As a mindless engine of destruction an Olthoi cannot join a fellowship!\n
        /// </summary>
        OPCODE_0x58B = 0x58B,

        /// <summary>
        /// The Olthoi only have an allegiance to the Olthoi Queen!\n
        /// </summary>
        OPCODE_0x58C = 0x58C,

        /// <summary>
        /// You cannot use that item!\n
        /// </summary>
        OPCODE_0x58D = 0x58D,

        /// <summary>
        /// This person will not interact with you!\n
        /// </summary>
        OPCODE_0x58E = 0x58E,

        /// <summary>
        /// Only Olthoi may pass through this portal!\n
        /// </summary>
        OPCODE_0x58F = 0x58F,

        /// <summary>
        /// Olthoi may not pass through this portal!\n
        /// </summary>
        OPCODE_0x590 = 0x590,

        /// <summary>
        /// You may not pass through this portal while Vitae weakens you!\n
        /// </summary>
        OPCODE_0x591 = 0x591,

        /// <summary>
        /// This character must be two weeks old or have been created on an account at least two weeks old to use this portal!\n
        /// </summary>
        OPCODE_0x592 = 0x592,

        /// <summary>
        /// Olthoi characters can only use Lifestone and PK Arena recalls!\n
        /// </summary>
        OPCODE_0x593 = 0x593,

    }

    /// <summary>
    /// The PositionFlags value defines the fields present in the Position structure.
    /// </summary>
    [Flags]
    public enum PositionFlags : uint {
        /// <summary>
        /// velocity vector is present
        /// </summary>
        HasVelocity = 0x0001,

        /// <summary>
        /// placement id is present
        /// </summary>
        HasPlacementID = 0x0002,

        /// <summary>
        /// object is grounded
        /// </summary>
        IsGrounded = 0x0004,

        /// <summary>
        /// orientation quaternion has no w component
        /// </summary>
        OrientationHasNoW = 0x0008,

        /// <summary>
        /// orientation quaternion has no x component
        /// </summary>
        OrientationHasNoX = 0x0010,

        /// <summary>
        /// orientation quaternion has no y component
        /// </summary>
        OrientationHasNoY = 0x0020,

        /// <summary>
        /// orientation quaternion has no z component
        /// </summary>
        OrientationHasNoZ = 0x0040,

    }

    /// <summary>
    /// The OptionPropertyID identifies a specific character option.
    /// </summary>
    public enum OptionPropertyID : int {
        AutomaticallyRepeatAttacks = 0x00,

        IgnoreAllegianceRequests = 0x01,

        IgnoreFellowshipRequests = 0x02,

        ShareFellowshipExperience = 0x0F,

        AcceptCorpseLootingPermissions = 0x10,

        ShareFellowshipLoot = 0x11,

        AutomaticallyAcceptFellowshipRequests = 0x12,

        UseChargeAttack = 0x19,

        ListenToAllegianceChat = 0x1B,

        ListenToGeneralChat = 0x23,

        ListenToTradeChat = 0x24,

        ListenToLFGChat = 0x25,

        ListenToRoleplayingChat = 0x26,

        LeadMissleTargets = 0x2A,

        UseFastMissles = 0x2B,

    }

    /// <summary>
    /// Height of the attack.  TODO these need to be verified.
    /// </summary>
    public enum AttackHeight : uint {
        High = 0x01,

        Medium = 0x02,

        Low = 0x03,

    }

    /// <summary>
    /// Container properties of an item
    /// </summary>
    public enum ContainerProperties : uint {
        None = 0x00,

        Container = 0x01,

        Foci = 0x02,

    }

    /// <summary>
    /// The objects type information
    /// </summary>
    [Flags]
    public enum ObjectType : uint {
        MeleeWeapon = 0x00000001,

        Armor = 0x00000002,

        Clothing = 0x00000004,

        Jewelry = 0x00000008,

        Creature = 0x00000010,

        Food = 0x00000020,

        Money = 0x00000040,

        Misc = 0x00000080,

        MissileWeapon = 0x00000100,

        Container = 0x00000200,

        Useless = 0x00000400,

        Gem = 0x00000800,

        SpellComponents = 0x00001000,

        Writable = 0x00002000,

        Key = 0x00004000,

        Caster = 0x00008000,

        Portal = 0x00010000,

        Lockable = 0x00020000,

        PromissoryNote = 0x00040000,

        ManaStone = 0x00080000,

        Service = 0x00100000,

        MagicWieldable = 0x00200000,

        CraftCookingBase = 0x00400000,

        CraftAlchemyBase = 0x00800000,

        CraftFletchingBase = 0x02000000,

        CraftAlchemyIntermediate = 0x04000000,

        CraftFletchingIntermediate = 0x08000000,

        LifeStone = 0x10000000,

        TinkeringTool = 0x20000000,

        TinkeringMaterial = 0x40000000,

        Gameboard = 0x80000000,

    }

    /// <summary>
    /// The IntPropertyID identifies a specific Character or Object int property.
    /// </summary>
    public enum IntPropertyID : uint {
        Undef = 0x0000,

        ItemType = 0x0001,

        CreatureType = 0x0002,

        PaletteTemplate = 0x0003,

        ClothingPriority = 0x0004,

        EncumbranceVal = 0x0005,

        ItemsCapacity = 0x0006,

        ContainersCapacity = 0x0007,

        Mass = 0x0008,

        ValidLocations = 0x0009,

        CurrentWieldedLocation = 0x000A,

        MaxStackSize = 0x000B,

        StackSize = 0x000C,

        StackUnitEncumbrance = 0x000D,

        StackUnitMass = 0x000E,

        StackUnitValue = 0x000F,

        ItemUseable = 0x0010,

        RareId = 0x0011,

        UiEffects = 0x0012,

        Value = 0x0013,

        CoinValue = 0x0014,

        TotalExperience = 0x0015,

        AvailableCharacter = 0x0016,

        TotalSkillCredits = 0x0017,

        AvailableSkillCredits = 0x0018,

        Level = 0x0019,

        AccountRequirements = 0x001A,

        ArmorType = 0x001B,

        ArmorLevel = 0x001C,

        AllegianceCpPool = 0x001D,

        AllegianceRank = 0x001E,

        ChannelsAllowed = 0x001F,

        ChannelsActive = 0x0020,

        Bonded = 0x0021,

        MonarchsRank = 0x0022,

        AllegianceFollowers = 0x0023,

        ResistMagic = 0x0024,

        ResistItemAppraisal = 0x0025,

        ResistLockpick = 0x0026,

        DeprecatedResistRepair = 0x0027,

        CombatMode = 0x0028,

        CurrentAttackHeight = 0x0029,

        CombatCollisions = 0x002A,

        NumDeaths = 0x002B,

        Damage = 0x002C,

        DamageType = 0x002D,

        DefaultCombatStyle = 0x002E,

        AttackType = 0x002F,

        WeaponSkill = 0x0030,

        WeaponTime = 0x0031,

        AmmoType = 0x0032,

        CombatUse = 0x0033,

        ParentLocation = 0x0034,

        PlacementPosition = 0x0035,

        WeaponEncumbrance = 0x0036,

        WeaponMass = 0x0037,

        ShieldValue = 0x0038,

        ShieldEncumbrance = 0x0039,

        MissileInventoryLocation = 0x003A,

        FullDamageType = 0x003B,

        WeaponRange = 0x003C,

        AttackersSkill = 0x003D,

        DefendersSkill = 0x003E,

        AttackersSkillValue = 0x003F,

        AttackersClass = 0x0040,

        Placement = 0x0041,

        CheckpointStatus = 0x0042,

        Tolerance = 0x0043,

        TargetingTactic = 0x0044,

        CombatTactic = 0x0045,

        HomesickTargetingTactic = 0x0046,

        NumFollowFailures = 0x0047,

        FriendType = 0x0048,

        FoeType = 0x0049,

        MerchandiseItemTypes = 0x004A,

        MerchandiseMinValue = 0x004B,

        MerchandiseMaxValue = 0x004C,

        NumItemsSold = 0x004D,

        NumItemsBought = 0x004E,

        MoneyIncome = 0x004F,

        MoneyOutflow = 0x0050,

        MaxGeneratedObjects = 0x0051,

        InitGeneratedObjects = 0x0052,

        ActivationResponse = 0x0053,

        OriginalValue = 0x0054,

        NumMoveFailures = 0x0055,

        MinLevel = 0x0056,

        MaxLevel = 0x0057,

        LockpickMod = 0x0058,

        BoosterEnum = 0x0059,

        BoostValue = 0x005A,

        MaxStructure = 0x005B,

        Structure = 0x005C,

        PhysicsState = 0x005D,

        TargetType = 0x005E,

        RadarBlipColor = 0x005F,

        EncumbranceCapacity = 0x0060,

        LoginTimestamp = 0x0061,

        CreationTimestamp = 0x0062,

        PkLevelModifier = 0x0063,

        GeneratorType = 0x0064,

        AiAllowedCombatStyle = 0x0065,

        LogoffTimestamp = 0x0066,

        GeneratorDestructionType = 0x0067,

        ActivationCreateClass = 0x0068,

        ItemWorkmanship = 0x0069,

        ItemSpellcraft = 0x006A,

        ItemCurMana = 0x006B,

        ItemMaxMana = 0x006C,

        ItemDifficulty = 0x006D,

        ItemAllegianceRankLimit = 0x006E,

        PortalBitmask = 0x006F,

        AdvocateLevel = 0x0070,

        Gender = 0x0071,

        Attuned = 0x0072,

        ItemSkillLevelLimit = 0x0073,

        GateLogic = 0x0074,

        ItemManaCost = 0x0075,

        Logoff = 0x0076,

        Active = 0x0077,

        AttackHeight = 0x0078,

        NumAttackFailures = 0x0079,

        AiCpThreshold = 0x007A,

        AiAdvancementStrategy = 0x007B,

        Version = 0x007C,

        Age = 0x007D,

        VendorHappyMean = 0x007E,

        VendorHappyVariance = 0x007F,

        CloakStatus = 0x0080,

        VitaeCpPool = 0x0081,

        NumServicesSold = 0x0082,

        MaterialType = 0x0083,

        NumAllegianceBreaks = 0x0084,

        ShowableOnRadar = 0x0085,

        PlayerKillerStatus = 0x0086,

        VendorHappyMaxItems = 0x0087,

        ScorePageNum = 0x0088,

        ScoreConfigNum = 0x0089,

        ScoreNumScores = 0x008A,

        DeathLevel = 0x008B,

        AiOptions = 0x008C,

        OpenToEveryone = 0x008D,

        GeneratorTimeType = 0x008E,

        GeneratorStartTime = 0x008F,

        GeneratorEndTime = 0x0090,

        GeneratorEndDestructionType = 0x0091,

        XpOverride = 0x0092,

        NumCrashAndTurns = 0x0093,

        ComponentWarningThreshold = 0x0094,

        HouseStatus = 0x0095,

        HookPlacement = 0x0096,

        HookType = 0x0097,

        HookItemType = 0x0098,

        AiPpThreshold = 0x0099,

        GeneratorVersion = 0x009A,

        HouseType = 0x009B,

        PickupEmoteOffset = 0x009C,

        WeenieIteration = 0x009D,

        WieldRequirements = 0x009E,

        WieldSkilltype = 0x009F,

        WieldDifficulty = 0x00A0,

        HouseMaxHooksUsable = 0x00A1,

        HouseCurrentHooksUsable = 0x00A2,

        AllegianceMinLevel = 0x00A3,

        AllegianceMaxLevel = 0x00A4,

        HouseRelinkHookCount = 0x00A5,

        SlayerCreatureType = 0x00A6,

        ConfirmationInProgress = 0x00A7,

        ConfirmationTypeInProgress = 0x00A8,

        TsysMutationData = 0x00A9,

        NumItemsInMaterial = 0x00AA,

        NumTimesTinkered = 0x00AB,

        AppraisalLongDescDecoration = 0x00AC,

        AppraisalLockpickSuccessPercent = 0x00AD,

        AppraisalPages = 0x00AE,

        AppraisalMaxPages = 0x00AF,

        AppraisalItemSkill = 0x00B0,

        GemCount = 0x00B1,

        GemType = 0x00B2,

        ImbuedEffect = 0x00B3,

        AttackersRawSkillValue = 0x00B4,

        ChessRank = 0x00B5,

        ChessTotalGames = 0x00B6,

        ChessGamesWon = 0x00B7,

        ChessGamesLost = 0x00B8,

        TypeOfAlteration = 0x00B9,

        SkillToBeAltered = 0x00BA,

        SkillAlterationCount = 0x00BB,

        HeritageGroup = 0x00BC,

        TransferFromAttribute = 0x00BD,

        TransferToAttribute = 0x00BE,

        AttributeTransferCount = 0x00BF,

        FakeFishingSkill = 0x00C0,

        NumKeys = 0x00C1,

        DeathTimestamp = 0x00C2,

        PkTimestamp = 0x00C3,

        VictimTimestamp = 0x00C4,

        HookGroup = 0x00C5,

        AllegianceSwearTimestamp = 0x00C6,

        HousePurchaseTimestamp = 0x00C7,

        RedirectableEquippedArmorCount = 0x00C8,

        MeleedefenseImbuedEffectTypeCache = 0x00C9,

        MissileDefenseImbuedEffectTypeCache = 0x00CA,

        MagicDefenseImbuedEffectTypeCache = 0x00CB,

        ElementalDamageBonus = 0x00CC,

        ImbueAttempts = 0x00CD,

        ImbueSuccesses = 0x00CE,

        CreatureKills = 0x00CF,

        PlayerKillsPk = 0x00D0,

        PlayerKillsPkl = 0x00D1,

        RaresTierOne = 0x00D2,

        RaresTierTwo = 0x00D3,

        RaresTierThree = 0x00D4,

        RaresTierFour = 0x00D5,

        RaresTierFive = 0x00D6,

        AugmentationStat = 0x00D7,

        AugmentationFamilyStat = 0x00D8,

        AugmentationInnateFamily = 0x00D9,

        AugmentationInnateStrength = 0x00DA,

        AugmentationInnateEndurance = 0x00DB,

        AugmentationInnateCoordination = 0x00DC,

        AugmentationInnateQuickness = 0x00DD,

        AugmentationInnateFocus = 0x00DE,

        AugmentationInnateSelf = 0x00DF,

        AugmentationSpecializeSalvaging = 0x00E0,

        AugmentationSpecializeItemTinkering = 0x00E1,

        AugmentationSpecializeArmorTinkering = 0x00E2,

        AugmentationSpecializeMagicItemTinkering = 0x00E3,

        AugmentationSpecializeWeaponTinkering = 0x00E4,

        AugmentationExtraPackSlot = 0x00E5,

        AugmentationIncreasedCarryingCapacity = 0x00E6,

        AugmentationLessDeathItemLoss = 0x00E7,

        AugmentationSpellsRemainPastDeath = 0x00E8,

        AugmentationCriticalDefense = 0x00E9,

        AugmentationBonusXp = 0x00EA,

        AugmentationBonusSalvage = 0x00EB,

        AugmentationBonusImbueChance = 0x00EC,

        AugmentationFasterRegen = 0x00ED,

        AugmentationIncreasedSpellDuration = 0x00EE,

        AugmentationResistanceFamily = 0x00EF,

        AugmentationResistanceSlash = 0x00F0,

        AugmentationResistancePierce = 0x00F1,

        AugmentationResistanceBlunt = 0x00F2,

        AugmentationResistanceAcid = 0x00F3,

        AugmentationResistanceFire = 0x00F4,

        AugmentationResistanceFrost = 0x00F5,

        AugmentationResistanceLightning = 0x00F6,

        RaresTierOneLogin = 0x00F7,

        RaresTierTwoLogin = 0x00F8,

        RaresTierThreeLogin = 0x00F9,

        RaresTierFourLogin = 0x00FA,

        RaresTierFiveLogin = 0x00FB,

        RaresLoginTimestamp = 0x00FC,

        RaresTierSix = 0x00FD,

        RaresTierSeven = 0x00FE,

        RaresTierSixLogin = 0x00FF,

        RaresTierSevenLogin = 0x0100,

        ItemAttributeLimit = 0x0101,

        ItemAttributeLevelLimit = 0x0102,

        ItemAttribute2ndLimit = 0x0103,

        ItemAttribute2ndLevelLimit = 0x0104,

        CharacterTitleId = 0x0105,

        NumCharacterTitles = 0x0106,

        ResistanceModifierType = 0x0107,

        FreeTinkersBitfield = 0x0108,

        EquipmentSetId = 0x0109,

        PetClass = 0x010A,

        Lifespan = 0x010B,

        RemainingLifespan = 0x010C,

        UseCreateQuantity = 0x010D,

        WieldRequirements2 = 0x010E,

        WieldSkilltype2 = 0x010F,

        WieldDifficulty2 = 0x0110,

        WieldRequirements3 = 0x0111,

        WieldSkilltype3 = 0x0112,

        WieldDifficulty3 = 0x0113,

        WieldRequirements4 = 0x0114,

        WieldSkilltype4 = 0x0115,

        WieldDifficulty4 = 0x0116,

        Unique = 0x0117,

        SharedCooldown = 0x0118,

        Faction1Bits = 0x0119,

        Faction2Bits = 0x011A,

        Faction3Bits = 0x011B,

        Hatred1Bits = 0x011C,

        Hatred2Bits = 0x011D,

        Hatred3Bits = 0x011E,

        SocietyRankCelhan = 0x011F,

        SocietyRankEldweb = 0x0120,

        SocietyRankRadblo = 0x0121,

        HearLocalSignals = 0x0122,

        HearLocalSignalsRadius = 0x0123,

        Cleaving = 0x0124,

        AugmentationSpecializeGearcraft = 0x0125,

        AugmentationInfusedCreatureMagic = 0x0126,

        AugmentationInfusedItemMagic = 0x0127,

        AugmentationInfusedLifeMagic = 0x0128,

        AugmentationInfusedWarMagic = 0x0129,

        AugmentationCriticalExpertise = 0x012A,

        AugmentationCriticalPower = 0x012B,

        AugmentationSkilledMelee = 0x012C,

        AugmentationSkilledMissile = 0x012D,

        AugmentationSkilledMagic = 0x012E,

        ImbuedEffect2 = 0x012F,

        ImbuedEffect3 = 0x0130,

        ImbuedEffect4 = 0x0131,

        ImbuedEffect5 = 0x0132,

        DamageRating = 0x0133,

        DamageResistRating = 0x0134,

        AugmentationDamageBonus = 0x0135,

        AugmentationDamageReduction = 0x0136,

        ImbueStackingBits = 0x0137,

        HealOverTime = 0x0138,

        CritRating = 0x0139,

        CritDamageRating = 0x013A,

        CritResistRating = 0x013B,

        CritDamageResistRating = 0x013C,

        HealingResistRating = 0x013D,

        DamageOverTime = 0x013E,

        ItemMaxLevel = 0x013F,

        ItemXpStyle = 0x0140,

        EquipmentSetExtra = 0x0141,

        AetheriaBitfield = 0x0142,

        HealingBoostRating = 0x0143,

        HeritageSpecificArmor = 0x0144,

        AlternateRacialSkills = 0x0145,

        AugmentationJackOfAllTrades = 0x0146,

        AugmentationResistanceNether = 0x0147,

        AugmentationInfusedVoidMagic = 0x0148,

        WeaknessRating = 0x0149,

        NetherOverTime = 0x014A,

        NetherResistRating = 0x014B,

        LuminanceAward = 0x014C,

        LumAugDamageRating = 0x014D,

        LumAugDamageReductionRating = 0x014E,

        LumAugCritDamageRating = 0x014F,

        LumAugCritReductionRating = 0x0150,

        LumAugSurgeEffectRating = 0x0151,

        LumAugSurgeChanceRating = 0x0152,

        LumAugItemManaUsage = 0x0153,

        LumAugItemManaGain = 0x0154,

        LumAugVitality = 0x0155,

        LumAugHealingRating = 0x0156,

        LumAugSkilledCraft = 0x0157,

        LumAugSkilledSpec = 0x0158,

        LumAugNoDestroyCraft = 0x0159,

        RestrictInteraction = 0x015A,

        OlthoiLootTimestamp = 0x015B,

        OlthoiLootStep = 0x015C,

        UseCreatesContractId = 0x015D,

        DotResistRating = 0x015E,

        LifeResistRating = 0x015F,

        CloakWeaveProc = 0x0160,

        WeaponType = 0x0161,

        MeleeMastery = 0x0162,

        RangedMastery = 0x0163,

        SneakAttackRating = 0x0164,

        RecklessnessRating = 0x0165,

        DeceptionRating = 0x0166,

        CombatPetRange = 0x0167,

        WeaponAuraDamage = 0x0168,

        WeaponAuraSpeed = 0x0169,

        SummoningMastery = 0x016A,

        HeartbeatLifespan = 0x016B,

        UseLevelRequirement = 0x016C,

        LumAugAllSkills = 0x016D,

        UseRequiresSkill = 0x016E,

        UseRequiresSkillLevel = 0x016F,

        UseRequiresSkillSpec = 0x0170,

        UseRequiresLevel = 0x0171,

        GearDamage = 0x0172,

        GearDamageResist = 0x0173,

        GearCrit = 0x0174,

        GearCritResist = 0x0175,

        GearCritDamage = 0x0176,

        GearCritDamageResist = 0x0177,

        GearHealingBoost = 0x0178,

        GearNetherResist = 0x0179,

        GearLifeResist = 0x017A,

        GearMaxHealth = 0x017B,

        Unknown380 = 0x017C,

        Unknown381 = 0x017D,

        Unknown382 = 0x017E,

        Unknown383 = 0x017F,

        Unknown384 = 0x0180,

        Unknown385 = 0x0181,

        Unknown386 = 0x0182,

        Unknown387 = 0x0183,

        Unknown388 = 0x0184,

        Unknown389 = 0x0185,

        Unknown390 = 0x0186,

    }

    /// <summary>
    /// The Int64PropertyID identifies a specific Character or Object int64 property.
    /// </summary>
    public enum Int64PropertyID : uint {
        Undef = 0x0000,

        TotalExperience = 0x0001,

        AvailableExperience = 0x0002,

        AugmentationCost = 0x0003,

        ItemTotalXp = 0x0004,

        ItemBaseXp = 0x0005,

        AvailableLuminance = 0x0006,

        MaximumLuminance = 0x0007,

        InteractionReqs = 0x0008,

    }

    /// <summary>
    /// The BooleanPropertyID identifies a specific Character or Object boolean property.
    /// </summary>
    public enum BooleanPropertyID : uint {
        Undef = 0x0000,

        Stuck = 0x0001,

        Open = 0x0002,

        Locked = 0x0003,

        RotProof = 0x0004,

        AllegianceUpdateRequest = 0x0005,

        AiUsesMana = 0x0006,

        AiUseHumanMagicAnimations = 0x0007,

        AllowGive = 0x0008,

        CurrentlyAttacking = 0x0009,

        AttackerAi = 0x000A,

        IgnoreCollisions = 0x000B,

        ReportCollisions = 0x000C,

        Ethereal = 0x000D,

        GravityStatus = 0x000E,

        LightsStatus = 0x000F,

        ScriptedCollision = 0x0010,

        Inelastic = 0x0011,

        Visibility = 0x0012,

        Attackable = 0x0013,

        SafeSpellComponents = 0x0014,

        AdvocateState = 0x0015,

        Inscribable = 0x0016,

        DestroyOnSell = 0x0017,

        UiHidden = 0x0018,

        IgnoreHouseBarriers = 0x0019,

        HiddenAdmin = 0x001A,

        PkWounder = 0x001B,

        PkKiller = 0x001C,

        NoCorpse = 0x001D,

        UnderLifestoneProtection = 0x001E,

        ItemManaUpdatePending = 0x001F,

        GeneratorStatus = 0x0020,

        ResetMessagePending = 0x0021,

        DefaultOpen = 0x0022,

        DefaultLocked = 0x0023,

        DefaultOn = 0x0024,

        OpenForBusiness = 0x0025,

        IsFrozen = 0x0026,

        DealMagicalItems = 0x0027,

        LogoffImDead = 0x0028,

        ReportCollisionsAsEnvironment = 0x0029,

        AllowEdgeSlide = 0x002A,

        AdvocateQuest = 0x002B,

        IsAdmin = 0x002C,

        IsArch = 0x002D,

        IsSentinel = 0x002E,

        IsAdvocate = 0x002F,

        CurrentlyPoweringUp = 0x0030,

        GeneratorEnteredWorld = 0x0031,

        NeverFailCasting = 0x0032,

        VendorService = 0x0033,

        AiImmobile = 0x0034,

        DamagedByCollisions = 0x0035,

        IsDynamic = 0x0036,

        IsHot = 0x0037,

        IsAffecting = 0x0038,

        AffectsAis = 0x0039,

        SpellQueueActive = 0x003A,

        GeneratorDisabled = 0x003B,

        IsAcceptingTells = 0x003C,

        LoggingChannel = 0x003D,

        OpensAnyLock = 0x003E,

        UnlimitedUse = 0x003F,

        GeneratedTreasureItem = 0x0040,

        IgnoreMagicResist = 0x0041,

        IgnoreMagicArmor = 0x0042,

        AiAllowTrade = 0x0043,

        SpellComponentsRequired = 0x0044,

        IsSellable = 0x0045,

        IgnoreShieldsBySkill = 0x0046,

        NoDraw = 0x0047,

        ActivationUntargeted = 0x0048,

        HouseHasGottenPriorityBootPos = 0x0049,

        GeneratorAutomaticDestruction = 0x004A,

        HouseHooksVisible = 0x004B,

        HouseRequiresMonarch = 0x004C,

        HouseHooksEnabled = 0x004D,

        HouseNotifiedHudOfHookCount = 0x004E,

        AiAcceptEverything = 0x004F,

        IgnorePortalRestrictions = 0x0050,

        RequiresBackpackSlot = 0x0051,

        DontTurnOrMoveWhenGiving = 0x0052,

        NpcLooksLikeObject = 0x0053,

        IgnoreCloIcons = 0x0054,

        AppraisalHasAllowedWielder = 0x0055,

        ChestRegenOnClose = 0x0056,

        LogoffInMinigame = 0x0057,

        PortalShowDestination = 0x0058,

        PortalIgnoresPkAttackTimer = 0x0059,

        NpcInteractsSilently = 0x005A,

        Retained = 0x005B,

        IgnoreAuthor = 0x005C,

        Limbo = 0x005D,

        AppraisalHasAllowedActivator = 0x005E,

        ExistedBeforeAllegianceXpChanges = 0x005F,

        IsDeaf = 0x0060,

        IsPsr = 0x0061,

        Invincible = 0x0062,

        Ivoryable = 0x0063,

        Dyable = 0x0064,

        CanGenerateRare = 0x0065,

        CorpseGeneratedRare = 0x0066,

        NonProjectileMagicImmune = 0x0067,

        ActdReceivedItems = 0x0068,

        Unknown105 = 0x0069,

        FirstEnterWorldDone = 0x006A,

        RecallsDisabled = 0x006B,

        RareUsesTimer = 0x006C,

        ActdPreorderReceivedItems = 0x006D,

        Afk = 0x006E,

        IsGagged = 0x006F,

        ProcSpellSelfTargeted = 0x0070,

        IsAllegianceGagged = 0x0071,

        EquipmentSetTriggerPiece = 0x0072,

        Uninscribe = 0x0073,

        WieldOnUse = 0x0074,

        ChestClearedWhenClosed = 0x0075,

        NeverAttack = 0x0076,

        SuppressGenerateEffect = 0x0077,

        TreasureCorpse = 0x0078,

        EquipmentSetAddLevel = 0x0079,

        BarberActive = 0x007A,

        TopLayerPriority = 0x007B,

        NoHeldItemShown = 0x007C,

        LoginAtLifestone = 0x007D,

        OlthoiPk = 0x007E,

        Account15Days = 0x007F,

        HadNoVitae = 0x0080,

        NoOlthoiTalk = 0x0081,

        AutowieldLeft = 0x0082,

    }

    /// <summary>
    /// The FloatPropertyID identifies a specific Character or Object float property.
    /// </summary>
    public enum FloatPropertyID : uint {
        Undef = 0x0000,

        HeartbeatInterval = 0x0001,

        HeartbeatTimestamp = 0x0002,

        HealthRate = 0x0003,

        StaminaRate = 0x0004,

        ManaRate = 0x0005,

        HealthUponResurrection = 0x0006,

        StaminaUponResurrection = 0x0007,

        ManaUponResurrection = 0x0008,

        StartTime = 0x0009,

        StopTime = 0x000A,

        ResetInterval = 0x000B,

        Shade = 0x000C,

        ArmorModVsSlash = 0x000D,

        ArmorModVsPierce = 0x000E,

        ArmorModVsBludgeon = 0x000F,

        ArmorModVsCold = 0x0010,

        ArmorModVsFire = 0x0011,

        ArmorModVsAcid = 0x0012,

        ArmorModVsElectric = 0x0013,

        CombatSpeed = 0x0014,

        WeaponLength = 0x0015,

        DamageVariance = 0x0016,

        CurrentPowerMod = 0x0017,

        AccuracyMod = 0x0018,

        StrengthMod = 0x0019,

        MaximumVelocity = 0x001A,

        RotationSpeed = 0x001B,

        MotionTimestamp = 0x001C,

        WeaponDefense = 0x001D,

        WimpyLevel = 0x001E,

        VisualAwarenessRange = 0x001F,

        AuralAwarenessRange = 0x0020,

        PerceptionLevel = 0x0021,

        PowerupTime = 0x0022,

        MaxChargeDistance = 0x0023,

        ChargeSpeed = 0x0024,

        BuyPrice = 0x0025,

        SellPrice = 0x0026,

        DefaultScale = 0x0027,

        LockpickMod = 0x0028,

        RegenerationInterval = 0x0029,

        RegenerationTimestamp = 0x002A,

        GeneratorRadius = 0x002B,

        TimeToRot = 0x002C,

        DeathTimestamp = 0x002D,

        PkTimestamp = 0x002E,

        VictimTimestamp = 0x002F,

        LoginTimestamp = 0x0030,

        CreationTimestamp = 0x0031,

        MinimumTimeSincePk = 0x0032,

        DeprecatedHousekeepingPriority = 0x0033,

        AbuseLoggingTimestamp = 0x0034,

        LastPortalTeleportTimestamp = 0x0035,

        UseRadius = 0x0036,

        HomeRadius = 0x0037,

        ReleasedTimestamp = 0x0038,

        MinHomeRadius = 0x0039,

        Facing = 0x003A,

        ResetTimestamp = 0x003B,

        LogoffTimestamp = 0x003C,

        EconRecoveryInterval = 0x003D,

        WeaponOffense = 0x003E,

        DamageMod = 0x003F,

        ResistSlash = 0x0040,

        ResistPierce = 0x0041,

        ResistBludgeon = 0x0042,

        ResistFire = 0x0043,

        ResistCold = 0x0044,

        ResistAcid = 0x0045,

        ResistElectric = 0x0046,

        ResistHealthBoost = 0x0047,

        ResistStaminaDrain = 0x0048,

        ResistStaminaBoost = 0x0049,

        ResistManaDrain = 0x004A,

        ResistManaBoost = 0x004B,

        Translucency = 0x004C,

        PhysicsScriptIntensity = 0x004D,

        Friction = 0x004E,

        Elasticity = 0x004F,

        AiUseMagicDelay = 0x0050,

        ItemMinSpellcraftMod = 0x0051,

        ItemMaxSpellcraftMod = 0x0052,

        ItemRankProbability = 0x0053,

        Shade2 = 0x0054,

        Shade3 = 0x0055,

        Shade4 = 0x0056,

        ItemEfficiency = 0x0057,

        ItemManaUpdateTimestamp = 0x0058,

        SpellGestureSpeedMod = 0x0059,

        SpellStanceSpeedMod = 0x005A,

        AllegianceAppraisalTimestamp = 0x005B,

        PowerLevel = 0x005C,

        AccuracyLevel = 0x005D,

        AttackAngle = 0x005E,

        AttackTimestamp = 0x005F,

        CheckpointTimestamp = 0x0060,

        SoldTimestamp = 0x0061,

        UseTimestamp = 0x0062,

        UseLockTimestamp = 0x0063,

        HealkitMod = 0x0064,

        FrozenTimestamp = 0x0065,

        HealthRateMod = 0x0066,

        AllegianceSwearTimestamp = 0x0067,

        ObviousRadarRange = 0x0068,

        HotspotCycleTime = 0x0069,

        HotspotCycleTimeVariance = 0x006A,

        SpamTimestamp = 0x006B,

        SpamRate = 0x006C,

        BondWieldedTreasure = 0x006D,

        BulkMod = 0x006E,

        SizeMod = 0x006F,

        GagTimestamp = 0x0070,

        GeneratorUpdateTimestamp = 0x0071,

        DeathSpamTimestamp = 0x0072,

        DeathSpamRate = 0x0073,

        WildAttackProbability = 0x0074,

        FocusedProbability = 0x0075,

        CrashAndTurnProbability = 0x0076,

        CrashAndTurnRadius = 0x0077,

        CrashAndTurnBias = 0x0078,

        GeneratorInitialDelay = 0x0079,

        AiAcquireHealth = 0x007A,

        AiAcquireStamina = 0x007B,

        AiAcquireMana = 0x007C,

        ResistHealthDrain = 0x007D,

        LifestoneProtectionTimestamp = 0x007E,

        AiCounteractEnchantment = 0x007F,

        AiDispelEnchantment = 0x0080,

        TradeTimestamp = 0x0081,

        AiTargetedDetectionRadius = 0x0082,

        EmotePriority = 0x0083,

        LastTeleportStartTimestamp = 0x0084,

        EventSpamTimestamp = 0x0085,

        EventSpamRate = 0x0086,

        InventoryOffset = 0x0087,

        CriticalMultiplier = 0x0088,

        ManaStoneDestroyChance = 0x0089,

        SlayerDamageBonus = 0x008A,

        AllegianceInfoSpamTimestamp = 0x008B,

        AllegianceInfoSpamRate = 0x008C,

        NextSpellcastTimestamp = 0x008D,

        AppraisalRequestedTimestamp = 0x008E,

        AppraisalHeartbeatDueTimestamp = 0x008F,

        ManaConversionMod = 0x0090,

        LastPkAttackTimestamp = 0x0091,

        FellowshipUpdateTimestamp = 0x0092,

        CriticalFrequency = 0x0093,

        LimboStartTimestamp = 0x0094,

        WeaponMissileDefense = 0x0095,

        WeaponMagicDefense = 0x0096,

        IgnoreShield = 0x0097,

        ElementalDamageMod = 0x0098,

        StartMissileAttackTimestamp = 0x0099,

        LastRareUsedTimestamp = 0x009A,

        IgnoreArmor = 0x009B,

        ProcSpellRate = 0x009C,

        ResistanceModifier = 0x009D,

        AllegianceGagTimestamp = 0x009E,

        AbsorbMagicDamage = 0x009F,

        CachedMaxAbsorbMagicDamage = 0x00A0,

        GagDuration = 0x00A1,

        AllegianceGagDuration = 0x00A2,

        GlobalXpMod = 0x00A3,

        HealingModifier = 0x00A4,

        ArmorModVsNether = 0x00A5,

        ResistNether = 0x00A6,

        CooldownDuration = 0x00A7,

        WeaponAuraOffense = 0x00A8,

        WeaponAuraDefense = 0x00A9,

        WeaponAuraElemental = 0x00AA,

        WeaponAuraManaConv = 0x00AB,

    }

    /// <summary>
    /// The StringPropertyID identifies a specific Character or Object string property.
    /// </summary>
    public enum StringPropertyID : uint {
        Undef = 0x0000,

        Name = 0x0001,

        Title = 0x0002,

        Sex = 0x0003,

        HeritageGroup = 0x0004,

        Template = 0x0005,

        AttackersName = 0x0006,

        Inscription = 0x0007,

        ScribeName = 0x0008,

        VendorsName = 0x0009,

        Fellowship = 0x000A,

        MonarchsName = 0x000B,

        LockCode = 0x000C,

        KeyCode = 0x000D,

        Use = 0x000E,

        ShortDesc = 0x000F,

        LongDesc = 0x0010,

        ActivationTalk = 0x0011,

        UseMessage = 0x0012,

        ItemHeritageGroupRestriction = 0x0013,

        PluralName = 0x0014,

        MonarchsTitle = 0x0015,

        ActivationFailure = 0x0016,

        ScribeAccount = 0x0017,

        TownName = 0x0018,

        CraftsmanName = 0x0019,

        UsePkServerError = 0x001A,

        ScoreCachedText = 0x001B,

        ScoreDefaultEntryFormat = 0x001C,

        ScoreFirstEntryFormat = 0x001D,

        ScoreLastEntryFormat = 0x001E,

        ScoreOnlyEntryFormat = 0x001F,

        ScoreNoEntry = 0x0020,

        Quest = 0x0021,

        GeneratorEvent = 0x0022,

        PatronsTitle = 0x0023,

        HouseOwnerName = 0x0024,

        QuestRestriction = 0x0025,

        AppraisalPortalDestination = 0x0026,

        TinkerName = 0x0027,

        ImbuerName = 0x0028,

        HouseOwnerAccount = 0x0029,

        DisplayName = 0x002A,

        DateOfBirth = 0x002B,

        ThirdPartyApi = 0x002C,

        KillQuest = 0x002D,

        Afk = 0x002E,

        AllegianceName = 0x002F,

        AugmentationAddQuest = 0x0030,

        KillQuest2 = 0x0031,

        KillQuest3 = 0x0032,

        UseSendsSignal = 0x0033,

        GearPlatingName = 0x0034,

    }

    /// <summary>
    /// The DataPropertyId identifies a specific Character or Object data property.
    /// </summary>
    public enum DataPropertyID : uint {
        Undef = 0x0000,

        Setup = 0x0001,

        MotionTable = 0x0002,

        SoundTable = 0x0003,

        CombatTable = 0x0004,

        QualityFilter = 0x0005,

        PaletteBase = 0x0006,

        ClothingBase = 0x0007,

        Icon = 0x0008,

        EyesTexture = 0x0009,

        NoseTexture = 0x000A,

        MouthTexture = 0x000B,

        DefaultEyesTexture = 0x000C,

        DefaultNoseTexture = 0x000D,

        DefaultMouthTexture = 0x000E,

        HairPalette = 0x000F,

        EyesPalette = 0x0010,

        SkinPalette = 0x0011,

        HeadObject = 0x0012,

        ActivationAnimation = 0x0013,

        InitMotion = 0x0014,

        ActivationSound = 0x0015,

        PhysicsEffectTable = 0x0016,

        UseSound = 0x0017,

        UseTargetAnimation = 0x0018,

        UseTargetSuccessAnimation = 0x0019,

        UseTargetFailureAnimation = 0x001A,

        UseUserAnimation = 0x001B,

        Spell = 0x001C,

        SpellComponent = 0x001D,

        PhysicsScript = 0x001E,

        LinkedPortalOne = 0x001F,

        WieldedTreasureType = 0x0020,

        UnknownGuessedname = 0x0021,

        UnknownGuessedname2 = 0x0022,

        DeathTreasureType = 0x0023,

        MutateFilter = 0x0024,

        ItemSkillLimit = 0x0025,

        UseCreateItem = 0x0026,

        DeathSpell = 0x0027,

        VendorsClassId = 0x0028,

        ItemSpecializedOnly = 0x0029,

        HouseId = 0x002A,

        AccountHouseId = 0x002B,

        RestrictionEffect = 0x002C,

        CreationMutationFilter = 0x002D,

        TsysMutationFilter = 0x002E,

        LastPortal = 0x002F,

        LinkedPortalTwo = 0x0030,

        OriginalPortal = 0x0031,

        IconOverlay = 0x0032,

        IconOverlaySecondary = 0x0033,

        IconUnderlay = 0x0034,

        AugmentationMutationFilter = 0x0035,

        AugmentationEffect = 0x0036,

        ProcSpell = 0x0037,

        AugmentationCreateItem = 0x0038,

        AlternateCurrency = 0x0039,

        BlueSurgeSpell = 0x003A,

        YellowSurgeSpell = 0x003B,

        RedSurgeSpell = 0x003C,

        OlthoiDeathTreasureType = 0x003D,

    }

    /// <summary>
    /// The InstancePropertyID identifies a specific Character or Object instance property.
    /// </summary>
    public enum InstancePropertyID : uint {
        Undef = 0x0000,

        Owner = 0x0001,

        Container = 0x0002,

        Wielder = 0x0003,

        Freezer = 0x0004,

        Viewer = 0x0005,

        Generator = 0x0006,

        Scribe = 0x0007,

        CurrentCombatTarget = 0x0008,

        CurrentEnemy = 0x0009,

        ProjectileLauncher = 0x000A,

        CurrentAttacker = 0x000B,

        CurrentDamager = 0x000C,

        CurrentFollowTarget = 0x000D,

        CurrentAppraisalTarget = 0x000E,

        CurrentFellowshipAppraisalTarget = 0x000F,

        ActivationTarget = 0x0010,

        Creator = 0x0011,

        Victim = 0x0012,

        Killer = 0x0013,

        Vendor = 0x0014,

        Customer = 0x0015,

        Bonded = 0x0016,

        Wounder = 0x0017,

        Allegiance = 0x0018,

        Patron = 0x0019,

        Monarch = 0x001A,

        CombatTarget = 0x001B,

        HealthQueryTarget = 0x001C,

        LastUnlocker = 0x001D,

        CrashAndTurnTarget = 0x001E,

        AllowedActivator = 0x001F,

        HouseOwner = 0x0020,

        House = 0x0021,

        Slumlord = 0x0022,

        ManaQueryTarget = 0x0023,

        CurrentGame = 0x0024,

        RequestedAppraisalTarget = 0x0025,

        AllowedWielder = 0x0026,

        AssignedTarget = 0x0027,

        LimboSource = 0x0028,

        Snooper = 0x0029,

        TeleportedCharacter = 0x002A,

        Pet = 0x002B,

        PetOwner = 0x002C,

        PetDevice = 0x002D,

    }

    /// <summary>
    /// The PositionPropertyID identifies a specific Character or Object position property.
    /// </summary>
    public enum PositionPropertyID : uint {
        /// <summary>
        /// Last Corpse Location
        /// </summary>
        OPCODE_0x0E = 0x0E,

    }

    /// <summary>
    /// The SkillID identifies a specific Character skill.
    /// </summary>
    public enum SkillID : uint {
        Axe = 0x01,

        Bow = 0x02,

        Crossbow = 0x03,

        Dagger = 0x04,

        Mace = 0x05,

        MeleeDefense = 0x06,

        MissileDefense = 0x07,

        Sling = 0x08,

        Spear = 0x09,

        Staff = 0x0A,

        Sword = 0x0B,

        ThrownWeapons = 0x0C,

        UnarmedCombat = 0x0D,

        ArcaneLore = 0x0E,

        MagicDefense = 0x0F,

        ManaConversion = 0x10,

        Spellcraft = 0x11,

        ItemTinkering = 0x12,

        AssessPerson = 0x13,

        Deception = 0x14,

        Healing = 0x15,

        Jump = 0x16,

        Lockpick = 0x17,

        Run = 0x18,

        Awareness = 0x19,

        ArmorRepair = 0x1A,

        AssessCreature = 0x1B,

        WeaponTinkering = 0x1C,

        ArmorTinkering = 0x1D,

        MagicItemTinkering = 0x1E,

        CreatureEnchantment = 0x1F,

        ItemEnchantment = 0x20,

        LifeMagic = 0x21,

        WarMagic = 0x22,

        Leadership = 0x23,

        Loyalty = 0x24,

        Fletching = 0x25,

        Alchemy = 0x26,

        Cooking = 0x27,

        Salvaging = 0x28,

        TwoHandedCombat = 0x29,

        Gearcraft = 0x2A,

        VoidMagic = 0x2B,

        HeavyWeapons = 0x2C,

        LightWeapons = 0x2D,

        FinesseWeapons = 0x2E,

        MissleWeapons = 0x2F,

        DualWield = 0x31,

        Recklessness = 0x32,

        SneakAttack = 0x33,

        DirtyFighting = 0x34,

        Challenge = 0x35,

        Summoning = 0x36,

    }

    /// <summary>
    /// The SkillState identifies whether a skill is untrained, trained or specialized.
    /// </summary>
    public enum SkillState : uint {
        Untrained = 0x01,

        Trained = 0x02,

        Specialized = 0x03,

    }

    /// <summary>
    /// The EmoteType identifies the type of emote action
    /// </summary>
    public enum EmoteType : uint {
        Invalid_EmoteType = 0x00,

        Invalid_VendorEmoteType = 0x00,

        Act_EmoteType = 0x01,

        AwardXP_EmoteType = 0x02,

        Give_EmoteType = 0x03,

        MoveHome_EmoteType = 0x04,

        Motion_EmoteType = 0x05,

        Move_EmoteType = 0x06,

        PhysScript_EmoteType = 0x07,

        Say_EmoteType = 0x08,

        Sound_EmoteType = 0x09,

        Tell_EmoteType = 0x0A,

        Turn_EmoteType = 0x0B,

        TurnToTarget_EmoteType = 0x0C,

        TextDirect_EmoteType = 0x0D,

        CastSpell_EmoteType = 0x0E,

        Activate_EmoteType = 0x0F,

        WorldBroadcast_EmoteType = 0x10,

        LocalBroadcast_EmoteType = 0x11,

        DirectBroadcast_EmoteType = 0x12,

        CastSpellInstant_EmoteType = 0x13,

        UpdateQuest_EmoteType = 0x14,

        InqQuest_EmoteType = 0x15,

        StampQuest_EmoteType = 0x16,

        StartEvent_EmoteType = 0x17,

        StopEvent_EmoteType = 0x18,

        BLog_EmoteType = 0x19,

        AdminSpam_EmoteType = 0x1A,

        TeachSpell_EmoteType = 0x1B,

        AwardSkillXP_EmoteType = 0x1C,

        AwardSkillPoints_EmoteType = 0x1D,

        InqQuestSolves_EmoteType = 0x1E,

        EraseQuest_EmoteType = 0x1F,

        DecrementQuest_EmoteType = 0x20,

        IncrementQuest_EmoteType = 0x21,

        AddCharacterTitle_EmoteType = 0x22,

        InqBoolStat_EmoteType = 0x23,

        InqIntStat_EmoteType = 0x24,

        InqFloatStat_EmoteType = 0x25,

        InqStringStat_EmoteType = 0x26,

        InqAttributeStat_EmoteType = 0x27,

        InqRawAttributeStat_EmoteType = 0x28,

        InqSecondaryAttributeStat_EmoteType = 0x29,

        InqRawSecondaryAttributeStat_EmoteType = 0x2A,

        InqSkillStat_EmoteType = 0x2B,

        InqRawSkillStat_EmoteType = 0x2C,

        InqSkillTrained_EmoteType = 0x2D,

        InqSkillSpecialized_EmoteType = 0x2E,

        AwardTrainingCredits_EmoteType = 0x2F,

        InflictVitaePenalty_EmoteType = 0x30,

        AwardLevelProportionalXP_EmoteType = 0x31,

        AwardLevelProportionalSkillXP_EmoteType = 0x32,

        InqEvent_EmoteType = 0x33,

        ForceMotion_EmoteType = 0x34,

        SetIntStat_EmoteType = 0x35,

        IncrementIntStat_EmoteType = 0x36,

        DecrementIntStat_EmoteType = 0x37,

        CreateTreasure_EmoteType = 0x38,

        ResetHomePosition_EmoteType = 0x39,

        InqFellowQuest_EmoteType = 0x3A,

        InqFellowNum_EmoteType = 0x3B,

        UpdateFellowQuest_EmoteType = 0x3C,

        StampFellowQuest_EmoteType = 0x3D,

        AwardNoShareXP_EmoteType = 0x3E,

        SetSanctuaryPosition_EmoteType = 0x3F,

        TellFellow_EmoteType = 0x40,

        FellowBroadcast_EmoteType = 0x41,

        LockFellow_EmoteType = 0x42,

        Goto_EmoteType = 0x43,

        PopUp_EmoteType = 0x44,

        SetBoolStat_EmoteType = 0x45,

        SetQuestCompletions_EmoteType = 0x46,

        InqNumCharacterTitles_EmoteType = 0x47,

        Generate_EmoteType = 0x48,

        PetCastSpellOnOwner_EmoteType = 0x49,

        TakeItems_EmoteType = 0x4A,

        InqYesNo_EmoteType = 0x4B,

        InqOwnsItems_EmoteType = 0x4C,

        DeleteSelf_EmoteType = 0x4D,

        KillSelf_EmoteType = 0x4E,

        UpdateMyQuest_EmoteType = 0x4F,

        InqMyQuest_EmoteType = 0x50,

        StampMyQuest_EmoteType = 0x51,

        InqMyQuestSolves_EmoteType = 0x52,

        EraseMyQuest_EmoteType = 0x53,

        DecrementMyQuest_EmoteType = 0x54,

        IncrementMyQuest_EmoteType = 0x55,

        SetMyQuestCompletions_EmoteType = 0x56,

        MoveToPos_EmoteType = 0x57,

        LocalSignal_EmoteType = 0x58,

        InqPackSpace_EmoteType = 0x59,

        RemoveVitaePenalty_EmoteType = 0x5A,

        SetEyeTexture_EmoteType = 0x5B,

        SetEyePalette_EmoteType = 0x5C,

        SetNoseTexture_EmoteType = 0x5D,

        SetNosePalette_EmoteType = 0x5E,

        SetMouthTexture_EmoteType = 0x5F,

        SetMouthPalette_EmoteType = 0x60,

        SetHeadObject_EmoteType = 0x61,

        SetHeadPalette_EmoteType = 0x62,

        TeleportTarget_EmoteType = 0x63,

        TeleportSelf_EmoteType = 0x64,

        StartBarber_EmoteType = 0x65,

        InqQuestBitsOn_EmoteType = 0x66,

        InqQuestBitsOff_EmoteType = 0x67,

        InqMyQuestBitsOn_EmoteType = 0x68,

        InqMyQuestBitsOff_EmoteType = 0x69,

        SetQuestBitsOn_EmoteType = 0x6A,

        SetQuestBitsOff_EmoteType = 0x6B,

        SetMyQuestBitsOn_EmoteType = 0x6C,

        SetMyQuestBitsOff_EmoteType = 0x6D,

        UntrainSkill_EmoteType = 0x6E,

        SetAltRacialSkills_EmoteType = 0x6F,

        SpendLuminance_EmoteType = 0x70,

        AwardLuminance_EmoteType = 0x71,

        InqInt64Stat_EmoteType = 0x72,

        SetInt64Stat_EmoteType = 0x73,

        OpenMe_EmoteType = 0x74,

        CloseMe_EmoteType = 0x75,

        SetFloatStat_EmoteType = 0x76,

        AddContract_EmoteType = 0x77,

        RemoveContract_EmoteType = 0x78,

        InqContractsFull_EmoteType = 0x79,

    }

    /// <summary>
    /// The EmoteCategory identifies the category of an emote.
    /// </summary>
    public enum EmoteCategory : uint {
        Invalid_EmoteCategory = 0x00,

        Refuse_EmoteCategory = 0x01,

        Vendor_EmoteCategory = 0x02,

        Death_EmoteCategory = 0x03,

        Portal_EmoteCategory = 0x04,

        HeartBeat_EmoteCategory = 0x05,

        Give_EmoteCategory = 0x06,

        Use_EmoteCategory = 0x07,

        Activation_EmoteCategory = 0x08,

        Generation_EmoteCategory = 0x09,

        PickUp_EmoteCategory = 0x0A,

        Drop_EmoteCategory = 0x0B,

        QuestSuccess_EmoteCategory = 0x0C,

        QuestFailure_EmoteCategory = 0x0D,

        Taunt_EmoteCategory = 0x0E,

        WoundedTaunt_EmoteCategory = 0x0F,

        KillTaunt_EmoteCategory = 0x10,

        NewEnemy_EmoteCategory = 0x11,

        Scream_EmoteCategory = 0x12,

        Homesick_EmoteCategory = 0x13,

        ReceiveCritical_EmoteCategory = 0x14,

        ResistSpell_EmoteCategory = 0x15,

        TestSuccess_EmoteCategory = 0x16,

        TestFailure_EmoteCategory = 0x17,

        HearChat_EmoteCategory = 0x18,

        Wield_EmoteCategory = 0x19,

        UnWield_EmoteCategory = 0x1A,

        EventSuccess_EmoteCategory = 0x1B,

        EventFailure_EmoteCategory = 0x1C,

        TestNoQuality_EmoteCategory = 0x1D,

        QuestNoFellow_EmoteCategory = 0x1E,

        TestNoFellow_EmoteCategory = 0x1F,

        GotoSet_EmoteCategory = 0x20,

        NumFellowsSuccess_EmoteCategory = 0x21,

        NumFellowsFailure_EmoteCategory = 0x22,

        NumCharacterTitlesSuccess_EmoteCategory = 0x23,

        NumCharacterTitlesFailure_EmoteCategory = 0x24,

        ReceiveLocalSignal_EmoteCategory = 0x25,

        ReceiveTalkDirect_EmoteCategory = 0x26,

    }

    /// <summary>
    /// The CharacterOptions1 word contains character options.
    /// </summary>
    [Flags]
    public enum CharacterOptions1 : uint {
        /// <summary>
        /// unused (was Automatically Create Shortcuts)
        /// </summary>
        OPCODE_0x00000001 = 0x00000001,

        /// <summary>
        /// Automatically Repeat Attacks
        /// </summary>
        OPCODE_0x00000002 = 0x00000002,

        /// <summary>
        /// Accept Allegiance Requests (Inverted)
        /// </summary>
        OPCODE_0x00000004 = 0x00000004,

        /// <summary>
        /// Accept Fellowship Requests (Inverted)
        /// </summary>
        OPCODE_0x00000008 = 0x00000008,

        /// <summary>
        /// unused (was Invert Mouse Look Up/Down)
        /// </summary>
        OPCODE_0x00000010 = 0x00000010,

        /// <summary>
        /// unused (was Disable House Restriction Effects)
        /// </summary>
        OPCODE_0x00000020 = 0x00000020,

        /// <summary>
        /// Let Other Players Give You Items
        /// </summary>
        OPCODE_0x00000040 = 0x00000040,

        /// <summary>
        /// Automatically keep combat targets in view
        /// </summary>
        OPCODE_0x00000080 = 0x00000080,

        /// <summary>
        /// Display Tooltips
        /// </summary>
        OPCODE_0x00000100 = 0x00000100,

        /// <summary>
        /// Attempt to Deceive Other Players
        /// </summary>
        OPCODE_0x00000200 = 0x00000200,

        /// <summary>
        /// Run as Default Movement
        /// </summary>
        OPCODE_0x00000400 = 0x00000400,

        /// <summary>
        /// Stay in Chat Mode after sending a Message
        /// </summary>
        OPCODE_0x00000800 = 0x00000800,

        /// <summary>
        /// Advanced Combat Interface (No Panel)
        /// </summary>
        OPCODE_0x00001000 = 0x00001000,

        /// <summary>
        /// Auto Target
        /// </summary>
        OPCODE_0x00002000 = 0x00002000,

        /// <summary>
        /// unused (was Right-click mouselook)
        /// </summary>
        OPCODE_0x00004000 = 0x00004000,

        /// <summary>
        /// Vivid Targeting Indicator
        /// </summary>
        OPCODE_0x00008000 = 0x00008000,

        /// <summary>
        /// unused (was Disable Most Weather Effects)
        /// </summary>
        OPCODE_0x00010000 = 0x00010000,

        /// <summary>
        /// Ignore All Trade Requests
        /// </summary>
        OPCODE_0x00020000 = 0x00020000,

        /// <summary>
        /// Share Fellowship Experience
        /// </summary>
        OPCODE_0x00040000 = 0x00040000,

        /// <summary>
        /// Accept Corpse-Looting Permissions
        /// </summary>
        OPCODE_0x00080000 = 0x00080000,

        /// <summary>
        /// Share Fellowship Loot
        /// </summary>
        OPCODE_0x00100000 = 0x00100000,

        /// <summary>
        /// Stretch UI
        /// </summary>
        OPCODE_0x00200000 = 0x00200000,

        /// <summary>
        /// Show Coordinates Below The Radar
        /// </summary>
        OPCODE_0x00400000 = 0x00400000,

        /// <summary>
        /// Display Spell Durations
        /// </summary>
        OPCODE_0x00800000 = 0x00800000,

        /// <summary>
        /// unused (was Play Sounds Only When Active Application)
        /// </summary>
        OPCODE_0x01000000 = 0x01000000,

        /// <summary>
        /// Disable House Restriction Effects
        /// </summary>
        OPCODE_0x02000000 = 0x02000000,

        /// <summary>
        /// Drag Items open secure Trade
        /// </summary>
        OPCODE_0x04000000 = 0x04000000,

        /// <summary>
        /// Show Allegiance Logons
        /// </summary>
        OPCODE_0x08000000 = 0x08000000,

        /// <summary>
        /// Use Charge Attack
        /// </summary>
        OPCODE_0x10000000 = 0x10000000,

        /// <summary>
        /// Automatically Accept Fellowship Requests
        /// </summary>
        OPCODE_0x20000000 = 0x20000000,

        /// <summary>
        /// Listen to Allegiance Chat
        /// </summary>
        OPCODE_0x40000000 = 0x40000000,

        /// <summary>
        /// Use Crafting Chance of Success Dialog
        /// </summary>
        OPCODE_0x80000000 = 0x80000000,

    }

    /// <summary>
    /// The CharacterOptions2 word contains additional character options.
    /// </summary>
    [Flags]
    public enum CharacterOptions2 : uint {
        /// <summary>
        /// Always Daylight Outdoors
        /// </summary>
        OPCODE_0x00000001 = 0x00000001,

        /// <summary>
        /// Allow Others to See Your Date of Birth
        /// </summary>
        OPCODE_0x00000002 = 0x00000002,

        /// <summary>
        /// Allow Others to See Your Chess Rank
        /// </summary>
        OPCODE_0x00000004 = 0x00000004,

        /// <summary>
        /// Allow Others to See Your Fishing Skill
        /// </summary>
        OPCODE_0x00000008 = 0x00000008,

        /// <summary>
        /// Allow Others to See Your Number of Deaths
        /// </summary>
        OPCODE_0x00000010 = 0x00000010,

        /// <summary>
        /// Allow Others to See Your Age
        /// </summary>
        OPCODE_0x00000020 = 0x00000020,

        /// <summary>
        /// Display Timestamps
        /// </summary>
        OPCODE_0x00000040 = 0x00000040,

        /// <summary>
        /// Salvage Multiple Materials at Once
        /// </summary>
        OPCODE_0x00000080 = 0x00000080,

        /// <summary>
        /// Listen to General Chat
        /// </summary>
        OPCODE_0x00000100 = 0x00000100,

        /// <summary>
        /// Listen to Trade Chat
        /// </summary>
        OPCODE_0x00000200 = 0x00000200,

        /// <summary>
        /// Listen to LFG Chat
        /// </summary>
        OPCODE_0x00000400 = 0x00000400,

        /// <summary>
        /// Listen to Roleplaying Chat
        /// </summary>
        OPCODE_0x00000800 = 0x00000800,

        /// <summary>
        /// Allow Others to See Your Number of Titles
        /// </summary>
        OPCODE_0x00002000 = 0x00002000,

        /// <summary>
        /// Use Main Pack as Default for Picking Up Items
        /// </summary>
        OPCODE_0x00004000 = 0x00004000,

        /// <summary>
        /// Lead Missle Targets
        /// </summary>
        OPCODE_0x00008000 = 0x00008000,

        /// <summary>
        /// Use Fast Missles
        /// </summary>
        OPCODE_0x00010000 = 0x00010000,

        /// <summary>
        /// Filter Language
        /// </summary>
        OPCODE_0x00020000 = 0x00020000,

        /// <summary>
        /// Confirm use of Rare Gems
        /// </summary>
        OPCODE_0x00040000 = 0x00040000,

    }

    /// <summary>
    /// The PropertyType value defines the structure and content of a property.
    /// </summary>
    public enum PropertyType : uint {
        /// <summary>
        /// chat window display mask
        /// </summary>
        OPCODE_0x1000007F = 0x1000007F,

        /// <summary>
        /// inactive window opacity
        /// </summary>
        OPCODE_0x10000080 = 0x10000080,

        /// <summary>
        /// active window opacity
        /// </summary>
        OPCODE_0x10000081 = 0x10000081,

        /// <summary>
        /// chat window position (x)
        /// </summary>
        OPCODE_0x10000086 = 0x10000086,

        /// <summary>
        /// chat window position (y)
        /// </summary>
        OPCODE_0x10000087 = 0x10000087,

        /// <summary>
        /// chat window size (x)
        /// </summary>
        OPCODE_0x10000088 = 0x10000088,

        /// <summary>
        /// chat window size (y)
        /// </summary>
        OPCODE_0x10000089 = 0x10000089,

        /// <summary>
        /// chat window enabled
        /// </summary>
        OPCODE_0x1000008A = 0x1000008A,

        /// <summary>
        /// a window property list
        /// </summary>
        OPCODE_0x1000008B = 0x1000008B,

        /// <summary>
        /// a vector of window property lists
        /// </summary>
        OPCODE_0x1000008C = 0x1000008C,

        /// <summary>
        /// chat window title
        /// </summary>
        OPCODE_0x1000008D = 0x1000008D,

    }

    /// <summary>
    /// The various options for filtering the spellbook
    /// </summary>
    [Flags]
    public enum SpellBookFilterOptions : uint {
        None = 0x00000000,

        Creature = 0x00000001,

        Item = 0x00000002,

        Life = 0x00000004,

        War = 0x00000008,

        Level1 = 0x00000010,

        Level2 = 0x00000020,

        Level3 = 0x00000040,

        Level4 = 0x00000080,

        Level5 = 0x00000100,

        Level6 = 0x00000200,

        Level7 = 0x00000400,

        Level8 = 0x00000800,

        Level9 = 0x00001000,

        Void = 0x00002000,

    }

    /// <summary>
    /// The EquipMask value describes the equipment slots an item uses.
    /// </summary>
    [Flags]
    public enum EquipMask : uint {
        Head = 0x00000001,

        ChestUnderwear = 0x00000002,

        AbdomenUnderwear = 0x00000004,

        UpperArmsUnderwear = 0x00000008,

        LowerArmsUnderwear = 0x00000010,

        Hands = 0x00000020,

        UpperLegsUnderwear = 0x00000040,

        LowerLegsUnderwear = 0x00000080,

        Feet = 0x00000100,

        Chest = 0x00000200,

        Abdomen = 0x00000400,

        UpperArms = 0x00000800,

        LowerArms = 0x00001000,

        UpperLegs = 0x00002000,

        LowerLegs = 0x00004000,

        Necklace = 0x00008000,

        RightBracelet = 0x00010000,

        LeftBracelet = 0x00020000,

        RightRing = 0x00040000,

        LeftRing = 0x00080000,

        MeleeWeapon = 0x00100000,

        Shield = 0x00200000,

        MissileWeapon = 0x00400000,

        Ammunition = 0x00800000,

        Wand = 0x01000000,

    }

    /// <summary>
    /// The type of the friend change event.
    /// </summary>
    [Flags]
    public enum FriendsUpdateType : uint {
        Full = 0x0000,

        Added = 0x0001,

        Removed = 0x0002,

        LoginChange = 0x0004,

    }

    /// <summary>
    /// The permission levels that can be given to an allegiance officer
    /// </summary>
    public enum AllegianceOfficerLevel : uint {
        Speaker = 0x01,

        Seneschal = 0x02,

        Castellan = 0x03,

    }

    /// <summary>
    /// Actions related to /allegiance lock
    /// </summary>
    public enum AllegianceLockAction : uint {
        LockedOff = 0x01,

        LockedOn = 0x02,

        ToggleLocked = 0x03,

        CheckLocked = 0x04,

        DisplayBypass = 0x05,

        ClearBypass = 0x06,

    }

    /// <summary>
    /// Actions related to /allegiance house
    /// </summary>
    public enum AllegianceHouseAction : uint {
        Help = 0x01,

        GuestOpen = 0x02,

        GuestClosed = 0x03,

        StorageOpen = 0x04,

        StorageClosed = 0x05,

    }

    /// <summary>
    /// The AttributeID identifies a specific Character attribute.
    /// </summary>
    public enum AttributeID : uint {
        Strength = 0x01,

        Endurance = 0x02,

        Quickness = 0x03,

        Coordination = 0x04,

        Focus = 0x05,

        Self = 0x06,

    }

    /// <summary>
    /// The VitalID identifies a specific Character vital (secondary attribute).
    /// </summary>
    public enum VitalID : uint {
        MaximumHealth = 0x01,

        MaximumStamina = 0x03,

        MaximumMana = 0x05,

    }

    /// <summary>
    /// The CurVitalID identifies a specific Character vital (secondary attribute).
    /// </summary>
    public enum CurVitalID : uint {
        CurrentHealth = 0x02,

        CurrentStamina = 0x04,

        CurrentMana = 0x06,

    }

    /// <summary>
    /// The combat mode for a character or monster.
    /// </summary>
    [Flags]
    public enum CombatMode : uint {
        NonCombat = 0x1,

        Melee = 0x2,

        Missle = 0x4,

        Magic = 0x8,

    }

    /// <summary>
    /// The ChatMessageType categorizes chat window messages to control color and filtering.
    /// </summary>
    public enum ChatMessageType : uint {
        Default = 0x00,

        Speech = 0x02,

        Tell = 0x03,

        OutgoingTell = 0x04,

        System = 0x05,

        Combat = 0x06,

        Magic = 0x07,

        Channels = 0x08,

        OutgoingChannel = 0x09,

        Social = 0x0A,

        OutgoingSocial = 0x0B,

        Emote = 0x0C,

        Advancement = 0x0D,

        Abuse = 0x0E,

        Help = 0x0F,

        Appraisal = 0x10,

        Spellcasting = 0x11,

        Allegiance = 0x12,

        Fellowship = 0x13,

        WorldBroadcast = 0x14,

        CombatEnemy = 0x15,

        CombatSelf = 0x16,

        Recall = 0x17,

        Craft = 0x18,

        Salvaging = 0x19,

        AdminTell = 0x1F,

    }

    /// <summary>
    /// Flags related to the use of the item.
    /// </summary>
    [Flags]
    public enum ObjectDescriptionFlag : uint {
        /// <summary>
        /// can be opened (false if locked)
        /// </summary>
        Openable = 0x00000001,

        /// <summary>
        /// inscribable
        /// </summary>
        Inscribable = 0x00000002,

        /// <summary>
        /// cannot be picked up
        /// </summary>
        Stuck = 0x00000004,

        /// <summary>
        /// player
        /// </summary>
        Player = 0x00000008,

        /// <summary>
        /// attackable
        /// </summary>
        Attackable = 0x00000010,

        /// <summary>
        /// player killer
        /// </summary>
        PlayerKiller = 0x00000020,

        /// <summary>
        /// hidden admin
        /// </summary>
        HiddenAdmin = 0x00000040,

        /// <summary>
        /// hidden
        /// </summary>
        UiHidden = 0x00000080,

        /// <summary>
        /// book
        /// </summary>
        Book = 0x00000100,

        /// <summary>
        /// merchant
        /// </summary>
        Vendor = 0x00000200,

        /// <summary>
        /// pk altar
        /// </summary>
        PkSwitch = 0x00000400,

        /// <summary>
        /// npk altar
        /// </summary>
        NpkSwitch = 0x00000800,

        /// <summary>
        /// door
        /// </summary>
        Door = 0x00001000,

        /// <summary>
        /// corpse
        /// </summary>
        Corpse = 0x00002000,

        /// <summary>
        /// lifestone
        /// </summary>
        LifeStone = 0x00004000,

        /// <summary>
        /// food
        /// </summary>
        Food = 0x00008000,

        /// <summary>
        /// healing kit
        /// </summary>
        Healer = 0x00010000,

        /// <summary>
        /// lockpick
        /// </summary>
        Lockpick = 0x00020000,

        /// <summary>
        /// portal
        /// </summary>
        Portal = 0x00040000,

        /// <summary>
        /// admin
        /// </summary>
        Admin = 0x00100000,

        /// <summary>
        /// free pk status
        /// </summary>
        FreePkStatus = 0x00200000,

        /// <summary>
        /// immute cell restrictions
        /// </summary>
        ImmuneCellRestrictions = 0x00400000,

        /// <summary>
        /// requires pack slot
        /// </summary>
        RequiresPackSlot = 0x00800000,

        /// <summary>
        /// retained
        /// </summary>
        Retained = 0x01000000,

        /// <summary>
        /// pklite status
        /// </summary>
        PkLiteStatus = 0x02000000,

        /// <summary>
        /// has an extra flags DWORD
        /// </summary>
        IncludesSecondHeader = 0x04000000,

        /// <summary>
        /// bindstone
        /// </summary>
        BindStone = 0x08000000,

        /// <summary>
        /// volatile rare
        /// </summary>
        VolatileRare = 0x10000000,

        /// <summary>
        /// wield on use
        /// </summary>
        WieldOnUse = 0x20000000,

        /// <summary>
        /// wield left
        /// </summary>
        WieldLeft = 0x40000000,

    }

    /// <summary>
    /// The AmmoType value describes the type of ammunition a missile weapon uses.
    /// </summary>
    [Flags]
    public enum AmmoType : ushort {
        ThrownWeapon = 0x0000,

        Arrow = 0x0001,

        Bolt = 0x0002,

        Dart = 0x0004,

    }

    /// <summary>
    /// The useablilty flags of the object
    /// </summary>
    [Flags]
    public enum UsableType : uint {
        /// <summary>
        /// source not usable
        /// </summary>
        SourceUnusable = 0x00000001,

        /// <summary>
        /// source self
        /// </summary>
        SourceSelf = 0x00000002,

        /// <summary>
        /// source usable while wielded
        /// </summary>
        SourceWielded = 0x00000004,

        /// <summary>
        /// source usable while contained (owned by player)
        /// </summary>
        SourceContained = 0x00000008,

        /// <summary>
        /// source usable while viewed
        /// </summary>
        SourceViewed = 0x00000010,

        /// <summary>
        /// source usable while remote
        /// </summary>
        SourceRemote = 0x00000020,

        /// <summary>
        /// source don&#39;t approach
        /// </summary>
        SourceNoApproach = 0x00000040,

        /// <summary>
        /// source object self
        /// </summary>
        SourceObjectSelf = 0x00000080,

        /// <summary>
        /// target not usable
        /// </summary>
        TargetUnusable = 0x00010000,

        /// <summary>
        /// target self
        /// </summary>
        TargetSelf = 0x00020000,

        /// <summary>
        /// target usable while wielded
        /// </summary>
        TargetWielded = 0x00040000,

        /// <summary>
        /// target usable while contained (owned by player)
        /// </summary>
        TargetContained = 0x00080000,

        /// <summary>
        /// target usable while viewed
        /// </summary>
        TargetViewed = 0x00100000,

        /// <summary>
        /// target usable while remote
        /// </summary>
        TargetRemote = 0x00200000,

        /// <summary>
        /// target don&#39;t approach
        /// </summary>
        TargetNoApproach = 0x00400000,

        /// <summary>
        /// target object self
        /// </summary>
        TargetObjectSelf = 0x00800000,

    }

    /// <summary>
    /// The CoverageMask value describes what parts of the body an item protects.
    /// </summary>
    [Flags]
    public enum CoverageMask : uint {
        UpperLegsUnderwear = 0x00000002,

        LowerLegsUnderwear = 0x00000004,

        ChestUnderwear = 0x00000008,

        AbdomenUnderwear = 0x00000010,

        UpperArmsUnderwear = 0x00000020,

        LowerArmsUnderwear = 0x00000040,

        UpperLegs = 0x00000100,

        LowerLegs = 0x00000200,

        Chest = 0x00000400,

        Abdomen = 0x00000800,

        UpperArms = 0x00001000,

        LowerArms = 0x00002000,

        Head = 0x00004000,

        Hands = 0x00008000,

        Feet = 0x00010000,

    }

    /// <summary>
    /// The HookType identifies the types of dwelling hooks.
    /// </summary>
    [Flags]
    public enum HookType : ushort {
        Floor = 0x0001,

        Wall = 0x0002,

        Ceiling = 0x0004,

        Yard = 0x0008,

        Roof = 0x0010,

    }

    /// <summary>
    /// The MaterialType identifies the material an object is made of.
    /// </summary>
    public enum MaterialType : uint {
        Ceramic = 0x00000001,

        Porcelain = 0x00000002,

        Linen = 0x00000004,

        Satin = 0x00000005,

        Silk = 0x00000006,

        Velvet = 0x00000007,

        Wool = 0x00000008,

        Agate = 0x0000000A,

        Amber = 0x0000000B,

        Amethyst = 0x0000000C,

        Aquamarine = 0x0000000D,

        Azurite = 0x0000000E,

        BlackGarnet = 0x0000000F,

        BlackOpal = 0x00000010,

        Bloodstone = 0x00000011,

        Carnelian = 0x00000012,

        Citrine = 0x00000013,

        Diamond = 0x00000014,

        Emerald = 0x00000015,

        FireOpal = 0x00000016,

        GreenGarnet = 0x00000017,

        GreenJade = 0x00000018,

        Hematite = 0x00000019,

        ImperialTopaz = 0x0000001A,

        Jet = 0x0000001B,

        LapisLazuli = 0x0000001C,

        LavenderJade = 0x0000001D,

        Malachite = 0x0000001E,

        Moonstone = 0x0000001F,

        Onyx = 0x00000020,

        Opal = 0x00000021,

        Peridot = 0x00000022,

        RedGarnet = 0x00000023,

        RedJade = 0x00000024,

        RoseQuartz = 0x00000025,

        Ruby = 0x00000026,

        Sapphire = 0x00000027,

        SmokeyQuartz = 0x00000028,

        Sunstone = 0x00000029,

        TigerEye = 0x0000002A,

        Tourmaline = 0x0000002B,

        Turquoise = 0x0000002C,

        WhiteJade = 0x0000002D,

        WhiteQuartz = 0x0000002E,

        WhiteSapphire = 0x0000002F,

        YellowGarnet = 0x00000030,

        YellowTopaz = 0x00000031,

        Zircon = 0x00000032,

        Ivory = 0x00000033,

        Leather = 0x00000034,

        ArmoredilloHide = 0x00000035,

        GromnieHide = 0x00000036,

        ReedSharkHide = 0x00000037,

        Brass = 0x00000039,

        Bronze = 0x0000003A,

        Copper = 0x0000003B,

        Gold = 0x0000003C,

        Iron = 0x0000003D,

        Pyreal = 0x0000003E,

        Silver = 0x0000003F,

        Steel = 0x00000040,

        Alabaster = 0x00000042,

        Granite = 0x00000043,

        Marble = 0x00000044,

        Obsidian = 0x00000045,

        Sandstone = 0x00000046,

        Serpentine = 0x00000047,

        Ebony = 0x00000049,

        Mahogany = 0x0000004A,

        Oak = 0x0000004B,

        Pine = 0x0000004C,

        Teak = 0x0000004D,

    }

    /// <summary>
    /// The ConfirmationType identifies the specific confirmation panel to be displayed.
    /// </summary>
    public enum ConfirmationType : uint {
        /// <summary>
        /// Swear Allegiance Request
        /// </summary>
        SwearAllegiance = 0x01,

        /// <summary>
        /// Alter Skill Confirmation Request
        /// </summary>
        AlterSkill = 0x02,

        /// <summary>
        /// Alter Attribute Confirmation Request
        /// </summary>
        AlterAttribute = 0x03,

        /// <summary>
        /// Fellowship Request
        /// </summary>
        Fellowship = 0x04,

        /// <summary>
        /// Craft Interaction Confirmation Request
        /// </summary>
        Craft = 0x05,

        /// <summary>
        /// Augmentation Confirmation Request
        /// </summary>
        Augmentation = 0x06,

        /// <summary>
        /// Yes/No Confirmation Request
        /// </summary>
        YesNo = 0x07,

    }

    /// <summary>
    /// The EnvrionChangeType identifies the environment option set.
    /// </summary>
    public enum EnvrionChangeType : uint {
        /// <summary>
        /// Removes all overrides
        /// </summary>
        Clear = 0x00,

        /// <summary>
        /// Sets Red Fog
        /// </summary>
        RedFog = 0x01,

        /// <summary>
        /// Sets Blue Fog
        /// </summary>
        BlueFog = 0x02,

        /// <summary>
        /// Sets White Fog
        /// </summary>
        WhiteFog = 0x03,

        /// <summary>
        /// Sets Green Fog
        /// </summary>
        GreenFog = 0x04,

        /// <summary>
        /// Sets Black Fog
        /// </summary>
        BlackFog = 0x05,

        /// <summary>
        /// Sets Black Fog
        /// </summary>
        BlackFog2 = 0x06,

        /// <summary>
        /// Play Roar Sound
        /// </summary>
        RoarSound = 0x65,

        /// <summary>
        /// Play Bell Sound
        /// </summary>
        BellSound = 0x66,

        /// <summary>
        /// Play Chant1 Sound
        /// </summary>
        Chant1Sound = 0x67,

        /// <summary>
        /// Play Chant2 Sound
        /// </summary>
        Chant2Sound = 0x68,

        /// <summary>
        /// Play DarkWhispers1 Sound
        /// </summary>
        DarkWhispers1Sound = 0x69,

        /// <summary>
        /// Play DarkWhispers2 Sound
        /// </summary>
        DarkWhispers2Sound = 0x6A,

        /// <summary>
        /// Play DarkLaugh Sound
        /// </summary>
        DarkLaughSound = 0x6B,

        /// <summary>
        /// Play DarkWind Sound
        /// </summary>
        DarkWindSound = 0x6C,

        /// <summary>
        /// Play DarkSpeech Sound
        /// </summary>
        DarkSpeechSound = 0x6D,

        /// <summary>
        /// Play Drums Sound
        /// </summary>
        DrumsSound = 0x6E,

        /// <summary>
        /// Play GhostSpeak Sound
        /// </summary>
        GhostSpeakSound = 0x6F,

        /// <summary>
        /// Play Breathing Sound
        /// </summary>
        BreathingSound = 0x70,

        /// <summary>
        /// Play Howl Sound
        /// </summary>
        HowlSound = 0x71,

        /// <summary>
        /// Play LostSouls Sound
        /// </summary>
        LostSoulsSound = 0x72,

        /// <summary>
        /// Play Squeal Sound
        /// </summary>
        SquealSound = 0x75,

        /// <summary>
        /// Play Thunder1 Sound
        /// </summary>
        Thunder1Sound = 0x76,

        /// <summary>
        /// Play Thunder2 Sound
        /// </summary>
        Thunder2Sound = 0x77,

        /// <summary>
        /// Play Thunder3 Sound
        /// </summary>
        Thunder3Sound = 0x78,

        /// <summary>
        /// Play Thunder4 Sound
        /// </summary>
        Thunder4Sound = 0x79,

        /// <summary>
        /// Play Thunder5 Sound
        /// </summary>
        Thunder5Sound = 0x7A,

        /// <summary>
        /// Play Thunder6 Sound
        /// </summary>
        Thunder6Sound = 0x7B,

    }

    /// <summary>
    /// The movement type defines the fields for the rest of the message
    /// </summary>
    public enum MovementType : byte {
        InterpertedMotionState = 0x00,

        MoveToObject = 0x06,

        MoveToPosition = 0x07,

        TurnToObject = 0x08,

        TurnToPosition = 0x09,

    }

    /// <summary>
    /// Additional movement options
    /// </summary>
    public enum MovementOption : byte {
        None = 0x00,

        StickToObject = 0x01,

        StandingLongJump = 0x02,

    }

    /// <summary>
    /// Command types
    /// </summary>
    public enum Command : ushort {
        Invalid = 0x00,

        HoldRun = 0x01,

        HoldSidestep = 0x02,

        Ready = 0x03,

        Stop = 0x04,

        WalkForward = 0x05,

        WalkBackwards = 0x06,

        RunForward = 0x07,

        Fallen = 0x08,

        Interpolating = 0x09,

        Hover = 0x0A,

        On = 0x0B,

        Off = 0x0C,

        TurnRight = 0x0D,

        TurnLeft = 0x0E,

        SideStepRight = 0x0F,

        SideStepLeft = 0x10,

        Dead = 0x11,

        Crouch = 0x12,

        Sitting = 0x13,

        Sleeping = 0x14,

        Falling = 0x15,

        Reload = 0x16,

        Unload = 0x17,

        Pickup = 0x18,

        StoreInBackpack = 0x19,

        Eat = 0x1A,

        Drink = 0x1B,

        Reading = 0x1C,

        JumpCharging = 0x1D,

        AimLevel = 0x1E,

        AimHigh15 = 0x1F,

        AimHigh30 = 0x20,

        AimHigh45 = 0x21,

        AimHigh60 = 0x22,

        AimHigh75 = 0x23,

        AimHigh90 = 0x24,

        AimLow15 = 0x25,

        AimLow30 = 0x26,

        AimLow45 = 0x27,

        AimLow60 = 0x28,

        AimLow75 = 0x29,

        AimLow90 = 0x2A,

        MagicBlast = 0x2B,

        MagicSelfHead = 0x2C,

        MagicSelfHeart = 0x2D,

        MagicBonus = 0x2E,

        MagicClap = 0x2F,

        MagicHarm = 0x30,

        MagicHeal = 0x31,

        MagicThrowMissile = 0x32,

        MagicRecoilMissile = 0x33,

        MagicPenalty = 0x34,

        MagicTransfer = 0x35,

        MagicVision = 0x36,

        MagicEnchantItem = 0x37,

        MagicPortal = 0x38,

        MagicPray = 0x39,

        StopTurning = 0x3A,

        Jump = 0x3B,

        HandCombat = 0x3C,

        NonCombat = 0x3D,

        SwordCombat = 0x3E,

        BowCombat = 0x3F,

        SwordShieldCombat = 0x40,

        CrossbowCombat = 0x41,

        UnusedCombat = 0x42,

        SlingCombat = 0x43,

        TwoHandedSwordCombat = 0x44,

        TwoHandedStaffCombat = 0x45,

        DualWieldCombat = 0x46,

        ThrownWeaponCombat = 0x47,

        Graze = 0x48,

        Magi = 0x49,

        Hop = 0x4A,

        Jumpup = 0x4B,

        Cheer = 0x4C,

        ChestBeat = 0x4D,

        TippedLeft = 0x4E,

        TippedRight = 0x4F,

        FallDown = 0x50,

        Twitch1 = 0x51,

        Twitch2 = 0x52,

        Twitch3 = 0x53,

        Twitch4 = 0x54,

        StaggerBackward = 0x55,

        StaggerForward = 0x56,

        Sanctuary = 0x57,

        ThrustMed = 0x58,

        ThrustLow = 0x59,

        ThrustHigh = 0x5A,

        SlashHigh = 0x5B,

        SlashMed = 0x5C,

        SlashLow = 0x5D,

        BackhandHigh = 0x5E,

        BackhandMed = 0x5F,

        BackhandLow = 0x60,

        Shoot = 0x61,

        AttackHigh1 = 0x62,

        AttackMed1 = 0x63,

        AttackLow1 = 0x64,

        AttackHigh2 = 0x65,

        AttackMed2 = 0x66,

        AttackLow2 = 0x67,

        AttackHigh3 = 0x68,

        AttackMed3 = 0x69,

        AttackLow3 = 0x6A,

        HeadThrow = 0x6B,

        FistSlam = 0x6C,

        BreatheFlame_ = 0x6D,

        SpinAttack = 0x6E,

        MagicPowerUp01 = 0x6F,

        MagicPowerUp02 = 0x70,

        MagicPowerUp03 = 0x71,

        MagicPowerUp04 = 0x72,

        MagicPowerUp05 = 0x73,

        MagicPowerUp06 = 0x74,

        MagicPowerUp07 = 0x75,

        MagicPowerUp08 = 0x76,

        MagicPowerUp09 = 0x77,

        MagicPowerUp10 = 0x78,

        ShakeFist = 0x79,

        Beckon = 0x7A,

        BeSeeingYou = 0x7B,

        BlowKiss = 0x7C,

        BowDeep = 0x7D,

        ClapHands = 0x7E,

        Cry = 0x7F,

        Laugh = 0x80,

        MimeEat = 0x81,

        MimeDrink = 0x82,

        Nod = 0x83,

        Point = 0x84,

        ShakeHead = 0x85,

        Shrug = 0x86,

        Wave = 0x87,

        Akimbo = 0x88,

        HeartyLaugh = 0x89,

        Salute = 0x8A,

        ScratchHead = 0x8B,

        SmackHead = 0x8C,

        TapFoot = 0x8D,

        WaveHigh = 0x8E,

        WaveLow = 0x8F,

        YawnStretch = 0x90,

        Cringe = 0x91,

        Kneel = 0x92,

        Plead = 0x93,

        Shiver = 0x94,

        Shoo = 0x95,

        Slouch = 0x96,

        Spit = 0x97,

        Surrender = 0x98,

        Woah = 0x99,

        Winded = 0x9A,

        YMCA = 0x9B,

        EnterGame = 0x9C,

        ExitGame = 0x9D,

        OnCreation = 0x9E,

        OnDestruction = 0x9F,

        EnterPortal = 0xA0,

        ExitPortal = 0xA1,

        Cancel = 0xA2,

        UseSelected = 0xA3,

        AutosortSelected = 0xA4,

        DropSelected = 0xA5,

        GiveSelected = 0xA6,

        SplitSelected = 0xA7,

        ExamineSelected = 0xA8,

        CreateShortcutToSelected = 0xA9,

        PreviousCompassItem = 0xAA,

        NextCompassItem = 0xAB,

        ClosestCompassItem = 0xAC,

        PreviousSelection = 0xAD,

        LastAttacker = 0xAE,

        PreviousFellow = 0xAF,

        NextFellow = 0xB0,

        ToggleCombat = 0xB1,

        HighAttack = 0xB2,

        MediumAttack = 0xB3,

        LowAttack = 0xB4,

        EnterChat = 0xB5,

        ToggleChat = 0xB6,

        SavePosition = 0xB7,

        OptionsPanel = 0xB8,

        ResetView = 0xB9,

        CameraLeftRotate = 0xBA,

        CameraRightRotate = 0xBB,

        CameraRaise = 0xBC,

        CameraLower = 0xBD,

        CameraCloser = 0xBE,

        CameraFarther = 0xBF,

        FloorView = 0xC0,

        MouseLook = 0xC1,

        PreviousItem = 0xC2,

        NextItem = 0xC3,

        ClosestItem = 0xC4,

        ShiftView = 0xC5,

        MapView = 0xC6,

        AutoRun = 0xC7,

        DecreasePowerSetting = 0xC8,

        IncreasePowerSetting = 0xC9,

        Pray = 0xCA,

        Mock = 0xCB,

        Teapot = 0xCC,

        SpecialAttack1 = 0xCD,

        SpecialAttack2 = 0xCE,

        SpecialAttack3 = 0xCF,

        MissileAttack1 = 0xD0,

        MissileAttack2 = 0xD1,

        MissileAttack3 = 0xD2,

        CastSpell = 0xD3,

        Flatulence = 0xD4,

        FirstPersonView = 0xD5,

        AllegiancePanel = 0xD6,

        FellowshipPanel = 0xD7,

        SpellbookPanel = 0xD8,

        SpellComponentsPanel = 0xD9,

        HousePanel = 0xDA,

        AttributesPanel = 0xDB,

        SkillsPanel = 0xDC,

        MapPanel = 0xDD,

        InventoryPanel = 0xDE,

        Demonet = 0xDF,

        UseMagicStaff = 0xE0,

        UseMagicWand = 0xE1,

        Blink = 0xE2,

        Bite = 0xE3,

        TwitchSubstate1 = 0xE4,

        TwitchSubstate2 = 0xE5,

        TwitchSubstate3 = 0xE6,

        CaptureScreenshotToFile = 0xE7,

        BowNoAmmo = 0xE8,

        CrossBowNoAmmo = 0xE9,

        ShakeFistState = 0xEA,

        PrayState = 0xEB,

        BowDeepState = 0xEC,

        ClapHandsState = 0xED,

        CrossArmsState = 0xEE,

        ShiverState = 0xEF,

        PointState = 0xF0,

        WaveState = 0xF1,

        AkimboState = 0xF2,

        SaluteState = 0xF3,

        ScratchHeadState = 0xF4,

        TapFootState = 0xF5,

        LeanState = 0xF6,

        KneelState = 0xF7,

        PleadState = 0xF8,

        ATOYOT = 0xF9,

        SlouchState = 0xFA,

        SurrenderState = 0xFB,

        WoahState = 0xFC,

        WindedState = 0xFD,

        AutoCreateShortcuts = 0xFE,

        AutoRepeatAttacks = 0xFF,

        AutoTarget = 0x100,

        AdvancedCombatInterface = 0x101,

        IgnoreAllegianceRequests = 0x102,

        IgnoreFellowshipRequests = 0x103,

        InvertMouseLook = 0x104,

        LetPlayersGiveYouItems = 0x105,

        AutoTrackCombatTargets = 0x106,

        DisplayTooltips = 0x107,

        AttemptToDeceivePlayers = 0x108,

        RunAsDefaultMovement = 0x109,

        StayInChatModeAfterSend = 0x10A,

        RightClickToMouseLook = 0x10B,

        VividTargetIndicator = 0x10C,

        SelectSelf = 0x10D,

        SkillHealSelf = 0x10E,

        NextMonster = 0x10F,

        PreviousMonster = 0x110,

        ClosestMonster = 0x111,

        NextPlayer = 0x112,

        PreviousPlayer = 0x113,

        ClosestPlayer = 0x114,

        SnowAngelState = 0x115,

        WarmHands = 0x116,

        CurtseyState = 0x117,

        AFKState = 0x118,

        MeditateState = 0x119,

        TradePanel = 0x11A,

        LogOut = 0x11B,

        DoubleSlashLow = 0x11C,

        DoubleSlashMed = 0x11D,

        DoubleSlashHigh = 0x11E,

        TripleSlashLow = 0x11F,

        TripleSlashMed = 0x120,

        TripleSlashHigh = 0x121,

        DoubleThrustLow = 0x122,

        DoubleThrustMed = 0x123,

        DoubleThrustHigh = 0x124,

        TripleThrustLow = 0x125,

        TripleThrustMed = 0x126,

        TripleThrustHigh = 0x127,

        MagicPowerUp01Purple = 0x128,

        MagicPowerUp02Purple = 0x129,

        MagicPowerUp03Purple = 0x12A,

        MagicPowerUp04Purple = 0x12B,

        MagicPowerUp05Purple = 0x12C,

        MagicPowerUp06Purple = 0x12D,

        MagicPowerUp07Purple = 0x12E,

        MagicPowerUp08Purple = 0x12F,

        MagicPowerUp09Purple = 0x130,

        MagicPowerUp10Purple = 0x131,

        Helper = 0x132,

        Pickup5 = 0x133,

        Pickup10 = 0x134,

        Pickup15 = 0x135,

        Pickup20 = 0x136,

        HouseRecall = 0x137,

        AtlatlCombat = 0x138,

        ThrownShieldCombat = 0x139,

        SitState = 0x13A,

        SitCrossleggedState = 0x13B,

        SitBackState = 0x13C,

        PointLeftState = 0x13D,

        PointRightState = 0x13E,

        TalktotheHandState = 0x13F,

        PointDownState = 0x140,

        DrudgeDanceState = 0x141,

        PossumState = 0x142,

        ReadState = 0x143,

        ThinkerState = 0x144,

        HaveASeatState = 0x145,

        AtEaseState = 0x146,

        NudgeLeft = 0x147,

        NudgeRight = 0x148,

        PointLeft = 0x149,

        PointRight = 0x14A,

        PointDown = 0x14B,

        Knock = 0x14C,

        ScanHorizon = 0x14D,

        DrudgeDance = 0x14E,

        HaveASeat = 0x14F,

        LifestoneRecall = 0x150,

        CharacterOptionsPanel = 0x151,

        SoundAndGraphicsPanel = 0x152,

        HelpfulSpellsPanel = 0x153,

        HarmfulSpellsPanel = 0x154,

        CharacterInformationPanel = 0x155,

        LinkStatusPanel = 0x156,

        VitaePanel = 0x157,

        ShareFellowshipXP = 0x158,

        ShareFellowshipLoot = 0x159,

        AcceptCorpseLooting = 0x15A,

        IgnoreTradeRequests = 0x15B,

        DisableWeather = 0x15C,

        DisableHouseEffect = 0x15D,

        SideBySideVitals = 0x15E,

        ShowRadarCoordinates = 0x15F,

        ShowSpellDurations = 0x160,

        MuteOnLosingFocus = 0x161,

        Fishing = 0x162,

        MarketplaceRecall = 0x163,

        EnterPKLite = 0x164,

        AllegianceChat = 0x165,

        AutomaticallyAcceptFellowshipRequests = 0x166,

        Reply = 0x167,

        MonarchReply = 0x168,

        PatronReply = 0x169,

        ToggleCraftingChanceOfSuccessDialog = 0x16A,

        UseClosestUnopenedCorpse = 0x16B,

        UseNextUnopenedCorpse = 0x16C,

        IssueSlashCommand = 0x16D,

        AllegianceHometownRecall = 0x16E,

        PKArenaRecall = 0x16F,

        OffhandSlashHigh = 0x170,

        OffhandSlashMed = 0x171,

        OffhandSlashLow = 0x172,

        OffhandThrustHigh = 0x173,

        OffhandThrustMed = 0x174,

        OffhandThrustLow = 0x175,

        OffhandDoubleSlashLow = 0x176,

        OffhandDoubleSlashMed = 0x177,

        OffhandDoubleSlashHigh = 0x178,

        OffhandTripleSlashLow = 0x179,

        OffhandTripleSlashMed = 0x17A,

        OffhandTripleSlashHigh = 0x17B,

        OffhandDoubleThrustLow = 0x17C,

        OffhandDoubleThrustMed = 0x17D,

        OffhandDoubleThrustHigh = 0x17E,

        OffhandTripleThrustLow = 0x17F,

        OffhandTripleThrustMed = 0x180,

        OffhandTripleThrustHigh = 0x181,

        OffhandKick = 0x182,

        AttackHigh4 = 0x183,

        AttackMed4 = 0x184,

        AttackLow4 = 0x185,

        AttackHigh5 = 0x186,

        AttackMed5 = 0x187,

        AttackLow5 = 0x188,

        AttackHigh6 = 0x189,

        AttackMed6 = 0x18A,

        AttackLow6 = 0x18B,

        PunchFastHigh = 0x18C,

        PunchFastMed = 0x18D,

        PunchFastLow = 0x18E,

        PunchSlowHigh = 0x18F,

        PunchSlowMed = 0x190,

        PunchSlowLow = 0x191,

        OffhandPunchFastHigh = 0x192,

        OffhandPunchFastMed = 0x193,

        OffhandPunchFastLow = 0x194,

        OffhandPunchSlowHigh = 0x195,

        OffhandPunchSlowMed = 0x196,

        OffhandPunchSlowLow = 0x197,

    }

    /// <summary>
    /// The stance for a character or monster.
    /// </summary>
    public enum StanceMode : ushort {
        HandCombat = 0x3C,

        NonCombat = 0x3D,

        SwordCombat = 0x3E,

        BowCombat = 0x3F,

        SwordShieldCombat = 0x40,

        CrossbowCombat = 0x41,

        UnusedCombat = 0x42,

        SlingCombat = 0x43,

        TwoHandedSwordCombat = 0x44,

        TwoHandedStaffCombat = 0x45,

        DualWieldCombat = 0x46,

        ThrownWeaponCombat = 0x47,

        BowNoAmmo = 0xE8,

        CrossBowNoAmmo = 0xE9,

        AtlatlCombat = 0x138,

        ThrownShieldCombat = 0x139,

    }

    /// <summary>
    /// The movement (forward, side, turn) for a character or monster.
    /// </summary>
    public enum MovementCommand : ushort {
        HoldRun = 0x01,

        HoldSidestep = 0x02,

        WalkForward = 0x05,

        WalkBackwards = 0x06,

        RunForward = 0x07,

        TurnRight = 0x0D,

        TurnLeft = 0x0E,

        SideStepRight = 0x0F,

        SideStepLeft = 0x10,

    }

    /// <summary>
    /// The type response to a chargen request
    /// </summary>
    public enum CharGenResponseType : uint {
        OK = 0x0001,

        NameInUse = 0x0003,

        NameBanned = 0x0004,

        Corrupt = 0x0005,

        Corrupt_0x0006 = 0x0006,

        AdminPrivilegeDenied = 0x0007,

    }

    /// <summary>
    /// The CharacterErrorType identifies the type of character error that has occured.
    /// </summary>
    public enum CharacterErrorType : uint {
        /// <summary>
        /// ID_CHAR_ERROR_LOGON
        /// </summary>
        Logon = 0x01,

        /// <summary>
        /// ID_CHAR_ERROR_ACCOUNT_LOGON
        /// </summary>
        AccountLogin = 0x03,

        /// <summary>
        /// ID_CHAR_ERROR_SERVER_CRASH
        /// </summary>
        ServerCrash = 0x04,

        /// <summary>
        /// ID_CHAR_ERROR_LOGOFF
        /// </summary>
        Logoff = 0x05,

        /// <summary>
        /// ID_CHAR_ERROR_DELETE
        /// </summary>
        Delete = 0x06,

        /// <summary>
        /// ID_CHAR_ERROR_SERVER_CRASH
        /// </summary>
        ServerCrash2 = 0x08,

        /// <summary>
        /// ID_CHAR_ERROR_ACCOUNT_INVALID
        /// </summary>
        AccountInvalid = 0x09,

        /// <summary>
        /// ID_CHAR_ERROR_ACCOUNT_DOESNT_EXIST
        /// </summary>
        AccountDoesntExist = 0x0A,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_GENERIC
        /// </summary>
        EnterGameGeneric = 0x0B,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_STRESS_ACCOUNT
        /// </summary>
        EnterGameStressAccount = 0x0C,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_CHARACTER_IN_WORLD
        /// </summary>
        EnterGameCharacterInWorld = 0x0D,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_PLAYER_ACCOUNT_MISSING
        /// </summary>
        EnterGamePlayerAccountMissing = 0x0E,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_CHARACTER_NOT_OWNED
        /// </summary>
        EnterGameCharacterNotOwned = 0x0F,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_CHARACTER_IN_WORLD_SERVER
        /// </summary>
        EnterGameCharacterInWorldServer = 0x10,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_OLD_CHARACTER
        /// </summary>
        EnterGameOldCharacter = 0x11,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_CORRUPT_CHARACTER
        /// </summary>
        EnterGameCorruptCharacter = 0x12,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_START_SERVER_DOWN
        /// </summary>
        EnterGameStartServerDown = 0x13,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_COULDNT_PLACE_CHARACTER
        /// </summary>
        EnterGameCouldntPlaceCharacter = 0x14,

        /// <summary>
        /// ID_CHAR_ERROR_LOGON_SERVER_FULL
        /// </summary>
        LogonServerFull = 0x15,

        /// <summary>
        /// ID_CHAR_ERROR_ENTER_GAME_CHARACTER_LOCKED
        /// </summary>
        EnterGameCharacterLocked = 0x17,

        /// <summary>
        /// ID_CHAR_ERROR_SUBSCRIPTION_EXPIRED
        /// </summary>
        SubscriptionExpired = 0x18,

    }

    /// <summary>
    /// The state flags for an object
    /// </summary>
    [Flags]
    public enum PhysicsState : uint {
        None = 0x00000000,

        Static = 0x00000001,

        Ethereal = 0x00000004,

        ReportCollision = 0x00000008,

        IgnoreCollision = 0x00000010,

        NoDraw = 0x00000020,

        Missle = 0x00000040,

        Pushable = 0x00000080,

        AlignPath = 0x00000100,

        PathClipped = 0x00000200,

        Gravity = 0x00000400,

        LightingOn = 0x00000800,

        ParticleEmitter = 0x00001000,

        Hidden = 0x00004000,

        ScriptedCollision = 0x00008000,

        HasPhysicsBsp = 0x00010000,

        Inelastic = 0x00020000,

        HasDefaultAnim = 0x00040000,

        HasDefaultScript = 0x00080000,

        Cloaked = 0x00100000,

        ReportCollisionAsEnvironment = 0x00200000,

        EdgeSlide = 0x00400000,

        Sledding = 0x00800000,

        Frozen = 0x01000000,

    }

    /// <summary>
    /// The TurbineChatType identifies the type of Turbine Chat message.
    /// </summary>
    public enum TurbineChatType : uint {
        ServerToClientMessage = 0x01,

        ClientToServerMessage = 0x03,

        AckClientToServerMessage = 0x05,

    }

    /// <summary>
    /// The ResourceType identifies the dat file to be used.
    /// </summary>
    public enum ResourceType : long {
        client_portal = 0x01,

        client_cell_1 = 0x02,

        client_local_English = 0x03,

    }

    /// <summary>
    /// The CompressionType identifies the type of data compression used.
    /// </summary>
    public enum CompressionType : byte {
        None = 0x00,

        ZLib = 0x01,

    }

    /// <summary>
    /// The AttributeMask selects which creature attributes highlighting is applied to.
    /// </summary>
    [Flags]
    public enum AttributeMask : ushort {
        Strength = 0x0001,

        Endurance = 0x0002,

        Quickness = 0x0004,

        Coordination = 0x0008,

        Focus = 0x0010,

        Self = 0x0020,

        Health = 0x0040,

        Stamina = 0x0080,

        Mana = 0x0100,

    }

    /// <summary>
    /// The DamageType identifies the type of damage.
    /// </summary>
    [Flags]
    public enum DamageType : uint {
        Slashing = 0x01,

        Piercing = 0x02,

        Bludgeoning = 0x04,

        Cold = 0x08,

        Fire = 0x10,

        Acid = 0x20,

        Electric = 0x40,

    }

    /// <summary>
    /// The HookAppraisalFlags identifies various properties for an item hooked.
    /// </summary>
    [Flags]
    public enum HookAppraisalFlags : uint {
        Inscribable = 0x01,

        IsHealer = 0x02,

        IsLockpick = 0x08,

    }

    /// <summary>
    /// The ArmorHighlightMask selects which armor attributes highlighting is applied to.
    /// </summary>
    [Flags]
    public enum ArmorHighlightMask : ushort {
        ArmorLevel = 0x0001,

        SlashingProtection = 0x0002,

        PiercingProtection = 0x0004,

        BludgeoningProtection = 0x0008,

        ColdProtection = 0x0010,

        FireProtection = 0x0020,

        AcidProtection = 0x0040,

        ElectricalProtection = 0x0080,

    }

    /// <summary>
    /// The ResistHighlightMask selects which wand attributes highlighting is applied to.
    /// </summary>
    [Flags]
    public enum ResistHighlightMask : ushort {
        ResistSlash = 0x0001,

        ResistPierce = 0x0002,

        ResistBludgeon = 0x0004,

        ResistFire = 0x0008,

        ResistCold = 0x0010,

        ResistAcid = 0x0020,

        ResistElectric = 0x0040,

        ResistHealthBoost = 0x0080,

        ResistStaminaDrain = 0x0100,

        ResistStaminaBoost = 0x0200,

        ResistManaDrain = 0x0400,

        ResistManaBoost = 0x0800,

        ManaConversionMod = 0x1000,

        ElementalDamageMod = 0x2000,

        ResistNether = 0x4000,

    }

    /// <summary>
    /// The WeaponHighlightMask selects which weapon attributes highlighting is applied to.
    /// </summary>
    [Flags]
    public enum WeaponHighlightMask : ushort {
        AttackSkill = 0x0001,

        MeleeDefense = 0x0002,

        Speed = 0x0004,

        Damage = 0x0008,

        DamageVariance = 0x0010,

        DamageMod = 0x0020,

    }

    /// <summary>
    /// Additional attack information
    /// </summary>
    [Flags]
    public enum AttackConditionsMask : uint {
        CriticalProtectionAugmentation = 0x01,

        Recklessness = 0x02,

        SneakAttack = 0x04,

    }

    /// <summary>
    /// The DamageLocation indicates where damage was done.
    /// </summary>
    public enum DamageLocation : uint {
        Head = 0x00,

        Chest = 0x01,

        Abdomen = 0x02,

        UpperArm = 0x03,

        LowerArm = 0x04,

        Hand = 0x05,

        UpperLeg = 0x06,

        LowerLeg = 0x07,

        Foot = 0x08,

    }

    /// <summary>
    /// The LogTextType indicates the kind of text going to the chat area.
    /// </summary>
    public enum LogTextType : uint {
        Default = 0x00,

        Speech = 0x02,

        Tell = 0x03,

        SpeechDirectSend = 0x04,

        System = 0x05,

        Combat = 0x06,

        Magic = 0x07,

        Channel = 0x08,

        ChannelSend = 0x09,

        Social = 0x0A,

        SocialSend = 0x0B,

        Emote = 0x0C,

        Advancement = 0x0D,

        Abuse = 0x0E,

        Help = 0x0F,

        Appraisal = 0x10,

        Spellcasting = 0x11,

        Allegiance = 0x12,

        Fellowship = 0x13,

        WorldBroadcast = 0x14,

        CombatEnemy = 0x15,

        CombatSelf = 0x16,

        Recall = 0x17,

        Craft = 0x18,

        Salvaging = 0x19,

        AdminTell = 0x1F,

    }

    /// <summary>
    /// The EndTradeReason identifies the reason trading was ended.
    /// </summary>
    public enum EndTradeReason : uint {
        Normal = 0x00,

        EnteredCombat = 0x02,

        Cancelled = 0x51,

    }

    /// <summary>
    /// The TradeSide identifies the side of the trade window.
    /// </summary>
    public enum TradeSide : uint {
        Self = 0x01,

        Partner = 0x02,

    }

    /// <summary>
    /// The HouseType identifies the type of house.
    /// </summary>
    public enum HouseType : uint {
        Cottage = 0x01,

        Villa = 0x02,

        Mansion = 0x03,

        Apartment = 0x04,

    }

    /// <summary>
    /// Identifies the chess move attempt result.  Negative/0 values are failures.
    /// </summary>
    public enum ChessMoveResult : int {
        /// <summary>
        /// Its not your turn, please wait for your opponents move.
        /// </summary>
        FailureNotYourTurn = -0x03,

        /// <summary>
        /// The selected piece cannot move that direction
        /// </summary>
        FailureInvalidDirection = -0x64,

        /// <summary>
        /// The selected piece cannot move that far
        /// </summary>
        FailureInvalidDistance = -0x65,

        /// <summary>
        /// You tried to move an empty square
        /// </summary>
        FailureMovingEmptySquare = -0x66,

        /// <summary>
        /// The selected piece is not yours
        /// </summary>
        FailureMovingOpponentPiece = -0x67,

        /// <summary>
        /// You cannot move off the board
        /// </summary>
        FailureMovedPieceOffBoard = -0x68,

        /// <summary>
        /// You cannot attack your own pieces
        /// </summary>
        FailureAttackingOwnPiece = -0x69,

        /// <summary>
        /// That move would put you in check
        /// </summary>
        FailureCannotMoveIntoCheck = -0x6A,

        /// <summary>
        /// You can only move through empty squares
        /// </summary>
        FailurePathBlocked = -0x6B,

        /// <summary>
        /// You cannot castle out of check
        /// </summary>
        FailureCastleOutOfCheck = -0x6C,

        /// <summary>
        /// You cannot castle through check
        /// </summary>
        FailureCastleThroughCheck = -0x6D,

        /// <summary>
        /// You cannot castle after moving the King or Rook
        /// </summary>
        FailureCastlePieceMoved = -0x6E,

        /// <summary>
        /// That move is invalid
        /// </summary>
        FailureInvalidMove = 0x0,

        /// <summary>
        /// Successful move.
        /// </summary>
        Success = 0x1,

        /// <summary>
        /// Your opponent is in Check.
        /// </summary>
        OpponentInCheck = 0x400,

        /// <summary>
        /// You have checkmated your opponent!
        /// </summary>
        CheckMatedOpponent = 0x800,

    }

    /// <summary>
    /// Type of fellow update
    /// </summary>
    public enum FellowUpdateType : uint {
        FullUpdate = 0x01,

        UpdateStats = 0x02,

        UpdateVitals = 0x03,

    }

    /// <summary>
    /// Stage a contract is in.  Values 4+ appear to provide contract specific update messages
    /// </summary>
    public enum ContractStage : uint {
        New = 0x01,

        InProgress = 0x02,

        /// <summary>
        /// If this is set, it looks at the time when repeats to show either Done, Available, or # to Repeat
        /// </summary>
        DoneOrPendingRepeat = 0x03,

    }

    /// <summary>
    /// Gender of a player
    /// </summary>
    public enum Gender : byte {
        Invalid = 0x00,

        Male = 0x01,

        Female = 0x02,

    }

    /// <summary>
    /// Heritage of a player
    /// </summary>
    public enum HeritageGroup : byte {
        Invalid = 0x00,

        Aluvian = 0x01,

        Gharundim = 0x02,

        Sho = 0x03,

        Viamontian = 0x04,

        Shadowbound = 0x05,

        Gearknight = 0x06,

        Tumerok = 0x07,

        Lugian = 0x08,

        Empyrean = 0x09,

        Penumbraen = 0x0A,

        Undead = 0x0B,

        Olthoi = 0x0C,

        OlthoiAcid = 0x0D,

    }

    /// <summary>
    /// the type of highlight (outline) applied to the object&#39;s icon
    /// </summary>
    public enum IconHighlight : uint {
        Invalid = 0x0000,

        Magical = 0x0001,

        Poisoned = 0x0002,

        BoostHealth = 0x0004,

        BoostMana = 0x0008,

        BoostStamina = 0x0010,

        Fire = 0x0020,

        Lightning = 0x0040,

        Frost = 0x0080,

        Acid = 0x0100,

        Bludgeoning = 0x0200,

        Slashing = 0x0400,

        Piercing = 0x0800,

        Nether = 0x1000,

    }

    /// <summary>
    /// the type of wieldable item this is
    /// </summary>
    public enum WieldType : byte {
        Invalid = 0x00000000,

        MeleeWeapon = 0x00000001,

        Armor = 0x00000002,

        Clothing = 0x00000004,

        Jewelry = 0x00000008,

    }

    /// <summary>
    /// List of primitive data types used in ac messages
    /// </summary>
    public enum PrimitiveDataType : ushort {
        /// <summary>
        /// Unknown ac primitive type
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// bool ac primitive type
        /// </summary>
        @bool = 1,

        /// <summary>
        /// byte ac primitive type
        /// </summary>
        @byte = 2,

        /// <summary>
        /// short ac primitive type
        /// </summary>
        @short = 3,

        /// <summary>
        /// ushort ac primitive type
        /// </summary>
        @ushort = 4,

        /// <summary>
        /// int ac primitive type
        /// </summary>
        @int = 5,

        /// <summary>
        /// uint ac primitive type
        /// </summary>
        @uint = 6,

        /// <summary>
        /// long ac primitive type
        /// </summary>
        @long = 7,

        /// <summary>
        /// ulong ac primitive type
        /// </summary>
        @ulong = 8,

        /// <summary>
        /// float ac primitive type
        /// </summary>
        @float = 9,

        /// <summary>
        /// double ac primitive type
        /// </summary>
        @double = 10,

        /// <summary>
        /// string ac primitive type
        /// </summary>
        @string = 11,

        /// <summary>
        /// WString ac primitive type
        /// </summary>
        @WString = 12,

    }
}
