﻿<#@ assembly name="System.Core" #>
<#@ assembly name="System.Linq" #>
<#@ assembly name="$(TargetPath)" #>
<#@ import namespace="System.Linq" #>
<#@ import namespace="System.Text" #>
<#@ import namespace="System.Collections.Generic" #>
<#@ template language="c#" hostspecific="true" inherits="T4RuntimeTemplates.TextTemplateBase" #>
<#@ output extension=".cs" #>
<#
    SetupMessageParser(this.Host.ResolvePath("messages.xml"));
    PrintLocalModificationWarning();
#>
using System.IO;
using System.Collections.Generic;
using ACMessageDefs.Lib;
using ACMessageDefs.Enums;

namespace ACMessageDefs.DataTypes {
<# 
    Indent();

    // generate a class for each non-primitive data type
    foreach (var kv in this.MessageReader.ACDataTypes) {
        var dataType = kv.Value;
        
        if (dataType.IsTemplated)
            continue;

        // summary docs and class definition
        WriteSummary(dataType.Text);
        WriteLine("public class " + dataType.TypeDeclaration + " : IACDataType {");

        using (new IndentHelper(this)) {
            // generate struct fields from messages.xml
            foreach (var baseModel in dataType.AllChildren) {
                GenerateStructFields(baseModel);
            }

            // empty constructor
            WriteLine("public " + dataType.Name +"() { }\n"); 

            // define method that can parse from binary
            WriteSummary("Reads instance data from a binary reader");
            WriteLine("public void ReadFromBuffer(BinaryReader buffer) {");
            using (new IndentHelper(this)) {
                GenerateBinaryConstructorContents(dataType);
            }
            WriteLine("}\n");
        }
        // close class definition
        WriteLine("}\n");
    }
    Outdent();

// close namespace
#>
}