﻿using FunkWerksPlugin.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using VirindiViewService;
using VirindiViewService.Controls;
using VirindiViewService.XMLParsers;

namespace FunkWerksPlugin.Views {
    class MainView : IDisposable {
        private HudView view;
        private ViewProperties properties;
        private ControlGroup controls;

        private HudButton UIRefreshScripts;
        private HudList UIScriptsList;
        private HudCombo UILoadScriptDropdown;
        private HudButton UILoadScript;

        public MainView() {
            new Decal3XMLParser().ParseFromResource("FunkWerksPlugin.Views.MainView.xml", out properties, out controls);

            view = new HudView(properties, controls);

            UIRefreshScripts = view["UIRefreshScripts"] as HudButton;
            UIScriptsList = view["UIScriptsList"] as HudList;
            UILoadScriptDropdown = view["UILoadScriptDropdown"] as HudCombo;
            UILoadScript = view["UILoadScript"] as HudButton;

            UIRefreshScripts.Hit += UIRefreshScripts_Hit;
            UILoadScript.Hit += UILoadScript_Hit;

            RefreshScriptsDropdown();
        }

        private void ScriptManager_ScriptStarted(object sender, ScriptManager.ScriptEventArgs e) {
            RefreshRunningScripts();
        }

        private void ScriptManager_ScriptStopped(object sender, ScriptManager.ScriptEventArgs e) {
            RefreshRunningScripts();
        }

        private void UILoadScript_Hit(object sender, EventArgs e) {
            try {
                var scriptPath = ((HudStaticText)UILoadScriptDropdown[UILoadScriptDropdown.Current]).Text;
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        private void UIRefreshScripts_Hit(object sender, EventArgs e) {
            RefreshScriptsDropdown();
        }

        private void RefreshScriptsDropdown() {
            UILoadScriptDropdown.Clear();
        }

        private void RefreshRunningScripts() {
            UIScriptsList.ClearRows();
        }

        public void Dispose() {
            if (view != null) {
                if (UIRefreshScripts != null) UIRefreshScripts.Hit -= UIRefreshScripts_Hit;
                if (UILoadScript != null) UILoadScript.Hit -= UILoadScript_Hit;

                view.Visible = false;
                view.Dispose();
            }
        }
    }
}
