﻿using ACMessageDefs;
using ACMessageDefs.HandlerEventArgs;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using FunkWerksPlugin.Lib;
using System;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace FunkWerksPlugin {
    /// <summary>
    /// This is basically a wrapper around PluginLogic that handles the serialization between plugin sessions.
    /// </summary>
    public class FunkWerksPlugin {
        public static FunkWerksPlugin Instance;
        private MessageHandler messageHandler;

        public Logger Logger { get; private set; }

        /// <summary>
        /// Directory used for storing plugin dll/xml files
        /// </summary>
        public string PluginDirectory { get; private set; }

        #region Startup / Shutdown
        /// <summary>
        /// Called once when the plugin is loaded
        /// </summary>
        public void Startup(FunkWerks.FunkWerks host, string pluginAssemblyDirectory) {
            Instance = this;
            PluginDirectory = pluginAssemblyDirectory;
            Logger = new Logger();
            Logger.PluginAssemblyDirectory = pluginAssemblyDirectory;
            Logger.WriteLine($"Startup {CoreManager.Current.CharacterFilter == null}");

            messageHandler = new ACMessageDefs.MessageHandler();
            messageHandler.Item_UpdateObject_S2C += MessageHandler_Item_UpdateObject_S2C;
        }

        public void EchoFilter_ServerDispatch(object sender, NetworkMessageEventArgs e) {
            try {
                using (MemoryStream stream = new MemoryStream(e.Message.RawData))
                using (BinaryReader buffer = new BinaryReader(stream)) {
                    try {
                        messageHandler.HandleIncomingMessageData(buffer);
                    }
                    catch (Exception ex) {
                        Logger.WriteLine($"Error at offset {buffer.BaseStream.Position}");
                        Logger.LogException(ex);
                    }
                }
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        private void MessageHandler_Item_UpdateObject_S2C(object sender, Item_UpdateObject_S2C_EventArgs e) {
            try {
                Logger.WriteLine($"Item_UpdateObject {e.Data.objectId:X8} : {e.Data.wdesc.name}");
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        /// <summary>
        /// Called when the plugin is shutting down.  Unregister from any events here and do any cleanup.
        /// </summary>
        public void Shutdown() {
            messageHandler.Item_UpdateObject_S2C -= MessageHandler_Item_UpdateObject_S2C;
            Logger?.Dispose();
        }
        #endregion
    }
}
