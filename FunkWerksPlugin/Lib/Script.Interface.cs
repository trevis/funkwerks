﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunkWerksPlugin.Lib {
    public partial class Script {
        /// <summary>
        /// Get the script interface, aka FWAPI
        /// </summary>
        /// <returns></returns>
        protected dynamic GetScriptInterface() {
            return new {
                log = LogToChatWindow()
            };
        }

        /// <summary>
        /// Logs a string to the chat window
        /// </summary>
        protected Func<object, Task<object>> LogToChatWindow() {
            return async (dynamic input) => {
                try {
                    Logger.WriteToChat(input);
                }
                catch (Exception e) { Logger.LogException(e); }

                return true;
            };
        }
    }
}
