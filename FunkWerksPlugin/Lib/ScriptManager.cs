﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using EdgeJs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FunkWerksPlugin.Lib {
    public class ScriptManager : IDisposable {

        internal ConcurrentQueue<Action> ActionQueue = new ConcurrentQueue<Action>();


        // TODO: unhardcode this
        public string ScriptDirectory = @"C:\Games\Decal Plugins\FunkWerks\Scripts\";
        public string EdgeDirectory {
            get {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.Combine(Path.GetDirectoryName(path), "edge");
            }
        }
        public Dictionary<string, Script> Scripts = new Dictionary<string, Script>();
        private bool disposed;

        private Func<object, Task<object>> _messageHandler = null;

        internal MessageTracker MessageTracker { get; }

        public event EventHandler<ScriptEventArgs> ScriptStarted;
        public event EventHandler<ScriptEventArgs> ScriptStopped;

        public class ScriptEventArgs {
            public string ScriptPath { get; }
            public Script Script { get; }

            public ScriptEventArgs(string scriptPath, Script script) {
                ScriptPath = scriptPath;
                Script = script;
            }
        }

        public ScriptManager() {
            //this.pluginLogic = pluginLogic;

            // enable nodejs inspector, can view with chrome://inspect
            // TODO: must specify a unique port here for multiple instances, otherwise it crashes ac
            // would also be cool to change the name to match server/character, right now it just shows up as edge.js
            Environment.SetEnvironmentVariable("EDGE_NODE_PARAMS", "--inspect");

            try {
                Func<object, Task<object>> log = async (dynamic input) => {
                    try {
                        Logger.WriteToChat(input);
                    }
                    catch { }

                    return true;
                };

                Func<object, Task<object>> _registerMessageHandler = async (dynamic input) => {
                    try {
                        _messageHandler = (Func<object, Task<object>>)input;
                    }
                    catch (Exception e) { Logger.LogException(e); }

                    return true;
                };

                // bootstrap globals and whatnot
                var func = Edge.Func(@"return require('" + Path.Combine(EdgeDirectory, "bootstrap.js").Replace(@"\", @"\\") + "')");

                func(new {
                    log,
                    _registerMessageHandler,
                    base_script_path = ScriptDirectory,
                    edge_path = EdgeDirectory + @"\"
                });
            }
            catch (Exception ex) { Logger.LogException(ex); }

            MessageTracker = new MessageTracker();

            // TODO
            Task.Run(async () => {
                while (!disposed) {
                    // i think we could get some better performance by creating a data structure
                    // that holds all combined events/aco updates and sending it over in one go.
                    while (ActionQueue.TryDequeue(out Action action)) {
                        action.Invoke();
                    }
                    //EventManager_RunHandlers(this, new EventManager.OnMessageBaseEventArgs() { EventId = EventIds.evidOnTick });
                    Thread.Sleep(1);
                }
            });
        }

        public List<string> GetAvailableScripts() {
            List<string> availableScripts = new List<string>();

            foreach (var directory in Directory.GetDirectories(ScriptDirectory)) {
                foreach (var file in Directory.GetFiles(directory)) {
                    if (file.EndsWith(".fwx")) {
                        availableScripts.Add(file.Replace(ScriptDirectory, ""));
                    }
                }
            }

            return availableScripts;
        }

        public void StartScript(string scriptPath) {
            var script = new Script(this, scriptPath);

            Logger.WriteToChat($"Start: {scriptPath}");

            // if this script is already unloaded we need to unload it
            if (Scripts.ContainsKey(scriptPath)) {
                Scripts[scriptPath].Stop();
                Scripts[scriptPath].Dispose();
                Scripts.Remove(scriptPath);
            }

            script.Start();
            Scripts.Add(scriptPath, script);

            ScriptStarted?.Invoke(this, new ScriptEventArgs(scriptPath, script));
        }

        public void Dispose() {
            disposed = true;

            MessageTracker.Dispose();

            foreach (var kv in Scripts) {
                ScriptStopped?.Invoke(this, new ScriptEventArgs(kv.Key, kv.Value));
                kv.Value.Stop();
                kv.Value.Dispose();
            }
        }
    }
}
