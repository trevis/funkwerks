﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace FunkWerksPlugin.Lib.MessageParser {
    class MessageParser {
        public MessageParser(Stream messagesXML) {
            LoadMessagesStream(messagesXML);
        }

        private void LoadMessagesStream(Stream messagesXML) {
            try {
                var s = "";
                var xml = XElement.Load(messagesXML);
                List<XElement> enumNodes = xml.XPathSelectElements("/datatypes/enums/enum").ToList();

                Logger.WriteToChat($"Found {enumNodes.Count} enum nodes");
                foreach (var e in enumNodes) {
                    s += $"Enum: {(string)e.Attribute("name")} - {(string)e.Attribute("text")}\n";
                    var valueNodes = e.XPathSelectElements("//value").ToList();
                }

                Logger.WriteToChat(s);
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }
    }
}
