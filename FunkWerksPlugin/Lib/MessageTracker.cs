﻿using Decal.Adapter;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace FunkWerksPlugin.Lib {
    class MessageTracker : IDisposable {

        public MessageTracker() {
            try {
                CoreManager.Current.EchoFilter.ClientDispatch += EchoFilter_ClientDispatch;
                CoreManager.Current.EchoFilter.ServerDispatch += EchoFilter_ServerDispatch;

                var messagesPath = Path.Combine(FunkWerksPlugin.Instance.PluginDirectory, "messages.xml");

                Logger.WriteToChat($"Loaded: ${ACMessageDefs.Enums.PrimitiveDataType.WString}");

                using (Stream stream = new FileStream(messagesPath, FileMode.Open)) {
                    var parser = new MessageParser.MessageParser(stream);
                }
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        private void EchoFilter_ServerDispatch(object sender, NetworkMessageEventArgs e) {
            try {
                using (var stream = new MemoryStream(e.Message.RawData)) {
                    using (var reader = new BinaryReader(stream)) {

                    }
                }
            }
            catch (Exception ex) { Logger.LogException(ex); }
        }

        private void EchoFilter_ClientDispatch(object sender, NetworkMessageEventArgs e) {
            try {
                using (var stream = new MemoryStream(e.Message.RawData)) {
                    using (var reader = new BinaryReader(stream)) {

                    }
                }
            }
            catch (Exception ex) { Logger.LogException(ex);  }
        }

        public void Dispose() {
            CoreManager.Current.EchoFilter.ClientDispatch -= EchoFilter_ClientDispatch;
            CoreManager.Current.EchoFilter.ServerDispatch -= EchoFilter_ServerDispatch;
        }
    }
}
