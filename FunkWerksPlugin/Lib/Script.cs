﻿using Decal.Adapter;
using EdgeJs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FunkWerksPlugin.Lib {
    public partial class Script : IDisposable {
        private Func<object, Task<object>> shutdownCallback;

        public ScriptManager ScriptManager { get; }

        public string ScriptPath { get; }
        public string Name { get => Directory.GetParent(ScriptPath).Name; }

        public string BasePath { get => Directory.GetParent(ScriptPath).FullName; }

        public List<string> Scripts { get; } = new List<string>();

        public Script(ScriptManager scriptManager, string scriptPath) {
            ScriptManager = scriptManager;
            ScriptPath = Path.Combine(ScriptManager.ScriptDirectory, scriptPath);
        }

        internal bool Start() {
            try {
                Logger.WriteToChat($"Loading: {ScriptPath}");
                if (!ParseScriptConfig())
                    return false;

                Run().Wait();
            }
            catch (Exception ex) { Logger.LogException(ex); }
            return true;
        }

        private async Task Run() {
            try {
                string scriptContents = "";

                Logger.WriteToChat($"Loading {Scripts.Count} script files");

                foreach (var script in Scripts) {
                    scriptContents += $"require('{Path.Combine(BasePath, script).Replace(@"\", @"\\")}');";
                }
                var bootstrappedScriptContents = @"
                    return (__FW__interface, __FW__callback) => {
                        // clear the require cache for this script so we get fresh modules
                        Object.keys(require.cache).forEach(function(key) {
                            if (key.indexOf('" + BasePath.Replace(@"\", @"\\") + @"') !== -1) {
                                delete require.cache[key];
                            }
                        });

                        class ActiveXObject {}
                        global.ActiveXObject = ActiveXObject;

                        // when we require skapi from outside of a script, we get a skapi factory
                        var fwapiFactoryPath = '" + ScriptManager.EdgeDirectory.Replace(@"\", @"\\") + @"\\fwapi.js';
                        var parentScript = '" + Name + @"';
                        var skapi = require(fwapiFactoryPath)(parentScript, __FW__interface.__FW__fwapi);

                        " + scriptContents + @"

                        skapi.startup();

                        __FW__callback(null, function (data, cb) {
                            delete __FW__.skapi_instances[parentScript];
                            try {
                                skapi.shutdown();
                            } catch (e) {
                                __FW__.log(e);
                            }
                            cb();
                        });
                    }
                ";

                var bootstrapFunc = Edge.Func(bootstrappedScriptContents);
                shutdownCallback = (Func<object, Task<object>>)await bootstrapFunc(new {
                    __FW__fwapi = GetScriptInterface()
                });
            }
            catch (Exception e) { Logger.LogException(e);  }
        }

        private bool ParseScriptConfig() {
            try {
                foreach (XElement script in XElement.Load(ScriptPath).Elements("script")) {
                    var path = ".\\" + script.Attribute("src").Value.Replace(@"\\", @"\");
                    //Logger.WriteToChat(" Src:" + path);
                    Scripts.Add(path);
                }
            }
            catch (Exception ex) {
                Logger.LogException(ex);
                return false;
            }

            return true;
        }

        internal void Stop() {
            if (shutdownCallback != null)
                shutdownCallback(null);
        }

        public void Dispose() {

        }
    }
}
