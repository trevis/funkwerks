﻿const Weenie = require('./weenie');

module.exports = class FunkWerks {
    constructor(interface) {
        this._interface = interface;

        interface._registerObjectUpdateHandler(_objectUpdateHandler);
    }

    log(text) {
        const args = Array.prototype.slice.call(arguments);
        console.log.apply(console, args);
        this._interface.log(args.join(', '));
    }

    _objectUpdateHandler(objectUpdate, cb) {
        switch (objectUpdate.updateType) {
            case 'WeenieCreated':
            case 'WeenieUpdated':
                //this.log(`${objectUpdate.updateType} ${objectUpdate.obj.Id.toString(16)} :: ${objectUpdate.obj.Name}`);
                if (!(objectUpdate.objId in this.weenieData)) {
                    this.weenieData[objectUpdate.objId] = new Weenie(objectUpdate.objId);
                }
                this.weenieData[objectUpdate.objId]._updateInternalData(objectUpdate.obj);
                break;

            case 'WeenieDeleted':
                //this.log(`Deleted ${objectUpdate.objId.ToString(16)} :: ${__FW__.weenieData[objectUpdate.objId].Name}`);
                delete this.weenieData[objectUpdate.objId];

            default:
                this.log(`_objectUpdateHandler called with unhandled updateType: ${objectUpdate.updateType}`)
        }
        cb();
    }
}