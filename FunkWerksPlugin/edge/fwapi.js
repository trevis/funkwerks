﻿(function(){
    // dont cache this module
    delete require.cache[__filename];

    const ACF = require('./ACF.js');
    const noop = () => {};
    let _timerInterval;

    // this tracks our skapi instance cache, globally
    if (typeof(__FW__.skapi_instances) == "undefined") {
        __FW__.skapi_instances = {};
    }

    const Skapi = class Skapi {
        constructor(parentScript, fwapiInstance) {
            this.parentScript = parentScript || 'NoParent';
            this.fwapiInstance = fwapiInstance;
            this._handlerId = 1;
            this._handlers = {};
            this._timers = [];
            this._isRunning = false;
        }

        startup() {
            try {
                this._isRunning = true;
                this.log(`Called skapi.startup for script ${this.parentScript} nodejs ${process.version}`);

                // this ticks down our timers and fires events
                // TODO: redo this, make it global? fix setInterval?
                let _lastTick = Date.now();
                _timerInterval = setInterval(() => {
                    const now = Date.now();
                    const tickTime = now - _lastTick;
                    _lastTick = now;
                    const hasTimerHandlers =  evidOnTimer in this._handlers;
                    this._timers.forEach((timer) => {
                        if (timer.fired && timer.cmsec < 0) {
                            timer.fired = false;
                        }
                                
                        timer.cmsec += tickTime;
                        timer.updates++;

                        if (timer.cmsec >= 0 && !timer.fired) {
                            timer.fired = true;
                            for (let [hid, handler] of Object.entries(this._handlers[evidOnTimer])) {
                                handler["OnTimer"](timer);
                            }
                        }
                    });
                }, 5);
            }
            catch (e) { this.error(e); }
        }

        shutdown() {
            try {
                this.log(`Called skapi.shutdown for ${this.parentScript}`);
                this._isRunning = false;
                clearInterval(_timerInterval);
                for(let [evid, evidHandlers] of Object.entries(this._handlers)) {
                    evid = parseInt(evid, 10);
                    for (let [hid, handler] of Object.entries(evidHandlers)) {
                        this.RemoveHandler(evid, handler);
                    }
                }
            }
            catch (e) { this.error(e); }
        }

        log(s) {
            console.log('[' + this.parentScript + ']', s);
            this.fwapiInstance.log('[' + this.parentScript + '] ' + s, noop);
        }

        error(s) {
            console.error('[' + this.parentScript + ']', s);
            this.fwapiInstance.log('[' + this.parentScript + '] Error: ' + ((s && s.stack) ? s.stack : s), noop);
        }

        OuputLine(s) {
            this.log(s);
        }

        AddHandler(evid, handler) {
            try {
                const hid = evid + '_' + ++this._handlerId;
                const handlerEventKey = __FW__.eventIdLookup[evid + ''];

                if (!(handlerEventKey in handler)) {
                    this.log(`Called AddHandler without handler entry for ${handlerEventKey}`);
                    return;
                }

                this.fwapiInstance.AddHandler({
                    parentScript: this.parentScript,
                    evid: parseInt(evid, 10),
                    hid,
                    handler: (res, cb) => {
                        // gross... but this is how we translate events to skapi style
                        switch (parseInt(evid, 10)) {
                            case evidOnTick:
                                handler[handlerEventKey]();
                                break;
                            case evidOnChatLocal:
                                handler[handlerEventKey](res.Sender, res.Message);
                                break;
                            case evidOnTell:
                                handler[handlerEventKey](res.Sender, res.Receiver, res.Message);
                                break;
                            case evidOnTellAllegiance:
                                handler[handlerEventKey](res.Sender, res.Message);
                                break;
                            case evidOnTellFellowship:
                                handler[handlerEventKey](res.Sender, res.Message);
                                break;
                            case evidOnChatBoxMessage:
                                handler[handlerEventKey](res.Message, res.Color);
                                break;
                            case evidOnChatServer:
                                handler[handlerEventKey](res.Message, res.Color);
                                break;
                            case evidOnChatEmoteCustom:
                                handler[handlerEventKey](res.Sender, res.Message);
                                break;
                            case evidOnChatEmoteStandard:
                                handler[handlerEventKey](res.Sender, res.Message);
                                break;
                            default:
                                this.log(`Unhandled evid translation: ${handlerEventKey} (${evid})`);
                        }
                        cb();
                    }
                }, noop);

                if (!(evid in this._handlers)) {
                    this._handlers[evid] = {}
                }

                this._handlers[evid][hid] = handler;
            } catch (e) { this.error(e); }
        }

        RemoveHandler(evid, handler) {
            try {
                if (!(evid in this._handlers)) {
                    this._handlers[evid] = {}
                }
                const handlerEventKey = __FW__.eventIdLookup[evid + ''];
                let hid = "";
        
                for(let [_hid, _handler] of Object.entries(this._handlers[evid])) {
                    if (_handler[handlerEventKey] == handler[handlerEventKey]) {
                        hid = _hid;
                        break;
                    }
                }

                if (hid == "" || !this._handlers[evid][hid][handlerEventKey]) {
                    return;
                }

                this.fwapiInstance.RemoveHandler({
                    parentScript: this.parentScript,
                    evid: parseInt(evid, 10),
                    hid,
                    handler: this._handlers[evid][hid][handlerEventKey]
                }, noop);
            } catch (e) { this.error(e); }
        }

        TimerNew() {
            const timer = { cmsec: 0, tag: '', fired: false, updates: 0 };
            this._timers.push(timer);
            return timer;
        }

        AcfNew() {
            return new ACF();
        }
    }

    // loop through parent modules, setting parent to the originating caller's script dir name
    let _cm = module;
    let lastParent;
    while (_cm && _cm.parent && _cm.parent.filename.indexOf(__FW__.base_script_path) !== -1) {
        lastParent = _cm.parent;
        _cm = lastParent;
    }

    // if fwapi is require'd from a script, it gets a cached version.
    // if fwapi is require'd outside of a script it gets a factory to create a
    // new skapi instance in the cache, passing in the cs script interface
    let parentScript;
    if (lastParent && lastParent.filename && lastParent.filename.indexOf(__FW__.base_script_path) !== -1) {
        parentScript = lastParent.filename.replace(__FW__.base_script_path, '').split('\\')[0];
    }
    else {
        module.exports = function(parentScript, fwapiInstance) {
            __FW__.skapi_instances[parentScript] = new Skapi(parentScript, fwapiInstance);
            return __FW__.skapi_instances[parentScript];
        };
        return;
    }

    module.exports = __FW__.skapi_instances[parentScript];
}());