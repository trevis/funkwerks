// dont cache this module
delete require.cache[__filename];

const skapiDefs = require('./skapiDefs');
const StringValueKey = require('./enums/StringValueKey');
const LongValueKey = require('./enums/LongValueKey');
const ObjectClass = require('./enums/ObjectClass');
const BehaviorFlags = require('./enums/BehaviorFlags');
const Weenie = require('./weenie');

const ACO = class ACO {
    constructor(id) {
        this._id = id;
    }

    get weenie() {
        if (!(this._id in __FW__.weenieData))
            throw new Error(`Error getting ACO. weenieData does not exist for object id ${this._id}`);
        
        return __FW__.weenieData[this._id];
    }

    get oid() {
        return this.weenie.Id;
    }

    get szName() {
        return this.weenie.Name;
    }

    get szPlural() {
        let secondary = this.weenie.stringValue(StringValueKey.SecondaryName, '');
        if (secondary.length === 0)
            secondary = this.weenie.Name;

        if (secondary[secondary.length - 1] != 's')
            secondary += 's';

        return secondary;
    }

    get oty() {
        return this.weenie.Behavior;
    }

    get ocm() {
        let category = skapiDefs.ocmNil;
        switch (this.weenie.ObjectClass) {
            case ObjectClass.Player:
                category |= skapiDefs.ocmPlayer;
                if ((this.weenie.Behavior & BehaviorFlags.PlayerKiller) != 0) {
                    category |= skapiDefs.ocmPK;
                }
                else {
                    category |= skapiDefs.ocmNonPK;
                }
                break;
            case ObjectClass.Monster:
                category |= skapiDefs.ocmMonster;
                break;
            case ObjectClass.Corpse:
                // TODO: check on this logic...
                if (this.weenie.Type == 21) {
                    category |= skapiDefs.ocmPlayerCorpse;
                }
                else {
                    category |= skapiDefs.ocmMonsterCorpse;
                }
                break;
            case ObjectClass.Lifestone:
                category |= skapiDefs.ocmLifestone;
                break;
            case ObjectClass.Portal:
                category |= skapiDefs.ocmPortal;
                break;
            case ObjectClass.Vendor:
                category |= skapiDefs.ocmMerchant;
                break;
            // TODO: should this include trinkets/cloaks/aeth?
            case ObjectClass.Armor:
            case ObjectClass.Clothing:
            case ObjectClass.WandStaffOrb:
            case ObjectClass.MeleeWeapon:
            case ObjectClass.MissileWeapon:
                category |= skapiDefs.ocmEquipment;
                break;
            case ObjectClass.Npc:
                category |= skapiDefs.ocmNPC;
                break;
            case ObjectClass.Container:
                // TODO: check on this logic...
                if (this.weenie.longValue(LongValueKey.HookType, 0) > 0) {
                    category |= skapiDefs.ocmHook;
                }
                break;
            //default:
            //    category = skapiDefs.ocmAll;
            //    break;
        }

        return category;
    }

    get olc() {
        var objLocation = skapiDefs.olcNil;

        if (this.weenie.Container != 0) {
            var packs = Weenie.Me().ContainerIds();
            if (packs.Contains(wo.Container)) {
                objLocation |= skapiDefs.olcInventory;
            }
            else {
                objLocation |= skapiDefs.olcContained;
            }
        }
        else {
            objLocation |= skapiDefs.olcOnGroud;
        }

        if (wo.Values(LongValueKey.EquippedSlots, 0) != 0) {
            objLocation |= skapiDefs.olcEquipped;
        }

        return objLocation;
    }
                /*
                
                olc = GetObjectLocation(wo),
                mcm = wo.Category,
                szMcm = GetVendorCategoryString(wo.Category),
                eqm = wo.Values(LongValueKey.EquipableSlots, 0),
                cpyValue = wo.Values(LongValueKey.Value, 0),
                material = wo.Values(LongValueKey.Material, 0),
                workmanship = wo.Values(DoubleValueKey.SalvageWorkmanship, 0),
                citemStack = wo.Values(LongValueKey.StackCount, 1),
                citemMaxStack = wo.Values(LongValueKey.StackMax, 1),
                cuseLeft = wo.Values(LongValueKey.UsesRemaining, 0),
                cuseMax = wo.Values(LongValueKey.UsesTotal, 0),
                */
}

module.exports = ACO;