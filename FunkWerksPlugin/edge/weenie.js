﻿// dont cache this module
delete require.cache[__filename];

const deepExtend = require('deep-extend');

module.exports = class Weenie {
    static Me() {
        
    }

    constructor(id) {
        this.Id = id;
        this.ActiveSpellCount = 0;
        this.Behavior = 0;
        this.BoolValueKeys = {};
        this.Category = 0;
        this.Container = 0;
        this.Coordinates = null;
        this.DoubleValueKeys = {};
        this.GameDataFlags1 = 0;
        this.HasIdData = false;
        this.Icon = 0;
        this.LastIdTime = 0;
        this.LongValueKeys = {};
        this.Name = "";
        this.ObjectClass = 0;
        this.Offset = null;
        this.Orientation = null;
        this.PhysicsDataFlags = 0;
        this.RawCoordinates = null;
        this.SpellCount = 0;
        this.Spells = [];
        this.StringValueKeys = {};
        this.WeenieType = 0;
    }

    _updateInternalData(data) {
        deepExtend(this, data);
    }

    longValue(key, defaultValue = 0) {
        if (key in this.LongValueKeys)
            return this.LongValueKeys[key];
        else
            return defaultValue;
    }

    doubleValue(key, defaultValue = 0) {
        if (key in this.DoubleValueKeys)
            return this.DoubleValueKeys[key];
        else
            return defaultValue;
    }

    boolValue(key, defaultValue = false) {
        if (key in this.BoolValueKeys)
            return this._data.BoolValueKeys[key];
        else
            return defaultValue;
    }

    stringValue(key, defaultValue = "") {
        if (key in this.StringValueKeys)
            return this.StringValueKeys[key];
        else
            return defaultValue;
    }
}