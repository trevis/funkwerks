﻿using Decal.Adapter;
using FunkWerks.Lib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace FunkWerks {
    [FriendlyName("FunkWerks")]
    public class FunkWerks : FilterBase {
        private object pluginInstance;
        private MethodInfo pluginServerDispatchMethod;
        private Assembly pluginAssembly;
        private Type pluginType;
        private FileSystemWatcher pluginWatcher = null;
        private bool isLoaded;

        private Stopwatch reloadTimer = Stopwatch.StartNew();

        /// <summary>
        /// Namespace of the plugin we want to hot reload
        /// </summary>
        public static string PluginAssemblyNamespace { get { return "FunkWerksPlugin.FunkWerksPlugin"; } }

        /// <summary>
        /// File name of the plugin we want to hot reload
        /// </summary>
        public static string PluginAssemblyName { get { return "FunkWerksPluginMerged.dll"; } }

        /// <summary>
        /// Assembly directory (contains both loader and plugin dlls)
        /// </summary>
        public static string PluginAssemblyDirectory {
            get {
                return System.IO.Path.GetDirectoryName(Assembly.GetAssembly(typeof(FunkWerks)).Location);
            }
        }

        /// <summary>
        /// Full path to plugin assembly
        /// </summary>
        public string PluginAssemblyPath {
            get {
                return System.IO.Path.Combine(PluginAssemblyDirectory, PluginAssemblyName);
            }
        }

        #region FilterBase overrides
        /// <summary>
        /// This is called when the filter is started up.  This happens when ac client is first started
        /// </summary>
        protected override void Startup() {
            try {
                reloadTimer.Reset();

                // watch the PluginAssemblyName for file changes
                pluginWatcher = new FileSystemWatcher();
                pluginWatcher.Path = PluginAssemblyDirectory;
                pluginWatcher.NotifyFilter = NotifyFilters.LastWrite;
                pluginWatcher.Filter = PluginAssemblyName;
                pluginWatcher.Changed += PluginWatcher_Changed;
                pluginWatcher.EnableRaisingEvents = true;

                ServerDispatch += FunkWerks_ServerDispatch;

                LoadPluginAssembly();
            }
            catch (Exception ex) { Utils.LogException(ex); }
        }

        private void FunkWerks_ServerDispatch(object sender, NetworkMessageEventArgs e) {
            try {
                if (isLoaded && pluginInstance != null && pluginServerDispatchMethod != null) {
                    pluginServerDispatchMethod.Invoke(pluginInstance, new object[] { sender, e });
                }
            }
            catch (Exception ex) { Utils.LogException(ex); }
        }

        /// <summary>
        /// This is called when the filter is shut down. This happens once when the game is closing.
        /// </summary>
        protected override void Shutdown() {
            try {
                ServerDispatch -= FunkWerks_ServerDispatch;

                if (reloadTimer.IsRunning) {
                    reloadTimer.Stop();
                    Core.RenderFrame -= Core_RenderFrame;
                }
                UnloadPluginAssembly();
            }
            catch (Exception ex) { Utils.LogException(ex); }
        }
        #endregion

        #region Decal Event Handlers
        private void Core_RenderFrame(object sender, EventArgs e) {
            try {
                if (reloadTimer.IsRunning && reloadTimer.Elapsed > TimeSpan.FromSeconds(2)) {
                    //Utils.WriteToChat($"Core_RenderFrame Reloading {PluginAssemblyName}");
                    Core.RenderFrame -= Core_RenderFrame;
                    reloadTimer.Reset();
                    LoadPluginAssembly();
                }
            }
            catch (Exception ex) { Utils.LogException(ex); }
        }
        #endregion

        #region Plugin Loading/Unloading
        internal void TryLoadPluginAssembly() {
            //Utils.WriteToChat($"TryLoadPluginAssembly {reloadTimer.IsRunning}");
            if (reloadTimer.IsRunning) {
                reloadTimer.Restart();
                return;
            }

            reloadTimer.Start();
            Core.RenderFrame += Core_RenderFrame;
        }

        private void LoadPluginAssembly() {
            try {
                if (isLoaded) {
                    UnloadPluginAssembly();
                    //Utils.WriteToChat($"Reloading {PluginAssemblyName}");
                }

                pluginAssembly = Assembly.Load(File.ReadAllBytes(PluginAssemblyPath));
                pluginType = pluginAssembly.GetType(PluginAssemblyNamespace);
                pluginInstance = Activator.CreateInstance(pluginType);
                pluginServerDispatchMethod = pluginType.GetMethod("EchoFilter_ServerDispatch");

                var startupMethod = pluginType.GetMethod("Startup");
                startupMethod.Invoke(pluginInstance, new object[] {
                    this,
                    PluginAssemblyDirectory
                });

                isLoaded = true;
            }
            catch (Exception ex) {
                Utils.LogException(ex);
                reloadTimer.Restart();
                TryLoadPluginAssembly();
            }
        }

        private void UnloadPluginAssembly() {
            try {
                //Utils.WriteToChat($"UnloadPluginAssembly {reloadTimer.IsRunning}");
                if (pluginInstance != null && pluginType != null) {
                    MethodInfo shutdownMethod = pluginType.GetMethod("Shutdown");
                    shutdownMethod.Invoke(pluginInstance, null);
                    pluginInstance = null;
                    pluginType = null;
                }
            }
            catch (Exception ex) { Utils.LogException(ex); }
        }

        private void PluginWatcher_Changed(object sender, FileSystemEventArgs e) {
            try {
                TryLoadPluginAssembly();
            }
            catch (Exception ex) { Utils.LogException(ex); }
        }
        #endregion
    }
}
